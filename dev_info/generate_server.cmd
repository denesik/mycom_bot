"../3rdparty/OpenSSL/bin/openssl.exe" genrsa -out hc_root.pem 4096
"../3rdparty/OpenSSL/bin/openssl.exe" req -x509 -new -days 20000 -key hc_root.pem -subj "/C=RU/O=MG_BOT/CN=Hustle Castle Bot" -out hc_root.crt
"../3rdparty/OpenSSL/bin/openssl.exe" genrsa -out hc_server.pem 4096
"../3rdparty/OpenSSL/bin/openssl.exe" req -new -key hc_server.pem -subj "/C=RU/O=MG_BOT/CN=37.228.116.78" -out hc_server.csr
"../3rdparty/OpenSSL/bin/openssl.exe" x509 -req -in hc_server.csr -CA hc_root.crt -CAkey hc_root.pem -CAcreateserial -out hc_server.crt -days 20000

copy hc_server.crt "../upload_server/"
copy hc_server.pem "../upload_server/"

"../3rdparty/OpenSSL/bin/openssl.exe" verify -CAfile hc_root.crt hc_root.crt
"../3rdparty/OpenSSL/bin/openssl.exe" verify -CAfile hc_root.crt hc_server.crt
"../3rdparty/OpenSSL/bin/openssl.exe" verify -CAfile hc_server.crt hc_server.crt

pause
