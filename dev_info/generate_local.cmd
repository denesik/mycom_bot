"../3rdparty/OpenSSL/bin/openssl.exe" genrsa -out hc_root_local.pem 4096
"../3rdparty/OpenSSL/bin/openssl.exe" req -x509 -new -days 20000 -key hc_root_local.pem -subj "/C=RU/O=MG_BOT/CN=Hustle Castle Bot" -out hc_root_local.crt
"../3rdparty/OpenSSL/bin/openssl.exe" genrsa -out hc_server_local.pem 4096
"../3rdparty/OpenSSL/bin/openssl.exe" req -new -key hc_server_local.pem -subj "/C=RU/O=MG_BOT/CN=127.0.0.1" -out hc_server_local.csr
"../3rdparty/OpenSSL/bin/openssl.exe" x509 -req -in hc_server_local.csr -CA hc_root_local.crt -CAkey hc_root_local.pem -CAcreateserial -out hc_server_local.crt -days 20000

copy hc_server_local.crt "../build_server/"
copy hc_server_local.pem "../build_server/"

"../3rdparty/OpenSSL/bin/openssl.exe" verify -CAfile hc_root_local.crt hc_root_local.crt
"../3rdparty/OpenSSL/bin/openssl.exe" verify -CAfile hc_root_local.crt hc_server_local.crt
"../3rdparty/OpenSSL/bin/openssl.exe" verify -CAfile hc_server_local.crt hc_server_local.crt

pause
