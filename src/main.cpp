#include "EditorApplication.h"

#include <boost/program_options.hpp>

#include "ActorTicker.h"

#include <atomic>
#include <opencv2/core/utils/logger.hpp>

#include "Profile.h"
#include "Logger.h"

#include "utf_helper/CommandLineToUtf8.h"

namespace po = boost::program_options;

#include <cstdlib>       // std::abort
#include <exception>     // std::set_terminate
#include <iostream>      // std::cerr

#include <boost/stacktrace.hpp>
#include "ActorRunner.h"

namespace
{
	struct
	{
		Profile* profile = nullptr;
	} gEditorArguments;

	static wxAppConsole* wxCreateEditorApp()
	{
		return new EditorApplication(*gEditorArguments.profile);
	}
}

int app_main(const std::vector<std::string>& args)
{
	po::options_description desc("options");
	desc.add_options()
		("profile", po::value<std::string>()->default_value("default"), "profile name")
		;

	po::variables_map vm;
	try
	{
		po::store(po::command_line_parser(args).options(desc).run(),vm);
		po::notify(vm);
	}
	catch (const std::exception & exc)
	{
		std::cout << exc.what() << std::endl;
		return 1;
	}

	Profile profile(vm["profile"].as<std::string>());
	
	auto logger = spdlog::basic_logger_mt("default_logger", profile.FindPath("log").string(), true);
	logger->set_level(spdlog::level::warn);
	spdlog::set_default_logger(logger);

	gEditorArguments.profile = &profile;
	wxAppInitializer initializer(wxCreateEditorApp);
	wxDISABLE_DEBUG_SUPPORT();
	::ShowWindow(::GetConsoleWindow(), SW_HIDE);
	return wxEntry();
}

int wmain(int ac, wchar_t* av[])
{
	cv::utils::logging::setLogLevel(cv::utils::logging::LogLevel::LOG_LEVEL_SILENT);
	setlocale(LC_ALL, "English");

	std::set_terminate([]()
	{
		try
		{
			std::cerr << boost::stacktrace::stacktrace() << std::endl;
		}
		catch (...) {}
		std::abort();
	});

	return app_main(utf_helper::CommandLineToUtf8(ac, av));
}


