#pragma once

#include "ModeCommon.h"

#include <string>
#include <filesystem>

class DownloadMode : public ModeCommon
{
public:

	int Run();

private:
	bool GetRemoteVersion(std::string & version) const;

	bool Download(std::string& buffer) const;

	bool RemoveDirectories(const std::filesystem::path& path) const;

	bool Unpack(const std::filesystem::path &upload_path, const std::string& buffer) const;

	bool CheckBotNotRunned() const;

	bool CheckNeedUpdate(bool& need_update) const;

	bool RunInstallMode() const;
};
