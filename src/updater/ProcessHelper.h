#pragma once

#include <string>
#include <filesystem>

namespace process_helper
{

	struct ProcessInfo
	{
		std::filesystem::path path_exe;
		std::string name_exe;
		std::uint32_t id = 0;
	};

	bool GetProcessListByName(std::string_view name, std::vector<ProcessInfo> &data);

	std::uint32_t GetCurrentProcessId();

	bool GetProcessInfoById(std::uint32_t id, ProcessInfo &info);

	bool RunProcess(const std::filesystem::path& exe_path, const std::filesystem::path& current_dir = {}, std::string_view args = {});

}



