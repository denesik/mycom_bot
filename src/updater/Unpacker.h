#pragma once

#include <string>
#include <functional>

class Unpacker
{
public:

	Unpacker& SetWriteCallback(const std::function<bool(const char*, char*, size_t)>& callback);
	Unpacker& SetProgressCallback(const std::function<bool(int, int)>& callback);

	bool Unpack(std::string_view buffer) const;

private:
	std::function<bool(const char*, char*, size_t)> mWriteFunction;
	std::function<bool(int, int)> mProgressFunction;

};
