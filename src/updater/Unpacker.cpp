#include "Unpacker.h"

#include <zip.h>
#include <stdexcept>
#include <memory>

Unpacker& Unpacker::SetWriteCallback(const std::function<bool(const char*, char*, size_t)>& callback)
{
		mWriteFunction = callback;
		return *this;
}

Unpacker& Unpacker::SetProgressCallback(const std::function<bool(int, int)>& callback)
{
	mProgressFunction = callback;
	return *this;
}

bool Unpacker::Unpack(std::string_view buffer) const
{
	zip_error_t err = {};
	std::unique_ptr<zip_error_t, std::function<void(zip_error_t*)>> zip_err_guard(&err, [](zip_error_t* ptr) { if (ptr) zip_error_fini(ptr); });
	
	auto zip_source = zip_source_buffer_create(buffer.data(), buffer.size(), 0, &err);
	std::unique_ptr<zip_source_t, std::function<void(zip_source_t*)>> zip_source_guard(zip_source, [](zip_source_t* ptr) { if (ptr) zip_source_free(ptr); });

	if (!zip_source)
		throw std::runtime_error(zip_error_strerror(&err));

	auto archive = zip_open_from_source(zip_source, 0, &err);
	if (!archive)
		throw std::runtime_error(zip_error_strerror(&err));

	std::string file_buffer;
	struct zip_stat file_info;
	auto count_files = zip_get_num_files(archive);

	for (decltype(count_files) i = 0; i < count_files; ++i)
	{
		zip_stat_init(&file_info);

		if (zip_stat_index(archive, i, 0, &file_info) != 0)
			throw std::runtime_error(zip_strerror(archive));

		if ((file_info.valid & ZIP_STAT_NAME) != ZIP_STAT_NAME)
			continue;
		if ((file_info.valid & ZIP_STAT_INDEX) != ZIP_STAT_INDEX)
			continue;
		if ((file_info.valid & ZIP_STAT_SIZE) != ZIP_STAT_SIZE)
			continue;

		auto file = zip_fopen_index(archive, file_info.index, 0);
		if (!file)
			throw std::runtime_error(zip_strerror(archive));

		file_buffer.resize(file_info.size);

		auto new_size = zip_fread(file, file_buffer.data(), file_buffer.size());
		zip_fclose(file);

		if (new_size == -1 || static_cast<size_t>(new_size) != file_buffer.size())
			throw std::runtime_error("zip_fread error");

		if (mWriteFunction)
			if (!mWriteFunction(file_info.name, file_buffer.data(), file_buffer.size()))
				return false;

		if (mProgressFunction)
			if (!mProgressFunction(i, count_files))
				return false;
	};

	return true;
}
