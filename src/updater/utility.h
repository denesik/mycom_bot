#pragma once

#include <filesystem>
#include <vector>
#include <string>


void FindPaths(const std::filesystem::path& path, const std::vector<std::string>& blacklist, const std::vector<std::string>& whitelist, std::vector<std::string>& dirs, std::vector<std::string>& files);

bool RemoveAllPaths(const std::filesystem::path& dst, const std::vector<std::string>& data);

bool CreateAllDirectories(const std::filesystem::path& dst, const std::vector<std::string>& data);

bool CopyAllFiles(const std::filesystem::path& src, const std::filesystem::path& dst, const std::vector<std::string>& data);

bool PreparePathsTree(const std::filesystem::path& path);

void RegexConvert(std::vector<std::string>& data);
