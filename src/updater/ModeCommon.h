#pragma once

#include <filesystem>

class ModeCommon
{
protected:

	bool CheckSingleRun() const;

	int ReturnPaused(int return_code = 1) const;

	bool CheckBotDirUnlocked(const std::filesystem::path& path) const;
};
