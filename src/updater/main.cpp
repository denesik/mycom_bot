#include <cstdlib>       // std::abort
#include <exception>     // std::set_terminate
#include <iostream>      // std::cerr
#include <boost/stacktrace.hpp>

#include <thread>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "utility.h"
#include "CommandLineToUtf8.h"

#include "DownloadMode.h"
#include "InstallMode.h"
#include "PathUtf8.h"
#include "InfoPrinter.h"
#include "CleanMode.h"
#include "Logger.h"

#pragma warning( push )
#pragma warning( disable : 4459 )
#include <spdlog/sinks/stdout_color_sinks.h>
#pragma warning( pop )


int app_main(const std::vector<std::string>& args, const std::atomic<bool>& is_runned)
{
	po::options_description desc("options");
	desc.add_options()
		("help", "produce help message")
		("install", "install")
		("clean", "clean")
		;

	po::variables_map vm;
	try
	{
		po::store(po::command_line_parser(args).options(desc).run(), vm);
		po::notify(vm);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return 1;
	}

	auto logger = spdlog::stdout_color_mt("console");
	logger->set_level(spdlog::level::info);
	spdlog::set_default_logger(logger);

	if (auto err = InfoPrinter().Run())
		return err;

	if (vm.count("help"))
	{
		std::cout << desc << std::endl;
		return 0;
	}

	if (vm.count("install"))
		return InstallMode().Run();

	if (vm.count("clean"))
		return CleanMode().Run();

	// default
	return DownloadMode().Run();
}



//==========================================

namespace
{
	std::atomic<bool> IS_RUNNED = true;
	std::atomic<bool> IS_FINISHED = false;
}

void my_terminate_handler()
{
	try
	{
		std::cerr << boost::stacktrace::stacktrace();
	}
	catch (...) {}
	std::abort();
}

BOOL ctrl_handler(DWORD event)
{
	switch (event)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	{
		IS_RUNNED = false;
		while (!IS_FINISHED)
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		return TRUE;
	}
	}

	return FALSE;
}

int wmain(int ac, wchar_t* av[])
{
	setlocale(LC_ALL, "English");

	std::set_terminate(&my_terminate_handler);
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)(ctrl_handler), TRUE);

	auto res = app_main(utf_helper::CommandLineToUtf8(ac, av), IS_RUNNED);
	IS_FINISHED = true;
	return res;
}


