#include "Requester.h"

#include <curl/curl.h>
#include <algorithm>
#include <stdexcept>
#include <memory>

size_t Requester::CURLWriter(char* data, size_t size, size_t nmemb, WriteContext* context)
{
	if (context && context->owner && context->owner->mWriteFunction)
		if (!context->owner->mWriteFunction(data, size * nmemb))
		{
			context->abort = true;
			return size * nmemb + 1; // stop and call CURLE_WRITE_ERROR
		}

	return size * nmemb;
}

int Requester::CURLProgress(Requester* owner, long long total_to_download, long long now_downloaded, long long total_to_upload, long long now_uploaded)
{
	if (owner && owner->mProgressFunction)
		if (!owner->mProgressFunction(total_to_download, now_downloaded, total_to_upload, now_uploaded))
			return 1;

	return 0;
}

bool Requester::RequestUrl(std::string_view url, long timeout_ms) const
{
	auto curl = curl_easy_init();
	std::unique_ptr<CURL, std::function<void(CURL*)>> curl_guard(curl, [](CURL* ptr) { if (ptr) curl_easy_cleanup(ptr); });

	if (curl)
	{
		if (auto err = curl_easy_setopt(curl, CURLOPT_URL, url.data()))
			throw std::runtime_error(curl_easy_strerror(err));

		if (auto err = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, FALSE))
			throw std::runtime_error(curl_easy_strerror(err));

		if (timeout_ms)
			if (auto err = curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, timeout_ms))
				throw std::runtime_error(curl_easy_strerror(err));

		WriteContext write_context{this, false};
		if (mWriteFunction)
		{
			if (auto err = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CURLWriter))
				throw std::runtime_error(curl_easy_strerror(err));

			if (auto err = curl_easy_setopt(curl, CURLOPT_WRITEDATA, &write_context))
				throw std::runtime_error(curl_easy_strerror(err));
		}
		if (mProgressFunction)
		{
			if (auto err = curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0))
				throw std::runtime_error(curl_easy_strerror(err));

			if (auto err = curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, CURLProgress))
				throw std::runtime_error(curl_easy_strerror(err));

			if (auto err = curl_easy_setopt(curl, CURLOPT_XFERINFODATA, this))
				throw std::runtime_error(curl_easy_strerror(err));
		}

		auto perfom_err = curl_easy_perform(curl);
		if (perfom_err == CURLE_WRITE_ERROR && write_context.abort)
			return false;
		if (perfom_err == CURLE_ABORTED_BY_CALLBACK)
			return false;

		if (perfom_err)
			throw std::runtime_error(curl_easy_strerror(perfom_err));
	}

	return true;
}

Requester& Requester::SetWriteCallback(const std::function<bool(char*, size_t)>& callback)
{
	mWriteFunction = callback;
	return *this;
}

Requester& Requester::SetProgressCallback(const std::function<bool(long long, long long, long long, long long)>& callback)
{
	mProgressFunction = callback;
	return *this;
}
