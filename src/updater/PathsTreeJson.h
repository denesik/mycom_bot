#pragma once

#include <rapidjson/document.h>

struct PathsTree;

class PathsTreeJson
{
public:

	void Serialize(const rapidjson::Document& json, PathsTree& object) const;
	void Serialize(const PathsTree& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};


