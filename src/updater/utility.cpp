#include "utility.h"

#include "PathUtf8.h"
#include <boost/regex/v5/regex.hpp>
#include "Logger.h"
#include <fstream>
#include <boost/algorithm/string/replace.hpp>

namespace
{
	void FindPaths(const std::filesystem::path& path, const std::vector<std::string>& blacklist, const std::vector<std::string>& whitelist, const std::filesystem::path& work_dir, std::vector<std::string>& dirs, std::vector<std::string>& files)
	{
		if (!std::filesystem::exists(path))
			return;
		if (!std::filesystem::is_directory(path))
			return;

		for (auto& it : std::filesystem::directory_iterator(path))
		{
			if (it.is_directory())
			{
				auto utf_path = utf_helper::PathToUtf8(it.path().lexically_relative(work_dir));
				auto name = "/" + utf_path + "/";

				bool ignore = false;
				for (size_t i = 0; i < whitelist.size(); ++i)
				{
					boost::regex xRegEx(whitelist[i]);
					boost::smatch xResults;
					if (boost::regex_match(name, xResults, xRegEx))
						if (!xResults.empty())
						{
							break;
						}
					if (i == whitelist.size() - 1)
						ignore = true;
				}

				if (ignore)
					continue;

				for (const auto& pattern : blacklist)
				{
					boost::regex xRegEx(pattern);
					boost::smatch xResults;
					if (boost::regex_match(name, xResults, xRegEx))
						if (!xResults.empty())
						{
							ignore = true;
							break;
						}
				}

				if (ignore)
					continue;

				dirs.emplace_back(utf_path);
				FindPaths(it.path(), blacklist, whitelist, work_dir, dirs, files);
			}

			if (it.is_regular_file())
			{
				auto utf_path = utf_helper::PathToUtf8(it.path().lexically_relative(work_dir));
				auto name = "/" + utf_path;

				bool ignore = false;
				for (size_t i = 0; i < whitelist.size(); ++i)
				{
					boost::regex xRegEx(whitelist[i]);
					boost::smatch xResults;
					if (boost::regex_match(name, xResults, xRegEx))
						if (!xResults.empty())
						{
							break;
						}
					if (i == whitelist.size() - 1)
						ignore = true;
				}

				if (ignore)
					continue;

				for (const auto& pattern : blacklist)
				{
					boost::regex xRegEx(pattern);
					boost::smatch xResults;
					if (boost::regex_match(name, xResults, xRegEx))
						if (!xResults.empty())
						{
							ignore = true;
							break;
						}
				}

				if (ignore)
					continue;

				files.emplace_back(utf_path);
			}
		}
	}
}

void FindPaths(const std::filesystem::path& path, const std::vector<std::string>& blacklist, const std::vector<std::string>& whitelist, std::vector<std::string>& dirs, std::vector<std::string>& files)
{
	FindPaths(path, blacklist, whitelist, path, dirs, files);
}

bool RemoveAllPaths(const std::filesystem::path& dst, const std::vector<std::string>& data)
{
	try
	{
		for (const auto& name : data)
		{
			auto path = dst / utf_helper::PathFromUtf8(name);
			if (!std::filesystem::exists(path))
				continue;

			if (std::filesystem::is_regular_file(path))
				if (std::filesystem::remove(path))
				{
					LOG_I(std::string("Remove file: " + utf_helper::PathToUtf8(path)));
					continue;
				}
				else
				{
					LOG_I(std::string("Error remove file: " + utf_helper::PathToUtf8(path)));
					return false;
				}

			if (std::filesystem::is_directory(path))
				if (std::filesystem::is_empty(path))
					if (std::filesystem::remove(path))
					{
						LOG_I(std::string("Remove directory: " + utf_helper::PathToUtf8(path)));
						continue;
					}
					else
					{
						LOG_I(std::string("Error remove directory: " + utf_helper::PathToUtf8(path)));
						return false;
					}
		}
	}
	catch (std::exception & ex)
	{
		LOG_I(std::string("Error remove path: ") + ex.what());
		return false;
	}

	return true;
}

bool CreateAllDirectories(const std::filesystem::path& dst, const std::vector<std::string>& data)
{
	try
	{
		if (!std::filesystem::exists(dst))
			if (std::filesystem::create_directory(dst))
			{
				LOG_I(std::string("Directory created: " + utf_helper::PathToUtf8(dst)));
			}
			else
			{
				LOG_I(std::string("Error create directory: " + utf_helper::PathToUtf8(dst)));
				return false;
			}

		for (const auto& name : data)
		{
			auto path = dst / utf_helper::PathFromUtf8(name);

			if (std::filesystem::exists(path))
				continue;

			if (std::filesystem::create_directory(path))
			{
				LOG_I(std::string("Directory created: " + utf_helper::PathToUtf8(path)));
				continue;
			}
			else
			{
				LOG_I(std::string("Error create directory: " + utf_helper::PathToUtf8(path)));
				return false;
			}
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Error create directory: ") + ex.what());
		return false;
	}

	return true;
}

bool CopyAllFiles(const std::filesystem::path& src, const std::filesystem::path& dst, const std::vector<std::string>& data)
{
	try
	{
		for (const auto& name : data)
		{
			auto src_path = src / utf_helper::PathFromUtf8(name);
			auto dst_path = dst / utf_helper::PathFromUtf8(name);

			if (!std::filesystem::exists(src_path))
			{
				LOG_I(std::string("Error file not exist: " + utf_helper::PathToUtf8(src_path)));
				return false;
			}

			if (std::filesystem::exists(dst_path))
				continue;

			if (std::filesystem::is_regular_file(src_path))
				if (std::filesystem::copy_file(src_path, dst_path))
				{
					LOG_I(std::string("File copied from: " + utf_helper::PathToUtf8(src_path) + " to: " + utf_helper::PathToUtf8(dst_path)));
					continue;
				}
				else
				{
					LOG_I(std::string("Error copy file from: " + utf_helper::PathToUtf8(src_path) + " to: " + utf_helper::PathToUtf8(dst_path)));
					return false;
				}
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Error copy file: ") + ex.what());
		return false;
	}

	return true;
}

bool PreparePathsTree(const std::filesystem::path& path)
{
	try
	{
		if (std::filesystem::exists(path))
			if (std::filesystem::is_regular_file(path))
				if (!std::filesystem::remove(path))
				{
					LOG_I(std::string("Error remove file: ") + utf_helper::PathToUtf8(path));
					return false;
				}

		{
			std::ofstream file(path);
		}
		if (!std::filesystem::exists(path) || !std::filesystem::is_regular_file(path))
		{
			LOG_I(std::string("Error prepare file: ") + utf_helper::PathToUtf8(path));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Error prepare file: ") + ex.what());
		return false;
	}

	return true;
}

void RegexConvert(std::vector<std::string>& data)
{
	// . => \.
	// / => \/
	// * => .*
	for (auto& value : data)
	{
		boost::replace_all(value, R"(.)", R"(\.)");
		boost::replace_all(value, R"(/)", R"(\/)");
		boost::replace_all(value, R"(*)", R"(.*)");
	}
}

