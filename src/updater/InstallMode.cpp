#include "InstallMode.h"

#include "ProcessHelper.h"
#include "Logger.h"
#include "updater_constants.h"
#include "PathUtf8.h"
#include "Timer.h"
#include "LoadJson.h"
#include "PathsTree.h"
#include "PathsTreeJson.h"
#include "utility.h"

int InstallMode::Run() const
{
	if (!CheckSingleRun())
		return ReturnPaused();

	Timer timer;
	while (!CheckBotDirUnlocked(std::filesystem::current_path().parent_path()))
	{
		if (timer.Elapsed() > updater_constants::WaitingTimeout)
		{
			LOG_I(std::string("Waiting for the program to close failed"));
			return ReturnPaused();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	if (!CleanBotDirectory(std::filesystem::current_path().parent_path(), std::filesystem::current_path().parent_path() / utf_helper::PathFromUtf8(updater_constants::PathsTreeFilename)))
		return ReturnPaused();

	if (!CleanBotDirectory(std::filesystem::current_path().parent_path(), std::filesystem::current_path() / utf_helper::PathFromUtf8(updater_constants::PathsTreeFilename)))
		return ReturnPaused();

	if (!RemoveAllPaths(std::filesystem::current_path().parent_path(), { updater_constants::PathsTreeFilename }))
		return ReturnPaused();

	if (!Install(std::filesystem::current_path(), std::filesystem::current_path().parent_path(), std::filesystem::current_path() / utf_helper::PathFromUtf8(updater_constants::PathsTreeFilename)))
		return ReturnPaused();

	if (!CopyAllFiles(std::filesystem::current_path(), std::filesystem::current_path().parent_path(), { updater_constants::PathsTreeFilename }))
		return false;

	if (!RunCleanMode())
		return ReturnPaused();

	return 0;
}

bool InstallMode::CleanBotDirectory(const std::filesystem::path& work_dir, const std::filesystem::path& pathstree_path) const
{
	PathsTree paths_tree;

	{
		rapidjson::Document json;
		if (!utility::LoadJson(pathstree_path, json))
			LOG_I("Warning load json: " + utf_helper::PathToUtf8(pathstree_path));
		PathsTreeJson().Serialize(json, paths_tree);
	}

	if (!RemoveAllPaths(work_dir, paths_tree.build_files))
		return false;
	std::reverse(paths_tree.build_dirs.begin(), paths_tree.build_dirs.end());
	if (!RemoveAllPaths(work_dir, paths_tree.build_dirs))
		return false;

	return true;
}

bool InstallMode::Install(const std::filesystem::path& src_dir, const std::filesystem::path& dst_dir, const std::filesystem::path& pathstree_path) const
{
	PathsTree paths_tree;

	{
		rapidjson::Document json;
		if (!utility::LoadJson(pathstree_path, json))
		{
			LOG_I("Error load json: " + utf_helper::PathToUtf8(pathstree_path));
			return false;
		}
		PathsTreeJson().Serialize(json, paths_tree);
	}

	if (!CreateAllDirectories(dst_dir, paths_tree.build_dirs))
		return false;

	if (!CreateAllDirectories(dst_dir, paths_tree.user_dirs))
		return false;

	if (!CopyAllFiles(src_dir, dst_dir, paths_tree.build_files))
		return false;

	if (!CopyAllFiles(src_dir, dst_dir, paths_tree.user_files))
		return false;

	return true;
}

bool InstallMode::RunCleanMode() const
{
	try
	{
		auto work_dir = std::filesystem::current_path().parent_path();
		auto updater_path = work_dir / utf_helper::PathFromUtf8(updater_constants::UpdaterExeName);
		if (!std::filesystem::exists(updater_path))
		{
			LOG_I("Instaled updater not exist: " + utf_helper::PathToUtf8(updater_path));
			return false;
		}
		if (!process_helper::RunProcess(updater_path, work_dir, "--clean"))
		{
			LOG_I("Failed run updater: " + utf_helper::PathToUtf8(updater_path));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Run clean mode error: ") + ex.what());
		return false;
	}

	return true;
}

