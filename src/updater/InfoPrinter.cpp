#include "InfoPrinter.h"

#include "ProcessHelper.h"

#include <filesystem>
#include "PathUtf8.h"
#include "Logger.h"

int InfoPrinter::Run() const
{
	try
	{
		process_helper::ProcessInfo process_info;
		if (!process_helper::GetProcessInfoById(process_helper::GetCurrentProcessId(), process_info))
		{
			LOG_E("Get my process info failed");
			return 1;
		}

		auto current_path = std::filesystem::current_path();

		LOG_I("Runned exe: {}", utf_helper::PathToUtf8(process_info.path_exe));
		LOG_I("Work dir: {}", utf_helper::PathToUtf8(current_path));
		LOG_I("Process id: {}", std::to_string(process_info.id));
	}
	catch (std::exception& ex)
	{
		LOG_E("InfoPrinter error: {}", ex.what());
		return 1;
	}

	return 0;
}
