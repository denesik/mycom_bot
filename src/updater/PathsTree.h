#pragma once

#include <vector>
#include <string>

struct PathsTree 
{
	std::vector<std::string> build_dirs;
	std::vector<std::string> build_files;

	std::vector<std::string> user_dirs;
	std::vector<std::string> user_files;
};

