#include "CleanMode.h"

#include "Logger.h"
#include "updater_constants.h"
#include "Timer.h"
#include "PathUtf8.h"

int CleanMode::Run() const
{
	if (!CheckSingleRun())
		return ReturnPaused();

	Timer timer;
	while (!CheckBotDirUnlocked(std::filesystem::absolute(utf_helper::PathFromUtf8(updater_constants::DownloadTempDirectory))))
	{
		if (timer.Elapsed() > updater_constants::WaitingTimeout)
		{
			LOG_I(std::string("Waiting for the program to close failed"));
			return ReturnPaused();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	if (!Clean(std::filesystem::absolute(utf_helper::PathFromUtf8(updater_constants::DownloadTempDirectory))))
		return ReturnPaused();

	return 0;
}

bool CleanMode::Clean(const std::filesystem::path& path) const
{
	try
	{
		if (std::filesystem::is_directory(path))
			if (std::filesystem::remove_all(path))
			{
				LOG_I(std::string("Clean directory: " + utf_helper::PathToUtf8(path)));
			}
			else
			{
				LOG_I(std::string("Error clean directory: " + utf_helper::PathToUtf8(path)));
				return false;
			}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Error clean directory: ") + ex.what());
		return false;
	}

	return true;
}
