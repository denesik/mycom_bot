#pragma once

#include "ModeCommon.h"


class InstallMode : public ModeCommon
{
public:

	int Run() const;

private:

	bool CleanBotDirectory(const std::filesystem::path&  work_dir, const std::filesystem::path& pathstree_path) const;

	bool Install(const std::filesystem::path& src_dir, const std::filesystem::path& dst_dir, const std::filesystem::path& pathstree_path) const;

	bool RunCleanMode() const;
};
