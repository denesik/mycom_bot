#include "DownloadMode.h"

#include "Requester.h"
#include "Unpacker.h"

#include <filesystem>
#include <fstream>

#include "updater_constants.h"
#include "Logger.h"
#include "Version.h"
#include "PathUtf8.h"
#include "ProcessHelper.h"

int DownloadMode::Run()
{
	if (!CheckSingleRun())
		return ReturnPaused();

	{
		bool need_update = false;
		if (!CheckNeedUpdate(need_update))
			return ReturnPaused();
		if (!need_update)
		{
			LOG_I("No update required");
			return ReturnPaused(0);
		}
	}

	if (!CheckBotNotRunned())
		return ReturnPaused();

	{
		const std::filesystem::path upload_path = utf_helper::PathFromUtf8(updater_constants::DownloadTempDirectory);
		if (!RemoveDirectories(upload_path))
			return ReturnPaused();

		std::string buffer;
		if (!Download(buffer))
			return ReturnPaused();
		if (!Unpack(upload_path, buffer))
			return ReturnPaused();
	}

	if (!RunInstallMode())
		return ReturnPaused();

	return 0;
}

bool DownloadMode::GetRemoteVersion(std::string& version) const
{
	std::string remote_version;

	try
	{
		if (!Requester().SetWriteCallback([&remote_version](char* data, size_t size)
			{
				remote_version.append(data, size);
				return true;
			}).RequestUrl(updater_constants::UrlVersionFile))
		{
			LOG_I(std::string("Request version failed"));
			return 1;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Request version error: ") + ex.what());
		return false;
	}

	version = remote_version;

	return true;
}

bool DownloadMode::Download(std::string& buffer) const
{
	try
	{
		if (!Requester().SetWriteCallback([&buffer](char* data, size_t size)
			{
				buffer.append(data, size);
				return true;
			}).SetProgressCallback([counter = 0LL](long long total_to_download, long long now_downloaded, long long total_to_upload, long long now_uploaded) mutable
			{
				auto downloaded_mb = now_downloaded / 1024 / 1024;
				if (counter <= downloaded_mb)
				{
					++counter;
					auto percent = total_to_download ? (now_downloaded * 100) / total_to_download : 0;
					LOG_I("All: " + std::to_string(total_to_download / 1024 / 1024) + " mb downloaded: " + std::to_string(downloaded_mb) + " mb " + std::to_string(percent) + "%");
				}
				return true;
			}).RequestUrl(updater_constants::UrlRepositoryArchive))
		{
			LOG_I(std::string("Download failed"));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Download error: ") + ex.what());
		return false;
	}

	return true;
}

bool DownloadMode::RemoveDirectories(const std::filesystem::path& path) const
{
	try
	{
		if (std::filesystem::exists(path))
		{
			if (!std::filesystem::is_directory(path))
			{
				LOG_I(std::string("Remove directories failed. Is not directory: " + utf_helper::PathToUtf8(path)));
				return false;
			}
			if (!std::filesystem::remove_all(path))
			{
				LOG_I(std::string("Remove directories failed ") + utf_helper::PathToUtf8(path));
				return false;
			}
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Remove directories error: ") + ex.what());
		return false;
	}

	return true;
}

bool DownloadMode::Unpack(const std::filesystem::path &upload_path, const std::string& buffer) const
{
	try
	{
		if (!Unpacker().SetWriteCallback([&upload_path](const char* name, char* data, size_t size)
			{
				std::string filename(name);
				if (!filename.empty() && filename.back() != '/')
				{
					auto path = std::filesystem::relative(filename, updater_constants::RepositoryArchiveDirectory);

					std::filesystem::path file_path(upload_path / path);
					if (!std::filesystem::exists(file_path.parent_path()))
						if (!std::filesystem::create_directories(file_path.parent_path()))
							return false;

					std::ofstream new_file;
					new_file.open(upload_path / path, std::ios::binary);
					if (!new_file.is_open())
						return false;

					new_file.write(data, size);
					new_file.close();

					if (new_file.fail())
						return false;
				}
				return true;
			}).SetProgressCallback([counter = 0LL](int current, int count) mutable
			{
				auto percent = count ? ((current + 1) * 100) / count : 0;
				if (counter <= percent)
				{
					++counter;
					LOG_I("All: " + std::to_string(count) + " unpacked: " + std::to_string(current + 1) + " " + std::to_string(percent) + "%");
				}
				return true;
			}).Unpack(buffer))
		{
			LOG_I(std::string("Unpack failed"));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Unpack error: ") + ex.what());
		return false;
	}

	return true;
}

bool DownloadMode::CheckBotNotRunned() const
{
	std::vector<process_helper::ProcessInfo> process_list;
	try
	{
		if (!process_helper::GetProcessListByName(updater_constants::BotExeName, process_list))
		{
			LOG_I(std::string("Get process list failed"));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Get process list error: ") + ex.what());
		return false;
	}

	try
	{
		for (const auto& info : process_list)
				if (info.name_exe == std::filesystem::relative(info.path_exe))
				{
					LOG_I(std::string("Bot is runned. Please close."));
					return false;
				}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Bot run check error: ") + ex.what());
		return false;
	}

	return true;
}

bool DownloadMode::CheckNeedUpdate(bool& need_update) const
{
	std::string remote_version;
	std::string local_version = LoadVersion();

	if (!GetRemoteVersion(remote_version))
		return false;

	LOG_I("Local version : " + local_version);
	LOG_I("Remote version : " + remote_version);

	if (remote_version != local_version)
		need_update = true;

	return true;
}

bool DownloadMode::RunInstallMode() const
{
	try
	{
		auto updater_dir = std::filesystem::absolute(utf_helper::PathFromUtf8(updater_constants::DownloadTempDirectory));
		auto updater_path = updater_dir / utf_helper::PathFromUtf8(updater_constants::UpdaterExeName);
		if (!std::filesystem::exists(updater_path))
		{
			LOG_I("Downloaded updater not exist: " + utf_helper::PathToUtf8(updater_path));
			return false;
		}
		if (!process_helper::RunProcess(updater_path, updater_dir, "--install"))
		{
			LOG_I("Failed run updater: " + utf_helper::PathToUtf8(updater_path));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Run install mode error: ") + ex.what());
		return false;
	}

	return true;
}

