#include "ModeCommon.h"

#include "ProcessHelper.h"
#include "Logger.h"
#include "updater_constants.h"
#include "PathUtf8.h"

bool ModeCommon::CheckSingleRun() const
{
	std::vector<process_helper::ProcessInfo> process_list;
	try
	{
		if (!process_helper::GetProcessListByName(updater_constants::UpdaterExeName, process_list))
		{
			LOG_I(std::string("Get process list failed"));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Get process list error: ") + ex.what());
		return false;
	}

	try
	{
		auto current_process_id = process_helper::GetCurrentProcessId();
		for (const auto& info : process_list)
			if (info.id != current_process_id)
				if (info.name_exe == std::filesystem::relative(info.path_exe))
				{
					LOG_I(std::string("Multiple run denied"));
					return false;
				}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Multiple run check error: ") + ex.what());
		return false;
	}

	return true;
}

int ModeCommon::ReturnPaused(int return_code /*= 1*/) const
{
	system("pause");
	return return_code;
}

bool ModeCommon::CheckBotDirUnlocked(const std::filesystem::path& path) const
{
	std::vector<process_helper::ProcessInfo> process_list;
	try
	{
		if (!process_helper::GetProcessListByName(updater_constants::UpdaterExeName, process_list))
		{
			LOG_I(std::string("Get process list failed"));
			return false;
		}
		if (!process_helper::GetProcessListByName(updater_constants::BotExeName, process_list))
		{
			LOG_I(std::string("Get process list failed"));
			return false;
		}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Get process list error: ") + ex.what());
		return false;
	}

	try
	{
		for (const auto& info : process_list)
			if (info.name_exe == std::filesystem::relative(info.path_exe, path))
			{
				LOG_I(std::string("Waiting for the program to close: ") + utf_helper::PathToUtf8(info.path_exe));
				return false;
			}
	}
	catch (std::exception& ex)
	{
		LOG_I(std::string("Multiple run check error: ") + ex.what());
		return false;
	}

	return true;
}
