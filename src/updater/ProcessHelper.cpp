#include "ProcessHelper.h"

#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>

#include <memory>
#include <functional>

#include "utf_helper/MultiWideUtf8.h"
#include <boost/format.hpp>

#include "PathUtf8.h"

namespace
{
	std::string ErrorToString(DWORD err)
	{
		if (err == 0) 
			return {};

		LPTSTR buffer = NULL;
		auto size = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&buffer, 0, NULL);
		
		std::string out = utf_helper::multiwide_to_utf8(std::wstring_view(buffer, size));

		LocalFree(buffer);
		return out;
	}

	void SmartCloseHandle(HANDLE &ptr)
	{
		if (ptr)
		{
			if (!CloseHandle(ptr))
				if (auto err = GetLastError())
					throw std::runtime_error(ErrorToString(err));
			ptr = nullptr;
		}
	}
}

bool process_helper::GetProcessListByName(std::string_view name, std::vector<ProcessInfo>& data)
{
	
	PROCESSENTRY32 peProcessEntry;
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	std::unique_ptr<void, std::function<void(HANDLE)>> snapshot_guard(snapshot, [](HANDLE ptr) { SmartCloseHandle(ptr); });

	if (snapshot == INVALID_HANDLE_VALUE)
	{
		if (auto err = GetLastError())
			throw std::runtime_error(ErrorToString(err));
		return false;
	}

	peProcessEntry.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(snapshot, &peProcessEntry))
	{
		auto err = GetLastError();
		if (err == ERROR_NO_MORE_FILES)
			return true;
		if (err)
			throw std::runtime_error(ErrorToString(err));
		return false;
	}

	const auto wname = utf_helper::utf8_to_multiwide(name);
	do
	{
		if (std::wstring(peProcessEntry.szExeFile) == wname)
		{
			ProcessInfo process_info;
			if (!GetProcessInfoById(peProcessEntry.th32ProcessID, process_info))
				continue;
			data.push_back(process_info);
		}

		if (!Process32Next(snapshot, &peProcessEntry))
		{
			if (auto err = GetLastError())
				if (err != ERROR_NO_MORE_FILES)
					throw std::runtime_error(ErrorToString(err));
			break;
		}
	} while (true);

	return true;
}

std::uint32_t process_helper::GetCurrentProcessId()
{
	return ::GetCurrentProcessId();
}

bool process_helper::GetProcessInfoById(std::uint32_t id, ProcessInfo& info)
{
	if (id)
	{
		HANDLE process = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, id);
		if (!process)
		{
			if (auto err = GetLastError())
				throw std::runtime_error(ErrorToString(err));
			return false;
		}
		std::unique_ptr<void, std::function<void(HANDLE)>> process_guard(process, [](HANDLE ptr) { SmartCloseHandle(ptr); });

		wchar_t filePath[MAX_PATH];
		auto size = GetModuleFileNameEx(process, NULL, filePath, MAX_PATH - 1);
		if (size == 0)
		{
			if (auto err = GetLastError())
				throw std::runtime_error(ErrorToString(err));
			return false;
		}

		std::filesystem::path path(std::wstring_view(filePath, size));
		info = { path, utf_helper::PathToUtf8(path.filename()), id };
	}
	return true;
}

bool process_helper::RunProcess(const std::filesystem::path& exe_path, const std::filesystem::path& current_dir, std::string_view args)
{
	std::wstring cmd;
	if (args.empty())
		cmd = (boost::wformat(LR"("%1%")") % exe_path.wstring()).str();
	else
		cmd = (boost::wformat(LR"("%1%" %2%)") % exe_path.wstring() % utf_helper::utf8_to_multiwide(args)).str();

	STARTUPINFOW si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	if (!CreateProcess(NULL, LPTSTR(cmd.c_str()), NULL, NULL, FALSE, 0, NULL, 
		current_dir.empty() ? NULL : LPTSTR(current_dir.c_str()),
		&si, &pi))
	{
		if (auto err = GetLastError())
			throw std::runtime_error(ErrorToString(err));

		return false;
	}

	std::unique_ptr<void, std::function<void(HANDLE)>> process_guard(pi.hProcess, [](HANDLE ptr) { SmartCloseHandle(ptr); });
	std::unique_ptr<void, std::function<void(HANDLE)>> thread_guard(pi.hThread, [](HANDLE ptr) { SmartCloseHandle(ptr); });

	return true;
}
