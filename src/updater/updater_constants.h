#pragma once

namespace updater_constants
{
//https://codeload.github.com/denesik/mycom_bot_hc/zip/refs/heads/master
//https://codeload.github.com/HowardHinnant/date/zip/refs/heads/master

	const auto PathsTreeFilename = ".pathstree";

	const auto UrlVersionFile = "https://raw.githubusercontent.com/denesik/mycom_bot_hc/master/version.txt";
	const auto UrlRepositoryArchive = "https://codeload.github.com/denesik/mycom_bot_hc/zip/refs/heads/master";

	const auto RepositoryArchiveDirectory = "mycom_bot_hc-master";
	const auto DownloadTempDirectory = "upload_tmp";

	const auto BotExeName = "bot.exe";
	const auto UpdaterExeName = "updater.exe";

	const auto WaitingTimeout = 1000 * 10;
}