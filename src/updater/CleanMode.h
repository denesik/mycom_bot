#pragma once

#include "ModeCommon.h"


class CleanMode : public ModeCommon
{
public:

	int Run() const;

private:

	bool Clean(const std::filesystem::path& path) const;

};
