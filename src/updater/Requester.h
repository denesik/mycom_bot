#pragma once

#include <string>
#include <functional>

class Requester
{
public:

	Requester& SetWriteCallback(const std::function<bool(char*, size_t)>& callback);
	Requester& SetProgressCallback(const std::function<bool(long long, long long, long long, long long)>& callback);

	bool RequestUrl(std::string_view url, long timeout_ms = 0) const;

private:
	std::function<bool(char*, size_t)> mWriteFunction;
	std::function<bool(long long, long long, long long, long long)> mProgressFunction;

	struct WriteContext
	{
		const Requester* owner = nullptr;
		bool abort = false;
	};

	static size_t CURLWriter(char* data, size_t size, size_t nmemb, WriteContext* context);

	static int CURLProgress(Requester* owner, long long total_to_download, long long now_downloaded, long long total_to_upload, long long now_uploaded);
};

