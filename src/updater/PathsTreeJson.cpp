#include "PathsTreeJson.h"

#include "PathsTree.h"

void PathsTreeJson::Serialize(const rapidjson::Document& json, PathsTree& object) const
{
	if (!json.IsObject())
		return;

	{
		object.build_dirs.clear();
		auto it = json.FindMember("build_dirs");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.build_dirs.emplace_back(el.GetString(), el.GetStringLength());
	}
	{
		object.build_files.clear();
		auto it = json.FindMember("build_files");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.build_files.emplace_back(el.GetString(), el.GetStringLength());
	}
	{
		object.user_dirs.clear();
		auto it = json.FindMember("user_dirs");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.user_dirs.emplace_back(el.GetString(), el.GetStringLength());
	}
	{
		object.user_files.clear();
		auto it = json.FindMember("user_files");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.user_files.emplace_back(el.GetString(), el.GetStringLength());
	}
}

void PathsTreeJson::Serialize(const PathsTree& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();

	{
		{
			auto it = json.FindMember("build_dirs");
			if (it == json.MemberEnd())
				json.AddMember("build_dirs", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["build_dirs"].IsArray())
			return;

		const auto& json_array = json["build_dirs"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.build_dirs.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.build_dirs[i], allocator), allocator);
	}
	{
		{
			auto it = json.FindMember("build_files");
			if (it == json.MemberEnd())
				json.AddMember("build_files", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["build_files"].IsArray())
			return;

		const auto& json_array = json["build_files"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.build_files.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.build_files[i], allocator), allocator);
	}
	{
		{
			auto it = json.FindMember("user_dirs");
			if (it == json.MemberEnd())
				json.AddMember("user_dirs", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["user_dirs"].IsArray())
			return;

		const auto& json_array = json["user_dirs"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.user_dirs.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.user_dirs[i], allocator), allocator);
	}
	{
		{
			auto it = json.FindMember("user_files");
			if (it == json.MemberEnd())
				json.AddMember("user_files", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["user_files"].IsArray())
			return;

		const auto& json_array = json["user_files"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.user_files.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.user_files[i], allocator), allocator);
	}
}
