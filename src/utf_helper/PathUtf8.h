#pragma once

#include <filesystem>
#include <string>

namespace utf_helper
{
	std::filesystem::path PathFromUtf8(std::string_view data);

	std::string PathToUtf8(const std::filesystem::path& path);
}


