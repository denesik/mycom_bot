#pragma once

#include <string>

namespace utf_helper
{
	std::string multiwide_to_utf8(std::wstring_view data);

	std::wstring utf8_to_multiwide(std::string_view data);

}


