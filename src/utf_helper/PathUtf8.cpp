#include "PathUtf8.h"

#include "MultiWideUtf8.h"


std::filesystem::path utf_helper::PathFromUtf8(std::string_view data)
{
	return std::filesystem::path(utf8_to_multiwide(data));
}

std::string utf_helper::PathToUtf8(const std::filesystem::path& path)
{
	return multiwide_to_utf8(path.generic_wstring());
}
