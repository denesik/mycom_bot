#include "MultiWideUtf8.h"

#include <windows.h>

std::string utf_helper::multiwide_to_utf8(std::wstring_view data)
{
	if (data.empty())
		return {};

	int size_needed = WideCharToMultiByte(CP_UTF8, 0, &data[0], (int)data.size(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed, 0);
	WideCharToMultiByte(CP_UTF8, 0, &data[0], (int)data.size(), &strTo[0], size_needed, NULL, NULL);
	return strTo;
}

std::wstring utf_helper::utf8_to_multiwide(std::string_view data)
{
	if (data.empty())
		return {};

	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &data[0], (int)data.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &data[0], (int)data.size(), &wstrTo[0], size_needed);
	return wstrTo;
}
