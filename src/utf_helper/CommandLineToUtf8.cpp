#include "CommandLineToUtf8.h"

#include "MultiWideUtf8.h"

std::vector<std::string> utf_helper::CommandLineToUtf8(int ac, wchar_t* av[])
{
	std::vector<std::string> out;
	std::vector<std::wstring> args(av + 1, av + ac);

	for (const auto& str : args)
		out.emplace_back(multiwide_to_utf8(str));

	return out;
}
