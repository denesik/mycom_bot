#pragma once

#include <string>
#include <vector>

namespace utf_helper
{
	std::vector<std::string> CommandLineToUtf8(int ac, wchar_t* av[]);
}


