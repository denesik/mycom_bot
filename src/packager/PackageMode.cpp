#include "PackageMode.h"

#include "Logger.h"
#include "GenerateSettings.h"
#include "PathUtf8.h"
#include "LoadJson.h"
#include "GenerateSettingsJson.h"
#include "PathsTree.h"
#include "PathsTreeJson.h"
#include "SaveJson.h"

#include "utility.h"

#include "updater_constants.h"

int PackageMode::Run(const std::filesystem::path& config_path, const std::filesystem::path& build_path, const std::filesystem::path& package_path) const
{
	GenerateSettings settings;

	{
		rapidjson::Document json;
		if (!utility::LoadJson(config_path, json))
		{
			LOG_I("Error load json: " + utf_helper::PathToUtf8(config_path));
			return 1;
		}
		GenerateSettingsJson().Serialize(json, settings);
	}

	RegexConvert(settings.build_ignore);
	RegexConvert(settings.clean_ignore);

	RegexConvert(settings.generate_build);
	RegexConvert(settings.generate_user);

	{
		std::vector<std::string> build_dirs;
		std::vector<std::string> build_files;
		FindPaths(build_path, settings.build_ignore, {}, build_dirs, build_files);

		std::vector<std::string> package_dirs;
		std::vector<std::string> package_files;
		FindPaths(package_path, settings.clean_ignore, {}, package_dirs, package_files);

		{
			if (!RemoveAllPaths(package_path, package_files))
				return 1;
			std::reverse(package_dirs.begin(), package_dirs.end());
			if (!RemoveAllPaths(package_path, package_dirs))
				return 1;

			if (!CreateAllDirectories(package_path, build_dirs))
				return 1;

			if (!CopyAllFiles(build_path, package_path, build_files))
				return 1;
		}
	}

	{
		PathsTree paths_tree;
		FindPaths(package_path, settings.generate_build, {}, paths_tree.build_dirs, paths_tree.build_files);
		FindPaths(package_path, {}, settings.generate_user, paths_tree.user_dirs, paths_tree.user_files);

		auto json_path = package_path / utf_helper::PathFromUtf8(updater_constants::PathsTreeFilename);
		{
			rapidjson::Document json;
			PathsTreeJson().Serialize(paths_tree, json, json.GetAllocator());
			if (!utility::SaveJson(json_path, json))
			{
				LOG_I("Error save json: " + utf_helper::PathToUtf8(json_path));
				return 1;
			}
		}
	}

	return 0;
}
