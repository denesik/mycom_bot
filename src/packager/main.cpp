#include <cstdlib>       // std::abort
#include <exception>     // std::set_terminate
#include <iostream>      // std::cerr
#include <boost/stacktrace.hpp>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "utility.h"
#include "CommandLineToUtf8.h"

#include "PackageMode.h"
#include "PathUtf8.h"
#include "Logger.h"

#pragma warning( push )
#pragma warning( disable : 4459 )
#include <spdlog/sinks/stdout_color_sinks.h>
#pragma warning( pop )


int app_main(const std::vector<std::string>& args, const std::atomic<bool>& is_runned)
{
	std::string po_config_path;
	std::string po_build_path;
	std::string po_package_path;

	po::options_description desc("options");
	desc.add_options()
		("help", "produce help message")
		("config_file", po::value<std::string>(&po_config_path)->default_value("package.json"), "config path")
		("build_path", po::value<std::string>(&po_build_path)->default_value("./build"), "build path")
		("package_path", po::value<std::string>(&po_package_path)->default_value("./package"), "package path")
		;

	po::positional_options_description p;
	p.add("config_file", 1);
	p.add("build_path", 1);
	p.add("package_path", 1);

	po::variables_map vm;
	try
	{
		po::store(po::command_line_parser(args).options(desc).positional(p).run(), vm);
		po::notify(vm);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return 1;
	}

	auto logger = spdlog::stdout_color_mt("console");
	logger->set_level(spdlog::level::info);
	spdlog::set_default_logger(logger);

	if (vm.count("help"))
	{
		std::cout << desc << std::endl;
		return 0;
	}

	return PackageMode().Run(utf_helper::PathFromUtf8(po_config_path), utf_helper::PathFromUtf8(po_build_path), utf_helper::PathFromUtf8(po_package_path));
}



//==========================================

namespace
{
	std::atomic<bool> IS_RUNNED = true;
	std::atomic<bool> IS_FINISHED = false;
}

void my_terminate_handler()
{
	try
	{
		std::cerr << boost::stacktrace::stacktrace();
	}
	catch (...) {}
	std::abort();
}

BOOL ctrl_handler(DWORD event)
{
	switch (event)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	{
		IS_RUNNED = false;
		while (!IS_FINISHED)
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		return TRUE;
	}
	}

	return FALSE;
}

int wmain(int ac, wchar_t* av[])
{
	setlocale(LC_ALL, "English");

	std::set_terminate(&my_terminate_handler);
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)(ctrl_handler), TRUE);

	auto res = app_main(utf_helper::CommandLineToUtf8(ac, av), IS_RUNNED);
	IS_FINISHED = true;
	return res;
}


