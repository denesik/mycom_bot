#pragma once

#include <rapidjson/document.h>

struct GenerateSettings;

class GenerateSettingsJson
{
public:

	void Serialize(const rapidjson::Document& json, GenerateSettings& object) const;
	void Serialize(const GenerateSettings& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};


