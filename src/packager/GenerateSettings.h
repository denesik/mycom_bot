#pragma once

#include <vector>
#include <string>

struct GenerateSettings 
{
	std::vector<std::string> clean_ignore;
	std::vector<std::string> build_ignore;
	std::vector<std::string> generate_build;
	std::vector<std::string> generate_user;
};

