#pragma once

#include <filesystem>

class PackageMode
{
public:

	int Run(const std::filesystem::path & config_path, const std::filesystem::path& build_path, const std::filesystem::path& package_path) const;

};
