#include "GenerateSettingsJson.h"

#include "GenerateSettings.h"

void GenerateSettingsJson::Serialize(const rapidjson::Document& json, GenerateSettings& object) const
{
	if (!json.IsObject())
		return;

	{
		object.clean_ignore.clear();
		auto it = json.FindMember("clean_ignore");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.clean_ignore.emplace_back(el.GetString(), el.GetStringLength());
	}
	{
		object.build_ignore.clear();
		auto it = json.FindMember("build_ignore");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.build_ignore.emplace_back(el.GetString(), el.GetStringLength());
	}
	{
		object.generate_build.clear();
		auto it = json.FindMember("generate_build");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.generate_build.emplace_back(el.GetString(), el.GetStringLength());
	}
	{
		object.generate_user.clear();
		auto it = json.FindMember("generate_user");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					object.generate_user.emplace_back(el.GetString(), el.GetStringLength());
	}
}

void GenerateSettingsJson::Serialize(const GenerateSettings& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();

	{
		{
			auto it = json.FindMember("clean_ignore");
			if (it == json.MemberEnd())
				json.AddMember("clean_ignore", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["clean_ignore"].IsArray())
			return;

		const auto& json_array = json["clean_ignore"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.clean_ignore.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.clean_ignore[i], allocator), allocator);
	}
	{
		{
			auto it = json.FindMember("build_ignore");
			if (it == json.MemberEnd())
				json.AddMember("build_ignore", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["build_ignore"].IsArray())
			return;

		const auto& json_array = json["build_ignore"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.build_ignore.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.build_ignore[i], allocator), allocator);
	}
	{
		{
			auto it = json.FindMember("generate_build");
			if (it == json.MemberEnd())
				json.AddMember("generate_build", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["generate_build"].IsArray())
			return;

		const auto& json_array = json["generate_build"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.generate_build.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.generate_build[i], allocator), allocator);
	}
	{
		{
			auto it = json.FindMember("generate_user");
			if (it == json.MemberEnd())
				json.AddMember("generate_user", rapidjson::Value(rapidjson::kArrayType), allocator);
		}
		if (!json["generate_user"].IsArray())
			return;

		const auto& json_array = json["generate_user"].GetArray();
		json_array.Clear();

		for (size_t i = 0; i < object.generate_user.size(); ++i)
			json_array.PushBack(rapidjson::Value(object.generate_user[i], allocator), allocator);
	}
}
