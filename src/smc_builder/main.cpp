#include <filesystem>
#include <fstream>
#include <iostream>
#include <../../3rdparty/boost_1_76_0/boost/format.hpp>


bool CompareFiles(const std::string& p1, const std::string& p2) 
{
	std::ifstream f1(p1, std::ifstream::binary | std::ifstream::ate);
	std::ifstream f2(p2, std::ifstream::binary | std::ifstream::ate);

	if (f1.fail() || f2.fail()) 
		return false; //file problem

	if (f1.tellg() != f2.tellg()) 
		return false; 

	//seek back to beginning and use std::equal to compare contents
	f1.seekg(0, std::ifstream::beg);
	f2.seekg(0, std::ifstream::beg);
	return std::equal(std::istreambuf_iterator<char>(f1.rdbuf()),
		std::istreambuf_iterator<char>(),
		std::istreambuf_iterator<char>(f2.rdbuf()));
}


int main(int ac, char* av[])
{
	if (ac != 4)
		return 1;

	try
	{

		std::string jar_path(av[1], strlen(av[1]));
		std::string work_dir(av[2], strlen(av[2]));
		std::string generated_dir(av[3], strlen(av[3]));

		std::error_code err;
		if (!std::filesystem::exists(std::filesystem::path(generated_dir)))
			std::filesystem::create_directories(std::filesystem::path(generated_dir), err);
		if (err)
			return 1;

		const auto temp_sm_path = std::filesystem::path(generated_dir) / "Temp.sm";
		const auto logic_sm_path = std::filesystem::path(generated_dir) / "Logic.sm";

		std::cout << "smc runned" << std::endl;

		if (std::filesystem::exists(temp_sm_path))
			if (!std::filesystem::remove(temp_sm_path))
				return 1;

		{
			std::ofstream temp_file;
			temp_file.open(temp_sm_path, std::ofstream::out);
			if (!temp_file.is_open())
				return 1;

			{
				std::ifstream in_file;
				in_file.open(std::filesystem::path(work_dir) / "Header.sm", std::ofstream::in);
				if (in_file.is_open())
				{
					char c = in_file.get();
					while (in_file.good())
					{
						temp_file << c;
						c = in_file.get();
					}
				}
			}

			for (auto& it : std::filesystem::directory_iterator(std::filesystem::path(work_dir)))
			{
				auto path = it.path();

				if (path.extension() == ".smp")
				{
					std::ifstream in_file;
					in_file.open(path, std::ofstream::in);

					char c = in_file.get();
					while (in_file.good())
					{
						temp_file << c;
						c = in_file.get();
					}
				}
			}

			temp_file.close();
		}

		if (CompareFiles(logic_sm_path.string(), temp_sm_path.string()))
		{
			if (std::filesystem::exists(temp_sm_path))
				if (!std::filesystem::remove(temp_sm_path))
					return 1;

			if (std::filesystem::exists(std::filesystem::path(generated_dir) / "Logic_sm.h") &&
				std::filesystem::exists(std::filesystem::path(generated_dir) / "Logic_sm.cpp"))
			{
				std::cout << "smc finished (not changed)" << std::endl;
				return 0;
			}
		}
		else
		{
			if (std::filesystem::exists(logic_sm_path))
				if (!std::filesystem::remove(logic_sm_path))
					return 1;

			std::filesystem::rename(temp_sm_path, logic_sm_path, err);
			if (err)
				return 1;
		}

		if (std::filesystem::exists(std::filesystem::path(generated_dir) / "Logic_sm.h"))
			std::filesystem::remove(std::filesystem::path(generated_dir) / "Logic_sm.h", err);
		if (err)
			return 1;

		if (std::filesystem::exists(std::filesystem::path(generated_dir) / "Logic_sm.cpp"))
			std::filesystem::remove(std::filesystem::path(generated_dir) / "Logic_sm.cpp", err);
		if (err)
			return 1;

		auto out = system((boost::format(R"(java -jar "%1%" -c++ "%2%")") % jar_path % logic_sm_path.string()).str().c_str());
		if (out)
			return out;
	}
	catch (const std::exception & exc)
	{
		std::cout << "smc error: " << exc.what() << std::endl;
		return 1;
	}

	std::cout << "smc finished" << std::endl;
	return 0;
}
