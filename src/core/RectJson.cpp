#include "RectJson.h"

#include "Rect.h"

void JsonSerializer<Rect>::Serialize(const rapidjson::Value& json, Rect& object) const
{
	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("x");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.x = it->value.GetInt();
	}
	{
		auto it = json.FindMember("y");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.y = it->value.GetInt();
	}
	{
		auto it = json.FindMember("w");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.width = it->value.GetInt();
	}
	{
		auto it = json.FindMember("h");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.height = it->value.GetInt();
	}
}

void JsonSerializer<Rect>::Serialize(const Rect& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("x");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = object.x;
		else
			json.AddMember("x", object.x, allocator);
	}
	{
		auto it = json.FindMember("y");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = object.y;
		else
			json.AddMember("y", object.y, allocator);
	}
	{
		auto it = json.FindMember("w");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = object.width;
		else
			json.AddMember("w", object.width, allocator);
	}
	{
		auto it = json.FindMember("h");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = object.height;
		else
			json.AddMember("h", object.height, allocator);
	}
}
