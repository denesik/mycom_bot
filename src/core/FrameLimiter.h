#pragma once

#include <functional>
#include <vector>
#include <chrono>

class FrameLimiter
{
public:
	using time_type = std::chrono::milliseconds::rep;

	void Start();

	time_type GetPause(time_type frame_time_milli);
private:

	time_type CalculatePause(std::chrono::system_clock::time_point start_time, time_type frame_time);

private:
	std::chrono::system_clock::time_point mStartTime;

	static constexpr size_t mMaxFrames = 100;
	struct WorkTime
	{
		std::chrono::time_point<std::chrono::system_clock> start;
		std::chrono::time_point<std::chrono::system_clock> stop;
	};
	std::vector<WorkTime> mTimeFrames;

	std::function<void()> mCallback;
};
