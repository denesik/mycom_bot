#pragma once

#include <string>
#include <filesystem>
#include <map>

class Profile
{
public:
	Profile(const std::string& name);


	const std::string& GetName() const;

	const std::filesystem::path& GetProfilePath() const;

	std::filesystem::path FindPath(std::string_view name) const;


private:
	bool Load();

private:
	std::string mName;
	std::filesystem::path mProfileDir;

	std::map<std::string, std::filesystem::path, std::less<>> mPaths;
};

