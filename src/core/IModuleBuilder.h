#pragma once

class Actor;
class Profile;

class IModuleBuilder
{
public:
	~IModuleBuilder() = default;

	virtual void AddComponents(const Profile& profile, Actor* actor) = 0;

};
