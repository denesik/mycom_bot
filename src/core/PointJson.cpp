#include "PointJson.h"

#include "Point.h"

void JsonSerializer<Point>::Serialize(const rapidjson::Value& json, Point& object) const
{
	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("x");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.x = it->value.GetInt();
	}
	{
		auto it = json.FindMember("y");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.y = it->value.GetInt();
	}
}

void JsonSerializer<Point>::Serialize(const Point& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("x");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = object.x;
		else
			json.AddMember("x", object.x, allocator);
	}
	{
		auto it = json.FindMember("y");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = object.y;
		else
			json.AddMember("y", object.y, allocator);
	}
}
