#pragma once


#include <chrono>

class Timer
{
public:
	Timer();

	void Start();

	// ms
	int Elapsed() const;

private:
	decltype(std::chrono::high_resolution_clock::now()) mStartTime;
};

