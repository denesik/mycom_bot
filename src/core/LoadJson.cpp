#include "LoadJson.h"

#include <fstream>
#include <rapidjson/istreamwrapper.h>

bool utility::LoadJson(const std::filesystem::path& path, rapidjson::Document& json)
{
	rapidjson::Document doc;

	std::ifstream ifs(path);
	rapidjson::IStreamWrapper isw(ifs);

	bool res = false;

	rapidjson::AutoUTFInputStream<unsigned, rapidjson::IStreamWrapper> eis(isw);

	doc.ParseStream<rapidjson::kParseDefaultFlags | rapidjson::kParseCommentsFlag | rapidjson::kParseTrailingCommasFlag | rapidjson::kParseStopWhenDoneFlag, rapidjson::AutoUTF<unsigned> >(eis);

	if (doc.GetParseError() == rapidjson::kParseErrorNone)
		res = true;

	if (res)
		json = std::move(doc);

	return res;
}
