#pragma once

#include <memory>
#include <vector>

#include "MessagePipe.h"

class ActorComponent;

class Actor
{
public:
	Actor() = default;
	virtual ~Actor() = default;

	virtual void BeginPlay();
	
	virtual void Tick();

	virtual void EndPlay();

	bool IsRunned() const;

public:
	template<class T, class... Args>
	T & Send(Args&&... args)
	{
		return mMessagePipe.Send<T>(0, std::forward<Args>(args)...);
	}

	template<class T>
	void Bind(std::function<void(T &)> callback)
	{
		mBindsList.emplace_back(mMessagePipe.Bind<T>(0, callback));
	}


	template<class T>
	std::shared_ptr<const T> FindComponent() const
	{
		for (auto& v : mComponents)
			if (auto component = std::dynamic_pointer_cast<T>(v))
				return component;

		return nullptr;
	}

	template<class T>
	std::shared_ptr<T> FindComponent()
	{
		for (auto& v : mComponents)
			if (auto component = std::dynamic_pointer_cast<T>(v))
				return component;

		return nullptr;
	}

	template<class T>
	std::vector<std::shared_ptr<const T>> FindComponents() const
	{
		std::vector<std::shared_ptr<T>> out;
		for (auto& v : mComponents)
			if (auto component = std::dynamic_pointer_cast<T>(v))
				out.emplace_back(component);
		return out;
	}

	template<class T>
	std::vector<std::shared_ptr<T>> FindComponents()
	{
		std::vector<std::shared_ptr<T>> out;
		for (auto& v : mComponents)
			if (auto component = std::dynamic_pointer_cast<T>(v))
				out.emplace_back(component);
		return out;
	}

	template<class T, class... Types>
	std::shared_ptr<T> CreateComponent(Types&&... args)
	{
		auto out = std::make_shared<T>(std::forward<Types>(args)...);
		AddComponent(out);
		return out;
	}

	void AddComponent(const std::shared_ptr<ActorComponent> &component);

protected:

	void Stop();

protected:
	bool mIsRunned = true;

private:
	MessagePipe mMessagePipe;
	std::vector<uint64_t> mBindsList;

	std::vector<std::shared_ptr<ActorComponent>> mComponents;

};

