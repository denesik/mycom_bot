#pragma once

class Actor;

class ActorComponent
{
public:
	virtual ~ActorComponent() = default;

	virtual void Initialize() {};

	virtual void BeginPlay() {};

	virtual void Tick() {};

	virtual void EndPlay() {};

	virtual void Deinitialize() {};

protected:

	Actor* GetActor();

	const Actor* GetActor() const;

private:
	friend Actor;
	void SetActor(Actor* actor);

private:
	Actor* mActor = nullptr;

};

