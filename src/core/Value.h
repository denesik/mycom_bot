#pragma once

#include <variant>
#include <string>

using Value = std::variant<int, bool, std::string>;
