#pragma once

#include <memory>

#include "FrameLimiter.h"

template<class T>
class ActorTicker
{
public:
	template<class... Args>
	ActorTicker(Args&&... args)
	{
		mActor = std::make_shared<T>(std::forward<Args>(args)...);
	}

	int Entry(const std::atomic<bool>& runned)
	{
		if (!mActor)
			return -1;

		mActor->BeginPlay();

		FrameLimiter frame_limiter;

		while (runned)
		{
			frame_limiter.Start();

			if (!mActor->IsRunned())
				break;

			mActor->Tick();

			auto pause = frame_limiter.GetPause(1000 / 60);
			if (pause > 0)
				std::this_thread::sleep_for(std::chrono::milliseconds(pause));
		}

		mActor->EndPlay();

		return 0;
	}

private:
	std::shared_ptr<T> mActor;

};



