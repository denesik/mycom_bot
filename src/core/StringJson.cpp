#include "StringJson.h"

#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

bool utility::JsonFromString(std::string_view data, rapidjson::Document& json)
{
	bool res = false;

	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags | rapidjson::kParseCommentsFlag | rapidjson::kParseTrailingCommasFlag | rapidjson::kParseStopWhenDoneFlag>(reinterpret_cast<const char*>(data.data()), data.size());

	if (doc.GetParseError() == rapidjson::kParseErrorNone)
		res = true;

	if (res)
		json = std::move(doc);

	return res;
}

bool utility::JsonToString(const rapidjson::Document& json, std::string& data)
{
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	if (json.Accept(writer))
	{
		data = buffer.GetString();
		return true;
	}
	return false;
}
