#pragma once

#include <rapidjson/document.h>

#include "JsonSerializer.h"

struct Rect;

template<>
class JsonSerializer<Rect>
{
public:

	void Serialize(const rapidjson::Value& json, Rect& values) const;
	void Serialize(const Rect& values, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};

