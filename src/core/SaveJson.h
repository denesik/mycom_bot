#pragma once

#include <rapidjson/document.h>
#include <filesystem>

namespace utility
{
	bool SaveJson(const std::filesystem::path& path, const rapidjson::Document& json);
}

