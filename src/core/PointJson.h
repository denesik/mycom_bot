#pragma once

#include <rapidjson/document.h>

#include "JsonSerializer.h"

struct Point;

template<>
class JsonSerializer<Point>
{
public:

	void Serialize(const rapidjson::Value& json, Point& values) const;
	void Serialize(const Point& values, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};

