#pragma once

#include "Actor.h"

class Profile;

class Application : public Actor
{
public:
	Application(const Profile& profile);

};

