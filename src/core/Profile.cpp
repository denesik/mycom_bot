#include "Profile.h"

#include <fstream>

#include "LoadJson.h"

#include "utf_helper/PathUtf8.h"

namespace
{
	std::map<std::string, std::string> GetSectionData(std::string_view section, const rapidjson::Document& json)
	{
		if (!json.IsObject())
			return {};

		auto it = json.FindMember(std::string(section));
		if (it == json.MemberEnd() || !it->value.IsObject())
			return {};

		std::map<std::string, std::string> out;
		const auto &sec = it->value.GetObject();
		for (auto jt = sec.MemberBegin(); jt != sec.MemberEnd(); ++jt)
			if (jt->name.IsString() && jt->value.IsString())
				out[jt->name.GetString()] = jt->value.GetString();

		return out;
	};

	void AddPaths(const std::filesystem::path& root, const std::map<std::string, std::string>& data, std::map<std::string, std::filesystem::path, std::less<>>& paths)
	{
		for (const auto pair : data)
			paths[pair.first] = root / utf_helper::PathFromUtf8(pair.second);
	}
}

Profile::Profile(const std::string& name)
	: mName(name)
{
	Load();
	try
	{
		if (!std::filesystem::exists(mProfileDir))
			std::filesystem::create_directories(mProfileDir);
	}
	catch (std::filesystem::filesystem_error&)
	{

	}
}

const std::string& Profile::GetName() const
{
	return mName;
}

const std::filesystem::path& Profile::GetProfilePath() const
{
	return mProfileDir;
}

std::filesystem::path Profile::FindPath(std::string_view name) const
{
	auto it = mPaths.find(name);
	if (it != mPaths.end())
		return it->second;

	return {};
}

bool Profile::Load()
{
	rapidjson::Document json;
	if (!utility::LoadJson("paths.txt", json))
		return false;

	AddPaths({}, GetSectionData("basic", json), mPaths);

	mProfileDir = FindPath("profiles_dir") / utf_helper::PathFromUtf8(mName);

	AddPaths(mProfileDir, GetSectionData("profile", json), mPaths);

	return true;
}
