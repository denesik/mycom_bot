#pragma once

#include <wx/wxprec.h>

#include "Application.h"


class EditorApplication : public wxApp
{
public:
	EditorApplication(const Profile &profile);

	virtual bool OnInit() override;

	virtual int OnExit() override;

private:
	wxTimer mTimer;
	Application mApplication;
};

wxDECLARE_APP(EditorApplication);
