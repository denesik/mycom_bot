#include "FrameLimiter.h"


FrameLimiter::time_type FrameLimiter::CalculatePause(std::chrono::system_clock::time_point start_time, time_type frame_time)
{
	// ������� ������, ������ �����.
	while (mTimeFrames.size() > mMaxFrames)
		mTimeFrames.erase(mTimeFrames.begin());

	// ������� ������� ����� ���� ���������� ������.
	time_type mean_pause = 0;
	if (!mTimeFrames.empty())
	{
		time_type work_time = 0;
		for (const auto& wt : mTimeFrames)
			work_time += std::chrono::duration_cast<std::chrono::milliseconds>(wt.stop - wt.start).count();

		auto real_time = std::chrono::duration_cast<std::chrono::milliseconds>(start_time - mTimeFrames.front().start).count();

		mean_pause = (real_time - work_time) / static_cast<time_type>(mTimeFrames.size());
	}

	auto stop_time = std::chrono::system_clock::now();
	mTimeFrames.push_back({ start_time, stop_time });

	auto curr_time = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time).count();
	auto curr_pause = frame_time > curr_time ? frame_time - curr_time : 0;
	
	auto pause = (curr_pause + mean_pause) / 2;

	return pause > frame_time ? frame_time : pause;
}

void FrameLimiter::Start()
{
	mStartTime = std::chrono::system_clock::now();
}

FrameLimiter::time_type FrameLimiter::GetPause(time_type frame_time_milli)
{
	return frame_time_milli > 0 ? CalculatePause(mStartTime, frame_time_milli) : 0;
}

