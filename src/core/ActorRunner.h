#pragma once

#include <memory>

template<class T>
class ActorRunner
{
public:
	template<class... Args>
	ActorRunner(Args&&... args)
	{
		mActor = std::make_shared<T>(std::forward<Args>(args)...);
	}

	int Entry()
	{
		if (!mActor)
			return -1;

		mActor->BeginPlay();
		mActor->EndPlay();

		return 0;
	}

private:
	std::shared_ptr<T> mActor;

};
