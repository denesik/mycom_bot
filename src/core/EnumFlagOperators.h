#pragma once



#define DEFINE_ENUM_FLAGS_OPERATORS(ENUMTYPE) \
inline ENUMTYPE operator | (ENUMTYPE a, ENUMTYPE b)		{ return static_cast<ENUMTYPE>(static_cast<std::underlying_type<ENUMTYPE>::type>(a) | static_cast<std::underlying_type<ENUMTYPE>::type>(b)); } \
inline ENUMTYPE operator & (ENUMTYPE a, ENUMTYPE b)		{ return static_cast<ENUMTYPE>(static_cast<std::underlying_type<ENUMTYPE>::type>(a) & static_cast<std::underlying_type<ENUMTYPE>::type>(b)); } \
inline ENUMTYPE operator ^ (ENUMTYPE a, ENUMTYPE b)		{ return static_cast<ENUMTYPE>(static_cast<std::underlying_type<ENUMTYPE>::type>(a) ^ static_cast<std::underlying_type<ENUMTYPE>::type>(b)); } \
inline ENUMTYPE operator ~ (ENUMTYPE a)					{ return static_cast<ENUMTYPE>(~static_cast<std::underlying_type<ENUMTYPE>::type>(a)); } \
inline ENUMTYPE &operator |= (ENUMTYPE &a, ENUMTYPE b)	{ a = static_cast<ENUMTYPE>(static_cast<std::underlying_type<ENUMTYPE>::type>(a) | static_cast<std::underlying_type<ENUMTYPE>::type>(b)); return a; } \
inline ENUMTYPE &operator &= (ENUMTYPE &a, ENUMTYPE b)	{ a = static_cast<ENUMTYPE>(static_cast<std::underlying_type<ENUMTYPE>::type>(a) & static_cast<std::underlying_type<ENUMTYPE>::type>(b)); return a; } \
inline ENUMTYPE &operator ^= (ENUMTYPE &a, ENUMTYPE b)	{ a = static_cast<ENUMTYPE>(static_cast<std::underlying_type<ENUMTYPE>::type>(a) ^ static_cast<std::underlying_type<ENUMTYPE>::type>(b)); return a; } 

