#include "Actor.h"

#include <ranges>

#include "ActorComponent.h"


void Actor::BeginPlay()
{
	for (const auto& component : mComponents)
		component->SetActor(this);
	for (const auto& component : mComponents)
		component->Initialize();
	for (const auto& component : mComponents)
		component->BeginPlay();
	mMessagePipe.Flush();
}

void Actor::Tick()
{
	for (const auto& component : mComponents)
		component->Tick();
	mMessagePipe.Flush();
}

void Actor::EndPlay()
{
	for (auto id : mBindsList)
		mMessagePipe.Unbind(id);
	mBindsList.clear();

	for (const auto& component : std::ranges::reverse_view(mComponents))
		component->EndPlay();
	
	for (const auto& component : std::ranges::reverse_view(mComponents))
		component->Deinitialize();
}

bool Actor::IsRunned() const
{
	return mIsRunned;
}

void Actor::AddComponent(const std::shared_ptr<ActorComponent>& component)
{
	mComponents.emplace_back(component);
}

void Actor::Stop()
{
	mIsRunned = false;
}
