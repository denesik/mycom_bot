#pragma once

#pragma warning( push )
#pragma warning( disable : 4459 )
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>
#pragma warning( pop )

const char* LoggerLevelString(spdlog::level::level_enum value);
spdlog::level::level_enum LoggerLevelString(std::string_view msg);

#define LOG_I(...) spdlog::info(__VA_ARGS__)
#define LOG_E(...) spdlog::error(__VA_ARGS__)
#define LOG_W(...) spdlog::warn(__VA_ARGS__)
#define LOG_D(...) spdlog::debug(__VA_ARGS__)
#define LOG_T(...) spdlog::trace(__VA_ARGS__)
#define LOG_F(...) spdlog::critical(__VA_ARGS__)



