#include "ActorComponent.h"

Actor* ActorComponent::GetActor()
{
	return mActor;
}

const Actor* ActorComponent::GetActor() const
{
	return mActor;
}

void ActorComponent::SetActor(Actor* actor)
{
	mActor = actor;
}
