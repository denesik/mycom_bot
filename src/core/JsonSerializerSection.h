#pragma once

#include <rapidjson/document.h>
#include <string>

#include "JsonSerializer.h"

template<class T>
class JsonSerializerSection
{
public:
	JsonSerializerSection(std::string_view name)
		: mName(name)
	{}

	void Serialize(const rapidjson::Value& json, T& object) const
	{
		if (!json.IsObject())
			return;

		auto it = json.FindMember(mName);
		if (it != json.MemberEnd() && it->value.IsObject())
		{
			const auto& section_json = it->value.GetObject();
			JsonSerializer<T>().Serialize(section_json, object);
		}
	}

	void Serialize(const T& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
	{
		if (!json.IsObject())
			json.SetObject();

		{
			auto it = json.FindMember(mName);
			if (it == json.MemberEnd())
				json.AddMember(rapidjson::Value(mName, allocator), rapidjson::Value(rapidjson::kObjectType), allocator);
		}
		if (!json[mName].IsObject())
			return;

		const auto& section_json = json[mName].GetObject();
		JsonSerializer<T>().Serialize(object, section_json, allocator);
	}

private:
	const std::string mName;

};
