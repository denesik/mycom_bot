#pragma once

#include <functional>

#include <thread>
#include <mutex>

class AsyncExecutor
{
public:
	~AsyncExecutor();

	bool IsBusy() const;

	bool RunTask(std::function<void()> task);

private:
	std::thread mThread;

	mutable std::mutex mMutex;
	bool mIsBusy = false;
	bool mIsReserved = false;
};


