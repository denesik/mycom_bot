#pragma once

#include <string>
#include <rapidjson/document.h>

namespace utility
{

	bool JsonFromString(std::string_view data, rapidjson::Document& json);

	bool JsonToString(const rapidjson::Document& json, std::string &data);


}


