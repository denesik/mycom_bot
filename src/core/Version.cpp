#include "Version.h"

#include <fstream>

namespace
{
	static std::string gVersion;
	static bool gInitialized = false;

}

std::string LoadVersion()
{
	if (gInitialized)
		return gVersion;

	gInitialized = true;

	std::ifstream file("version.txt");
	if (file.is_open())
		gVersion = std::string(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

	return gVersion;
}
