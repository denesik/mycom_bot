#include "EditorApplication.h"

EditorApplication::EditorApplication(const Profile& profile)
	: mApplication(profile)
{

}

bool EditorApplication::OnInit()
{
	mApplication.BeginPlay();

	mTimer.Bind(wxEVT_TIMER, [this](wxTimerEvent& event)
		{
			if (!mApplication.IsRunned())
				Exit();
			else
				mApplication.Tick();
		});
	mTimer.Start(1000 / 60);

	return true;
}

int EditorApplication::OnExit()
{
	mApplication.EndPlay();
	return 0;
}

EditorApplication& wxGetApp() { return *static_cast<EditorApplication*>(wxApp::GetInstance()); }  

