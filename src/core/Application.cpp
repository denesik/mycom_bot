#include "Application.h"

#include "Profile.h"
#include "ManagerModuleBuilder.h"
#include "LogicModuleBuilder.h"
#include "GuiModuleBuilder.h"
#include "DetectorModuleBuilder.h"


Application::Application(const Profile& profile)
{
	DetectorModuleBuilder().AddComponents(profile, this);
	LogicModuleBuilder().AddComponents(profile, this);
	ManagerModuleBuilder().AddComponents(profile, this);
	GuiModuleBuilder().AddComponents(profile, this);

}
