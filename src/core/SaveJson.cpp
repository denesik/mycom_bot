#include "SaveJson.h"

#include <fstream>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/prettywriter.h>

bool utility::SaveJson(const std::filesystem::path& path, const rapidjson::Document& json)
{
	std::ofstream ofs(path);
	if (ofs.is_open())
	{
		rapidjson::OStreamWrapper osw(ofs);
		rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(osw);
		return json.Accept(writer);
	}

	return false;
}
