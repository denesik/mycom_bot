#include "Logger.h"

const char* LoggerLevelString(spdlog::level::level_enum value)
{
	switch (value)
	{
	case spdlog::level::trace:
		return "trace";
	case spdlog::level::debug:
		return "debug";
	case spdlog::level::info:
		return "info";
	case spdlog::level::warn:
		return "warning";
	case spdlog::level::err:
		return "error";
	case spdlog::level::critical:
		return "fatal";
	case spdlog::level::off:
		return "none";
	default:
		break;
	}

	return "none";
}

spdlog::level::level_enum LoggerLevelString(std::string_view msg)
{
	if (msg == LoggerLevelString(spdlog::level::off))
		return spdlog::level::off;
	if (msg == LoggerLevelString(spdlog::level::trace))
		return spdlog::level::trace;
	if (msg == LoggerLevelString(spdlog::level::debug))
		return spdlog::level::debug;
	if (msg == LoggerLevelString(spdlog::level::info))
		return spdlog::level::info;
	if (msg == LoggerLevelString(spdlog::level::warn))
		return spdlog::level::warn;
	if (msg == LoggerLevelString(spdlog::level::err))
		return spdlog::level::err;
	if (msg == LoggerLevelString(spdlog::level::critical))
		return spdlog::level::critical;

	return spdlog::level::off;
}
