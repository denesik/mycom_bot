#pragma once

#include <rapidjson/document.h>
#include <filesystem>

namespace utility
{
	bool LoadJson(const std::filesystem::path& path, rapidjson::Document& json);
}


