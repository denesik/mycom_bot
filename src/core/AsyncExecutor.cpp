#include "AsyncExecutor.h"



AsyncExecutor::~AsyncExecutor()
{
	if (mThread.joinable())
		mThread.join();
}

bool AsyncExecutor::IsBusy() const
{
	std::lock_guard<std::mutex> lock(mMutex);
	return mIsBusy || mIsReserved;
}

bool AsyncExecutor::RunTask(std::function<void()> task)
{
	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mIsBusy || mIsReserved)
			return false;
		mIsReserved = true;
	}

	if (mThread.joinable())
		mThread.join();

	{
		std::lock_guard<std::mutex> lock(mMutex);
		mIsBusy = true;
	}

	mThread = std::thread([this, task]()
		{
			task();
			std::lock_guard<std::mutex> lock(mMutex);
			mIsBusy = false;
		});

	std::lock_guard<std::mutex> lock(mMutex);
	mIsReserved = false;
	return true;
}


