#include "MessagePipe.h"

#include <ranges>
#include <algorithm>

void MessagePipe::Unbind(uint64_t session_id)
{
	mUnbinds.emplace_back(session_id);
}

void MessagePipe::Flush()
{
	if (!mNewSubscriptions.empty())
	{
		std::ranges::move(mNewSubscriptions, std::back_inserter(mSubscriptions));
		mNewSubscriptions.clear();
	}

	if (!mUnbinds.empty())
	{
		for (auto id : mUnbinds)
		{
			const auto [first, last] = std::ranges::remove_if(mSubscriptions, [id](const Subscription & s) { return s.subscription_id == id; });
			mSubscriptions.erase(first, last);
		}
		mUnbinds.clear();
	}

	auto subscriptions = mSubscriptions;
	auto packs = std::move(mPackages);

	for (auto & pack : packs)
		for (const auto & subscription : subscriptions)
			subscription.caller(pack.message);
}
