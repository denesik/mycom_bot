#pragma once

struct IMessage
{
	IMessage() = default;
	virtual ~IMessage() = default;
};

template<class T>
struct MessageInstance : public IMessage
{
	template<class... Args>
	MessageInstance(Args&&... args)
		: data(std::forward<Args>(args)...)
	{}

	MessageInstance(T && _data)
		: data(std::forward<T>(_data))
	{}

	T data;
};


