#pragma once

#include <string>
#include "LogicResult.h"

struct MessageLogicStart
{ 
	std::string program_type; 
};


struct MessageLogicFinish
{
	LogicResult result;
};






