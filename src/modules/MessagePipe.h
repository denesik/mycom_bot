#pragma once

#include <memory>
#include <functional>

#include "IMessage.h"


class MessagePipe
{
public:

	template<class T, class... Args>
	T & Send(uint64_t session_id, Args&&... args)
	{
		auto msg = std::make_shared<MessageInstance<T>>(std::forward<Args>(args)...);
		mPackages.emplace_back(session_id, msg);
		return msg->data;
	}

	template<class T>
	uint64_t Bind(uint64_t session_id, std::function<void(T &)> callback)
	{
		return mNewSubscriptions.emplace_back(++mSubscriptionId, session_id, [callback](const std::shared_ptr<IMessage> & msg)
		{
			if (auto out = std::dynamic_pointer_cast<MessageInstance<T>>(msg))
				callback(out->data);
		}).subscription_id;
	}

	void Unbind(uint64_t session_id);

	void Flush();

private:
	struct Package
	{
		uint64_t id;
		std::shared_ptr<IMessage> message;
	};
	std::vector<Package> mPackages;

	struct Subscription
	{
		uint64_t subscription_id = 0;
		uint64_t section_id = 0;
		std::function<void(const std::shared_ptr<IMessage> & msg)> caller;
	};
	uint64_t mSubscriptionId = 0;
	std::vector<Subscription> mSubscriptions;
	std::vector<Subscription> mNewSubscriptions;
	std::vector<uint64_t> mUnbinds;
};
