#include "Detector.h"

#include <fstream>

#include "Emulator.h"
#include "Logger.h"
#include "utility/MaskToRect.h"

#include "Book.h"

Detector::Detector(std::shared_ptr<Book> book, std::shared_ptr<Emulator> emulator)
	: mBook(book), mScanner(emulator)
{
	mScanErrorPage = Page("capture_error-missing"); //scan_error //size_error //minimized
	mSizeErrorPage = Page("capture_error-size");
	mMinimizedPage = Page("capture_error-minimized");

	mScanner.Capture(mActualImage);
	mActualImage.copyTo(mBufferImage);
	mActualImage.copyTo(mBufferLastImage);
	mActualImage.copyTo(mScannedImage);

	mActualPage = Page("not_loaded");
}


Detector::~Detector()
{

}

void Detector::Run()
{
	mThread = std::jthread([this](std::stop_token stoken)
		{
			while (!stoken.stop_requested())
			{
				mFrameLimiter.Start();

				auto capture_status = mScanner.Capture(mBufferImage);

				DetectorStatus status = DetectorStatus::NoError;

				if (capture_status == ScanningStatus::Minimized)
					status = DetectorStatus::Minimized;
				if (capture_status == ScanningStatus::WrongSize)
					status = DetectorStatus::SizeError;
				if (capture_status == ScanningStatus::NotFound)
					status = DetectorStatus::NotFound;
				if (capture_status == ScanningStatus::UnknownEmulator)
					status = DetectorStatus::ScanError;
				if (capture_status == ScanningStatus::CaptureError)
					status = DetectorStatus::ScanError;

				bool was_updated = false;
				if (!MatEqual(mBufferLastImage, mBufferImage))
				{
					mBufferImage.copyTo(mBufferLastImage);
					was_updated = true;
				}

				if (status == DetectorStatus::NoError && (was_updated || mLastStatus == DetectorStatus::NotLoaded))
				{
					mBufferPage = mBook->Identify(mBufferImage);
					if (mBufferPage.FullName() == "not_loaded")
						status = DetectorStatus::NotLoaded;
					else if (!mBufferPage.Parse(mBufferImage))
						LOG_E("Page " + mBufferPage.FullName() + " parsed error");
				}

				if (mLastStatus != status)
				{
					mLastStatus = status;
					was_updated = true;
				}

				if (was_updated)
				{
					switch (mLastStatus)
					{
					case DetectorStatus::NotFound:
						mBufferPage = mScanErrorPage;
						break;
					case DetectorStatus::ScanError:
						mBufferPage = mScanErrorPage;
						break;
					case DetectorStatus::Minimized:
						mBufferPage = mMinimizedPage;
						break;
					case DetectorStatus::SizeError:
						mBufferPage = mSizeErrorPage;
						break;
					}

					std::lock_guard<std::mutex> lock(mScannedMutex);
					mBufferImage.copyTo(mScannedImage);
					mScannedPage = mBufferPage;
					mScannedUpdated = true;
					mScannedStatus = mLastStatus;
				}

				{
					unsigned int fps = mFrameLimiterTime;
					auto pause = mFrameLimiter.GetPause(fps == 0 ? 0 : 1000 / fps);
					if (pause > 0)
						std::this_thread::sleep_for(std::chrono::milliseconds(pause));
				}
			}
		});
}

void Detector::SetFps(unsigned int fps)
{
	if (mFrameLimiterTime != fps)
	{
		mFrameLimiterTime = fps;
		signal_data();
	}
}

unsigned int Detector::GetFps() const
{
	return mFrameLimiterTime;
}

const Page& Detector::GetPage() const
{
	return mActualPage;
}

const cv::Mat& Detector::GetImage() const
{
	return mActualImage;
}

DetectorStatus Detector::GetStatus() const
{
	return mActualStatus;
}

void Detector::BeginPlay()
{
	Run();
}

// Tick, GetPage, GetImage ������ ���������� � ����� ������.
void Detector::Tick()
{
	bool was_updated = false;
	{
		std::lock_guard<std::mutex> lock(mScannedMutex);
		if (mScannedUpdated)
		{
			mScannedUpdated = false;
			was_updated = true;
			mActualPage = mScannedPage;
			mScannedImage.copyTo(mActualImage);
			mActualStatus = mScannedStatus;
		}
	}
	if (was_updated)
		signal_detect();
}

void Detector::EndPlay()
{
	std::jthread().swap(mThread);
}

bool Detector::MatEqual(const cv::Mat& a, const cv::Mat& b) const
{
	if (a.empty() && b.empty())
		return true;

	if (a.cols != b.cols || a.rows != b.rows || a.dims != b.dims || a.type() != b.type())
		return false;
	
	for (int i = 0; i < a.rows; i++)
		for (int j = 0; j < a.cols; j++)
			if (a.at<cv::Vec3b>(i, j) != b.at<cv::Vec3b>(i, j))
				return false;

	return true;
}

