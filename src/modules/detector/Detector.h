#pragma once

#include <thread>
#include <mutex>
#include <atomic>
#include <sigslot/signal.hpp>

#include "DetectorStatus.h"

#include "FrameLimiter.h"
#include "ActorComponent.h"

#include "Page.h"
#include "Scanner.h"

class Emulator;
class Book;

class Detector : public ActorComponent
{
public:
	Detector(std::shared_ptr<Book> book, std::shared_ptr<Emulator> emulator);
	~Detector();

	sigslot::signal<> signal_data;
	sigslot::signal<> signal_detect;

	void SetFps(unsigned int fps);
	unsigned int GetFps() const;

	const Page& GetPage() const;
	const cv::Mat& GetImage() const;
	DetectorStatus GetStatus() const;


	virtual void BeginPlay() override;

	virtual void Tick() override;

	virtual void EndPlay() override;

private:
	std::atomic<unsigned int> mFrameLimiterTime = 0;
	FrameLimiter mFrameLimiter;
	Scanner mScanner;

	Page mScanErrorPage;
	Page mSizeErrorPage;
	Page mMinimizedPage;

	std::shared_ptr<Book> mBook;
	std::jthread mThread;

private:
	cv::Mat mBufferImage;
	cv::Mat mBufferLastImage;
	DetectorStatus mLastStatus = DetectorStatus::NoError;
	Page mBufferPage;

	std::mutex mScannedMutex;
	bool mScannedUpdated = false;
	Page mScannedPage;
	cv::Mat mScannedImage;
	DetectorStatus mScannedStatus = DetectorStatus::NoError;

	Page mActualPage;
	cv::Mat mActualImage;
	DetectorStatus mActualStatus = DetectorStatus::NoError;

private:
	bool MatEqual(const cv::Mat& a, const cv::Mat& b) const;

	void Run();

};
