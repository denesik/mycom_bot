#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>

class AutoRunner;

class EditorSettingsWidget : public wxPanel
{
public:
	EditorSettingsWidget(wxWindow *parent, std::shared_ptr<AutoRunner> auto_runner);

private:
	void UpdateAutoCloseEmulator();

private:
	std::shared_ptr<AutoRunner> mAutoRunner;

	wxCheckBox* mStopEmulatorMode = nullptr;
	wxCheckBox* mAutoRun = nullptr;

	sigslot::scoped_connection mConnectionAutoCloseEmulator;
};


