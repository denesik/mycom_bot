#include "EditorBaseOptions.h"

void EditorBaseOptions::SetShowCaptureMode(bool value)
{
	if (mShowCaptureMode != value)
	{
		mShowCaptureMode = value;
		signal_data();
	}
}

bool EditorBaseOptions::GetShowCaptureMode() const
{
	return mShowCaptureMode;
}

void EditorBaseOptions::SetTestClickPoint(const Point& point)
{
	if (mTestClickPoint != point)
	{
		mTestClickPoint = point;
		signal_data();
	}
}

const Point& EditorBaseOptions::GetTestClickPoint() const
{
	return mTestClickPoint;
}

