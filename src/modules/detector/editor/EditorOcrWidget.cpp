#include "EditorOcrWidget.h"

#include <wx/spinctrl.h>
#include <wx/dcbuffer.h>

#include <opencv2/imgproc.hpp>

#include "Ocr.h"
#include "NumberSaver.h"

EditorOcrWidget::EditorOcrWidget(wxWindow* parent, std::shared_ptr<Ocr> ocr, std::shared_ptr<NumberSaver> number_saver)
	: wxPanel(parent, wxID_ANY), mOcr(ocr), mNumberSaver(number_saver)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);
	vsizer_owner->AddSpacer(3);
	{
		auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this);

		vsizer_block->GetStaticBox()->SetForegroundColour(wxColor(0, 170, 0));
		vsizer_block->GetStaticBox()->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		vsizer_block->AddSpacer(5);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(260, -1));
				element->SetBackgroundStyle(wxBG_STYLE_PAINT);
				element->Bind(wxEVT_PAINT, &EditorOcrWidget::RefreshCaptureUnknown, this);
				mNumberRender = element;
				hsizer_block->Add(element, 0, wxEXPAND);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxButton(this, wxID_ANY, _("Apply"), wxDefaultPosition, wxSize(160, 25));
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
				{
					if (!mNumberPath.empty())
						if (auto ocr = mOcr)
						{
							auto number = mNumberValue->GetValue();
							ocr->AddKnownImage(mNumberPath, number);
							if (number != ocr->Recognize(mNumberImage))
								if (auto failed = ocr->GetFailedTest())
									ocr->AddTrain(*failed);
						}
					UpdateUnknownNumber();
				});
				mNumberButton = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, 1);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxButton(this, wxID_ANY, _("Remove"), wxDefaultPosition, wxSize(160, 25));
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
				{
					try
					{
						if (!mNumberPath.empty())
						{
							std::filesystem::remove(mNumberPath);
							UpdateUnknownNumber();
						}
					}
					catch (const std::exception&)
					{
						// TODO: log
					}
				});
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, 1);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxButton(this, wxID_ANY, _("Train"), wxDefaultPosition, wxSize(160, 25));
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
				{
					mTestLabel->SetLabel("");
					if (auto ocr = mOcr)
						if (auto failed = ocr->GetFailedTest())
						{
							ocr->AddTrain(*failed);
							mTestLabel->SetLabel("Failed");
						}
						else
						{
							mTestLabel->SetLabel("Ok");
						}
				});
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, 1);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		vsizer_block->AddSpacer(5);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(260, -1));
				element->SetBackgroundStyle(wxBG_STYLE_PAINT);
				element->Bind(wxEVT_PAINT, &EditorOcrWidget::RefreshCaptureRecognized, this);
				mNumberLabel = element;
				hsizer_block->Add(element, 0, wxEXPAND);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxSize(158, -1));
				element->SetMin(0);
				element->SetMax(100000000);
				mNumberValue = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 1);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxStaticText(this, wxID_ANY, "", wxDefaultPosition, wxSize(260, -1));
				mTestLabel = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 1);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		vsizer_block->AddSpacer(5);
		{
			auto element = new wxCheckBox(this, wxID_ANY, _("Enable saving screenshots with numbers"));
			element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
				{
					mNumberSaver->SetEnable(element->GetValue());
				});
			mNumberSaverCheck = element;
			vsizer_block->Add(element, 0);
		}
		vsizer_owner->Add(vsizer_block, 1, wxEXPAND | wxLEFT | wxRIGHT | wxDOWN, 5);
	}
	SetSizer(vsizer_owner);

	mConnectionNumberSaver = mNumberSaver->signal_data.connect(&EditorOcrWidget::UpdateNumberSaver, this);

	UpdateNumberSaver();
}

void EditorOcrWidget::RefreshCaptureUnknown(wxPaintEvent& evt)
{
	wxBufferedPaintDC dc(mNumberRender); // ������ � ������� wxEVT_PAINT

	if (!mNumberPath.empty() && !mNumberImage.empty())
	{
		cv::Mat bgr_img;
		cv::cvtColor(mNumberImage, bgr_img, cv::COLOR_RGB2BGR);
		wxImage img(bgr_img.cols, bgr_img.rows, bgr_img.data, true);

		int pan_w = 0, pan_h = 0;
		mNumberRender->GetSize(&pan_w, &pan_h);
		if (pan_w * pan_h != 0)
		{
			dc.DrawBitmap(img.Scale(pan_w, pan_h), wxPoint(0, 0));
			return;
		}
	}

	dc.SetBackground(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	dc.Clear();
}

void EditorOcrWidget::RefreshCaptureRecognized(wxPaintEvent& evt)
{
	wxBufferedPaintDC dc(mNumberLabel); // ������ � ������� wxEVT_PAINT

	if (!mLabelImage.empty())
	{
		cv::Mat bgr_img;
		cv::cvtColor(mLabelImage, bgr_img, cv::COLOR_RGB2BGR);
		wxImage img(bgr_img.cols, bgr_img.rows, bgr_img.data, true);

		int pan_w = 0, pan_h = 0;
		mNumberLabel->GetSize(&pan_w, &pan_h);
		if (pan_w * pan_h != 0)
		{
			dc.DrawBitmap(img.Scale(pan_w, pan_h), wxPoint(0, 0));
			return;
		}
	}

	dc.SetBackground(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	dc.Clear();
}

void EditorOcrWidget::UpdateUnknownNumber()
{
	mNumberPath.clear();
	mNumberImage = cv::Mat();
	mLabelImage = cv::Mat();
	mNumberValue->SetValue(0);
	if (auto ocr = mOcr)
	{
		std::tie(mNumberPath, mNumberImage) = ocr->GetUnknownImage();
		auto number = ocr->Recognize(mNumberImage);
		mLabelImage = ocr->Generate(mNumberImage);
		mNumberValue->SetValue(number);
		std::string number_str;
		{
			std::stringstream ss;
			ss.imbue(std::locale(""));
			ss << std::fixed << number;
			number_str = ss.str();
		}
	}
	mNumberRender->Refresh();
	mNumberLabel->Refresh();
}

void EditorOcrWidget::UpdateNumberSaver()
{
	mNumberSaverCheck->SetValue(mNumberSaver->GetEnable());
}
