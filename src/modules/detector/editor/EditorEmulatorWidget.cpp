#include "EditorEmulatorWidget.h"

#include <wx/spinctrl.h>
#include <wx/dcbuffer.h>
#include <filesystem>
#include <opencv2/imgproc.hpp>

#include "utility/CaptureSize.h"

#include "Detector.h"
#include "Emulator.h"
#include "ScreenshotterComponent.h"
#include "EditorBaseOptions.h"
#include "Controller.h"
#include "Scanner.h"
#include "EmulatorType.h"
#include "BlockButton.h"
#include "BlockLifeTime.h"


//https://wiki.wxwidgets.org/Drawing_on_a_panel_with_a_DC

namespace
{
	static const auto ErrorColor = wxColor(170, 0, 0);
	static const auto InfoColor = wxColor(0, 170, 0);
	static const auto WarningColor = wxColor(0, 0, 170);
}

EditorEmulatorWidget::EditorEmulatorWidget(wxWindow* parent, std::shared_ptr<Emulator> emulator, std::shared_ptr<Detector> detector, std::shared_ptr<EditorBaseOptions> base_options, std::shared_ptr<ScreenshotterComponent> screenshotter)
	: wxScrolledWindow(parent, wxID_ANY), mEmulator(emulator), mDetector(detector), mEditorBaseOptions(base_options), mScreenshotter(screenshotter)
{
	SetScrollbars(0, 10, 0, 100, 0, 0);

	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);
	vsizer_owner->AddSpacer(3);
	{
		auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this);

		vsizer_block->GetStaticBox()->SetForegroundColour(wxColor(0, 170, 0));
		vsizer_block->GetStaticBox()->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		
		{
			auto hsizer_column = new wxBoxSizer(wxHORIZONTAL);
			{
				auto vsizer_column = new wxBoxSizer(wxVERTICAL);
				vsizer_column->AddSpacer(5);
				{
					auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
					{
						auto element = new wxStaticText(this, wxID_ANY, _("Emulator type and number"), wxDefaultPosition, wxSize(250, -1));
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						wxArrayString elements;
						for (size_t i = 0; i < static_cast<size_t>(EmulatorType::MAX_COUNT); ++i)
							elements.push_back(EmulatorTypeToDefaultWindowName(static_cast<EmulatorType>(i)));

						auto element = new wxComboBox(this, wxID_ANY, elements.front(), wxDefaultPosition, wxSize(158, 23), elements);
						element->Bind(wxEVT_COMBOBOX, [this, element](wxCommandEvent& event)
							{
								mEmulator->SetType(static_cast<EmulatorType>(element->GetSelection()));
							});
						element->SetSelection(0);
						mEmulatorType = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 1);
					}
					hsizer_block->AddSpacer(5);
					{
						wxArrayString elements;
						for (size_t i = 0; i < 20; ++i)
							elements.push_back(std::to_string(i));

						auto element = new wxComboBox(this, wxID_ANY, elements.front(), wxDefaultPosition, wxSize(158, 23), elements);
						element->Bind(wxEVT_COMBOBOX, [this, element](wxCommandEvent& event)
							{
								mEmulator->SetInstance(element->GetSelection());
							});
						element->SetSelection(0);
						mEmulatorId = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 1);
					}
					vsizer_column->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
				}
				vsizer_column->AddSpacer(5);
				{
					auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
					{
						auto element = new wxStaticText(this, wxID_ANY, _("Emulator name"), wxDefaultPosition, wxSize(250, -1));
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxSize(158, 23));
						element->Bind(wxEVT_TEXT, [this, element](wxCommandEvent& event)
							{
								mEmulator->SetWindowName(element->GetValue().utf8_string());
							});
						mEmulatorName = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 1);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Fix name"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								auto name = Controller(mEmulator).FindWindowTitle();
								if (!name.empty())
									mEmulator->SetWindowName(name);
							});
						mFixEmulatorName = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					vsizer_column->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
				}
				vsizer_column->AddSpacer(5);
				{
					auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
					{
						auto panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(250, -1));
						auto hsizer_panel = new wxBoxSizer(wxHORIZONTAL);
						{
							auto element = new wxStaticText(panel, wxID_ANY, _("Status"));
							//element->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
							hsizer_panel->Add(element, 0, wxALIGN_CENTER_VERTICAL);
						}
						hsizer_panel->AddSpacer(5);
						{
							auto element = new wxStaticText(panel, wxID_ANY, "");
							element->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
							mEmulatorStatus = element;
							hsizer_panel->Add(element, 0, wxALIGN_CENTER_VERTICAL);
						}
						panel->SetSizer(hsizer_panel);
						hsizer_block->Add(panel, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Fix size"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								Scanner(mEmulator).SetValidSize();
							});
						mFixEmulatorSize = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Expand"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								Controller(mEmulator).ShowWindow();
							});
						mExpandEmulator = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					vsizer_column->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
				}
				vsizer_column->AddSpacer(5);
				{
					auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
					{
						auto element = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(250, -1));
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Run emulator"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								Controller(mEmulator).Start();
							});
						mRunEmulator = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Stop emulator"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								Controller(mEmulator).Stop();
							});
						mStopEmulator = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					vsizer_column->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
				}
				vsizer_column->AddSpacer(5);
				{
					auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
					{
						auto element = new wxCheckBox(this, wxID_ANY, _("Show screen capture"), wxDefaultPosition, wxSize(250, 23));
						element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
							{
								mEditorBaseOptions->SetShowCaptureMode(element->GetValue());
							});
						mShowCapture = element;
						hsizer_block->Add(element, 0);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Test click"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								auto editor_settings = mEditorBaseOptions;
						Controller(mEmulator).Click(editor_settings->GetTestClickPoint().x, editor_settings->GetTestClickPoint().y);
							});
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Test esc"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								Controller(mEmulator).PressEscape();
							});
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					vsizer_column->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
				}
				vsizer_column->AddSpacer(5);
				{
					auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
					{
						auto element = new wxCheckBox(this, wxID_ANY, _("Background mode"), wxDefaultPosition, wxSize(250, 23));
						element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
							{
								mEmulator->SetBackgroundMode(element->GetValue());
							});
						mBackgroundMode = element;
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					hsizer_block->AddSpacer(5);
					{
						auto element = new wxButton(this, wxID_ANY, _("Take screenshot"), wxDefaultPosition, wxSize(160, 25));
						element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
							{
								mScreenshotter->CreateScreenshot();
							});
						hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
					}
					vsizer_column->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
				}

				hsizer_column->Add(vsizer_column, 0, wxEXPAND);
			}
			hsizer_column->AddSpacer(10);
			{
				{
					auto vsizer_el = new wxBoxSizer(wxVERTICAL);
					vsizer_el->AddSpacer(5);
					{
						auto element = new wxStaticText(this, wxID_ANY, _("Page state:"));
						vsizer_el->Add(element, 0);
					}
					vsizer_el->AddSpacer(5);
					{
						auto element = new wxStaticText(this, wxID_ANY, "");
						element->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "Courier New"));
						mCurrentPageInfo = element;
						vsizer_el->Add(element, 0);
					}
					vsizer_el->AddSpacer(5);
					hsizer_column->Add(vsizer_el, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
				}
			}
			vsizer_block->Add(hsizer_column, 0, wxEXPAND);
		}
		vsizer_block->AddSpacer(5);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxStaticText(this, wxID_ANY, _("Detector fps"), wxDefaultPosition, wxSize(250, -1));
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxSize(158, -1));
				element->Bind(wxEVT_SPINCTRL, [this, element](wxCommandEvent& evt)
					{
						mDetector->SetFps(element->GetValue());
					});
				mDetectorFps = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 1);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		vsizer_block->AddSpacer(5);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxStaticText(this, wxID_ANY, "", wxDefaultPosition, wxSize(250, -1));
				element->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				mEmulatorPathInfo = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxButton(this, wxID_ANY, _("Fix path"), wxDefaultPosition, wxSize(160, 25));
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
					{
						auto new_path = Controller(mEmulator).FindWindowPath();
				if (!new_path.empty())
					mEmulator->SetPath(new_path);
					});
				mFixEmulatorPath = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, 1);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxSize(-1, 23));
				element->Bind(wxEVT_TEXT, [this, element](wxCommandEvent& event)
					{
						mEmulator->SetPath(element->GetValue().utf8_string());
					});
				mEmulatorPath = element;
				hsizer_block->Add(element, 1, wxALIGN_CENTER_VERTICAL);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		vsizer_block->AddSpacer(5);
		vsizer_owner->Add(vsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 5);
	}
	vsizer_owner->AddSpacer(3);
	{
		auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this);
		{
			auto dc_panel = new wxPanel(this);
			dc_panel->SetBackgroundStyle(wxBG_STYLE_PAINT);
			dc_panel->Bind(wxEVT_PAINT, &EditorEmulatorWidget::RefreshCapture, this);
			dc_panel->Bind(wxEVT_LEFT_UP, [this, dc_panel](wxMouseEvent& evt)
				{
					const auto pos = evt.GetPosition();
					const auto pan_size = dc_panel->GetSize();
					const auto cap_size = utility::GetCaptureSize();
					cv::Vec2d cs(cap_size.width, cap_size.height);
					cv::Vec2d ps(pan_size.GetWidth(), pan_size.GetHeight());
					cv::Vec2d scale;
					cv::Vec2d cap_pos(pos.x, pos.y);
					cv::divide(cs, ps, scale);
					cv::multiply(cap_pos, scale, cap_pos);
					cv::Vec2i cp = cap_pos;

					Controller(mEmulator).Click(cp[0], cp[1]);
				});
			mRenderPanel = dc_panel;
			vsizer_block->Add(dc_panel, 1, wxEXPAND);
		}
		vsizer_owner->Add(vsizer_block, 1, wxEXPAND | wxLEFT | wxRIGHT | wxDOWN, 5);
	}
	SetSizer(vsizer_owner);
	
	mConnectionDetectorData = mDetector->signal_data.connect(&EditorEmulatorWidget::UpdateDetectorData, this);
	mConnectionDetector = mDetector->signal_detect.connect(&EditorEmulatorWidget::UpdateDetectorDetect, this);
	mConnectionEmulator = mEmulator->signal_data.connect(&EditorEmulatorWidget::UpdateEmulator, this);
	mConnectionEditorSettings = mEditorBaseOptions->signal_data.connect(&EditorEmulatorWidget::UpdateEditorSettings, this);

	UpdateDetectorData();
	UpdateDetectorDetect();
	UpdateEmulator();
	UpdateEditorSettings();
}

void EditorEmulatorWidget::UpdateDetectorData()
{
	mDetectorFps->SetValue(mDetector->GetFps());
}

void EditorEmulatorWidget::UpdateDetectorDetect()
{
	auto detector = mDetector;

	{
		std::string text;
		const auto& page = detector->GetPage();
		text += "page: " + page.FullName() + "\n";
		page.Foreach([&text](const IPageComponent& block)
			{
				if (block.GetType() == std::string_view(BlockButton::StaticType()))
					return false;
				if (block.GetType() == std::string_view(BlockLifeTime::StaticType()))
					return false;
				text += (std::string("[") + block.GetType() + " " + block.GetName() + "] " + block.ToString() + "\n");
				return false;
			});
		mCurrentPageInfo->SetLabel(text);
	}

	mRenderPanel->Refresh();

	auto status = detector->GetStatus();
	mEmulatorStatus->SetLabel("[" + _(DetectorStatusDescription(status)) + "]");
	switch (status)
	{
	case DetectorStatus::NotLoaded:
		mEmulatorStatus->SetForegroundColour(WarningColor);
		break;
	case DetectorStatus::NotFound:
	case DetectorStatus::ScanError:
	case DetectorStatus::SizeError:
	case DetectorStatus::Minimized:
	case DetectorStatus::Unknown:
		mEmulatorStatus->SetForegroundColour(ErrorColor);
		break;
	case DetectorStatus::NoError:
		mEmulatorStatus->SetForegroundColour(InfoColor);
		break;
	default:
		break;
	}
	mEmulatorStatus->Refresh();

	mRunEmulator->Enable(status == DetectorStatus::NotFound && Controller(mEmulator).CheckPath());
	mStopEmulator->Enable(status != DetectorStatus::NotFound);
	mExpandEmulator->Enable(status == DetectorStatus::Minimized);
	mFixEmulatorSize->Enable(status == DetectorStatus::SizeError && Scanner(mEmulator).IsResizable());
	mFixEmulatorName->Enable(status == DetectorStatus::NotFound);
}

void EditorEmulatorWidget::UpdateEmulator()
{
	mBackgroundMode->SetValue(mEmulator->GetBackgroundMode());
	mEmulatorType->SetSelection(static_cast<size_t>(mEmulator->GetType()));
	mEmulatorId->SetSelection(mEmulator->GetInstance());
	mEmulatorName->ChangeValue(wxString::FromUTF8(mEmulator->GetWindowName()));
	mEmulatorPath->ChangeValue(wxString::FromUTF8(mEmulator->GetPath()));

	if (Controller(mEmulator).CheckPath())
	{
		mEmulatorPathInfo->SetLabel(_("Correct emulator path"));
		mEmulatorPathInfo->SetForegroundColour(InfoColor);
		mFixEmulatorPath->Enable(false);
	}
	else
	{
		mEmulatorPathInfo->SetLabel(_("Wrong emulator path"));
		mEmulatorPathInfo->SetForegroundColour(ErrorColor);
		mFixEmulatorPath->Enable(true);
	}
	mEmulatorPathInfo->Refresh();

	mRunEmulator->Enable(mDetector->GetStatus() == DetectorStatus::NotFound && Controller(mEmulator).CheckPath());
}

void EditorEmulatorWidget::UpdateEditorSettings()
{
	mShowCapture->SetValue(mEditorBaseOptions->GetShowCaptureMode());
	mRenderPanel->Refresh();
}

void EditorEmulatorWidget::RefreshCapture(wxPaintEvent& evt)
{
	wxBufferedPaintDC dc(mRenderPanel); // ������ � ������� wxEVT_PAINT
	if (mEditorBaseOptions->GetShowCaptureMode())
	{
		cv::Mat bgr_img;
		cv::cvtColor(mDetector->GetImage(), bgr_img, cv::COLOR_RGB2BGR);
		wxImage img(bgr_img.cols, bgr_img.rows, bgr_img.data, true);

		int pan_w = 0, pan_h = 0;
		mRenderPanel->GetSize(&pan_w, &pan_h);
		dc.DrawBitmap(img.Scale(pan_w, pan_h), wxPoint(0, 0));
	}
	else
	{
		dc.SetBackground(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
		dc.Clear();
	}
}
