#pragma once

#include <memory>

#include "IEditorPageBuilder.h"
#include "ActorComponent.h"

class Emulator;
class Detector;
class EditorBaseOptions;
class ScreenshotterComponent;
class NumberSaver;
class Ocr;

class EditorDetectorBuilder : public ActorComponent, public IEditorPageBuilder
{
public:
		EditorDetectorBuilder(
		std::shared_ptr<Emulator> emulator,
		std::shared_ptr<Detector> detector,
		std::shared_ptr<EditorBaseOptions> options,
		std::shared_ptr<ScreenshotterComponent> screenshotter,
		std::shared_ptr<NumberSaver> number_saver,
		std::shared_ptr<Ocr> ocr
	) : 
		mEmulator(emulator),
		mDetector(detector),
		mEditorBaseOptions(options),
		mScreenshotterComponent(screenshotter),
		mNumberSaver(number_saver),
		mOcr(ocr)
	{	}

	virtual void AddPages(wxNotebook* notebook) override;

private:
	std::shared_ptr<Emulator> mEmulator;
	std::shared_ptr<Detector> mDetector;
	std::shared_ptr<EditorBaseOptions> mEditorBaseOptions;
	std::shared_ptr<ScreenshotterComponent> mScreenshotterComponent;
	std::shared_ptr<NumberSaver> mNumberSaver;
	std::shared_ptr<Ocr> mOcr;
};


