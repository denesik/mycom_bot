#include "EditorSettingsWidget.h"

#include <wx/spinctrl.h>

#include "AutoRunner.h"


EditorSettingsWidget::EditorSettingsWidget(wxWindow* parent, std::shared_ptr<AutoRunner> auto_runner)
	: wxPanel(parent, wxID_ANY), mAutoRunner(auto_runner)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);
	vsizer_owner->AddSpacer(3);
	{
		auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this);

		vsizer_block->GetStaticBox()->SetForegroundColour(wxColor(0, 170, 0));
		vsizer_block->GetStaticBox()->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		vsizer_block->AddSpacer(5);
		{
			auto element = new wxCheckBox(this, wxID_ANY, _("Autorun"), wxDefaultPosition, wxSize(-1, 23));
			element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
				{
					mAutoRunner->SetAutorunMode(element->GetValue());
				});
			mAutoRun = element;
			vsizer_block->Add(element, 0);
		}
		vsizer_block->AddSpacer(5);
		{
			auto element = new wxCheckBox(this, wxID_ANY, _("Close emulator on shutdown"), wxDefaultPosition, wxSize(-1, 23));
			element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
				{
					mAutoRunner->SetStopEmulatorMode(element->GetValue());
				});
			mStopEmulatorMode = element;
			vsizer_block->Add(element, 0);
		}
		vsizer_owner->Add(vsizer_block, 1, wxEXPAND | wxLEFT | wxRIGHT | wxDOWN, 5);
	}
	SetSizer(vsizer_owner);

	mConnectionAutoCloseEmulator = mAutoRunner->signal_data.connect(&EditorSettingsWidget::UpdateAutoCloseEmulator, this);

	UpdateAutoCloseEmulator();
}

void EditorSettingsWidget::UpdateAutoCloseEmulator()
{
	mAutoRun->SetValue(mAutoRunner->GetAutorunMode());
	mStopEmulatorMode->SetValue(mAutoRunner->GetStopEmulatorMode());
}

