#pragma once

#include <sigslot/signal.hpp>

#include "ActorComponent.h"
#include "Point.h"

class EditorBaseOptions : public ActorComponent
{
public:
	EditorBaseOptions() = default;

	sigslot::signal<> signal_data;

	void SetShowCaptureMode(bool value);
	bool GetShowCaptureMode() const;

	void SetTestClickPoint(const Point& point);
	const Point& GetTestClickPoint() const;

private:
	Point mTestClickPoint;
	bool mShowCaptureMode = false;

};
