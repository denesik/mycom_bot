#include "EditorBaseOptionsSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "EditorBaseOptions.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

#undef GetObject

#include "JsonSerializerSection.h"
#include "RectJson.h"
#include "PointJson.h"

EditorBaseOptionsSettings::EditorBaseOptionsSettings(std::shared_ptr<EditorBaseOptions> editor, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mEditor(editor), mCorePath(core_path), mUserPath(user_path)
{

}

void EditorBaseOptionsSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void EditorBaseOptionsSettings::BeginPlay()
{
	mEditorConnection = mEditor->signal_data.connect([this]() { Save(mUserPath); });
}

void EditorBaseOptionsSettings::EndPlay()
{
	mEditorConnection.disconnect();
}

void EditorBaseOptionsSettings::Deinitialize()
{
	Save(mUserPath);
}

void EditorBaseOptionsSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		Point point = { -1, -1 };
		JsonSerializerSection<Point>("test_click").Serialize(json, point);
		if (point != Point{ -1, -1 }) // HACK!!! TODO FIX
			mEditor->SetTestClickPoint(point);
	}
	{
		auto it = json.FindMember("show_capture_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			mEditor->SetShowCaptureMode(it->value.GetBool());
	}
}

void EditorBaseOptionsSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("show_capture_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = mEditor->GetShowCaptureMode();
		else
			json.AddMember("show_capture_mode", mEditor->GetShowCaptureMode(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


