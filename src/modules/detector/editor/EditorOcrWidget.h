#pragma once

#include <wx/wxprec.h>
#include <opencv2/core/mat.hpp>
#include <filesystem>

#include <sigslot/signal.hpp>

class Ocr;
class NumberSaver;

class EditorOcrWidget : public wxPanel
{
public:
	EditorOcrWidget(wxWindow *parent, std::shared_ptr<Ocr> ocr, std::shared_ptr<NumberSaver> number_saver);

private:
	void RefreshCaptureUnknown(wxPaintEvent& evt);
	void RefreshCaptureRecognized(wxPaintEvent& evt);
	void UpdateUnknownNumber();
	void UpdateNumberSaver();

private:
	std::shared_ptr<Ocr> mOcr;
	std::shared_ptr<NumberSaver> mNumberSaver;

	wxPanel* mNumberRender = nullptr;
	class wxSpinCtrl* mNumberValue = nullptr;
	wxPanel *mNumberLabel = nullptr;
	wxButton* mNumberButton = nullptr;
	std::filesystem::path mNumberPath;
	cv::Mat mNumberImage;
	cv::Mat mLabelImage;

	wxStaticText *mTestLabel = nullptr;
	class wxCheckBox* mNumberSaverCheck = nullptr;
	
	sigslot::scoped_connection mConnectionNumberSaver;
};

