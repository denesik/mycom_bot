#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>

class Emulator;
class Detector;
class EditorBaseOptions;
class ScreenshotterComponent;

class EditorEmulatorWidget : public wxScrolledWindow
{
public:
	EditorEmulatorWidget(wxWindow *parent, 
		std::shared_ptr<Emulator> emulator, 
		std::shared_ptr<Detector> detector, 
		std::shared_ptr<EditorBaseOptions> base_options, 
		std::shared_ptr<ScreenshotterComponent> screenshotter);

private:
	void UpdateDetectorData();
	void UpdateDetectorDetect();
	void UpdateEmulator();
	void UpdateEditorSettings();

	void RefreshCapture(wxPaintEvent& evt);

private:
	std::shared_ptr<Emulator> mEmulator;
	std::shared_ptr<Detector> mDetector;
	std::shared_ptr<EditorBaseOptions> mEditorBaseOptions;
	std::shared_ptr<ScreenshotterComponent> mScreenshotter;

	wxPanel* mRenderPanel = nullptr;
	wxComboBox* mEmulatorId = nullptr;
	wxComboBox* mEmulatorType = nullptr;
	wxTextCtrl* mEmulatorName = nullptr;
	wxStaticText* mEmulatorStatus = nullptr;
	wxTextCtrl *mEmulatorPath = nullptr;
	wxStaticText* mEmulatorPathInfo = nullptr;
	wxButton* mFixEmulatorPath = nullptr;
	wxButton* mFixEmulatorName = nullptr;
	wxButton* mRunEmulator = nullptr;
	wxButton* mStopEmulator = nullptr;
	wxButton* mExpandEmulator = nullptr;
	wxButton* mFixEmulatorSize = nullptr;
	wxCheckBox *mBackgroundMode = nullptr;
	wxCheckBox *mShowCapture = nullptr;
	wxStaticText* mCurrentPageInfo = nullptr;
	class wxSpinCtrl* mDetectorFps = nullptr;
	
	sigslot::scoped_connection mConnectionDetectorData;
	sigslot::scoped_connection mConnectionDetector;
	sigslot::scoped_connection mConnectionEmulator;
	sigslot::scoped_connection mConnectionEditorSettings;
};


