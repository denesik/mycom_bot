#include <wx/wxprec.h>
#include <wx/notebook.h>

#include "EditorDetectorBuilder.h"

#include "EditorEmulatorWidget.h"
#include "EditorOcrWidget.h"

#include "Actor.h"

void EditorDetectorBuilder::AddPages(wxNotebook* notebook)
{
	notebook->AddPage(new EditorEmulatorWidget(notebook, mEmulator, mDetector, mEditorBaseOptions, mScreenshotterComponent), _("Emulator"));
	notebook->AddPage(new EditorOcrWidget(notebook, mOcr, mNumberSaver), _("OCR"));
}
