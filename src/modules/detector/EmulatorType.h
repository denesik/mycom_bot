#pragma once

enum class EmulatorType
{
	BLUESTACKS,
	MEMU,
	NOX,
	UNKNOWN_TYPE,

	MAX_COUNT = UNKNOWN_TYPE,
};

const char* EmulatorTypeToName(EmulatorType type);
EmulatorType EmulatorTypeFromName(const char* name);

const char* EmulatorTypeToExe(EmulatorType type);

const char* EmulatorTypeToDefaultWindowName(EmulatorType type);

