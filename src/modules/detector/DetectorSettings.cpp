#include "DetectorSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "Detector.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

DetectorSettings::DetectorSettings(std::shared_ptr<Detector> detector, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mDetector(detector), mCorePath(core_path), mUserPath(user_path)
{

}

void DetectorSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void DetectorSettings::BeginPlay()
{
	mDetectorConnection = mDetector->signal_data.connect([this]() { Save(mUserPath); });
}

void DetectorSettings::EndPlay()
{
	mDetectorConnection.disconnect();
}

void DetectorSettings::Deinitialize()
{
	Save(mUserPath);
}

void DetectorSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("detector_fps");
		if (it != json.MemberEnd() && it->value.IsInt())
			mDetector->SetFps(it->value.GetInt());
	}
}

void DetectorSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("detector_fps");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = mDetector->GetFps();
		else
			json.AddMember("detector_fps", mDetector->GetFps(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


