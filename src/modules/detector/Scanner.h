#pragma once

#include <array>
#include <memory>

#include "EmulatorType.h"
#include "IEmulatorScanner.h"

class Emulator;

class Scanner 
{
public:
	Scanner(std::shared_ptr<const Emulator> emulator);

	ScanningStatus Capture(cv::Mat &img) const;

	bool SetValidSize() const;

	bool IsValidSize() const;

	bool IsResizable() const;

private:
	std::shared_ptr<const Emulator> mEmulator;

	std::array<std::unique_ptr<IEmulatorScanner>, static_cast<size_t>(EmulatorType::UNKNOWN_TYPE)> mScanners;
};


