#include "Emulator.h"


void Emulator::SetType(EmulatorType type)
{
	bool changed = false;

	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mType != type)
		{
			mType = type;
			changed = true;
		}
	}

	if (changed)
		signal_data();
}

void Emulator::SetApplication(std::string_view application)
{
	bool changed = false;

	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mApplication != application)
		{
			mApplication = application;
			changed = true;
		}
	}

	if (changed)
		signal_data();
}

void Emulator::SetWindowName(std::string_view name)
{
	bool changed = false;

	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mName != name)
		{
			mName = name;
			changed = true;
		}
	}

	if (changed)
		signal_data();
}

void Emulator::SetInstance(unsigned int instance)
{
	bool changed = false;

	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mInstance != instance)
		{
			mInstance = instance;
			changed = true;
		}
	}

	if (changed)
		signal_data();
}

void Emulator::SetPath(std::string_view path)
{
	bool changed = false;

	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mPath != path)
		{
			mPath = path;
			changed = true;
		}
	}

	if (changed)
		signal_data();
}

void Emulator::SetBackgroundMode(bool value)
{
	bool changed = false;

	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mBackground != value)
		{
			mBackground = value;
			changed = true;
		}
	}

	if (changed)
		signal_data();
}

EmulatorType Emulator::GetType() const
{
	std::lock_guard<std::mutex> lock(mMutex);
	return mType;
}

std::string Emulator::GetApplication() const
{
	std::lock_guard<std::mutex> lock(mMutex);
	return mApplication;
}

std::string Emulator::GetWindowName() const
{
	std::lock_guard<std::mutex> lock(mMutex);
	return mName;
}

unsigned int Emulator::GetInstance() const
{
	std::lock_guard<std::mutex> lock(mMutex);
	return mInstance;
}

std::string Emulator::GetPath() const
{
	std::lock_guard<std::mutex> lock(mMutex);
	return mPath;
}

bool Emulator::GetBackgroundMode() const
{
	std::lock_guard<std::mutex> lock(mMutex);
	return mBackground;
}
