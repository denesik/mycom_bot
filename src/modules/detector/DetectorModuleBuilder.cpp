#include "DetectorModuleBuilder.h"

#include "Actor.h"
#include "Profile.h"

#include "Emulator.h"
#include "Book.h"
#include "Detector.h"
#include "ScreenshotterComponent.h"
#include "EmulatorSettings.h"
#include "DetectorSettings.h"
#include "EditorBaseOptions.h"
#include "EditorBaseOptionsSettings.h"
#include "NumberSaver.h"
#include "NumberSaverSettings.h"

#include "Ocr.h"
#include "EditorDetectorBuilder.h"

void DetectorModuleBuilder::AddComponents(const Profile& profile, Actor* actor)
{
	auto ocr = actor->CreateComponent<Ocr>(profile.FindPath("ocr_unknown"), profile.FindPath("ocr_test"), profile.FindPath("ocr_train"), profile.FindPath("ocr_digits"));

	auto emulator = actor->CreateComponent<Emulator>();
	auto book = actor->CreateComponent<Book>(PageMap(ocr), profile.FindPath("pages_dir"));
	auto detector = actor->CreateComponent<Detector>(book, emulator);

	auto screenshotter = actor->CreateComponent<ScreenshotterComponent>(emulator, profile.FindPath("screenshots_dir"));
	
	actor->CreateComponent<EmulatorSettings>(emulator, profile.FindPath("emulator_core_file"), profile.FindPath("emulator_user_file"));
	actor->CreateComponent<DetectorSettings>(detector, profile.FindPath("core_settings_core_file"), profile.FindPath("core_settings_user_file"));
	
	auto editor_base_options = actor->CreateComponent<EditorBaseOptions>();
	actor->CreateComponent<EditorBaseOptionsSettings>(editor_base_options, profile.FindPath("editor_core_file"), profile.FindPath("editor_user_file"));

	auto number_saver = actor->CreateComponent<NumberSaver>(detector, profile.FindPath("ocr_unknown"));
	actor->CreateComponent<NumberSaverSettings>(number_saver, profile.FindPath("core_settings_core_file"), profile.FindPath("core_settings_user_file"));
	
	actor->CreateComponent<EditorDetectorBuilder>(emulator, detector, editor_base_options, screenshotter, number_saver, ocr);
}

