#pragma once

#include <string>

#include "EmulatorType.h"

namespace EmulatorUtility
{

	std::string GetExeNameFromWindowHandle(void *handle);

	bool CheckEmulatorTypePath(std::string_view path, EmulatorType type);

}



