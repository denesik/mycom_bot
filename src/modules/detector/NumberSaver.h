#pragma once

#include <string>
#include <filesystem>
#include <sigslot/signal.hpp>

#include "Timer.h"
#include "ActorComponent.h"

class Detector;

class NumberSaver : public ActorComponent
{
public:
	NumberSaver(std::shared_ptr<Detector> detector, const std::filesystem::path & path);

	sigslot::signal<> signal_data;

	void SetEnable(bool value);
	bool GetEnable() const;

protected:
	virtual void BeginPlay() override;

	virtual void Tick() override;

	virtual void EndPlay() override;

private:
	const std::filesystem::path mPath;
	bool mEnable = false;

	std::shared_ptr<Detector> mDetector;
	sigslot::connection mConnectionDetect;

	std::string mLastPageName;
	bool mSaved = false;
	Timer mTimer;
};

