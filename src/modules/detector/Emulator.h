#pragma once

#include "Controller.h"
#include "Scanner.h"
#include "ActorComponent.h"

#include <mutex>
#include <sigslot/signal.hpp>

class Emulator : public ActorComponent
{
public:
	Emulator() = default;

	sigslot::signal<> signal_data;

	void SetType(EmulatorType type);

	void SetApplication(std::string_view application);

	void SetWindowName(std::string_view name);

	void SetInstance(unsigned int instance);

	void SetPath(std::string_view path);

	void SetBackgroundMode(bool value);


	EmulatorType GetType() const;

	std::string GetApplication() const;

	std::string GetWindowName() const;

	unsigned int GetInstance() const;

	std::string GetPath() const;

	bool GetBackgroundMode() const;


private:
	mutable std::mutex mMutex;

	EmulatorType mType = EmulatorType::UNKNOWN_TYPE;
	std::string mName;
	bool mBackground = false;
	std::string mApplication;
	unsigned int mInstance = 0;
	std::string mPath;
};
