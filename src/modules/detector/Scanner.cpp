#include "Scanner.h"

#include "NoxScanner.h"
#include "BluestacksScanner.h"
#include "MemuScanner.h"

#include "utf_helper/MultiWideUtf8.h"
#include "utility/CaptureSize.h"

#include "Emulator.h"

Scanner::Scanner(std::shared_ptr<const Emulator> emulator)
	: mEmulator(emulator)
{
	mScanners[static_cast<size_t>(EmulatorType::NOX)] = std::make_unique<NoxScanner>();
	mScanners[static_cast<size_t>(EmulatorType::BLUESTACKS)] = std::make_unique<BluestacksScanner>();
	mScanners[static_cast<size_t>(EmulatorType::MEMU)] = std::make_unique<MemuScanner>();
}

ScanningStatus Scanner::Capture(cv::Mat& img) const
{
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());
	bool background = mEmulator->GetBackgroundMode();
	EmulatorType type = mEmulator->GetType();

	ScanningStatus result = ScanningStatus::UnknownEmulator;
	if (static_cast<size_t>(type) < static_cast<size_t>(EmulatorType::UNKNOWN_TYPE))
	{
		result = mScanners[static_cast<size_t>(type)]->Capture(img, name, background);
		if (result == ScanningStatus::Success)
			return result;
	}

	auto img_size = utility::GetCaptureSize();
	if (img.size() != img_size)
		img.create(img_size.height, img_size.width, CV_8UC3);
	img.setTo(cv::Scalar(0, 0, 0));

	return result;
}

bool Scanner::SetValidSize() const
{
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());
	EmulatorType type = mEmulator->GetType();

	return mScanners[static_cast<size_t>(type)]->SetValidSize(name);
}

bool Scanner::IsValidSize() const
{
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());
	EmulatorType type = mEmulator->GetType();

	return mScanners[static_cast<size_t>(type)]->IsValidSize(name);
}

bool Scanner::IsResizable() const
{
	return mEmulator->GetType() == EmulatorType::BLUESTACKS;
}
