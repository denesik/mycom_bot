#pragma once

#include <string>

enum class DetectorStatus
{
	NotLoaded,
	NotFound,
	ScanError,
	SizeError,
	Minimized,
	NoError,
	Unknown,

	Default = Unknown,
};

const char* DetectorStatusString(DetectorStatus value);
DetectorStatus DetectorStatusString(std::string_view msg);

const char* DetectorStatusDescription(DetectorStatus value);
