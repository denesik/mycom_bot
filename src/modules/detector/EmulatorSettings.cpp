#include "EmulatorSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "Emulator.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

EmulatorSettings::EmulatorSettings(std::shared_ptr<Emulator> emulator, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mEmulator(emulator), mCorePath(core_path), mUserPath(user_path)
{

}

void EmulatorSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void EmulatorSettings::BeginPlay()
{
	mEmulatorConnection = mEmulator->signal_data.connect([this]() { Save(mUserPath); });
}

void EmulatorSettings::EndPlay()
{
	mEmulatorConnection.disconnect();
}

void EmulatorSettings::Deinitialize()
{
	Save(mUserPath);
}

void EmulatorSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("game_application");
		if (it != json.MemberEnd() && it->value.IsString())
			mEmulator->SetApplication(std::string(it->value.GetString(), it->value.GetStringLength()));
	}
	{
		auto it = json.FindMember("background_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			mEmulator->SetBackgroundMode(it->value.GetBool());
	}
	{
		auto it = json.FindMember("emulator_path");
		if (it != json.MemberEnd() && it->value.IsString())
			mEmulator->SetPath(std::string(it->value.GetString(), it->value.GetStringLength()));
	}
	{
		auto it = json.FindMember("emulator_name");
		if (it != json.MemberEnd() && it->value.IsString())
			mEmulator->SetWindowName(std::string(it->value.GetString(), it->value.GetStringLength()));
	}
	{
		auto it = json.FindMember("emulator_type");
		if (it != json.MemberEnd() && it->value.IsString())
			mEmulator->SetType(EmulatorTypeFromName(std::string(it->value.GetString(), it->value.GetStringLength()).c_str()));
	}
	{
		auto it = json.FindMember("emulator_instance");
		if (it != json.MemberEnd() && it->value.IsUint())
			mEmulator->SetInstance(it->value.GetInt());
	}
}

void EmulatorSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("game_application");
		if (it != json.MemberEnd() && it->value.IsString())
			it->value = rapidjson::Value(mEmulator->GetApplication(), json.GetAllocator());
		else
			json.AddMember("game_application", rapidjson::Value(mEmulator->GetApplication(), json.GetAllocator()), json.GetAllocator());
	}
	{
		auto it = json.FindMember("background_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = mEmulator->GetBackgroundMode();
		else
			json.AddMember("background_mode", mEmulator->GetBackgroundMode(), json.GetAllocator());
	}
	{
		auto it = json.FindMember("emulator_path");
		if (it != json.MemberEnd() && it->value.IsString())
			it->value = rapidjson::Value(mEmulator->GetPath(), json.GetAllocator());
		else
			json.AddMember("emulator_path", rapidjson::Value(mEmulator->GetPath(), json.GetAllocator()), json.GetAllocator());
	}
	{
		auto it = json.FindMember("emulator_name");
		if (it != json.MemberEnd() && it->value.IsString())
			it->value = rapidjson::Value(mEmulator->GetWindowName(), json.GetAllocator());
		else
			json.AddMember("emulator_name", rapidjson::Value(mEmulator->GetWindowName(), json.GetAllocator()), json.GetAllocator());
	}
	{
		std::string str = EmulatorTypeToName(mEmulator->GetType());

		auto it = json.FindMember("emulator_type");
		if (it != json.MemberEnd() && it->value.IsString())
			it->value = rapidjson::Value(str, json.GetAllocator());
		else
			json.AddMember("emulator_type", rapidjson::Value(str, json.GetAllocator()), json.GetAllocator());
	}
	{
		auto it = json.FindMember("emulator_instance");
		if (it != json.MemberEnd() && it->value.IsUint())
			it->value = mEmulator->GetInstance();
		else
			json.AddMember("emulator_instance", mEmulator->GetInstance(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


