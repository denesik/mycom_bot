#include "NumberSaver.h"

#include "Detector.h"
#include "Logger.h"
#include "utility/GenerateScreenshotPath.h"
#include "utility/OpencvFilesystem.h"
#include "utf_helper/PathUtf8.h"
#include "BlockNumber.h"

NumberSaver::NumberSaver(std::shared_ptr<Detector> detector, const std::filesystem::path& path)
	: mPath(path), mDetector(detector)
{
	mLastPageName = mDetector->GetPage().FullName();
}

void NumberSaver::SetEnable(bool value)
{
	if (mEnable != value)
	{
		mEnable = value;
		signal_data();
	}
}

bool NumberSaver::GetEnable() const
{
	return mEnable;
}


void NumberSaver::BeginPlay()
{
	mConnectionDetect = mDetector->signal_detect.connect([this]()
		{
			if (mEnable)
			{
				const Page& page = mDetector->GetPage();
				if (mLastPageName != page.FullName())
				{
					mTimer.Start();
					mLastPageName = page.FullName();
					mSaved = false;
				}
			}
		});
}

namespace
{
	void SaveScreenshot(const std::filesystem::path& path, std::string_view prefix, const cv::Mat& img)
	{
		auto file_path = utility::GenerateScreenshotPath(path, prefix);
		if (file_path == "")
		{
			LOG_E("Error creating screenshot");
		}
		else
		{
			if (!std::filesystem::exists(file_path.parent_path()))
				std::filesystem::create_directories(file_path.parent_path());

			auto res = utility::Imwrite(file_path, img);
			if (!res)
				LOG_E("Error creating screenshot " + utf_helper::PathToUtf8(file_path));
		}
	}


}

void NumberSaver::Tick()
{
	if (mEnable)
	{
		const Page& page = mDetector->GetPage();
		if (!mSaved && mTimer.Elapsed() >= 400 && mLastPageName == page.FullName())
		{
			bool has_number = false;
			cv::Rect rect;
			mDetector->GetPage().Foreach([&has_number, &rect](const IPageComponent& component)
				{
					if (component.GetType() == BlockNumber::StaticType())
					{
						if (component.GetName() == "other_power")
						{
							rect = static_cast<const BlockNumber&>(component).GetRect();
							has_number = true;
							return true;
						}
					}
			return false;
				});
			if (has_number)
			{
				const cv::Mat& img = mDetector->GetImage();

				SaveScreenshot(mPath, page.FullName() + "_", cv::Mat(img, rect));
				mSaved = true;
			}
		}
	}
}

void NumberSaver::EndPlay()
{
	mConnectionDetect.disconnect();
}

