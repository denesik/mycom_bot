#include "DetectorStatus.h"

const char* DetectorStatusString(DetectorStatus value)
{
	switch (value)
	{
	case DetectorStatus::NotLoaded:
		return "not_loaded";
	case DetectorStatus::NotFound:
		return "not_found";
	case DetectorStatus::ScanError:
		return "scan_error";
	case DetectorStatus::SizeError:
		return "size_error";
	case DetectorStatus::Minimized:
		return "minimized";
	case DetectorStatus::NoError:
		return "ok";
	case DetectorStatus::Unknown:
		return "unknown";
	default:
		break;
	}

	return "";
}

DetectorStatus DetectorStatusString(std::string_view msg)
{
	if (msg == DetectorStatusString(DetectorStatus::NotLoaded))
		return DetectorStatus::NotLoaded;
	if (msg == DetectorStatusString(DetectorStatus::NotFound))
		return DetectorStatus::NotFound;
	if (msg == DetectorStatusString(DetectorStatus::ScanError))
		return DetectorStatus::ScanError;
	if (msg == DetectorStatusString(DetectorStatus::SizeError))
		return DetectorStatus::SizeError;
	if (msg == DetectorStatusString(DetectorStatus::Minimized))
		return DetectorStatus::Minimized;
	if (msg == DetectorStatusString(DetectorStatus::NoError))
		return DetectorStatus::NoError;
	if (msg == DetectorStatusString(DetectorStatus::Unknown))
		return DetectorStatus::Unknown;

	return DetectorStatus::Default;
}

const char* DetectorStatusDescription(DetectorStatus value)
{
	switch (value)
	{
	case DetectorStatus::NotLoaded:
		return "Data not loaded";
	case DetectorStatus::NotFound:
		return "Emulator not found";
	case DetectorStatus::ScanError:
		return "Scan error";
	case DetectorStatus::SizeError:
		return "Emulator size error";
	case DetectorStatus::Minimized:
		return "Emulator minimized";
	case DetectorStatus::NoError:
		return "Ok";
	case DetectorStatus::Unknown:
		return "Unknown";
	default:
		break;
	}

	return "";
}
