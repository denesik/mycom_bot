#pragma once

#include <mutex>

#include "IEmulatorScanner.h"

class BluestacksScanner : public IEmulatorScanner
{
public:
	BluestacksScanner();

	virtual ScanningStatus Capture(cv::Mat& dst, const std::wstring& name, bool background) override;

	virtual bool SetValidSize(const std::wstring& name) const override;

	virtual bool IsValidSize(const std::wstring& name) const override;

private:
	void *FindWnd(const std::wstring &name) const;

	bool IsValidSize(void* wnd) const;

private:
	const cv::Size mCaptureSize;
	const cv::Size mOffsetLeftTop = { 1, 33 };
	const cv::Size mOffsetRightBottom = { 33, 1 };
	const cv::Size mWindowSize = mOffsetLeftTop + mCaptureSize + mOffsetRightBottom;

private:
	mutable std::mutex mMutex;
	cv::Mat mBufferCapture;
	cv::Mat mBufferFlip;

public:
	bool CaptureModeWindow(void * wnd, cv::Mat &img);
	bool CaptureModeDesktop(void * wnd, cv::Mat &img);
};


