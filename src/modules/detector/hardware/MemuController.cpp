#include "MemuController.h"

#include <windows.h>
#include <boost\format.hpp>
#include <thread>

bool MemuController::Start(const std::wstring& path, int instance, const std::wstring& name, const std::wstring& application) const
{
	bool res = true;

	std::wstring cmd;
	try
	{
		//F:\Microvirt\MEmu\Memu.exe MEmu applink com.my.hc.rpg.kingdom.simulator/com.my.hustlecastle.HustleCastleActivity
		//"F:\Microvirt\MEmu\Memu.exe" MEmu_1 applink com.my.hc.rpg.kingdom.simulator/com.my.hustlecastle.HustleCastleActivity
		cmd = (boost::wformat(LR"("%1%" MEmu_%2% applink %3%)") % path % instance % application).str();
	}
	catch (const std::exception&)
	{
		res = false;
	}

	STARTUPINFOW si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	res &= !!CreateProcessW(NULL, LPWSTR(cmd.c_str()), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return res;
}


bool MemuController::Stop(const std::wstring& name) const
{
	auto wnd = FindWnd(ConvertName(name));
	if (!wnd)
		return false;

	return !!PostMessage((HWND)wnd, WM_CLOSE, 0, 0);
}


bool MemuController::Click(const std::wstring& name, int x, int y) const
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	int click_x = x + mOffsetLeftTop.width;
	int click_y = y + mOffsetLeftTop.height;

	PostMessage((HWND)wnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(click_x, click_y));
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	PostMessage((HWND)wnd, WM_LBUTTONUP, 0, MAKELPARAM(click_x, click_y));

	if (false)
	{
		RECT rect;
		GetWindowRect((HWND)wnd, &rect);
		SetCursorPos(rect.left + click_x, rect.top + click_y);
	}

	return true;
}

bool MemuController::Move(const std::wstring& name, int start_x, int start_y, int stop_x, int stop_y) const
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	auto click_wnd = wnd;

	auto start = cv::Vec2i(start_x, start_y);
	auto stop = cv::Vec2i(stop_x, stop_y);
	auto path = stop - start;

	cv::Vec2f path_dir = path;
	path_dir = cv::normalize(path_dir);

	DWORD procces_id = 0;
	auto thread_id = GetWindowThreadProcessId((HWND)wnd, &procces_id);
	{
		AttachThreadInput(GetCurrentThreadId(), thread_id, TRUE);

		//auto err = GetLastError();

		BYTE arr[256];
		memset(arr, 0, sizeof(256));
		GetKeyboardState(arr);
		auto old = arr[VK_LBUTTON];
		arr[VK_LBUTTON] = 128;
		SetKeyboardState(arr);


		SendMessage((HWND)click_wnd, WM_MOUSEACTIVATE, (WPARAM)wnd, MAKELPARAM(HTCLIENT, WM_LBUTTONDOWN));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		SendMessage((HWND)click_wnd, WM_SETCURSOR, (WPARAM)click_wnd, MAKELPARAM(HTCLIENT, WM_LBUTTONDOWN));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		PostMessage((HWND)click_wnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(start[0], start[1]));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		const int count = int(cv::norm(path));

		cv::Vec2i last_pos;

		for (int i = 0; i < count; ++i)
		{
			auto pos = cv::Vec2i(path_dir * float(i));
			last_pos = start + pos;

			SetKeyboardState(arr);
			PostMessage((HWND)click_wnd, WM_MOUSEMOVE, MK_LBUTTON, MAKELPARAM(last_pos[0], last_pos[1]));

			const int pause = 50;
			const int offset = 15;
			if (i < offset)
			{
				auto p = 10 + ((offset - (i + 1)) * pause) / offset;
				std::this_thread::sleep_for(std::chrono::milliseconds(p));
			}
			else if (i > count - offset)
			{
				auto p = 30 + ((offset - (count - i)) * pause) / offset;
				std::this_thread::sleep_for(std::chrono::milliseconds(p));
			}
			else if (i % 5 == 0)
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}

		PostMessage((HWND)click_wnd, WM_LBUTTONUP, 0, MAKELPARAM(last_pos[0], last_pos[1]));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		SendMessage((HWND)click_wnd, WM_SETCURSOR, (WPARAM)click_wnd, MAKELPARAM(HTCLIENT, WM_MOUSEMOVE));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		arr[VK_LBUTTON] = old;
		SetKeyboardState(arr);

		AttachThreadInput(GetCurrentThreadId(), thread_id, FALSE);
	}

	return true;
}

bool MemuController::PressEscape(const std::wstring& name) const
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	PostMessage((HWND)wnd, WM_SETFOCUS, 1, 0);

	auto scan = MapVirtualKey(VK_ESCAPE, 0);
	LPARAM lparam = 0x00000001 | (LPARAM)(scan << 16);
	PostMessage((HWND)wnd, WM_KEYDOWN, VK_ESCAPE, lparam);

	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	lparam = lparam | 0x40000000;
	lparam = lparam | 0x80000000;
	PostMessage((HWND)wnd, WM_KEYUP, VK_ESCAPE, lparam);

	return true;
}

bool MemuController::ShowWindow(const std::wstring& name) const
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	return ::ShowWindow((HWND)wnd, SW_NORMAL);
}

namespace
{
	struct Params
	{
		HWND hwnd_out = nullptr;
		std::wstring hwnd_name;
	};
}

static BOOL CALLBACK enumWindowCallback(HWND hWnd, LPARAM lparam)
{
	std::wstring title;
	auto size = size_t(GetWindowTextLength(hWnd));
	title.resize(size + 1);
	GetWindowText(hWnd, const_cast<WCHAR*>(title.c_str()), int(size + 1));

	title.resize(size);

	Params& params = *((Params*)lparam);

	if (
		((params.hwnd_name == L"MEmu" || params.hwnd_name == L"Memu") && (title == L"MEmu" || title == L"Memu")) ||
		((params.hwnd_name != L"MEmu" && params.hwnd_name != L"Memu") && title == params.hwnd_name)
		)
	{
		auto main_hwnd = FindWindowEx(hWnd, NULL, NULL, L"MainWindowWindow");
		if (main_hwnd != NULL)
		{
			params.hwnd_out = hWnd;
			return FALSE;
		}
	}

	return TRUE;
}

void* MemuController::FindWnd(const std::wstring& name) const
{
	Params params;
	params.hwnd_name = name;
	params.hwnd_out = nullptr;
	EnumWindows(enumWindowCallback, (LPARAM)(&params));
	auto wnd = params.hwnd_out;
	if (!wnd)
		return nullptr;

	return wnd;
}

std::wstring MemuController::ConvertInstance(int instance) const
{
	if (instance == 0)
		return L"MEmu";
	return std::wstring(L"MEmu_") + std::to_wstring(instance);
}

std::wstring MemuController::ConvertName(const std::wstring& name) const
{
// 	if (name != L"MEmu")
// 		return L"(" + name + L")";

	return name;
}
