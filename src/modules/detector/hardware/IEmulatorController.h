#pragma once

#include <string>


class IEmulatorController
{
public:
	virtual ~IEmulatorController() = default;

	virtual bool Start(const std::wstring& path, int instance, const std::wstring& name, const std::wstring& application) const = 0;

	virtual bool Stop(const std::wstring& name) const = 0;

	virtual bool Click(const std::wstring& name, int x, int y) const = 0;

	virtual bool Move(const std::wstring& name, int start_x, int start_y, int stop_x, int stop_y) const = 0;

	virtual bool PressEscape(const std::wstring& name) const = 0;

	virtual bool ShowWindow(const std::wstring& name) const = 0;

};

