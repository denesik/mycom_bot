#pragma once

#include <string>

#include <opencv2\core\mat.hpp>

enum class ScanningStatus
{
	Success,
	NotFound,
	WrongSize,
	Minimized,
	CaptureError,
	UnknownEmulator,
};


class IEmulatorScanner
{
public:
	virtual ~IEmulatorScanner() = default;

	virtual ScanningStatus Capture(cv::Mat& dst, const std::wstring& name, bool background) = 0;

	virtual bool SetValidSize(const std::wstring& name) const = 0;

	virtual bool IsValidSize(const std::wstring& name) const = 0;
};


