#include "BluestacksController.h"

#include <windows.h>
#include <boost\format.hpp>
#include <thread>

#include <opencv2\core\mat.hpp>

bool BluestacksController::Start(const std::wstring& path, int instance, const std::wstring& name, const std::wstring& application) const
{
	bool res = true;

	auto app = application.substr(0, application.find(L"/"));

	std::wstring cmd;
	try
	{
		//"C:\Program Files\BlueStacks_nxt\HD-Player.exe" --instance Nougat32 --cmd launchApp --package "com.my.hc.rpg.kingdom.simulator"
		//"C:\Program Files\BlueStacks_nxt\HD-Player.exe" --instance Nougat32_1 --cmd launchApp --package "com.my.hc.rpg.kingdom.simulator"
		cmd = (boost::wformat(LR"("%1%" --instance %2% --cmd launchApp --package %3%)") % path % ConvertInstance(instance) % app).str();
	}
	catch (const std::exception&)
	{
		res = false;
	}

	STARTUPINFOW si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	res &= !!CreateProcessW(NULL, LPWSTR(cmd.c_str()), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return res;
}

namespace
{
	struct Params
	{
		std::wstring classname;
		HWND hwnd_out = nullptr;
		HWND hwnd_own = nullptr;
	};

	static BOOL CALLBACK CloseWindowFinder(HWND hWnd, LPARAM lparam)
	{
		WCHAR classname[512];
		GetClassName(hWnd, classname, 512);

		Params& params = *((Params*)lparam);
		if (params.hwnd_own == GetParent(hWnd) && classname == params.classname)
		{
			params.hwnd_out = hWnd;
			return FALSE;
		}

		return TRUE;
	}

}

bool BluestacksController::Stop(const std::wstring& name) const
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	if (!!PostMessage((HWND)wnd, WM_CLOSE, 0, 0))
		for (size_t i = 0; i < 50; ++i)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));

			{
				Params params;
				params.classname = L"Qt5154QWindowOwnDC";
				params.hwnd_own = (HWND)wnd;
				params.hwnd_out = nullptr;
				EnumWindows(CloseWindowFinder, (LPARAM)(&params));
				if (params.hwnd_out)
				{
					auto x = 275;
					auto y = 120;
					bool res = true;

					res &= !!PostMessage((HWND)params.hwnd_out, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x, y));
					std::this_thread::sleep_for(std::chrono::milliseconds(10));
					res &= !!PostMessage((HWND)params.hwnd_out, WM_LBUTTONUP, 0, MAKELPARAM(x, y));

					if (res)
						return true;
				}
			}

			{
				Params params;
				params.classname = L"Qt5154QWindowOwnDCIcon";
				params.hwnd_own = (HWND)wnd;
				params.hwnd_out = nullptr;
				EnumWindows(CloseWindowFinder, (LPARAM)(&params));
				if (params.hwnd_out)
				{
					auto x = 260;
					auto y = 325;
					bool res = true;

					res &= !!PostMessage((HWND)params.hwnd_out, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x, y));
					std::this_thread::sleep_for(std::chrono::milliseconds(10));
					res &= !!PostMessage((HWND)params.hwnd_out, WM_LBUTTONUP, 0, MAKELPARAM(x, y));

					if (res)
						return true;
				}
			}
		}

	return false;
}


bool BluestacksController::Click(const std::wstring& name, int x, int y) const
{
	auto click_wnd = FindClickWnd(name);
	if (!click_wnd)
		return false;

	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	SendMessage((HWND)wnd, WM_SETFOCUS, 0, 0);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	PostMessage((HWND)click_wnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x, y));
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	PostMessage((HWND)click_wnd, WM_LBUTTONUP, 0, MAKELPARAM(x, y));

	if (false)
	{
		RECT rect;
		GetWindowRect((HWND)click_wnd, &rect);
		SetCursorPos(rect.left + x, rect.top + y);
	}

	return true;
}

bool BluestacksController::Move(const std::wstring& name, int start_x, int start_y, int stop_x, int stop_y) const
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	auto click_wnd = FindClickWnd(name);
	if (!click_wnd)
		return false;


	auto start = cv::Vec2i(start_x, start_y);
	auto stop = cv::Vec2i(stop_x, stop_y);
	auto path = stop - start;

	cv::Vec2f path_dir = path;
	path_dir = cv::normalize(path_dir);

	DWORD procces_id = 0;
	auto thread_id = GetWindowThreadProcessId((HWND)wnd, &procces_id);
	{
		AttachThreadInput(GetCurrentThreadId(), thread_id, TRUE);

		//auto err = GetLastError();

		BYTE arr[256];
		memset(arr, 0, sizeof(256));
		GetKeyboardState(arr);
		auto old = arr[VK_LBUTTON];
		arr[VK_LBUTTON] = 128;
		SetKeyboardState(arr);


		SendMessage((HWND)click_wnd, WM_MOUSEACTIVATE, (WPARAM)wnd, MAKELPARAM(HTCLIENT, WM_LBUTTONDOWN));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		SendMessage((HWND)click_wnd, WM_SETCURSOR, (WPARAM)click_wnd, MAKELPARAM(HTCLIENT, WM_LBUTTONDOWN));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		PostMessage((HWND)click_wnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(start[0], start[1]));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		const int count = int(cv::norm(path));

		cv::Vec2i last_pos;

		for (int i = 0; i < count; ++i)
		{
			auto pos = cv::Vec2i(path_dir * float(i));
			last_pos = start + pos;

			SetKeyboardState(arr);
			PostMessage((HWND)click_wnd, WM_MOUSEMOVE, MK_LBUTTON, MAKELPARAM(last_pos[0], last_pos[1]));

			const int pause = 50;
			const int offset = 15;
			if (i < offset)
			{
				auto p = 10 + ((offset - (i + 1)) * pause) / offset;
				std::this_thread::sleep_for(std::chrono::milliseconds(p));
			}
			else if (i > count - offset)
			{
				auto p = 30 + ((offset - (count - i)) * pause) / offset;
				std::this_thread::sleep_for(std::chrono::milliseconds(p));
			}
			else if (i % 5 == 0)
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}

		PostMessage((HWND)click_wnd, WM_LBUTTONUP, 0, MAKELPARAM(last_pos[0], last_pos[1]));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		SendMessage((HWND)click_wnd, WM_SETCURSOR, (WPARAM)click_wnd, MAKELPARAM(HTCLIENT, WM_MOUSEMOVE));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		arr[VK_LBUTTON] = old;
		SetKeyboardState(arr);

		AttachThreadInput(GetCurrentThreadId(), thread_id, FALSE);
	}

	return true;
}

bool BluestacksController::PressEscape(const std::wstring& name) const
{
	auto click_wnd = FindClickWnd(name);
	if (!click_wnd)
		return false;

	auto wnd = FindWnd(name);
	if (!wnd)
		return false;


	if (true)
	{
// 		SetForegroundWindow((HWND)wnd);
// 		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		SendMessage((HWND)wnd, WM_SETFOCUS, 0, 0);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		auto scan = MapVirtualKey(VK_ESCAPE, 0);
		LPARAM lparam = 0x00000001 | (LPARAM)(scan << 16);
		PostMessage((HWND)click_wnd, WM_KEYDOWN, VK_ESCAPE, MapVirtualKey(VK_ESCAPE, MAPVK_VK_TO_VSC));
		//PostMessage((HWND)click_wnd, WM_KEYDOWN, VK_ESCAPE, lparam);
		//SendMessage((HWND)click_wnd, WM_KEYDOWN, VK_ESCAPE, MapVirtualKey(VK_ESCAPE, MAPVK_VK_TO_VSC));

		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		lparam = lparam | 0x40000000;
		lparam = lparam | 0x80000000;
		PostMessage((HWND)click_wnd, WM_KEYUP, VK_ESCAPE, MapVirtualKey(VK_ESCAPE, MAPVK_VK_TO_VSC));
		//PostMessage((HWND)click_wnd, WM_KEYUP, VK_ESCAPE, lparam);
		//SendMessage((HWND)click_wnd, WM_KEYUP, VK_ESCAPE, MapVirtualKey(VK_ESCAPE, MAPVK_VK_TO_VSC));
	}
	else
	{
//		SetActiveWindow((HWND)click_wnd);
// 		SetForegroundWindow(nullptr);
// 
// 		std::this_thread::sleep_for(std::chrono::milliseconds(500));
// 
 		SetForegroundWindow((HWND)wnd);

		std::this_thread::sleep_for(std::chrono::milliseconds(500));

		INPUT input;
		WORD vkey = VK_ESCAPE; // see link below
		input.type = INPUT_KEYBOARD;
		input.ki.wScan = static_cast<WORD>(MapVirtualKey(vkey, MAPVK_VK_TO_VSC));
		input.ki.time = 0;
		input.ki.dwExtraInfo = 0;
		input.ki.wVk = vkey;
		input.ki.dwFlags = 0; // there is no KEYEVENTF_KEYDOWN
		SendInput(1, &input, sizeof(INPUT));

		std::this_thread::sleep_for(std::chrono::milliseconds(500));

		input.ki.dwFlags = KEYEVENTF_KEYUP;
		SendInput(1, &input, sizeof(INPUT));

		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}


	return true;
}

bool BluestacksController::ShowWindow(const std::wstring& name) const
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return false;

	return ::ShowWindow((HWND)wnd, SW_NORMAL);
}

void * BluestacksController::FindWnd(const std::wstring& name) const
{
	auto wnd = FindWindow(NULL, name.c_str());
	if (!wnd)
		return nullptr;
	return wnd;
}

std::wstring BluestacksController::ConvertInstance(int instance) const
{
	if (instance)
		return std::wstring(L"Nougat32_") + std::to_wstring(instance);
	return std::wstring(L"Nougat32");
}

namespace
{
	struct ParamsClick
	{
		HWND hwnd_out = nullptr;
		std::string name;
	};

	static BOOL CALLBACK EnumChildProc(HWND hwChild, LPARAM lParam)
	{
		ParamsClick& params = *((ParamsClick*)lParam);

		char finded_name[200];
		if (GetWindowTextA(hwChild, finded_name, 200) != 0)
			if (finded_name == params.name)
			{
				params.hwnd_out = hwChild;
				return FALSE;
			}

		return TRUE;
	}
}

void* BluestacksController::FindClickWnd(const std::wstring& name) const
{
	ParamsClick params = { nullptr, "HD-Player" };
	auto wnd = FindWnd(name);
	EnumChildWindows((HWND)wnd, EnumChildProc, (LPARAM)&params);
	return params.hwnd_out;
}

