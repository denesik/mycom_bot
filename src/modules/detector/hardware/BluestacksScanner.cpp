#include <windows.h>

#include "BluestacksScanner.h"

#include <opencv2/imgproc.hpp>

#include "utility/CaptureSize.h"

#include "EmulatorUtility.h"

BluestacksScanner::BluestacksScanner()
	: mCaptureSize(utility::GetCaptureSize())
{
	mBufferCapture = cv::Mat::zeros(mWindowSize.height, mWindowSize.width, CV_8UC4);
	mBufferFlip = cv::Mat::zeros(mCaptureSize.height, mCaptureSize.width, CV_8UC3);
}

ScanningStatus BluestacksScanner::Capture(cv::Mat& dst, const std::wstring& name, bool background)
{
	auto wnd = FindWnd(name);
	if (!wnd)
		return ScanningStatus::NotFound;

	if (IsIconic((HWND)wnd))
		return ScanningStatus::Minimized;

	if (!IsValidSize(wnd))
		return ScanningStatus::WrongSize;

	std::lock_guard<std::mutex> lock(mMutex);
	if (!background)
	{
		if (!CaptureModeDesktop(wnd, mBufferCapture))
			return ScanningStatus::CaptureError;
	}
	else
		if (!CaptureModeWindow(wnd, mBufferCapture))
			return ScanningStatus::CaptureError;

	cv::Mat roi(mBufferCapture, cv::Rect(mOffsetLeftTop.width, mOffsetRightBottom.height, mCaptureSize.width, mCaptureSize.height));
	cv::cvtColor(roi, mBufferFlip, cv::COLOR_BGRA2BGR);
	cv::flip(mBufferFlip, dst, 0);

	return ScanningStatus::Success;
}

bool BluestacksScanner::IsValidSize(void* wnd) const
{
	WINDOWINFO pwi;
	pwi.cbSize = sizeof(WINDOWINFO);
	if (!GetWindowInfo((HWND)wnd, &pwi))
		return false;

	auto width = pwi.rcWindow.right - pwi.rcWindow.left;
	auto height = pwi.rcWindow.bottom - pwi.rcWindow.top;

	if (width != mWindowSize.width || height != mWindowSize.height)
		return false;

	return true;
}

bool BluestacksScanner::IsValidSize(const std::wstring& name) const
{
	auto wnd = FindWindow(NULL, name.c_str());
	if (!wnd)
		return false;

	return IsValidSize(wnd);
}

bool BluestacksScanner::SetValidSize(const std::wstring& name) const
{
	auto wnd = FindWindow(NULL, name.c_str());
	if (!wnd)
		return false;

	return !!SetWindowPos((HWND)wnd, nullptr, 0, 0, mWindowSize.width, mWindowSize.height, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
}

void * BluestacksScanner::FindWnd(const std::wstring &name) const
{
	auto wnd = FindWindow(NULL, name.c_str());
	if (!wnd)
		return nullptr;

	if (!EmulatorUtility::CheckEmulatorTypePath(EmulatorUtility::GetExeNameFromWindowHandle(wnd), EmulatorType::BLUESTACKS))
		return nullptr;

	return wnd;
}

bool BluestacksScanner::CaptureModeDesktop(void * wnd, cv::Mat &buf)
{
	bool res = true;

	WINDOWINFO pwi = { 0 };
	pwi.cbSize = sizeof(WINDOWINFO);
	if (GetWindowInfo((HWND)wnd, &pwi))
	{
		auto &rc = pwi.rcWindow;
		auto x = rc.left;
		auto y = rc.top;
		int width = rc.right - rc.left;
		int height = rc.bottom - rc.top;

		if (width == mWindowSize.width && height == mWindowSize.height)
		{
			if (HDC hdc = GetDC(HWND_DESKTOP))
			{
				if (HDC memdc = CreateCompatibleDC(hdc))
				{
					if (HBITMAP hbitmap = CreateCompatibleBitmap(hdc, width, height))
					{
						if (HBITMAP oldbmp = (HBITMAP)SelectObject(memdc, hbitmap))
						{
							res |= !!BitBlt(memdc, 0, 0, width, height, hdc, x, y, SRCCOPY | CAPTUREBLT);
							res |= !!SelectObject(memdc, oldbmp);

							BITMAPINFOHEADER bi = { sizeof(BITMAPINFOHEADER), width, height, 1, 32 };
							res |= !!GetDIBits(memdc, hbitmap, 0, height, buf.data, (BITMAPINFO*)&bi, DIB_RGB_COLORS);

							res |= !!DeleteObject(hbitmap);
						}
						else
							res &= false;
					}
					else
						res &= false;
					DeleteDC(memdc);
				}
				else
					res &= false;
				res |= !!ReleaseDC(HWND_DESKTOP, hdc);
			}
			else
				res &= false;
		}
		else
			res &= false;
	}
	else
		res &= false;

	if (buf.empty())
		res &= false;

	return res;
}


bool BluestacksScanner::CaptureModeWindow(void * wnd, cv::Mat &buf)
{
	bool res = true;

	WINDOWINFO pwi;
	pwi.cbSize = sizeof(WINDOWINFO);
	if (GetWindowInfo((HWND)wnd, &pwi))
	{
		if (auto window_dc = GetWindowDC((HWND)wnd))
		{
			auto width = pwi.rcWindow.right - pwi.rcWindow.left;
			auto height = pwi.rcWindow.bottom - pwi.rcWindow.top;

			if (auto CompatibleHdc = CreateCompatibleDC(window_dc))
			{
				if (HBITMAP bitmap = CreateCompatibleBitmap(window_dc, width, height))
				{
					if (HGDIOBJ gdiobj = SelectObject(CompatibleHdc, bitmap))
					{
						res |= !!BitBlt(CompatibleHdc, 0, 0, width, height, window_dc, 0, 0, SRCCOPY /*| CAPTUREBLT*/);
						res |= !!SelectObject(CompatibleHdc, gdiobj);

						BITMAPINFOHEADER bi = { sizeof(BITMAPINFOHEADER), width, height, 1, 32 };
						res |= !!GetDIBits(CompatibleHdc, bitmap, 0, height, buf.data, (BITMAPINFO*)&bi, DIB_RGB_COLORS);
					}
					else
						res &= false;

					res |= !!DeleteObject(bitmap);
				}
				else
					res &= false;

				res |= !!DeleteDC(CompatibleHdc);
			}
			else
				res &= false;

			res |= !!ReleaseDC((HWND)wnd, window_dc);
		}
		else
			res &= false;
	}
	else 
		res &= false;

	return res;
}

