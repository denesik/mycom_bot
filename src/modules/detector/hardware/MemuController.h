#pragma once

#include "IEmulatorController.h"

#include <opencv2\core\types.hpp>

class MemuController : public IEmulatorController
{
public:

	virtual bool Start(const std::wstring& path, int instance, const std::wstring& name, const std::wstring& application) const override;

	virtual bool Stop(const std::wstring& name) const override;

	virtual bool Click(const std::wstring& name, int x, int y) const override;

	virtual bool Move(const std::wstring& name, int start_x, int start_y, int stop_x, int stop_y) const override;

	virtual bool PressEscape(const std::wstring& name) const override;

	virtual bool ShowWindow(const std::wstring& name) const override;

private:
	void* FindWnd(const std::wstring& name) const;

	std::wstring ConvertInstance(int instance) const;
	std::wstring ConvertName(const std::wstring& name) const;

	const cv::Size mOffsetLeftTop = { 1, 32 };
};


