#include "BlockNumber.h"

#include <format>

#include "utility/OpencvFilesystem.h"
#include "utility/MaskToRect.h"
#include "utility/CaptureSize.h"
#include "Ocr.h"

BlockNumber::BlockNumber(std::shared_ptr<Ocr> recognizer)
	: mRecognizer(recognizer)
{

}

const char* BlockNumber::StaticType()
{
	return "number";
}

IPageBlock* BlockNumber::Clone() const
{
	return new BlockNumber(*this);
}

bool BlockNumber::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{
	std::string type_prefix = GetType();

	auto mask = utility::Imread(path / (type_prefix + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);

	if (mask.empty())
		return false;

	if (!utility::CheckCaptureSize(mask))
		return false;

	cv::Rect rect;
	if (!utility::MaskToRect(mask, rect))
		return false;

	mName = name;
	mRect = rect;
	return true;
}

const char* BlockNumber::GetType() const
{
	return StaticType();
}

const std::string& BlockNumber::GetName() const
{
	return mName;
}

bool BlockNumber::Parse(const cv::Mat& image)
{
	mPower = -1;
	if (mRecognizer)
	{
		int power = mRecognizer->Recognize(cv::Mat(image, mRect));
		if (power >= 0)
		{
			mPower = power;
			return true;
		}
	}
	return false;
}

std::string BlockNumber::ToString() const
{
	return std::format("{}", mPower);
}

const cv::Rect& BlockNumber::GetRect() const
{
	return mRect;
}

int BlockNumber::GetPower(int scale) const
{
	return static_cast<int>(static_cast<double>(mPower) * (static_cast<double>(scale) / 100.0));
}
