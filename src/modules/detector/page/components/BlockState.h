#pragma once

#include "IPageBlock.h"

#include <opencv2\core\types.hpp>

#include <memory>
#include "ImageComparator.h"

class BlockState : public IPageBlock
{
public:
	BlockState(std::shared_ptr<ImageComparator> comparator);

	static const char* StaticType();

	virtual IPageBlock* Clone() const override;

	virtual bool Load(const std::filesystem::path& path, std::string_view type, std::string_view name) override;

	virtual const char* GetType() const override;

	virtual const std::string& GetName() const override;

	virtual bool Parse(const cv::Mat& image) override;

	virtual std::string ToString() const override;

	double GetDifference() const;

	const cv::Rect& GetRect() const;

	bool IsActive() const;

private:
	std::shared_ptr<ImageComparator> mComparator;

	double mLastDifference = 0;
	std::string mName;

	struct Template
	{
		cv::Mat sample;
		cv::Rect rect;
	};
	Template mDetectCompare;

	bool mIsActive = false;
};
