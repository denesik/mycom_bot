#pragma once

#include "IPageBlock.h"

#include <opencv2\core\types.hpp>

class Ocr;

class BlockNumber : public IPageBlock
{
public:
	BlockNumber(std::shared_ptr<Ocr> recognizer);

	static const char* StaticType();

	virtual IPageBlock* Clone() const override;

	virtual bool Load(const std::filesystem::path& path, std::string_view type, std::string_view name) override;

	virtual const char* GetType() const override;

	virtual const std::string& GetName() const override;

	virtual bool Parse(const cv::Mat& image) override;

	virtual std::string ToString() const override;

	const cv::Rect& GetRect() const;

	int GetPower(int scale = 100) const;

private:
	std::shared_ptr<Ocr> mRecognizer;

	int mPower = -1;

	std::string mName;
	cv::Rect mRect;
};
