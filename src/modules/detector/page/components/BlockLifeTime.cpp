#include "BlockLifeTime.h"

#include <opencv2/imgcodecs.hpp>
#include "utility/MaskToRect.h"


BlockLifeTime::BlockLifeTime()
{
	mTime = std::time(nullptr);
}

const char* BlockLifeTime::StaticType()
{
	return "lifetime";
}

IPageBlock* BlockLifeTime::Clone() const
{
	return new BlockLifeTime(*this);
}

bool BlockLifeTime::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{

	return true;
}

const char* BlockLifeTime::GetType() const
{
	return StaticType();
}

const std::string& BlockLifeTime::GetName() const
{
	return mName;
}

bool BlockLifeTime::Parse(const cv::Mat &image)
{
	mTime = std::time(nullptr);
	return true;
}

std::string BlockLifeTime::ToString() const
{
	return "";
}

int BlockLifeTime::GetMilliseconds() const
{
	return static_cast<int>(std::time(nullptr) - mTime) * 1000;
}

