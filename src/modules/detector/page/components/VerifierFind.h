#pragma once

#include "IPageVerifier.h"

#include <opencv2\core\mat.hpp>
#include <opencv2\core\types.hpp>

class ImageComparator;

class VerifierFind : public IPageVerifier
{
public:
	VerifierFind(std::shared_ptr<ImageComparator> comparator);

	static const char* StaticType();

	virtual IPageVerifier* Clone() const override;

	virtual bool Load(const std::filesystem::path& path, std::string_view type, std::string_view name) override;

	virtual const char* GetType() const override;

	virtual const std::string& GetName() const override;

	virtual bool Identify(const cv::Mat& image) override;

	virtual std::string ToString() const override;

	const cv::Rect &GetRect() const;

	double GetSimilarity() const;

private:
	std::shared_ptr<ImageComparator> mComparator;

	double mLastSimilarity = 0;
	cv::Rect mArea;
	std::string mName;

	cv::Mat mImage;
};
