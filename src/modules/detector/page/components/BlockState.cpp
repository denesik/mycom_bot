#include "BlockState.h"

#include "utility/OpencvFilesystem.h"
#include "utility/MaskToRect.h"
#include "utility/CaptureSize.h"
#include <format>


BlockState::BlockState(std::shared_ptr<ImageComparator> comparator)
	: mComparator(comparator)
{

}

const char* BlockState::StaticType()
{
	return "state";
}

IPageBlock* BlockState::Clone() const
{
	return new BlockState(*this);
}

bool BlockState::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{
	auto mask = utility::Imread(path / (std::string(type) + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);
	auto sample = utility::Imread(path / (std::string("sample") + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);

	if (sample.empty() || mask.empty())
		return false;

	if (!utility::CheckCaptureSize(mask))
		return false;

	if (!utility::CheckCaptureSize(sample))
		return false;

	cv::Rect rect;
	if (!utility::MaskToRect(mask, rect))
		return false;

	mDetectCompare = Template{ sample(rect) , rect };
	mName = name;

	return true;
}

const char* BlockState::GetType() const
{
	return StaticType();
}

const std::string& BlockState::GetName() const
{
	return mName;
}

bool BlockState::Parse(const cv::Mat &image)
{
	mIsActive = mComparator->Compare(cv::Mat(image, mDetectCompare.rect), mDetectCompare.sample, mLastDifference);
	return true;
}


std::string BlockState::ToString() const
{
	return std::format("active: {} acc: {:.3f}", mIsActive, mLastDifference);
}

double BlockState::GetDifference() const
{
	return mLastDifference;
}

const cv::Rect& BlockState::GetRect() const
{
	return mDetectCompare.rect;
}

bool BlockState::IsActive() const
{
	return mIsActive;
}

