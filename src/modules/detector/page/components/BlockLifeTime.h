#pragma once

#include "IPageBlock.h"

#include <opencv2\core\types.hpp>


class BlockLifeTime : public IPageBlock
{
public:
	BlockLifeTime();

	static const char* StaticType();

	virtual IPageBlock* Clone() const override;

	virtual bool Load(const std::filesystem::path& path, std::string_view type, std::string_view name) override;

	virtual const char* GetType() const override;

	virtual const std::string& GetName() const override;

	virtual bool Parse(const cv::Mat& image) override;

	virtual std::string ToString() const override;

	int GetMilliseconds() const;

private:
	std::string mName;

	time_t mTime = 0;
};
