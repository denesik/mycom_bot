#pragma once

#include "IPageComponent.h"

#include <opencv2\core\mat.hpp>


class IPageVerifier : public IPageComponent
{
public:
	virtual ~IPageVerifier() = default;

	virtual IPageVerifier* Clone() const = 0;

	virtual bool Identify(const cv::Mat& image) = 0;

};



