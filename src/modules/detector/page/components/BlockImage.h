#pragma once

#include "IPageBlock.h"

#include <opencv2\core\types.hpp>

#include <memory>
#include "ImageComparator.h"

class BlockImage : public IPageBlock
{
public:
	BlockImage(std::shared_ptr<ImageComparator> comparator);

	static const char* StaticType();

	virtual IPageBlock* Clone() const override;

	virtual bool Load(const std::filesystem::path& path, std::string_view type, std::string_view name) override;

	virtual const char* GetType() const override;

	virtual const std::string& GetName() const override;

	virtual bool Parse(const cv::Mat& image) override;

	virtual std::string ToString() const override;

	size_t GetCount() const;

	double GetDifference(size_t index) const;

	const cv::Rect& GetRect(size_t index) const;

private:
	std::shared_ptr<ImageComparator> mComparator;

	std::string mName;

	cv::Mat mImage;
	
	std::vector<std::pair<cv::Rect, double>> mFinded;

};
