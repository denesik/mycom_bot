#include "BlockIcon.h"

#include "utility/OpencvFilesystem.h"
#include "utility/MaskToRect.h"
#include "utility/CaptureSize.h"
#include <format>


BlockIcon::BlockIcon(std::shared_ptr<ImageComparator> comparator)
	: mComparator(comparator)
{

}

const char* BlockIcon::StaticType()
{
	return "icon";
}

IPageBlock* BlockIcon::Clone() const
{
	return new BlockIcon(*this);
}

bool BlockIcon::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{
	auto mask = utility::Imread(path / (std::string(type) + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);
	auto sample = utility::Imread(path / (std::string("sample") + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);

	if (sample.empty() || mask.empty())
		return false;

	if (!utility::CheckCaptureSize(mask))
		return false;

	if (!utility::CheckCaptureSize(sample))
		return false;

	cv::Rect rect;
	if (!utility::MaskToRect(mask, rect))
		return false;

	mImage = sample(rect);
	mName = name;

	return true;
}

const char* BlockIcon::GetType() const
{
	return StaticType();
}

const std::string& BlockIcon::GetName() const
{
	return mName;
}

bool BlockIcon::Parse(const cv::Mat &image)
{
	mFinded.clear();
	mComparator->Find(image, mImage, mFinded);
	return true;
}


std::string BlockIcon::ToString() const
{
	std::string out;
	for (const auto& [rect, acc] : mFinded)
	{
		const auto center = (rect.br() + rect.tl()) * 0.5;
		out += std::format("({},{}) acc: {:.3f}| ", center.x, center.y, acc);
	}
	if (!out.empty())
		out.resize(out.size() - 2);
	return out;
}

size_t BlockIcon::GetCount() const
{
	return mFinded.size();
}

double BlockIcon::GetDifference(size_t index) const
{
	return mFinded[index].second;
}

const cv::Rect& BlockIcon::GetRect(size_t index) const
{
	return mFinded[index].first;
}
