#include "BlockLine.h"

#include "utility/OpencvFilesystem.h"
#include "utility/MaskToRect.h"
#include "utility/CaptureSize.h"


const char* BlockLine::StaticType()
{
	return "line";
}

IPageBlock* BlockLine::Clone() const
{
	return new BlockLine(*this);
}

bool BlockLine::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{
	auto mask = utility::Imread(path / (std::string(type) + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);

 	if (mask.empty())
 		return false;

	if (!utility::CheckCaptureSize(mask))
		return false;

	std::vector<cv::Rect> rects;
	if (!utility::MaskToRect(mask, rects))
		return false;

	if (rects.size() != 2)
		return false;

	if (rects[0].area() != 9 || rects[1].area() != 9)
		return false;

	cv::Point points[2];
	points[0] = (rects[0].br() + rects[0].tl()) * 0.5;
	points[1] = (rects[1].br() + rects[1].tl()) * 0.5;

	cv::Vec3b colors[2];
	colors[0] = mask.at<cv::Vec3b>(points[0]);
	colors[1] = mask.at<cv::Vec3b>(points[1]);

	mName = name;

	auto start = cv::Vec3b(255, 255, 255);
	auto stop = cv::Vec3b(0, 0, 0);

	if (colors[0] == start && colors[1] == stop)
	{
		mStart = points[0];
		mStop = points[1];
		return true;
	}

	if (colors[1] == start && colors[0] == stop)
	{
		mStart = points[1];
		mStop = points[0];
		return true;
	}

	return false;
}

const char* BlockLine::GetType() const
{
	return StaticType();
}

const std::string& BlockLine::GetName() const
{
	return mName;
}

bool BlockLine::Parse(const cv::Mat &image)
{
	return true;
}

std::string BlockLine::ToString() const
{
	return "";
}

const cv::Point& BlockLine::GetPointStart() const
{
	return mStart;
}

const cv::Point& BlockLine::GetPointStop() const
{
	return mStop;
}

