#pragma once

#include "IPageVerifier.h"

#include <opencv2\core\mat.hpp>
#include <opencv2\core\types.hpp>

#include <memory>
#include "ImageComparator.h"


class VerifierCompare : public IPageVerifier
{
public:
	VerifierCompare(std::shared_ptr<ImageComparator> comparator);

	static const char* StaticType();

	virtual IPageVerifier* Clone() const override;

	virtual bool Load(const std::filesystem::path& path, std::string_view type, std::string_view name) override;

	virtual const char* GetType() const override;

	virtual const std::string& GetName() const override;

	const cv::Rect &GetRect() const;

	virtual std::string ToString() const override;

	virtual bool Identify(const cv::Mat& image) override;

	double GetDifference() const;

private:
	std::shared_ptr<ImageComparator> mComparator;
	double mLastDifference = 0;
	std::string mName;

	struct Template
	{
		cv::Mat sample;
		cv::Rect rect;
	};
	Template mDetectCompare;
};
