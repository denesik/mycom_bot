#include "BlockButton.h"

#include "utility/OpencvFilesystem.h"
#include "utility/MaskToRect.h"
#include "utility/CaptureSize.h"


const char* BlockButton::StaticType()
{
	return "button";
}

IPageBlock* BlockButton::Clone() const
{
	return new BlockButton(*this);
}

bool BlockButton::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{
	auto mask = utility::Imread(path / (std::string(type) + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);

 	if (mask.empty())
 		return false;

	if (!utility::CheckCaptureSize(mask))
		return false;

	cv::Rect rect;
	if (!utility::MaskToRect(mask, rect))
		return false;

	mName = name;
	mRect = rect;
	return true;
}

const char* BlockButton::GetType() const
{
	return StaticType();
}

const std::string& BlockButton::GetName() const
{
	return mName;
}

bool BlockButton::Parse(const cv::Mat &image)
{
	return true;
}

std::string BlockButton::ToString() const
{
	return "";
}

const cv::Rect& BlockButton::GetRect() const
{
	return mRect;
}

