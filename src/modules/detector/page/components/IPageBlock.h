#pragma once

#include "IPageComponent.h"

#include <opencv2\core\mat.hpp>

class IPageBlock : public IPageComponent
{
public:
	virtual ~IPageBlock() = default;

	virtual IPageBlock* Clone() const = 0;

	virtual bool Parse(const cv::Mat& image) = 0;

};



