#include "VerifierFind.h"

#include <format>
#include <opencv2/imgcodecs.hpp>

#include "utility/MaskToRect.h"
#include "utility/OpencvFilesystem.h"
#include "utility/CaptureSize.h"
#include "ImageComparator.h"


VerifierFind::VerifierFind(std::shared_ptr<ImageComparator> comparator)
	: mComparator(comparator)
{

}

const char* VerifierFind::StaticType()
{
	return "find";
}

IPageVerifier* VerifierFind::Clone() const
{
	return new VerifierFind(*this);
}

bool VerifierFind::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{
	auto mask = utility::Imread(path / (std::string(type) + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);
	auto sample = utility::Imread(path / "sample.png", cv::IMREAD_COLOR);

	if (sample.empty() || mask.empty())
		return false;

	if (!utility::CheckCaptureSize(mask))
		return false;

	if (!utility::CheckCaptureSize(sample))
		return false;

	cv::Rect rect;
	if (!utility::MaskToRect(mask, rect))
		return false;

	mImage = sample(rect);
	mName = name;

	return true;
}

const char* VerifierFind::GetType() const
{
	return StaticType();
}

const std::string& VerifierFind::GetName() const
{
	return mName;
}

const cv::Rect& VerifierFind::GetRect() const
{
	return mArea;
}

bool VerifierFind::Identify(const cv::Mat& image)
{
	mArea = {};
	mLastSimilarity = 0;
	return mComparator->Find(image, mImage, mArea, mLastSimilarity);
}

std::string VerifierFind::ToString() const
{
	std::string out;
	const auto center = (mArea.br() + mArea.tl()) * 0.5;
	out += std::format("({},{}) acc: {:.3f}| ", center.x, center.y, mLastSimilarity);
	return out;
}

double VerifierFind::GetSimilarity() const
{
	return mLastSimilarity;
}
