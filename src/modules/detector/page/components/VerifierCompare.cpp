#include "VerifierCompare.h"

#include "utility/MaskToRect.h"

#include "ImageComparator.h"
#include "utility/OpencvFilesystem.h"
#include "utility/CaptureSize.h"
#include <format>


VerifierCompare::VerifierCompare(std::shared_ptr<ImageComparator> comparator)
	: mComparator(comparator)
{

}

const char* VerifierCompare::StaticType()
{
	return "compare";
}

IPageVerifier* VerifierCompare::Clone() const
{
	return new VerifierCompare(*this);
}

bool VerifierCompare::Load(const std::filesystem::path& path, std::string_view type, std::string_view name)
{
	auto mask = utility::Imread(path / (std::string(type) + "_" + std::string(name) + ".png"), cv::IMREAD_COLOR);
	auto sample = utility::Imread(path / "sample.png", cv::IMREAD_COLOR);

	if (sample.empty() || mask.empty())
		return false;

	if (!utility::CheckCaptureSize(mask))
		return false;

	if (!utility::CheckCaptureSize(sample))
		return false;

	cv::Rect rect;
	if (!utility::MaskToRect(mask, rect))
		return false;

	mDetectCompare = Template{ sample(rect) , rect };
	mName = name;

	return true;
}

const char* VerifierCompare::GetType() const
{
	return StaticType();
}

const std::string& VerifierCompare::GetName() const
{
	return mName;
}

const cv::Rect& VerifierCompare::GetRect() const
{
	return mDetectCompare.rect;
}

std::string VerifierCompare::ToString() const
{
	return std::format("acc: {:.3f}", mLastDifference);
}

bool VerifierCompare::Identify(const cv::Mat &image)
{
	return mComparator->Compare(cv::Mat(image, mDetectCompare.rect), mDetectCompare.sample, mLastDifference);
}

double VerifierCompare::GetDifference() const
{
	return mLastDifference;
}
