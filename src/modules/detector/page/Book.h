#pragma once


#include <vector>
#include <thread>
#include <string>
#include <filesystem>
#include <atomic>


#include "ActorComponent.h"
#include "Page.h"
#include "PageMap.h"


class Book : public ActorComponent
{
public:
	Book(const PageMap &page_map, const std::filesystem::path& path);

	const Page& Identify(const cv::Mat& img);

public:

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

private:
	bool IsLoaded() const;

	void Load();

	bool LoadPageList(std::vector<std::string>& names) const;

	bool LoadPage(Page& page);

private:
	const unsigned int mThreadCount;

	const std::filesystem::path mPath;

	const PageMap mPageMap;
	const Page mUnknownPage = Page("unknown");
	const Page mNotLoadedPage = Page("not_loaded");

	std::vector<std::jthread> mThreads;
	std::atomic<unsigned int> mLoadedCount = 0;

	std::vector<Page> mPages;
};

