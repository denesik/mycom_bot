#include "Page.h"

#include "BlockLifeTime.h"
#include "PageMap.h"

#include "utf_helper/PathUtf8.h"
#include "utility/split_string.h"

namespace 
{
	bool check_ascii(std::string_view data)
	{
		return !std::any_of(data.begin(), data.end(), [](char c)
			{
				return static_cast<unsigned char>(c) > 127;
			});
	}
}



Page::Page(std::string_view name)
{
	if (check_ascii(name))
	{
		std::string first_name;
		std::string last_name;
		utility::split_string("-", name, first_name, last_name);

		mFirstName = first_name;
		mFullName = name;
	}

	mBlocks.push_back(std::make_unique<BlockLifeTime>());
}

Page::Page(const Page& other)
{
	mFirstName = other.mFirstName;
	mFullName = other.mFullName;

	mVerifiers.clear();
	mVerifiers.reserve(other.mVerifiers.size());
	for (auto const& p : other.mVerifiers) 
		mVerifiers.push_back(std::unique_ptr<IPageVerifier>(p->Clone()));

	mBlocks.clear();
	mBlocks.reserve(other.mBlocks.size());
	for (auto const& p : other.mBlocks)
		mBlocks.push_back(std::unique_ptr<IPageBlock>(p->Clone()));
}

Page::Page(Page&& other) noexcept
{
	std::swap(mVerifiers, other.mVerifiers);
	std::swap(mBlocks, other.mBlocks);
	std::swap(mFirstName, other.mFirstName);
	std::swap(mFullName, other.mFullName);
}

Page& Page::operator=(const Page& other)
{
	return *this = Page(other);
}

Page& Page::operator=(Page&& other) noexcept
{
	std::swap(mVerifiers, other.mVerifiers);
	std::swap(mBlocks, other.mBlocks);
	std::swap(mFirstName, other.mFirstName);
	std::swap(mFullName, other.mFullName);
	return *this;
}



bool Page::Load(const PageMap& page_map, const std::filesystem::path& path)
{
	if (!std::filesystem::exists(path / utf_helper::PathFromUtf8(mFullName)))
		return false;

	for (auto& it : std::filesystem::directory_iterator(path / utf_helper::PathFromUtf8(mFullName)))
	{
		auto component_name = utf_helper::PathToUtf8(it.path().stem());
		if (check_ascii(component_name))
		{
			std::string type;
			std::string name;
			utility::split_string("_", component_name, type, name);

			if (auto v = page_map.Verifiers(type))
			{
				auto verifier = std::unique_ptr<IPageVerifier>(v->Clone());
				if (verifier->Load(it.path().parent_path(), type, name))
					mVerifiers.emplace_back(std::move(verifier));
			}

			if (auto v = page_map.Blocks(type))
			{
				auto block = std::unique_ptr<IPageBlock>(v->Clone());
				if (block->Load(it.path().parent_path(), type, name))
					mBlocks.emplace_back(std::move(block));
			}
		}
	}

	mLoaded = !mVerifiers.empty();
	return mLoaded;
}

bool Page::IsLoaded() const
{
	return mLoaded;
}

bool Page::Identify(const cv::Mat &image)
{
	bool res = true;
	for (auto& ptr : mVerifiers)
		res &= ptr->Identify(image);

	return res;
}

bool Page::Parse(const cv::Mat& image)
{
	for (auto& ptr : mBlocks)
		if (!ptr->Parse(image))
			return false;

	return true;
}

const std::string & Page::FirstName() const
{
	return mFirstName;
}

const std::string & Page::FullName() const
{
	return mFullName;
}

void Page::Foreach(const std::function<bool(const IPageComponent&)>& callback) const
{
	for (auto& v : mVerifiers)
		if (callback(*v.get()))
			return;
	for (auto& v : mBlocks)
		if (callback(*v.get()))
			return;
}

