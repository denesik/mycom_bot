#include "ImageComparator.h"

#include <opencv2/imgproc.hpp>

namespace
{
//	constexpr auto COMPARE_ERROR = 0.1;
}

ImageComparator::ImageComparator()
{
	
}

bool ImageComparator::Compare(const cv::Mat& img, const cv::Mat& templ, cv::InputArray mask, double& err, double compare_error)
{
	if (img.empty() || templ.empty())
		return false;

	if (img.type() != templ.type())
		return false;

	if (img.dims != templ.dims)
		return false;

	if (img.size() != templ.size())
		return false;

	ReserveBuffer(img, templ);

	int result_cols = img.cols - templ.cols + 1;
	int result_rows = img.rows - templ.rows + 1;

	cv::Mat result(mBuffer, cv::Rect(0, 0, result_cols, result_rows));
	//result.setTo(0);

	cv::matchTemplate(img, templ, result, cv::TM_SQDIFF_NORMED, mask);

	auto result_error = result.at<float>(0, 0);
	err = result_error;

	return result_error < compare_error;
}

bool ImageComparator::Find(const cv::Mat& img, const cv::Mat& templ, cv::InputArray mask, cv::Rect& rect, double& err, double compare_error)
{
	if (img.empty() || templ.empty())
		return false;

	if (img.type() != templ.type())
		return false;

	if (img.dims != templ.dims)
		return false;

	ReserveBuffer(img, templ);

	int result_cols = img.cols - templ.cols + 1;
	int result_rows = img.rows - templ.rows + 1;

	cv::Mat result(mBuffer, cv::Rect(0, 0, result_cols, result_rows));
	//result.setTo(0);

	cv::matchTemplate(img, templ, result, cv::TM_SQDIFF_NORMED, mask);

	cv::Point minLoc; 
	double minVal = 0; 

	cv::minMaxLoc(result, &minVal, nullptr, &minLoc, nullptr);

	if (minVal < compare_error)
	{
		err = minVal;
		rect = cv::Rect(minLoc, templ.size());
		return true;
	}

	return false;
}

size_t ImageComparator::Find(const cv::Mat& img, const cv::Mat& templ, cv::InputArray mask, std::vector<std::pair<cv::Rect, double>>& out, double compare_error)
{
	if (img.empty() || templ.empty())
		return false;

	if (img.type() != templ.type())
		return false;

	if (img.dims != templ.dims)
		return false;

	ReserveBuffer(img, templ);

	int result_cols = img.cols - templ.cols + 1;
	int result_rows = img.rows - templ.rows + 1;

	cv::Mat result(mBuffer, cv::Rect(0, 0, result_cols, result_rows));
	//result.setTo(0);

	cv::matchTemplate(img, templ, result, cv::TM_SQDIFF_NORMED, mask);
	
	{
		mData.clear();
		std::mutex mutex;
		result.forEach<float>([this, &templ, compare_error, &mutex](float& p, const int* position)
			{
				if (p < compare_error)
				{
					std::lock_guard<std::mutex> lock(mutex);
					mData.push_back({ cv::Rect({position[1], position[0]}, templ.size()) , p });
				}
			});
	}

	// ��������� �� �������� ��������
	std::sort(mData.begin(), mData.end(), [](const std::pair<cv::Rect, double>& a, const std::pair<cv::Rect, double>& b)
		{
			return a.second < b.second;
		});

	// ��������� �����, ���� ��� ���������
	for (const auto & data : mData)
	{
		auto it = std::find_if(out.begin(), out.end(), [&data](const std::pair<cv::Rect, double>& a)
			{
				auto dist = cv::norm(data.first.tl() - a.first.tl());
				return dist < 10;
			});
		if (it == out.end())
			out.emplace_back(data);
	}

	return out.size();
}


void ImageComparator::ReserveBuffer(const cv::Mat& img, const cv::Mat& templ)
{
	int need_cols = img.cols - templ.cols + 1;
	int need_rows = img.rows - templ.rows + 1;

	if (mBuffer.type() != CV_32FC1 || mBuffer.cols < need_cols || mBuffer.rows < need_rows)
		mBuffer.create(need_rows, need_cols, CV_32FC1);
}


bool ImageComparator::Compare(const cv::Mat& img, const cv::Mat& templ, double& err, double compare_error)
{
	return Compare(img, templ, cv::noArray(), err, compare_error);
}

bool ImageComparator::Find(const cv::Mat& img, const cv::Mat& templ, cv::Rect& rect, double& err, double compare_error)
{
	return Find(img, templ, cv::noArray(), rect, err, compare_error);
}

size_t ImageComparator::Find(const cv::Mat& img, const cv::Mat& templ, std::vector<std::pair<cv::Rect, double>>& out, double compare_error)
{
	return Find(img, templ, cv::noArray(), out, compare_error);
}
