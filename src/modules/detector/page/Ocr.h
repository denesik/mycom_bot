#pragma once

#include <vector>
#include <filesystem>
#include <utility>
#include <atomic>
#include <thread>
#include <mutex>
#include <opencv2/ml.hpp>
#include <opencv2/core/mat.hpp>

#include "ActorComponent.h"
#include <set>
#include <optional>

class Ocr : public ActorComponent
{
public:
	Ocr(const std::filesystem::path& unknown, const std::filesystem::path& test, const std::filesystem::path& train, const std::filesystem::path& digits);
	~Ocr() = default;

	Ocr(const Ocr& other) = delete;
	Ocr(Ocr&& other) noexcept = delete;
	Ocr& operator=(const Ocr& other) = delete;
	Ocr& operator=(Ocr&& other) noexcept = delete;

	struct Params
	{
		int gray_threshold = 242;
		int glyph_min_width = 7;
		int glyph_min_height = 13;
		int glyph_min_area = 90;

		friend auto operator<=>(const Params& a, const Params& b) = default;
	};

	void SetParams(const Params& settings);
	const Params& GetParams() const;

	virtual void Initialize() override;
	
	int Recognize(const cv::Mat & img) const;
	cv::Mat Generate(const cv::Mat & img) const;

public:
	struct TestUnit
	{
		std::filesystem::path path;
		size_t file_size = 0;
		cv::Mat img;
		int number = 0;
	};

	bool IsLoaded() const;

	std::pair<std::filesystem::path, cv::Mat> GetUnknownImage() const;

	void AddKnownImage(const std::filesystem::path& path, int number);
	
	void AddTrain(const TestUnit & unit);

	std::optional<TestUnit> GetFailedTest();

private:
	void LoadKnown();

	void Train();

public:
	cv::Mat Preprocess(const cv::Mat& img) const;

	std::vector<cv::Rect> FindGlyphs(const cv::Mat& img) const;
	int RecognizeDigit(const cv::Mat & img) const;

	cv::Mat DrawRects(const cv::Mat& img, const std::vector<cv::Rect>& rects) const;

	void LoadDigits();

private:
	mutable std::mutex mKnearesMutex;
	cv::Ptr<cv::ml::KNearest> mKNearest;

	const std::filesystem::path mUnknownDir;
	const std::filesystem::path mTestDir;
	const std::filesystem::path mTrainDir;
	const std::filesystem::path mDigitsDir;

	struct TestUnitCmp
	{
		bool operator()(const TestUnit& a, const TestUnit& b) const
		{
			auto a_size = a.img.rows * a.img.cols * a.img.elemSize1();
			auto b_size = b.img.rows * b.img.cols * b.img.elemSize1();

			return (a.file_size < b.file_size) || 
				((!(b.file_size < a.file_size)) && (std::string_view(reinterpret_cast<const char *>(a.img.data), a_size) < std::string_view(reinterpret_cast<const char*>(b.img.data), b_size)));
		}
	};
	std::set<TestUnit, TestUnitCmp> mTestUnits;

	std::array<cv::Mat, 10> mDigitsMat;

	std::jthread mLoadThread;
	std::atomic<bool> mIsLoaded = false;

	Params mParams;
};



