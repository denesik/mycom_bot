#pragma once

#include <filesystem>
#include <string>


class IPageComponent
{
public:
	virtual ~IPageComponent() = default;

	virtual bool Load(const std::filesystem::path& path, std::string_view type, std::string_view name) = 0;

	virtual const char* GetType() const = 0;

	virtual const std::string& GetName() const = 0;

	virtual std::string ToString() const = 0;
};
