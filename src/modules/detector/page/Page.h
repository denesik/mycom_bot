#pragma once

#include <string>
#include <vector>
#include <functional>

#include <filesystem>

#include "IPageVerifier.h"
#include "IPageBlock.h"

class PageMap;

class Page
{
public:
	Page(std::string_view name = "");

	~Page() = default;

	Page(const Page& other);
	Page(Page&& other) noexcept;
	Page& operator=(const Page& other);
	Page& operator=(Page&& other) noexcept;

	bool Load(const PageMap &page_map, const std::filesystem::path &path);

	bool IsLoaded() const;

	bool Identify(const cv::Mat& image);

	bool Parse(const cv::Mat& image);

	const std::string &FirstName() const;
	const std::string &FullName() const;

	void Foreach(const std::function<bool(const IPageComponent&)>& callback) const;

private:
	bool mLoaded = false;

	std::vector<std::unique_ptr<IPageVerifier>> mVerifiers;
	std::vector<std::unique_ptr<IPageBlock>> mBlocks;

	std::string mFirstName;
	std::string mFullName;
};

