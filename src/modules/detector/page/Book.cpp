#include "Book.h"

#include <algorithm>
#include <fstream>

#include "Logger.h"

Book::Book(const PageMap& page_map, const std::filesystem::path& path)
	: mPath(path), mPageMap(page_map), mThreadCount(std::clamp(std::thread::hardware_concurrency(), 1U, 64U))
{
}

bool Book::IsLoaded() const
{
	return mLoadedCount == mThreadCount;
}

bool Book::LoadPageList(std::vector<std::string>& names) const
{
	std::ifstream fs(mPath / "pages.txt");
	if (!fs.is_open())
		return false;

	std::string line;
	while (std::getline(fs, line))
		names.push_back(line);

	return true;
}

bool Book::LoadPage(Page& page)
{
	return page.Load(mPageMap, mPath);
}

const Page& Book::Identify(const cv::Mat& img)
{
	if (!IsLoaded())
		return mNotLoadedPage;

	for (auto& page : mPages)
		if (page.Identify(img))
			return page;

	return mUnknownPage;
}

void Book::BeginPlay()
{
	Load();
}

void Book::EndPlay()
{
	for (auto & thread : mThreads)
		std::jthread().swap(thread);
}

void Book::Load()
{
	std::vector<std::string> names;
	if (!LoadPageList(names))
		LOG_E("Pages list not loaded");

	for (const auto &name : names)
		mPages.emplace_back(name);

	std::vector<std::vector<size_t>> parts;
	parts.resize(mThreadCount);
	for (size_t i = 0; i < names.size(); ++i)
		parts[i % mThreadCount].push_back(i);

	for (const auto &part : parts)
		mThreads.emplace_back([this, part, names](std::stop_token stoken)
			{
				bool is_loaded = false;
				size_t loaded_index = 0;

				while (!is_loaded && !stoken.stop_requested())
				{
					if (loaded_index < part.size())
					{
						auto index = part[loaded_index++];
						LOG_I("Loading page " + names[index]);
						if (!LoadPage(mPages[index]))
							LOG_E("Page " + names[index] + " not loaded");
					}

					if (loaded_index >= part.size())
						is_loaded = true;
				}

				++mLoadedCount;
			});
}
