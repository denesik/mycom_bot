#pragma once

#include <map>
#include <memory>
#include <string>
#include <filesystem>

#include "IPageVerifier.h"
#include "IPageBlock.h"

class Ocr;
class PageMap
{
public:
	PageMap(std::shared_ptr<Ocr> ocr);

	const IPageVerifier* Verifiers(const std::string& type) const;

	const IPageBlock* Blocks(const std::string& type) const;


private:

	std::shared_ptr<Ocr> mOcr;
	std::map<std::string, std::shared_ptr<IPageVerifier>> mPageVerifiers;
	std::map<std::string, std::shared_ptr<IPageBlock>> mPageBlocks;
};

