#include "Ocr.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <fstream>
#include <random>

#include "utility/OpencvFilesystem.h"

namespace
{
	const cv::Size gTemplateSymbolSize = { 20, 30 }; // mean: 16, 21
};

Ocr::Ocr(const std::filesystem::path& unknown, const std::filesystem::path& test, const std::filesystem::path& train, const std::filesystem::path& digits)
	: mUnknownDir(unknown), mTestDir(test), mTrainDir(train), mDigitsDir(digits)
{
	
}

void Ocr::SetParams(const Params& settings)
{
	mParams = settings;
}

const Ocr::Params& Ocr::GetParams() const
{
	return mParams;
}

void Ocr::Initialize()
{
	Train();

//	LoadDigits();
//	LoadKnown();
}

int Ocr::Recognize(const cv::Mat& img) const
{
	if (img.empty())
		return -1;

	auto preprocessed = Preprocess(img);
	auto glyphs = FindGlyphs(preprocessed);

	if (glyphs.empty())
		return -1;

	int number = 0;
	for (size_t i = 0; i < glyphs.size(); ++i)
	{
		auto digit = RecognizeDigit(cv::Mat(preprocessed, glyphs[glyphs.size() - i - 1]));
		if (digit < 0 || digit > 9)
			return -1;
		int power = static_cast<int>(std::pow(10, i));
		number += digit * power;
	}
	return number;
}

cv::Mat Ocr::Generate(const cv::Mat& img) const
{
	cv::Mat out = cv::Mat::ones(img.size(), CV_8UC3);
	
	if (img.empty())
		return out;

	auto preprocessed = Preprocess(img);
	auto glyphs = FindGlyphs(preprocessed);

	if (glyphs.empty())
		return out;

	for (size_t i = 0; i < glyphs.size(); ++i)
	{
		const auto &glyph = glyphs[glyphs.size() - i - 1];
		auto digit = RecognizeDigit(cv::Mat(preprocessed, glyph));
		if (digit < 0 || digit > 9)
		{
			out = cv::Mat::ones(img.size(), CV_8UC3);
			return out;
		}

		cv::Mat scaled;
		cv::resize(mDigitsMat[digit], scaled,glyph.size());
		scaled.copyTo(cv::Mat(out, glyph));
	}

	return out;
}

bool Ocr::IsLoaded() const
{
	return mIsLoaded;
}

std::pair<std::filesystem::path, cv::Mat> Ocr::GetUnknownImage() const
{
	if (!mIsLoaded)
		return {};
	// ����� �� ������, ���������� �� ����� ���������.

	for (auto& it : std::filesystem::directory_iterator(mUnknownDir))
		if (it.path().has_extension() && it.path().extension() == ".png")
		{
			const auto& path = it.path();
			size_t size = std::filesystem::file_size(path);

			TestUnit unit{ path, size, utility::Imread(path), 0 };
			if (mTestUnits.find(unit) == mTestUnits.end())
			{
				return { path, unit.img };
			}
			else
				std::filesystem::remove(path);
		}

	return {};
}

void Ocr::AddKnownImage(const std::filesystem::path& path, int number)
{
	if (!mIsLoaded)
		return;

	if (!std::filesystem::exists(path))
		return;

		size_t size = std::filesystem::file_size(path);
		TestUnit unit{ path, size, utility::Imread(path), 0 };
		if (mTestUnits.find(unit) == mTestUnits.end())
		{
			const auto new_path = mTestDir / path.filename();
			std::filesystem::rename(path, new_path);
			unit.path = new_path;
			auto txt_path = new_path;
			txt_path.replace_extension("txt");
			{
				std::ofstream file;
				file.open(txt_path);
				file << number;
			}
			unit.number = number;
			mTestUnits.emplace(unit);
		}
		else
			std::filesystem::remove(path);
}

namespace
{
	inline std::vector<int> GlyphsCount(size_t number)
	{
		std::vector<int> out;
		std::string data = std::to_string(number);
		for (auto c : data)
			if (c >= '0' && c <= '9')
				out.push_back(c - '0');
		return out;
	}
}

void Ocr::AddTrain(const TestUnit& unit)
{
	if (!mIsLoaded)
		return;

	auto src_glyphs = GlyphsCount(unit.number);
	auto preprocessed = Preprocess(unit.img);
	auto glyphs = FindGlyphs(preprocessed);
	if (glyphs.size() != src_glyphs.size())
	{
		// TODO: error
		return;
	}
	
	for (size_t i = 0; i < glyphs.size(); ++i)
	{
		const auto digit_img = cv::Mat(preprocessed, glyphs[i]);
		auto digit = RecognizeDigit(digit_img);
		if (digit < 0 || digit != src_glyphs[i])
		{
			std::vector<uchar> buf;
			cv::imencode(".png", digit_img, buf);
			const std::string_view digit_img_data(reinterpret_cast<const char *>(buf.data()), buf.size());
			const size_t suffix = std::hash<std::string_view>()(digit_img_data);

			const auto path = mTrainDir / (std::to_string(src_glyphs[i]) + "_" + std::to_string(suffix) + ".png");
			if (std::filesystem::exists(path))
			{
				// TODO: warning
				continue;
			}
			
			if (!path.empty())
			{
				utility::Imwrite(path, digit_img);
				break;
			}
		}
	}

	Train();
}

std::optional<Ocr::TestUnit> Ocr::GetFailedTest()
{
	if (!mIsLoaded)
		return {{}};

	for (const auto & unit : mTestUnits)
		if (Recognize(unit.img) != unit.number)
			return unit;

	return {};
}

void Ocr::LoadKnown()
{
	mLoadThread = std::jthread([this](std::stop_token stoken)
		{
			for (auto& it : std::filesystem::directory_iterator(mTestDir))
				if (it.path().has_extension() && it.path().extension() == ".png")
				{
					if (stoken.stop_requested())
						return;

					const auto& path = it.path();
					size_t size = std::filesystem::file_size(path);

					int number = 0;
					{
						auto txt_path = path;
						txt_path.replace_extension("txt");
						std::ifstream stream(txt_path, std::ifstream::binary);
						if (!stream)
						{
							// TODO: logw
							continue;
						}
						std::string buffer;
						stream.seekg(0, stream.end);
						buffer.resize(stream.tellg());
						stream.seekg(0, stream.beg);
						stream.read(buffer.data(), buffer.size());
						stream.close();
						if (buffer.empty())
						{
							// TODO: logw
							continue;
						}
						number = std::stoi(buffer);
					}

					TestUnit unit{ path, size, utility::Imread(path), number };

					if (mTestUnits.find(unit) == mTestUnits.end())
						mTestUnits.emplace(unit);
					else
						std::filesystem::remove(path);
				}

			mIsLoaded = true;
		});
}

int Ocr::RecognizeDigit(const cv::Mat& img) const
{
	if (img.empty())
		return false;

	cv::Mat template_img;
	cv::resize(img, template_img, gTemplateSymbolSize);
	cv::Mat template_img_float;
	template_img.convertTo(template_img_float, CV_32FC1);
	cv::Mat template_img_reshape = template_img_float.reshape(1, 1);

	float res = -1;
	{
		std::scoped_lock lock(mKnearesMutex);
		if (!mKNearest)
			return -1;
		try
		{
			cv::Mat finded_symbol_img(0, 0, CV_32F);
			res = mKNearest->findNearest(template_img_reshape, 1, finded_symbol_img);
		}
		catch (const std::exception &)
		{
			// TODO: warning
			return -1;
		}
	}

	auto symbol = static_cast<char>(res);

	if (symbol >= '0' && symbol <= '9')
		return static_cast<int>(symbol - '0');

	return -1;
}

void Ocr::Train()
{	
	std::array<std::vector<cv::Mat>, 10> glyphs;

	for (auto& it : std::filesystem::directory_iterator(mTrainDir))
		if (it.path().has_extension() && it.path().extension() == ".png")
		{
			const std::string file_name = it.path().filename().replace_extension().string();
			if (!file_name.empty() && file_name[0] >= '0' && file_name[0] <= '9')
			{
				glyphs[file_name[0] - '0'].emplace_back(utility::Imread(it.path(), cv::IMREAD_GRAYSCALE));
			}
			else
			{
				// TODO: warning
			}
		}

	if (false)
	{
		cv::Size mean_size;
		int glyphs_count = 0;
		for (const auto& v : glyphs)
			for (const auto& m : v)
			{
				mean_size += m.size();
				++glyphs_count;
			}
		if (glyphs_count)
			mean_size = mean_size /= glyphs_count;
	}

	cv::Mat array_chars;
	cv::Mat array_images;
	for (size_t w = 0; w < glyphs.size(); ++w)
		for (const auto &img: glyphs[w])
		{
			cv::Mat template_img;
			cv::resize(img, template_img, gTemplateSymbolSize);
			cv::Mat template_img_float;
			template_img.convertTo(template_img_float, CV_32FC1);
			cv::Mat template_img_reshape = template_img_float.reshape(1, 1);

			array_images.push_back(template_img_reshape);
			array_chars.push_back('0' + static_cast<int>(w));
		}

	std::scoped_lock lock(mKnearesMutex);
	if (!array_images.empty())
	{
		mKNearest = cv::ml::KNearest::create();
		mKNearest->setDefaultK(1);
		if (!mKNearest->train(array_images, cv::ml::ROW_SAMPLE, array_chars))
		{
			// TODO: warning
		}	
	}
}

cv::Mat Ocr::Preprocess(const cv::Mat& img) const
{
	cv::Mat gray;
	cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

	cv::Mat filter;
	cv::inRange(gray, mParams.gray_threshold, 255, filter);

	cv::Mat out;
	cv::bitwise_and(gray, gray, out, filter);

	return out;
}

std::vector<cv::Rect> Ocr::FindGlyphs(const cv::Mat& img) const
{
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours(img, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

	std::vector<cv::Rect> out;
	for (const auto& contour : contours)
	{
		auto brect = cv::boundingRect(contour);
		if (brect.size().width < mParams.glyph_min_width)
			continue;
		if (brect.size().height < mParams.glyph_min_height)
			continue;
		if (brect.area() < mParams.glyph_min_area)
			continue;
		
		out.push_back(brect);
	}

	std::sort(out.begin(), out.end(), [](const cv::Rect& a, const cv::Rect& b)
		{
			return (a.br() + a.tl()).x / 2 < (b.br() + b.tl()).x / 2;
		});
	return out;
}

cv::Mat Ocr::DrawRects(const cv::Mat& img, const std::vector<cv::Rect>& rects) const
{
	cv::Mat out;
	img.copyTo(out);
	for (const auto & rect : rects)
		cv::rectangle(out, rect, cv::Scalar(255, 0, 0), cv::FILLED);
	return out;
}

void Ocr::LoadDigits()
{
	for (size_t i = 0; i <= 9; ++i)
	{
		const auto path = mDigitsDir / (std::to_string(i) + ".png");
		if (std::filesystem::exists(path))
			mDigitsMat[i] = utility::Imread(path);
	}
}

/*


using Chunks = std::vector<std::tuple<cv::Mat, cv::Mat, cv::Mat>>;

struct Stats
{
	int glyps_count = 0;

};

namespace
{
	int GlyphsCount(const Chunks& chunks, const Ocr::Params& settings)
	{
		int res = 0;
		Ocr ocr("", "", "", "");
		ocr.SetParams(settings);
		for (const auto& [img, pre, glyps] : chunks)
		{
			const auto p = ocr.Preprocess(img);
			const auto g = ocr.FindGlyphs(p);
			res += static_cast<decltype(res)>(g.size());
		}

		return res;
	}

	cv::Mat Process(Chunks& chunks, const Ocr::Params & settings, Stats & stats)
	{
		Ocr ocr("", "", "", "");
		ocr.SetParams(settings);
		for (auto& [img, pre, glyps] : chunks)
		{
			auto p = ocr.Preprocess(img);
			const auto g = ocr.FindGlyphs(p);
			stats.glyps_count += static_cast<decltype(stats.glyps_count)>(g.size());

			cv::cvtColor(p, pre, cv::COLOR_GRAY2BGR);
			glyps = ocr.DrawRects(pre, g);
		}

		cv::Size page_size;
		for (const auto& chunk : chunks)
		{
			const auto& img = std::get<0>(chunk);
			const auto size = img.size();
			if (page_size.width < size.width)
				page_size.width = size.width;
			page_size.height += size.height;
		}

		cv::Mat page_img;

		if (!page_size.empty())
		{
			const auto column_size = page_size.width;
			page_size.width *= std::tuple_size_v<Chunks::value_type>;

			page_img = cv::Mat::zeros(page_size, CV_8UC3);

			int h = 0;
			for (const auto& chunk : chunks)
			{
				auto add = [&](int i, const cv::Mat& img)
				{
					const auto size = img.size();
					if (!size.empty())
					{
						cv::Rect r = cv::Rect(column_size * i, h, size.width, size.height);
						img.copyTo(cv::Mat(page_img, r));
					}
				};
				std::apply([&add](auto&&... args)
					{
						int n = 0;
				((add(n++, args)), ...);
					}, chunk);
				h += std::get<0>(chunk).size().height;
			}
		}

		return page_img;
	}

}


// Gray TH										��� ����, ��� ����� (������ ������)
// ����������� ������ �����		��� ������, ��� �����
// ����������� ������ �����   ��� ������, ��� �����
// ����������� ������� �����  ��� ������, ��� �����

int ConsoleTrain::Entry(size_t count)
{
	Chunks chunks;
	Ocr::Params ocr_settings;
	constexpr Ocr::Params gui_offsets = { 0, 0, 0, 0 };
	constexpr Ocr::Params gui_max = { 255, 30, 30, 150 };

	Stats stats;

	{
		struct FileStats
		{
			std::filesystem::path path;
			size_t file_size;
			std::string data;
		};
		struct FileStatsCmp
		{
			bool operator()(const FileStats& a, const FileStats& b) const
			{
				return (a.file_size < b.file_size) || ((!(b.file_size < a.file_size)) && (a.data < b.data));
			}
		};

		std::set<FileStats, FileStatsCmp> file_stats;

		for (auto& it : std::filesystem::directory_iterator(mProfile.FindPath("train_data_dir")))
			if (it.path().has_extension() && it.path().extension() == ".png")
			{
				const auto& path = it.path();
				size_t size = std::filesystem::file_size(path);
				std::ifstream stream(path, std::ifstream::binary);
				if (stream)
				{
					std::string buffer;
					stream.seekg(0, stream.end);
					buffer.resize(stream.tellg());
					stream.seekg(0, stream.beg);
					stream.read(buffer.data(), buffer.size());
					stream.close();
					FileStats fstats{ path, size, std::move(buffer) };
					if (file_stats.find(fstats) == file_stats.end())
					{
						file_stats.emplace(fstats);
					}
					else
					{
						std::filesystem::remove(path);
					}
				}
			}

	}


	for (auto& it : std::filesystem::directory_iterator(mProfile.FindPath("train_data_dir")))
		if (it.path().has_extension() && it.path().extension() == ".png")
		{
			chunks.emplace_back();
			std::get<0>(chunks.back()) = utility::Imread(it.path());
		}
	auto page_img = Process(chunks, ocr_settings, stats);

	cv::namedWindow("Settings", cv::WINDOW_AUTOSIZE);
	cv::resizeWindow("Settings", cv::Size(600, 280));
	cv::createTrackbar("Gray TH", "Settings", 0, gui_max.gray_threshold - gui_offsets.gray_threshold, [](int pos, void* val) { *reinterpret_cast<int*>(val) = pos + gui_offsets.gray_threshold; }, &ocr_settings.gray_threshold);
	cv::createTrackbar("Gl MW", "Settings", 0, gui_max.glyph_min_width - gui_offsets.glyph_min_width, [](int pos, void* val) { *reinterpret_cast<int*>(val) = pos + gui_offsets.glyph_min_width; }, &ocr_settings.glyph_min_width);
	cv::createTrackbar("Gl MH", "Settings", 0, gui_max.glyph_min_height - gui_offsets.glyph_min_height, [](int pos, void* val) { *reinterpret_cast<int*>(val) = pos + gui_offsets.glyph_min_height; }, &ocr_settings.glyph_min_height);
	cv::createTrackbar("Gl Area", "Settings", 0, gui_max.glyph_min_area - gui_offsets.glyph_min_area, [](int pos, void* val) { *reinterpret_cast<int*>(val) = pos + gui_offsets.glyph_min_area; }, &ocr_settings.glyph_min_area);
	cv::createTrackbar("Gl cnt", "Settings", 0, 10000);

	cv::setTrackbarPos("Gray TH", "Settings", ocr_settings.gray_threshold - gui_offsets.gray_threshold);
	cv::setTrackbarPos("Gl MW", "Settings", ocr_settings.glyph_min_width - gui_offsets.glyph_min_width);
	cv::setTrackbarPos("Gl MH", "Settings", ocr_settings.glyph_min_height - gui_offsets.glyph_min_height);
	cv::setTrackbarPos("Gl Area", "Settings", ocr_settings.glyph_min_area - gui_offsets.glyph_min_area);

	int scrolHight = 0;
	int scrolWidth = 0;
	int winH = 900;
	int winW = 1000;
	{
		if (winH >= page_img.rows) winH = page_img.rows - 1;
		if (winW >= page_img.cols) winW = page_img.cols - 1;

		cv::createTrackbar("Hscroll", "Settings", &scrolHight, (page_img.rows - winH));
		cv::createTrackbar("Wscroll", "Settings", &scrolWidth, (page_img.cols - winW));
	}

	if (false)
	{
		constexpr Ocr::Params start = { 0, 0, 0, 0 };
		constexpr Ocr::Params stop = { 255, 20, 20, 200 };

		int glyps_count = 936;
		std::vector<Ocr::Params> combinations_in;
		std::vector<Ocr::Params> combinations_out;

		for (int t = start.gray_threshold; t <= stop.gray_threshold; ++t)
			for (int w = start.glyph_min_width; w <= stop.glyph_min_width; ++w)
				for (int h = start.glyph_min_height; h <= stop.glyph_min_height; ++h)
					for (int a = start.glyph_min_area; a <= stop.glyph_min_area; ++a)
					{
						combinations_in.emplace_back(t, w, h, a);
					}

		std::atomic<size_t> progress = 0;
		constexpr size_t threads_count = 4;
		std::vector<std::jthread> threads;
		threads.reserve(threads_count);
		std::mutex mutex;
		for (size_t tn = 0; tn < threads_count; tn++)
		{
			threads.emplace_back(std::jthread([&, thread_num = tn]()
				{
					for (size_t i = 0; i < combinations_in.size(); ++i)
					{
						if ((i % threads_count) == thread_num)
						{
							if (GlyphsCount(chunks, combinations_in[i]) == glyps_count)
							{
								std::lock_guard lock(mutex);
								combinations_out.emplace_back(combinations_in[i]);
							}
							++progress;
						}
					}
				}));
		}

		int progress_scroll = 0;
		cv::createTrackbar("Progress", "Settings", &progress_scroll, 100);
		while (cv::waitKey(1) == -1)
		{
			progress_scroll = static_cast<int>(((progress + 1) * 100) / combinations_in.size());
			cv::setTrackbarPos("Progress", "Settings", progress_scroll);

			if (progress == combinations_in.size())
				break;
		}

		std::vector<size_t> gray_threshold_stats(stop.gray_threshold + 1);
		std::vector<size_t> glyph_min_width_stats(stop.glyph_min_width + 1);
		std::vector<size_t> glyph_min_height_stats(stop.glyph_min_height + 1);
		std::vector<size_t> glyph_min_area_stats(stop.glyph_min_area + 1);

		for (const auto & s : combinations_out)
		{
			++gray_threshold_stats[s.gray_threshold];
			++glyph_min_width_stats[s.glyph_min_width];
			++glyph_min_height_stats[s.glyph_min_height];
			++glyph_min_area_stats[s.glyph_min_area];
		}

		std::ofstream file(mProfile.FindPath("train_data_dir") / "stat.txt");

		auto &stream = file;

		for (size_t i = 0; i < gray_threshold_stats.size(); i++)
			stream << "T " << std::setw(5) << i << " " << gray_threshold_stats[i] << std::endl;
		stream << std::endl;
		for (size_t i = 0; i < glyph_min_width_stats.size(); i++)
			stream << "W " << std::setw(5) << i << " " << glyph_min_width_stats[i] << std::endl;
		stream << std::endl;
		for (size_t i = 0; i < glyph_min_height_stats.size(); i++)
			stream << "H " << std::setw(5) << i << " " << glyph_min_height_stats[i] << std::endl;
		stream << std::endl;
		for (size_t i = 0; i < glyph_min_area_stats.size(); i++)
			stream << "A " << std::setw(5) << i << " " << glyph_min_area_stats[i] << std::endl;
		stream << std::endl;
	}


	Ocr::Params last_settings = { 0 };
	while (true)
	{
		auto k = cv::getWindowProperty("Settings", cv::WND_PROP_AUTOSIZE);
		if (k != 1)
			break;
 		//if (cv::getWindowProperty("Settings", 0) < 0)
 		//	break;

		if (cv::waitKey(1) != -1)
			break;

		last_settings = ocr_settings;
		stats = {};
		page_img = Process(chunks, ocr_settings, stats);
		cv::setTrackbarPos("Gl cnt", "Settings", stats.glyps_count);

		if (last_settings != ocr_settings)
		{
			last_settings = ocr_settings;
			utility::Imwrite(mProfile.FindPath("train_data_dir") / "res" / "full.png", page_img);
		}

		cv::Mat out_img = page_img(cv::Rect(scrolWidth, scrolHight, winW, winH));
		cv::imshow("page_img", out_img);
	}

	return 0;
}



*/
