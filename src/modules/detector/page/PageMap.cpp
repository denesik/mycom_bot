#include "PageMap.h"

#include "ImageComparator.h"
#include "VerifierCompare.h"
#include "VerifierFind.h"
#include "BlockButton.h"
#include "BlockNumber.h"
#include "BlockState.h"
#include "BlockImage.h"
#include "BlockIcon.h"
#include "BlockLine.h"

PageMap::PageMap(std::shared_ptr<Ocr> ocr)
	: mOcr(ocr)
{
	std::shared_ptr<ImageComparator> comparator = std::make_shared<ImageComparator>();

	{
		auto part = std::make_shared<VerifierCompare>(comparator);
		mPageVerifiers.emplace(part->GetType(), std::move(part));
	}
	{
		auto part = std::make_shared<VerifierFind>(comparator);
		mPageVerifiers.emplace(part->GetType(), std::move(part));
	}

	{
		auto part = std::make_shared<BlockButton>();
		mPageBlocks.emplace(part->GetType(), std::move(part));
	}
	{
		auto part = std::make_shared<BlockLine>();
		mPageBlocks.emplace(part->GetType(), std::move(part));
	}
	{
		auto part = std::make_shared<BlockNumber>(mOcr);
		mPageBlocks.emplace(part->GetType(), std::move(part));
	}
	{
		auto part = std::make_shared<BlockState>(comparator);
		mPageBlocks.emplace(part->GetType(), std::move(part));
	}
	{
		auto part = std::make_shared<BlockImage>(comparator);
		mPageBlocks.emplace(part->GetType(), std::move(part));
	}
	{
		auto part = std::make_shared<BlockIcon>(comparator);
		mPageBlocks.emplace(part->GetType(), std::move(part));
	}
}

const IPageVerifier* PageMap::Verifiers(const std::string& type) const
{
	auto it = mPageVerifiers.find(type);
	if (it != mPageVerifiers.end())
		return it->second.get();
	return nullptr;
}

const IPageBlock* PageMap::Blocks(const std::string& type) const
{
	auto it = mPageBlocks.find(type);
	if (it != mPageBlocks.end())
		return it->second.get();
	return nullptr;
}
