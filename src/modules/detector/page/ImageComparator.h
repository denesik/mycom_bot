#pragma once

#include <opencv2\core\mat.hpp>

#include <opencv2\core\types.hpp>


class ImageComparator 
{
public:
	ImageComparator();
	~ImageComparator() = default;

	ImageComparator(const ImageComparator& other) = delete;
	ImageComparator(ImageComparator&& other) noexcept = delete;
	ImageComparator& operator=(const ImageComparator& other) = delete;
	ImageComparator& operator=(ImageComparator&& other) noexcept = delete;


	bool Compare(const cv::Mat& img, const cv::Mat& templ, double &err, double compare_error = 0.01);

	bool Find(const cv::Mat& img, const cv::Mat& templ, cv::Rect& rect, double& err, double compare_error = 0.01);

	size_t Find(const cv::Mat& img, const cv::Mat& templ, std::vector<std::pair<cv::Rect, double>>& out, double compare_error = 0.01);


	bool Compare(const cv::Mat& img, const cv::Mat& templ, cv::InputArray mask, double& err, double compare_error = 0.01);

	bool Find(const cv::Mat& img, const cv::Mat& templ, cv::InputArray mask, cv::Rect& rect, double& err, double compare_error = 0.01);

	size_t Find(const cv::Mat& img, const cv::Mat& templ, cv::InputArray mask, std::vector<std::pair<cv::Rect, double>>& out, double compare_error = 0.01);

private:
	void ReserveBuffer(const cv::Mat& img, const cv::Mat& templ);

private:
	cv::Mat mBuffer;
	std::vector<std::pair<cv::Rect, double>> mData;

};
