#include "EmulatorUtility.h"

#include <windows.h>
#include <psapi.h>
#include "MultiWideUtf8.h"

std::string EmulatorUtility::GetExeNameFromWindowHandle(void* handle)
{
	DWORD dwProcId = 0;
	GetWindowThreadProcessId((HWND)handle, &dwProcId);

	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcId);

	if (hProcess != NULL)
	{
		wchar_t filePath[MAX_PATH];
		if (GetModuleFileNameEx(hProcess, NULL, filePath, MAX_PATH))
		{
			auto out = std::wstring(filePath);
			CloseHandle(hProcess);
			return utf_helper::multiwide_to_utf8(out);
		}
		CloseHandle(hProcess);
	}

	return "";
}

bool EmulatorUtility::CheckEmulatorTypePath(std::string_view path, EmulatorType type)
{
	std::string name = EmulatorTypeToExe(type);
	if (path.size() < name.size())
		return false;

	std::string short_path(path.end() - name.size(), path.end());
	return name == short_path;
}
