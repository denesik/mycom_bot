#pragma once

#include "IModuleBuilder.h"

class DetectorModuleBuilder : public IModuleBuilder
{
public:

	virtual void AddComponents(const Profile& profile, Actor* actor) override;

};


