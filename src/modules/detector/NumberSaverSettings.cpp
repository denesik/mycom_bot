#include "NumberSaverSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "NumberSaver.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

#undef GetObject

NumberSaverSettings::NumberSaverSettings(std::shared_ptr<NumberSaver> number_saver, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mNumberSaver(number_saver), mCorePath(core_path), mUserPath(user_path)
{

}

void NumberSaverSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void NumberSaverSettings::BeginPlay()
{
	mNumberSaverConnection = mNumberSaver->signal_data.connect([this]() { Save(mUserPath); });
}

void NumberSaverSettings::EndPlay()
{
	mNumberSaverConnection.disconnect();
}

void NumberSaverSettings::Deinitialize()
{
	Save(mUserPath);
}

void NumberSaverSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("number_saver");
		if (it != json.MemberEnd() && it->value.IsBool())
			mNumberSaver->SetEnable(it->value.GetBool());
	}
}

void NumberSaverSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("number_saver");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = mNumberSaver->GetEnable();
		else
			json.AddMember("number_saver", mNumberSaver->GetEnable(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


