#include "Controller.h"

#include "NoxController.h"
#include "BluestacksController.h"
#include "MemuController.h"

#include "Emulator.h"

#include <windows.h>
#include <filesystem>
#include <tlhelp32.h>
#include <psapi.h>

#include "utf_helper/MultiWideUtf8.h"

// ��� ������� �� ESC �� ��������� ������ ����.

Controller::Controller(std::shared_ptr<const Emulator> emulator)
	: mEmulator(emulator)
{
	mControllers[static_cast<size_t>(EmulatorType::NOX)] = std::make_unique<NoxController>();
	mControllers[static_cast<size_t>(EmulatorType::BLUESTACKS)] = std::make_unique<BluestacksController>();
	mControllers[static_cast<size_t>(EmulatorType::MEMU)] = std::make_unique<MemuController>();
}

bool Controller::Start() const
{
	EmulatorType type = mEmulator->GetType();
	std::wstring app = utf_helper::utf8_to_multiwide(mEmulator->GetApplication());
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());
	int instance = mEmulator->GetInstance();
	std::wstring path = utf_helper::utf8_to_multiwide(mEmulator->GetPath());

	if (static_cast<size_t>(type) < static_cast<size_t>(EmulatorType::UNKNOWN_TYPE))
		return mControllers[static_cast<size_t>(type)]->Start(path, instance, name, app);

	return false;
}

bool Controller::Stop() const
{
	EmulatorType type = mEmulator->GetType();
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());

	if (static_cast<size_t>(type) < static_cast<size_t>(EmulatorType::UNKNOWN_TYPE))
		return mControllers[static_cast<size_t>(type)]->Stop(name);

	return false;
}

bool Controller::Click(int x, int y) const
{
	EmulatorType type = mEmulator->GetType();
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());

	if (static_cast<size_t>(type) < static_cast<size_t>(EmulatorType::UNKNOWN_TYPE))
		return mControllers[static_cast<size_t>(type)]->Click(name, x, y);

	return false;
}

bool Controller::Move(int start_x, int start_y, int stop_x, int stop_y) const
{
	EmulatorType type = mEmulator->GetType();
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());

	if (static_cast<size_t>(type) < static_cast<size_t>(EmulatorType::UNKNOWN_TYPE))
		return mControllers[static_cast<size_t>(type)]->Move(name, start_x, start_y, stop_x, stop_y);

	return false;
}

bool Controller::PressEscape() const
{
	EmulatorType type = mEmulator->GetType();
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());

	if (static_cast<size_t>(type) < static_cast<size_t>(EmulatorType::UNKNOWN_TYPE))
		return mControllers[static_cast<size_t>(type)]->PressEscape(name);

	return false;
}

bool Controller::ShowWindow() const
{
	EmulatorType type = mEmulator->GetType();
	std::wstring name = utf_helper::utf8_to_multiwide(mEmulator->GetWindowName());

	if (static_cast<size_t>(type) < static_cast<size_t>(EmulatorType::UNKNOWN_TYPE))
		return mControllers[static_cast<size_t>(type)]->ShowWindow(name);

	return false;
}


bool Controller::CheckPath() const
{
	EmulatorType type = mEmulator->GetType();
	std::string path = mEmulator->GetPath();

	return CheckPath(type, path);
}

bool Controller::CheckPath(EmulatorType type, const std::string& path)
{
	std::filesystem::path fpath(utf_helper::utf8_to_multiwide(path));
	auto fname = fpath.filename().wstring();
	//std::wstring resfname;
	//std::transform(fname.begin(), fname.end(), std::back_inserter(resfname), tolower);
	if (fname != utf_helper::utf8_to_multiwide(EmulatorTypeToExe(type)))
		return false;

	if (!std::filesystem::exists(utf_helper::utf8_to_multiwide(path)))
		return false;

	return true;
}

std::string Controller::FindWindowPath() const
{
	EmulatorType type = mEmulator->GetType();

	return FindWindowPath(type);
}

std::string Controller::FindWindowPath(EmulatorType type)
{
	std::wstring out;

	PROCESSENTRY32 peProcessEntry;
	HANDLE CONST hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapshot)
		return utf_helper::multiwide_to_utf8(out);

	peProcessEntry.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hSnapshot, &peProcessEntry);
	do
	{
		if (std::wstring(peProcessEntry.szExeFile) == utf_helper::utf8_to_multiwide(EmulatorTypeToExe(type)))
			if (peProcessEntry.th32ProcessID != 0)
			{
				HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, peProcessEntry.th32ProcessID);
				if (hProcess != NULL)
				{
					wchar_t filePath[MAX_PATH];
					if (GetModuleFileNameEx(hProcess, NULL, filePath, MAX_PATH))
					{
						out = std::wstring(filePath);
						CloseHandle(hProcess);
						return utf_helper::multiwide_to_utf8(out);
					}

					CloseHandle(hProcess);
					break;
				}
			}
	} while (Process32Next(hSnapshot, &peProcessEntry));

	CloseHandle(hSnapshot);

	return utf_helper::multiwide_to_utf8(out);
}

namespace
{
	struct handle_data 
	{
		unsigned long process_id;
		HWND window_handle;
	};

	BOOL is_main_window(HWND handle)
	{
		return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
	}

	BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam)
	{
		handle_data& data = *(handle_data*)lParam;
		unsigned long process_id = 0;
		GetWindowThreadProcessId(handle, &process_id);
		if (data.process_id != process_id || !is_main_window(handle))
			return TRUE;
		data.window_handle = handle;
		return FALSE;
	}
};

#undef FindWindow

std::string Controller::FindWindowTitle() const
{
	EmulatorType type = mEmulator->GetType();

	return FindWindowTitle(type);
}

std::string Controller::FindWindowTitle(EmulatorType type)
{
	PROCESSENTRY32 peProcessEntry;
	HANDLE CONST hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapshot)
		return "";

	peProcessEntry.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hSnapshot, &peProcessEntry);
	do
	{
		if (std::wstring(peProcessEntry.szExeFile) == utf_helper::utf8_to_multiwide(EmulatorTypeToExe(type)))
			if (peProcessEntry.th32ProcessID != 0)
			{
				handle_data data;
				data.process_id = peProcessEntry.th32ProcessID;
				data.window_handle = NULL;
				EnumWindows(enum_windows_callback, (LPARAM)&data);

				if (data.window_handle)
				{
					std::wstring title;
					auto size = size_t(GetWindowTextLength(data.window_handle));
					title.resize(size + 1);
					GetWindowText(data.window_handle, const_cast<WCHAR*>(title.c_str()), int(size + 1));

					title.resize(size);

					return utf_helper::multiwide_to_utf8(title);
				}
			}
	} while (Process32Next(hSnapshot, &peProcessEntry));

	CloseHandle(hSnapshot);

	return "";
}
