#include "EmulatorType.h"

#include <string>

const char* EmulatorTypeToName(EmulatorType type)
{
	switch (type)
	{
	case EmulatorType::NOX:
		return "nox";
		break;
	case EmulatorType::BLUESTACKS:
		return "bluestacks";
		break;
	case EmulatorType::MEMU:
		return "memu";
		break;
	case EmulatorType::UNKNOWN_TYPE:
		break;
	default:
		break;
	}
	return "";
}

EmulatorType EmulatorTypeFromName(const char* name)
{
	std::string str(name);
	if (str == "nox")
		return EmulatorType::NOX;
	if (str == "bluestacks")
		return EmulatorType::BLUESTACKS;
	if (str == "memu")
		return EmulatorType::MEMU;

	return EmulatorType::UNKNOWN_TYPE;
}

const char* EmulatorTypeToExe(EmulatorType type)
{
	switch (type)
	{
	case EmulatorType::NOX:
		return "Nox.exe";
		break;
	case EmulatorType::BLUESTACKS:
		return "HD-Player.exe";
		break;
	case EmulatorType::MEMU:
		return "MEmu.exe";
		break;
	case EmulatorType::UNKNOWN_TYPE:
		break;
	default:
		break;
	}
	return "";
}

const char* EmulatorTypeToDefaultWindowName(EmulatorType type)
{
	switch (type)
	{
	case EmulatorType::NOX:
		return "NoxPlayer";
		break;
	case EmulatorType::BLUESTACKS:
		return "BlueStacks";
		break;
	case EmulatorType::MEMU:
		return "MEmu";
		break;
	case EmulatorType::UNKNOWN_TYPE:
		break;
	default:
		break;
	}
	return "";
}
