#pragma once

#include <array>
#include <memory>

#include "EmulatorType.h"
#include "IEmulatorController.h"

class Emulator;


class Controller
{
public:
	Controller(std::shared_ptr<const Emulator> emulator);

	bool Start() const;

	bool Stop() const;

	bool Click(int x, int y) const;

	bool Move(int start_x, int start_y, int stop_x, int stop_y) const;

	bool PressEscape() const;

	bool ShowWindow() const;


	bool CheckPath() const;

	static bool CheckPath(EmulatorType type, const std::string& path);
	static std::string FindWindowPath(EmulatorType type);
	static std::string FindWindowTitle(EmulatorType type);

	std::string FindWindowPath() const;
	std::string FindWindowTitle() const;

private:
	std::shared_ptr<const Emulator> mEmulator;

	std::array<std::unique_ptr<IEmulatorController>, static_cast<size_t>(EmulatorType::UNKNOWN_TYPE)> mControllers;
};


