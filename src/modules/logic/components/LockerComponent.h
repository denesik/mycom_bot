#pragma once

#include <map>
#include <vector>
#include <string>
#include <array>
#include <sigslot/signal.hpp>

#include "ILogicComponent.h"
#include "Timer.h"
#include "LogicData.h"
#include "TimeDayZone.h"

class LockerComponent : public ILogicComponent
{
public:
	LockerComponent() = default;

	sigslot::signal<> signal_data;

	enum class RecordType
	{
		UnavaliableTask = 0,
		SuccessTask = 1,
		RequirementTask = 2,
	};

	void AddRecord(RecordType type, time_t time, std::string_view name);
	size_t CountRecords(RecordType type) const;
	time_t GetRecordTime(RecordType type, size_t index) const;
	const std::string &GetRecordName(RecordType type, size_t index) const;

	virtual void BeginComponent(std::string_view program) override;

	virtual void Tick() override;

	virtual void EndComponent(LogicResult result) override;

public:
	bool AllowedWithoutResources(std::string_view name, const LogicData& config) const;
	bool AllowedWithResources(std::string_view name, const LogicData& config) const;

private:
	void SetUnavailable(std::string_view name);
	void SetSuccess(std::string_view name);
	void SetRequirement(std::string_view name);
	void ResetRequirement(std::string_view name);

	bool IsFullLock(std::string_view name, const LogicData& config) const;

	bool IsLockedTask(std::string_view name) const;

	size_t SuccessTasksToday(std::string_view name) const;
	size_t SuccessTasksZone(std::string_view name) const;

	size_t ResourceRequestCount(int timeleft_minutes = 240) const;
	bool ResourceRequested(std::string_view name, int timeleft_minutes = 240) const;

private:

	bool RemoveOldRecords(RecordType type);

private:

	struct Record
	{
		time_t time = 0;
		std::string name;
	};
	std::array<std::vector<Record>, 3> mData;
	std::vector<Record> &mUnavaliableTasks = mData[static_cast<std::underlying_type<RecordType>::type>(RecordType::UnavaliableTask)];
	std::vector<Record> &mSuccessTasks = mData[static_cast<std::underlying_type<RecordType>::type>(RecordType::SuccessTask)];
	std::vector<Record> &mRequirementTasks = mData[static_cast<std::underlying_type<RecordType>::type>(RecordType::RequirementTask)];

	TimeDayZone mTimeDayZone;
	Timer mTimer;
	std::string mProgramName;
};
