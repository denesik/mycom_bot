#pragma once

#include "ILogicComponent.h"
#include "AsyncExecutor.h"

#include "Controller.h"

#include <memory>

class Detector;
class Emulator;
class Informer;

class ActionWrapper : public ILogicComponent
{
public:
	ActionWrapper(std::shared_ptr<Detector> detector, std::shared_ptr<Emulator> emulator, std::shared_ptr<Informer> informer);
	~ActionWrapper();

	bool IsBusy() const;

	void Move(const std::string& name) const;

	void PageButtonPress(int index) const;
	void PageButtonPress(const std::string& name) const;
	void PageButtonPress(const std::string& name, int index) const;

	void EmulatorStart() const;
	void EmulatorStop() const;

	void PressEscape() const;
	void ShowWindow() const;

	void PressIcon(std::string_view name, size_t index = 0) const;
	void PressImage(std::string_view name, size_t index = 0) const;

private:
	std::shared_ptr<Detector> mDetector;
	Controller mController;

	std::unique_ptr<AsyncExecutor> mAsyncExecutor;
	std::shared_ptr<Informer> mInformer;
};

