#include "ArenaAlgorithmDown.h"
#include <algorithm>


int ArenaAlgorithmDown::GetAttackPos(const std::vector<PowerInfo> &data) const
{
	auto available = arena_utility::GetAllSorted(data);

	auto phase = arena_utility::GetPhase(data);
	if (!arena_utility::IsValidPhase(phase))
		return 0;

	auto attack_pos = 4 - phase;

	if (attack_pos < static_cast<int>(available.size()))
		return available[attack_pos].pos;

	if (!available.empty())
		return available.front().pos;

	return 0;
}

