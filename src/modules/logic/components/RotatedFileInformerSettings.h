#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class RotatedFileInformerSink;

class RotatedFileInformerSettings : public ActorComponent
{
public:
	RotatedFileInformerSettings(std::shared_ptr<RotatedFileInformerSink> informer, const std::filesystem::path& core_path, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mCorePath;
	const std::filesystem::path mUserPath;

	std::shared_ptr<RotatedFileInformerSink> mInformer;

	sigslot::connection mInformerConnection;
};


