#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class LockerComponent;

class LockerSettings : public ActorComponent
{
public:
	LockerSettings(std::shared_ptr<LockerComponent> journal, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mUserPath;

	std::shared_ptr<LockerComponent> mJournal;

	sigslot::connection mJournalConnection;
};


