#pragma once

#include <vector>

// ������ ��������� ������������ ���� �������� ���, ��� �� ������� ����, � ���� �������� ����.
// ������� ��������� ������������ ���� �������� ���, ��� �� ������� �� ����, � ���� �������� ����.
// ���� ������, ��� �������� ����.
// ������� �� ������, ��� �������� ����.
struct PowerInfo
{
	enum class Status : int
	{
		UNKNOWN,
		IS_ME,
		AVAILABLE,
		VICTORY,
		DEFEAT,
	};

	int pos = -1;
	Status status = Status::UNKNOWN;
	int my_power_min = 0;
	int my_power_max = 0;
	int other_power = 0;
	int blacklist_count = 0;
};

namespace arena_utility
{
	bool IsValidPowers(const std::vector<PowerInfo>& data);

	int GetPhase(const std::vector<PowerInfo>& data);

	bool IsValidPhase(int phase);

	int GetMyPower(const std::vector<PowerInfo>& data);

	// ����� ���� ������� � �������.
	int FindMe(const std::vector<PowerInfo>& data);

	// ������� ��� �� ���������
	int CountDefeat(const std::vector<PowerInfo>& data);

	// ������ ���� �����������, ������� ����� ����� �� ������ ������.
	std::vector<PowerInfo> GetAllSorted(const std::vector<PowerInfo>& data);

	// ������ ���� �����������, �������� ���, ��� ����� � ������ ������, ������� ����� ����� �� ������ ������.
	std::vector<PowerInfo> GetAllWithoutBlacklistSorted(const std::vector<PowerInfo>& data);

	// ������ ���� ������ �����������, ������� ����� ����� �� ������ ������.
	std::vector<PowerInfo> GetWeakSorted(const std::vector<PowerInfo>& data);

	// ������ ������ ����������� ���� ����, ������� ����� ����� �� ������ ������.
	std::vector<PowerInfo> GetWeakAboveSorted(const std::vector<PowerInfo>& data);

	// ������ ������ ����������� ���� ����, ������� ����� ����� �� ������ ������.
	std::vector<PowerInfo> GetWeakBelowSorted(const std::vector<PowerInfo>& data);

	// ������ ������ ����������� ���� ����, ������� �� ����� ����� ���� �� ���� ����.
	std::vector<PowerInfo> GetWeakAboveMeSmallSorted(const std::vector<PowerInfo>& data);

	// ������ ������ ����������� ���� ����, ������� ����� ����� ���� �� ���� ����.
	std::vector<PowerInfo> GetWeakAboveMeBigSorted(const std::vector<PowerInfo>& data);

	// ���������� ������ ����������� ���� 
	int CountWeatAbove(const std::vector<PowerInfo>& data, int pos);

	// ���������� ������� ����������� 
	int CountStrong(const std::vector<PowerInfo>& data);
}


class IArenaAlgorithm
{
public:
	virtual ~IArenaAlgorithm() = default;

	virtual int GetAttackPos(const std::vector<PowerInfo> &data) const = 0;
};
