#pragma once

#include "ILogicComponent.h"

#include <opencv2\core\mat.hpp>
#include <filesystem>
#include <sigslot/signal.hpp>

#include "ffmpeg\ffmpeg_encode.h"
#include "utility\FileTimeleftController.h"

class Detector;

class VideoLoggerComponent : public ILogicComponent
{
public:
	VideoLoggerComponent(const std::filesystem::path &path, std::shared_ptr<Detector> detector);

	sigslot::signal<> signal_data;

	void SetEnable(bool value);
	bool GetEnable() const;

	void SetStoreTime(unsigned int hours);
	unsigned int GetStoreTime() const;

	void SetLevel(LogicResult level);
	LogicResult GetLevel() const;


	virtual void BeginComponent(std::string_view program) override;

	virtual void EndComponent(LogicResult result) override;

	void SetImage(const cv::Mat &img);

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

private:
	std::string GenerateName(std::string_view program);

private:
	std::filesystem::path mDirectory;

	cv::Mat mImage;

	FfmpegEncoder mFfmpegEncoder;

	FileTimeleftController mFileTimeleftController;
	bool mEnable = false;
	LogicResult mLevel = LogicResult::All;

	std::string mRunnedName;
	bool mRunned = false;

	std::shared_ptr<Detector> mDetector;

	sigslot::connection mConnectionDetect;
};

