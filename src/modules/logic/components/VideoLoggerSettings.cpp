#include "VideoLoggerSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "VideoLoggerComponent.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"
#include "LogicResultJson.h"

#undef GetObject

VideoLoggerSettings::VideoLoggerSettings(std::shared_ptr<VideoLoggerComponent> informer, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mInformer(informer), mCorePath(core_path), mUserPath(user_path)
{

}

void VideoLoggerSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void VideoLoggerSettings::BeginPlay()
{
	mInformerConnection = mInformer->signal_data.connect([this]() { Save(mUserPath); });
}

void VideoLoggerSettings::EndPlay()
{
	mInformerConnection.disconnect();
}

void VideoLoggerSettings::Deinitialize()
{
	Save(mUserPath);
}

void VideoLoggerSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("video_logger");
		if (it != json.MemberEnd() && it->value.IsObject())
		{
			auto section_json = it->value.GetObject();
			{
				auto jt = section_json.FindMember("enable");
				if (jt != section_json.MemberEnd() && jt->value.IsBool())
					mInformer->SetEnable(jt->value.GetBool());
			}
			{
				auto jt = section_json.FindMember("store_time");
				if (jt != section_json.MemberEnd() && jt->value.IsInt())
					mInformer->SetStoreTime(jt->value.GetInt());
			}

			{
				auto value = mInformer->GetLevel();
				auto jt = section_json.FindMember("level");
				if (jt != section_json.MemberEnd() && jt->value.IsObject())
				{
					LogicResultJson().Serialize(jt->value, value);
					mInformer->SetLevel(value);
				}
			}
		}
	}
}

void VideoLoggerSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		{
			auto it = json.FindMember("video_logger");
			if (it == json.MemberEnd())
				json.AddMember("video_logger", rapidjson::Value(rapidjson::kObjectType), json.GetAllocator());
		}
		if (!json["video_logger"].IsObject())
			return;

		const auto& section_json = json["video_logger"].GetObject();
		{
			auto it = section_json.FindMember("enable");
			if (it != section_json.MemberEnd() && it->value.IsBool())
				it->value = mInformer->GetEnable();
			else
				section_json.AddMember("enable", mInformer->GetEnable(), json.GetAllocator());
		}
		{
			auto it = section_json.FindMember("store_time");
			if (it != section_json.MemberEnd() && it->value.IsInt())
				it->value = mInformer->GetStoreTime();
			else
				section_json.AddMember("store_time", mInformer->GetStoreTime(), json.GetAllocator());
		}

		{
			auto value = mInformer->GetLevel();

			{
				auto jt = section_json.FindMember("level");
				if (jt == section_json.MemberEnd())
					section_json.AddMember("level", rapidjson::Value(rapidjson::kObjectType), json.GetAllocator());
			}
			if (!section_json["level"].IsObject())
				return;

			LogicResultJson().Serialize(value, section_json["level"], json.GetAllocator());
		}
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


