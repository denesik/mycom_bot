#pragma once

#include <mutex>
#include <filesystem>
#include <sigslot/signal.hpp>

#include "ILogicComponent.h"
#include "utility/FileTimeleftController.h"

class Informer;

class RotatedFileInformerSink : public ILogicComponent
{
public:
	RotatedFileInformerSink(std::shared_ptr<Informer> informer, const std::filesystem::path &path);
	~RotatedFileInformerSink();

	sigslot::signal<> signal_data;

	void SetEnable(bool value);
	bool GetEnable() const;

	void SetCompressed(bool value);
	bool GetCompressed() const;

	void SetStoreTime(unsigned int hours);
	unsigned int GetStoreTime() const;

	void SetLevel(LogicResult level);
	LogicResult GetLevel() const;

	virtual void BeginComponent(std::string_view program) override;

	virtual void EndComponent(LogicResult result) override;

private:
	std::string GenerateName(std::string_view program);

private:
	FileTimeleftController mFileTimeleftController;
	std::mutex mMutex;

	std::filesystem::path mPath;

	std::string mBuffer;
	std::string mRunnedName;
	bool mRunned = false;

	bool mEnable = false;
	bool mCompressed = true;

	LogicResult mLevel = LogicResult::All;

	sigslot::connection mConnectionPrint;
};
