#pragma once

#include <string>
#include <vector>
#include <filesystem>
#include <sigslot/signal.hpp>

#include "ILogicComponent.h"


class CaptchaComponent : public ILogicComponent
{
public:
	CaptchaComponent(const std::filesystem::path &path);

	sigslot::signal<> signal_data;

	void PlayOne() const;

	void PlayStart() const;

	void PlayStop() const;


	void SetVolume(int value);
	int GetVolume() const;


	std::vector<std::string> GetSoundNames() const;

	void SetSoundName(std::string_view name);
	const std::string& GetSoundName() const;


	void Tick();


private:
	int GetSystemVolume() const;
	void SetSystemVolume(int value);

private:
	const std::filesystem::path mPath;

	int mVolume = 100;
	std::string mSoundName;

};

