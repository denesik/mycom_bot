#include "StateWrapper.h"

#include "Detector.h"
#include "BlockLifeTime.h"
#include "BlockState.h"
#include "BlockNumber.h"

#include "utility/split_string.h"
#include "BlockIcon.h"
#include <BlockImage.h>
#include "BlockButton.h"
#include "Logger.h"

StateWrapper::StateWrapper(std::shared_ptr<Detector> detector)
	: mDetector(detector)
{

}

std::string StateWrapper::PageFirstName() const
{
	return mDetector->GetPage().FirstName();
}

std::string StateWrapper::PageFullName() const
{
	return mDetector->GetPage().FullName();
}

bool StateWrapper::PageCheckName(std::string_view name) const
{
	const auto &page = mDetector->GetPage();
	return name == page.FullName() || name == page.FirstName();
}

bool StateWrapper::IsKnownPage() const
{
	const auto& page = mDetector->GetPage();
	return page.IsLoaded();
}

int StateWrapper::PageLifeTime() const
{
	int result = INT_MAX;
	bool finded = false;
	mDetector->GetPage().Foreach([&result, &finded](const IPageComponent& component)
		{
			if (component.GetType() == BlockLifeTime::StaticType())
			{
				result = static_cast<const BlockLifeTime&>(component).GetMilliseconds();
				finded = true;
				return true;
			}
			return false;
		});
	if (!finded)
		;// LOG_E(MakeCompositeFormat(FormatLogicComponent, "LifeTime not found"), mDetector->GetPage().FullName());
	return result;
}

bool StateWrapper::PageState(const std::string& name) const
{
	bool finded = false;
	bool result = false;
	mDetector->GetPage().Foreach([&result, &finded, &name](const IPageComponent& component)
		{
			if (component.GetType() == BlockState::StaticType())
			{
				const auto& fullname = component.GetName();
				std::string first;
				std::string second;
				utility::split_string("-", fullname, first, second);

				if (name == fullname || name == first)
				{
					result |= static_cast<const BlockState&>(component).IsActive();
					finded = true;
				}
			}
			return false;
		});
	if (!finded)
		;//LOG_E(MakeCompositeFormat(FormatLogicComponent, "State %s not found"), mDetector->GetPage().FullName(), name);
	return result;
}

int StateWrapper::PageNumberPower(const std::string& name, int scale) const
{
	bool finded = false;
	int result = 0;
	mDetector->GetPage().Foreach([&result, &finded, &name, scale](const IPageComponent& component)
		{
			if (component.GetType() == BlockNumber::StaticType())
			{
				if (component.GetName() == name)
				{
					result = static_cast<const BlockNumber&>(component).GetPower(scale);
					finded = true;
					return true;
				}
			}
			return false;
		});
	if (!finded)
		;//LOG_E(MakeCompositeFormat(FormatLogicComponent, "Number %s not found"), mDetector->GetPage().FullName(), name);
	return result;
}

size_t StateWrapper::IconsCount(std::string_view name) const
{
	bool finded = false;
	size_t result = 0;
	mDetector->GetPage().Foreach([&result, &finded, &name](const IPageComponent& component)
		{
			if (component.GetType() == BlockIcon::StaticType())
			{
				if (component.GetName() == name)
				{
					result = static_cast<const BlockIcon&>(component).GetCount();
					finded = true;
					return true;
				}
			}
			return false;
		});
	if (!finded)
		;//LOG_E(MakeCompositeFormat(FormatLogicComponent, "Icon %s not found"), mDetector->GetPage().FullName(), name);
	return result;
}

size_t StateWrapper::ImagesCount(std::string_view name) const
{
	bool finded = false;
	size_t result = 0;
	mDetector->GetPage().Foreach([&result, &finded, &name](const IPageComponent& component)
		{
			if (component.GetType() == BlockImage::StaticType())
			{
				const auto& fullname = component.GetName();
				std::string first;
				std::string second;
				utility::split_string("-", fullname, first, second);

				if (name == fullname || name == first)
				{
					result += static_cast<const BlockImage&>(component).GetCount();
					finded = true;
				}
			}
			return false;
		});
	if (!finded)
		;//LOG_E(MakeCompositeFormat(FormatLogicComponent, "Image %s not found"), mDetector->GetPage().FullName(), name);
	return result;
}

size_t StateWrapper::ButtonsCount(std::string_view name) const
{
	bool finded = false;
	size_t result = 0;
	mDetector->GetPage().Foreach([&result, &finded, &name](const IPageComponent& component)
		{
			if (component.GetType() == BlockButton::StaticType())
			{
				const auto& fullname = component.GetName();
				std::string first;
				std::string second;
				utility::split_string("-", fullname, first, second);

				if (name == fullname || name == first)
				{
					++result;
					finded = true;
				}
			}
			return false;
		});
	if (!finded)
		;//LOG_E(MakeCompositeFormat(FormatLogicComponent, "Button %s not found"), mDetector->GetPage().FullName(), name);
	return result;
}
