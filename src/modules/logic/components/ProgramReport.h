#pragma once

#include <unordered_map>
#include <vector>
#include <memory>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

#include "Timer.h"
#include "LogicResult.h"

class ProgramReport : public ActorComponent
{
public:
	sigslot::signal<> signal_data;

public:
	void Clear();

	void AddResult(std::string_view name, LogicResult result, time_t time);

	size_t GetResultCount(std::string_view name, LogicResult result, time_t timeout) const;

	const std::vector<time_t> &GetResultTimes(std::string_view name, LogicResult result) const;

	std::vector<std::string> GetNames() const;

protected:
	virtual void BeginPlay() override;

	virtual void Tick() override;

private:
	void RemoveOld(time_t timeout);

private:
	std::unordered_map<std::string, std::unordered_map<std::string, std::vector<time_t>>> mData;
	std::string mCurrentProgram;

	Timer mTimer;
};

