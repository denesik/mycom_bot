#include "ProgramReportSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "ProgramReport.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

ProgramReportSettings::ProgramReportSettings(std::shared_ptr<ProgramReport> program_report, const std::filesystem::path& user_path)
	: mProgramReport(program_report), mUserPath(user_path)
{

}

void ProgramReportSettings::Initialize()
{
	Load(mUserPath);
}

void ProgramReportSettings::BeginPlay()
{
	mProgramReportConnection = mProgramReport->signal_data.connect([this]() { Save(mUserPath); });
}

void ProgramReportSettings::EndPlay()
{
	mProgramReportConnection.disconnect();
}

void ProgramReportSettings::Deinitialize()
{
	Save(mUserPath);
}

void ProgramReportSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	for (auto it = json.MemberBegin(); it != json.MemberEnd(); ++it)
		if (it->value.IsObject())
		{
			std::string_view name = it->name.GetString();
			if (!name.empty())
				for (auto jt = it->value.MemberBegin(); jt != it->value.MemberEnd(); ++jt)
				{
					std::string_view result = jt->name.GetString();
					if (!result.empty())
						if (jt->value.IsArray())
						{
							const auto& array = jt->value.GetArray();
							for (const auto& el : array)
								if (el.IsUint64())
									mProgramReport->AddResult(name, LogicResultString(result), el.GetUint64());
						}
				}
		}
}

void ProgramReportSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;

	if (!json.IsObject())
		json.SetObject();

	for (const auto& name : mProgramReport->GetNames())
		for (auto i = 0; i < static_cast<size_t>(LogicResult::Size); ++i)
		{
			auto times = mProgramReport->GetResultTimes(name, static_cast<LogicResult>(1 << i));
			if (!times.empty())
			{
				{
					auto it = json.FindMember(name);
					if (it == json.MemberEnd())
						json.AddMember(rapidjson::Value(name, json.GetAllocator()), rapidjson::Value(rapidjson::kObjectType), json.GetAllocator());
				}
				if (!json[name].IsObject())
					return;

				auto& program_json = json[name];
				const std::string lrs = LogicResultString(static_cast<LogicResult>(1 << i));
				{
					auto it = program_json.FindMember(lrs);
					if (it == program_json.MemberEnd())
						program_json.AddMember(rapidjson::Value(lrs, json.GetAllocator()), rapidjson::Value(rapidjson::kArrayType), json.GetAllocator());
				}
				if (!program_json[lrs].IsArray())
					return;

				auto time_json = program_json[lrs].GetArray();
				for (auto time : times)
					time_json.PushBack(time, json.GetAllocator());
			}
		}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


