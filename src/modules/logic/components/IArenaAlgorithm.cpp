#include "IArenaAlgorithm.h"
#include <algorithm>

bool arena_utility::IsValidPowers(const std::vector<PowerInfo>& data)
{
	// � ���� � � ��������� ��� ���� ����������
// ����������� ������ 5
	if (data.size() != 10 && data.size() != 15)
		return false;

	{
		int unavailable_count = 0;
		for (const auto& info : data)
			unavailable_count += (info.status != PowerInfo::Status::AVAILABLE);
		if (unavailable_count > 5) // � + 5 ������
			return false;
	}

	{
		int me_count = 0;
		for (const auto& info : data)
			me_count += (info.status == PowerInfo::Status::IS_ME);
		if (me_count != 1)
			return false;
	}

	int my_power_min = GetMyPower(data);
	{
		int count = 0;
		for (const auto& info : data)
			if (info.status == PowerInfo::Status::IS_ME)
				continue;
			else
				count += (info.my_power_min == my_power_min);
		if (count != static_cast<int>(data.size()) - 1)
			return false;
	}

	return true;
}

int arena_utility::GetPhase(const std::vector<PowerInfo>& data)
{
	int unavailable_count = 0;
	for (const auto& info : data)
		unavailable_count += (info.status != PowerInfo::Status::AVAILABLE);
	if (unavailable_count > 0 && unavailable_count <= 5) // � + 5 ������
		return unavailable_count - 1;

	return -1;
}

bool arena_utility::IsValidPhase(int phase)
{
	return phase >= 0 && phase < 5;
}

int arena_utility::GetMyPower(const std::vector<PowerInfo>& data)
{
	int my_power_min = -1;
	{
		if (data[0].status == PowerInfo::Status::IS_ME)
			my_power_min = data[1].my_power_min;
		else
			my_power_min = data[0].my_power_min;
	}
	return my_power_min;
}

int arena_utility::FindMe(const std::vector<PowerInfo>& data)
{
	for (size_t i = 0; i < data.size(); ++i)
		if (data[i].status == PowerInfo::Status::IS_ME)
			return static_cast<int>(i);

	return -1;
}

int arena_utility::CountDefeat(const std::vector<PowerInfo>& data)
{
	int out = 0;
	for (size_t i = 0; i < data.size(); ++i)
		if (data[i].status == PowerInfo::Status::DEFEAT)
			++out;

	return out;
}


std::vector<PowerInfo> arena_utility::GetAllSorted(const std::vector<PowerInfo>& data)
{
	std::vector<PowerInfo> out;

	for (const auto& info : data)
		if (info.status == PowerInfo::Status::AVAILABLE)
			out.push_back(info);

	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}

std::vector<PowerInfo> arena_utility::GetAllWithoutBlacklistSorted(const std::vector<PowerInfo>& data)
{
	std::vector<PowerInfo> out;

	for (const auto& info : data)
		if (info.status == PowerInfo::Status::AVAILABLE && info.blacklist_count == 0)
			out.push_back(info);

	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}

std::vector<PowerInfo> arena_utility::GetWeakSorted(const std::vector<PowerInfo>& data)
{
	std::vector<PowerInfo> out;

	for (const auto& info : data)
		if (info.status == PowerInfo::Status::AVAILABLE && info.my_power_min > info.other_power && info.blacklist_count == 0)
			out.push_back(info);

	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}

std::vector<PowerInfo> arena_utility::GetWeakAboveSorted(const std::vector<PowerInfo>& data)
{
	std::vector<PowerInfo> out;

	for (const auto& info : data)
	{
		if (info.status == PowerInfo::Status::IS_ME)
			break;
		if (info.status == PowerInfo::Status::AVAILABLE && info.my_power_min > info.other_power && info.blacklist_count == 0)
			out.push_back(info);
	}
	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}

std::vector<PowerInfo> arena_utility::GetWeakBelowSorted(const std::vector<PowerInfo>& data)
{
	std::vector<PowerInfo> out;

	bool me_finded = false;
	for (const auto& info : data)
	{
		if (info.status == PowerInfo::Status::IS_ME)
		{
			me_finded = true;
			continue;
		}
		if (!me_finded)
			continue;

		if (info.status == PowerInfo::Status::AVAILABLE && info.my_power_min > info.other_power && info.blacklist_count == 0)
			out.push_back(info);
	}
	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}

std::vector<PowerInfo> arena_utility::GetWeakAboveMeSmallSorted(const std::vector<PowerInfo>& data)
{
	std::vector<PowerInfo> out;

	for (const auto& info : data)
	{
		if (info.status == PowerInfo::Status::IS_ME)
			break;

		if (info.status == PowerInfo::Status::AVAILABLE && info.my_power_min > info.other_power && CountWeatAbove(data, info.pos) == 0 && info.blacklist_count == 0)
			out.push_back(info);
	}
	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}

std::vector<PowerInfo> arena_utility::GetWeakAboveMeBigSorted(const std::vector<PowerInfo>& data)
{
	std::vector<PowerInfo> out;

	for (const auto& info : data)
	{
		if (info.status == PowerInfo::Status::IS_ME)
			break;

		if (info.status == PowerInfo::Status::AVAILABLE && info.my_power_min > info.other_power && CountWeatAbove(data, info.pos) > 0 && info.blacklist_count == 0)
			out.push_back(info);
	}
	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}

int arena_utility::CountWeatAbove(const std::vector<PowerInfo>& data, int pos)
{
	int out = 0;
	for (const auto& info : data)
	{
		if (info.pos == pos)
			break;
		if (data[pos].other_power > info.other_power)
			++out;
	}
	return out;
}

int arena_utility::CountStrong(const std::vector<PowerInfo>& data)
{
	int out = 0;
	for (const auto& info : data)
	{
		if (info.status != PowerInfo::Status::IS_ME && (info.other_power >= info.my_power_min || info.blacklist_count > 0))
			++out;
	}

	return out;
}
