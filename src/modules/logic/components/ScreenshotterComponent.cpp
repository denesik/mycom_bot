#include "ScreenshotterComponent.h"

#include "Emulator.h"
#include "utility/GenerateScreenshotPath.h"
#include "Logger.h"
#include "PathUtf8.h"
#include "utility/OpencvFilesystem.h"

ScreenshotterComponent::ScreenshotterComponent(std::shared_ptr<Emulator> emulator, const std::filesystem::path& screenshot_dir)
	: mEmulator(emulator), mScreenshotsDirectory(screenshot_dir)
{

}

void ScreenshotterComponent::CreateScreenshot()
{
	Scanner scanner(mEmulator);
	cv::Mat img;
	scanner.Capture(img);

	auto path = utility::GenerateScreenshotPath(mScreenshotsDirectory);
	if (path == "")
		LOG_I("Error creating screenshot");
	else
	{
		try
		{
			if (!std::filesystem::exists(mScreenshotsDirectory))
				std::filesystem::create_directories(mScreenshotsDirectory);
		}
		catch (std::filesystem::filesystem_error&)
		{
			LOG_I("Error create directory " + utf_helper::PathToUtf8(mScreenshotsDirectory));
		}

		auto res = utility::Imwrite(path, img);
		if (res)
			LOG_I("Screenshot saved " + utf_helper::PathToUtf8(path));
		else
			LOG_I("Error creating screenshot " + utf_helper::PathToUtf8(path));
	}
}
