#include "ProgramReport.h"

#include "Actor.h"
#include "Messages.h"

void ProgramReport::Clear()
{
	if (!mData.empty())
	{
		mData.clear();
		signal_data();
	}
}

void ProgramReport::AddResult(std::string_view name, LogicResult result, time_t time)
{
	auto& value = mData[std::string(name)];

	std::string_view result_name = LogicResultString(result);
	if (!result_name.empty())
	{
		value[std::string(result_name)].emplace_back(time);
		signal_data();
	}
}

size_t ProgramReport::GetResultCount(std::string_view name, LogicResult result, time_t timeout) const
{
	auto it = mData.find(std::string(name));
	if (it == mData.end())
		return 0;

	std::string_view result_name = LogicResultString(result);
	if (!result_name.empty())
	{
		auto jt = it->second.find(std::string(result_name));
		if (jt != it->second.end())
		{
			size_t count = 0;
			auto curr_time = time(nullptr);
			for (auto time : jt->second)
				if (curr_time > timeout && curr_time - timeout < time)
					++count;
			return count;
		}
	}
	return 0;
}

const std::vector<time_t>& ProgramReport::GetResultTimes(std::string_view name, LogicResult result) const
{
	static std::vector<time_t> fake;

	auto it = mData.find(std::string(name));
	if (it == mData.end())
		return fake;

	std::string_view result_name = LogicResultString(result);
	if (!result_name.empty())
	{
		auto jt = it->second.find(std::string(result_name));
		if (jt != it->second.end())
			return jt->second;
	}
	return fake;
}

std::vector<std::string> ProgramReport::GetNames() const
{
	std::vector<std::string> out;
	out.reserve(mData.size());

	for (const auto& data : mData)
		out.emplace_back(data.first);

	return out;
}

void ProgramReport::BeginPlay()
{
	GetActor()->Bind<MessageLogicStart>([this](MessageLogicStart & msg)
	{
		mCurrentProgram = msg.program_type;
	});

	GetActor()->Bind<MessageLogicFinish>([this](MessageLogicFinish & msg)
	{
		AddResult(mCurrentProgram, msg.result, time(nullptr));
		mCurrentProgram.clear();
	});
}

void ProgramReport::Tick()
{
	if (mTimer.Elapsed() >= 1000 * 5)
	{
		mTimer.Start();
		RemoveOld(60 * 60 * 24 * 3);
	}
}

void ProgramReport::RemoveOld(time_t timeout)
{
	bool changed = false;
	auto curr_time = time(nullptr);
	auto last_time = curr_time > timeout ? curr_time - timeout : 0;
	for (auto& data : mData)
		for (auto& results : data.second)
		{
			results.second.erase(std::remove_if(results.second.begin(), results.second.end(), [last_time](time_t val) { return val < last_time; }), results.second.end());
			changed = true;
		}
	if (changed)
		signal_data();
}
