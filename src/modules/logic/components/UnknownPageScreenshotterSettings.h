#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class UnknownPageScreenshotter;

class UnknownPageScreenshotterSettings : public ActorComponent
{
public:
	UnknownPageScreenshotterSettings(std::shared_ptr<UnknownPageScreenshotter> screenshotter, const std::filesystem::path& core_path, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mCorePath;
	const std::filesystem::path mUserPath;

	std::shared_ptr<UnknownPageScreenshotter> mScreenshotter;

	sigslot::connection mScreenshotterConnection;
};


