#pragma once

#include <memory>
#include <string>
#include <set>

#include "ILogicComponent.h"

class Detector;

class RecoveryValuesComponent : public ILogicComponent
{
public:
	RecoveryValuesComponent(std::shared_ptr<Detector> detector);

	template<typename ...T>
	bool SetPageFullNames(T&... args)
	{
		mValues.clear();
		(mValues.emplace(args), ...);
		return true;
	}

	bool IsRecoveryPage() const;

private:
	std::shared_ptr<Detector> mDetector;

	std::set<std::string> mValues;
};

