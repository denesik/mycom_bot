#include "TaskStatisticComponent.h"

#include <fstream>


TaskStatisticComponent::TaskStatisticComponent(const std::filesystem::path &path)
{
	mPath = path;
}

void TaskStatisticComponent::SetEnable(bool value)
{
	if (mEnable != value)
	{
		mEnable = value;
		signal_data();
	}
}

bool TaskStatisticComponent::GetEnable() const
{
	return mEnable;
}

void TaskStatisticComponent::BeginComponent(std::string_view program)
{
	auto t = std::time(nullptr);
	struct tm buf;
	localtime_s(&buf, &t);

	std::stringstream ss;
	ss << std::put_time(&buf, "%d-%m-%y %H-%M-%S  ");
	ss << std::setiosflags(std::ios::fixed) << std::setw(20) << std::left;
	ss << program;

	mRecord = ss.str();
}

void TaskStatisticComponent::EndComponent(LogicResult result)
{
	if (mEnable)
	{
		mRecord += LogicResultDescription(result);

		std::ofstream mLogFile;
		mLogFile.open(mPath, std::ofstream::app);
		if (mLogFile.is_open())
			mLogFile << mRecord << std::endl;
	}

	mRecord.clear();
}
