#include "LockerComponent.h"

#include <chrono>

namespace
{
	time_t TIME_OFFSET = 3 * 60 * 60;
	time_t DAY_SECONDS = 24 * 60 * 60;
}

// success_launch_max_day
// success_launch_max_zone
// request_resources_timeleft
// lock_unavailable_launch
// lock_if_enough_apples
//
// ��������� ������ ��������� ���� ���� ����������� �� ���-�� �������� ���������� �� ����/����.
// ��������� ������ ��������� �� ����, ���� ���� ���������� � ���� ���� � ����������� "����������"
// ��������� ������ ���������, ���� ��� �������� �� ������ � ������ ���� ������.
// 
// ��������� ������ ����� ��������� �� ������� � ���������, �������� ����� �� ������ � ������, ����� �� ������ � ���������
// 
// ��������� ������ �� �������, ���� ���� ������� �� ������� �� ������ �����
// 
// 01. ����� [��� ��������] ����������� �������
// 02. ����� [���� �������] ����������� (�� �������) ��� ����������� �� ������ � ������������ ����
// 03. ����� [���� �������] ����������� (�� �������) 
// 04. ����� [��� ��������] ����������� �� ������� ��� ������, ������ �������������
// 05. ����� [��� ��������] ����������� �������
// 06. ����� [���� �������] ����������� (�� �������) 
// 07. ����� [���� �������] ����������� (�� �������) ��� ����������� �� ������ � ������������ ����
// 09. ����� [��� ��������] ����������� ���������, �� ������� ��� ������, ������ �������������
// 10. ����� [��� ��������] ����������� �������
// 11. ����� [���� �������] ����������� (�� �������) ��� ����������� �� ������ � ������������ ����
// 12. ����� [���� �������] ����������� (�� �������) ��� ����������� �� ������ � ������������ ����
// 13. ����� [��� ��������] �����������, ������ �������������
// 


void LockerComponent::AddRecord(RecordType type, time_t time, std::string_view name)
{
	mData[static_cast<std::underlying_type<RecordType>::type>(type)].emplace_back(time, std::string(name));
	signal_data();
}

size_t LockerComponent::CountRecords(RecordType type) const
{
	return mData[static_cast<std::underlying_type<RecordType>::type>(type)].size();
}

time_t LockerComponent::GetRecordTime(RecordType type, size_t index) const
{
	return mData[static_cast<std::underlying_type<RecordType>::type>(type)][index].time;
}

const std::string& LockerComponent::GetRecordName(RecordType type, size_t index) const
{
	return mData[static_cast<std::underlying_type<RecordType>::type>(type)][index].name;
}

void LockerComponent::BeginComponent(std::string_view program)
{
	mProgramName = program;
}

void LockerComponent::Tick()
{
	if (mTimer.Elapsed() >= 1000 * 60)
	{
		mTimer.Start();

		bool removed = false;
		removed |= RemoveOldRecords(RecordType::UnavaliableTask);
		removed |= RemoveOldRecords(RecordType::SuccessTask);
		removed |= RemoveOldRecords(RecordType::RequirementTask);
		if (removed)
			signal_data();
	}
}

void LockerComponent::EndComponent(LogicResult result)
{
	switch (result)
	{
	case LogicResult::None:
		break;
	case LogicResult::Full:
		SetSuccess(mProgramName);
		ResetRequirement(mProgramName);
		break;
	case LogicResult::Partly:
		break;
	case LogicResult::Error:
		break;
	case LogicResult::Stop:
		break;
	case LogicResult::Condition:
		break;
	case LogicResult::Unavailable:
		SetUnavailable(mProgramName);
		break;
	case LogicResult::Requirement:
		SetRequirement(mProgramName);
		break;
	case LogicResult::Unknown:
		break;
	case LogicResult::All:
		break;
	}
}

bool LockerComponent::AllowedWithResources(std::string_view name, const LogicData& config) const
{
	if (IsFullLock(name, config))
		return false;
	const auto timeleft = config.GetIntValue("request_resources_timeleft");

	// ��������� ������ �� �������, ���� ���� ������� �� ������� �� ������ �����
	// ��������� ���� ���� ������ �� ����� ������ � ���-�� �������� ������ 1
	// ��������� ���� ��� �������� �� ����� ������ � ���-�� �������� ������ 0

	if (ResourceRequested(name, timeleft) && ResourceRequestCount(timeleft) > 1)
		return false;

	if (!ResourceRequested(name, timeleft) && ResourceRequestCount(timeleft) > 0)
		return false;

	return true;
}

bool LockerComponent::AllowedWithoutResources(std::string_view name, const LogicData& config) const
{
	if (IsFullLock(name, config))
		return false;

	return true;
}

void LockerComponent::SetUnavailable(std::string_view name)
{
	auto current_time = mTimeDayZone.GetCurrentTime();
	auto day_zone = mTimeDayZone.GetDayZone(current_time);

	auto it = std::find_if(mUnavaliableTasks.begin(), mUnavaliableTasks.end(), [this, &day_zone, &name](const Record& record)
		{
			auto dz = mTimeDayZone.GetDayZone(record.time);
	return dz.day == day_zone.day && dz.zone == day_zone.zone && record.name == name;
		});
	if (it == mUnavaliableTasks.end())
	{
		mUnavaliableTasks.emplace_back(current_time, std::string(name));
		signal_data();
	}
	else if (it->time != current_time)
	{
		it->time = current_time;
		signal_data();
	}
}

void LockerComponent::SetSuccess(std::string_view name)
{
	mSuccessTasks.emplace_back(mTimeDayZone.GetCurrentTime(), std::string(name));
	signal_data();
}

void LockerComponent::SetRequirement(std::string_view name)
{
	auto current_time = mTimeDayZone.GetCurrentTime();
	auto it = std::find_if(mRequirementTasks.begin(), mRequirementTasks.end(), [&name](const Record& record)
		{
			return record.name == name;
		});
	if (it == mRequirementTasks.end())
	{
		mRequirementTasks.emplace_back(current_time, std::string(name));
		signal_data();
	}
	else if (it->time != current_time)
	{
		it->time = current_time;
		signal_data();
	}
}

void LockerComponent::ResetRequirement(std::string_view name)
{
	auto it = std::remove_if(mRequirementTasks.begin(), mRequirementTasks.end(), [&name](const Record& record)
		{
			return record.name == name;
		});
	if (it != mRequirementTasks.end())
	{
		mRequirementTasks.erase(it, mRequirementTasks.end());
		signal_data();
	}
}



bool LockerComponent::IsFullLock(std::string_view name, const LogicData& config) const
{
	// ��������� ������ ��������� �� ����, ���� ���� ���������� � ���� ���� � ����������� "����������"
	if (IsLockedTask(name) && config.GetBoolValue("lock_unavailable_launch"))
		return true;
	// ��������� ������ ��������� ���� ���� ����������� �� ���-�� �������� ���������� �� ����/����.
	if (config.GetIntValue("success_launch_max_day") && SuccessTasksToday(name) >= config.GetIntValue("success_launch_max_day"))
		return true;
	if (config.GetIntValue("success_launch_max_zone") && SuccessTasksZone(name) >= config.GetIntValue("success_launch_max_zone"))
		return true;
	// ��������� ������ ���������, ���� ��� �������� �� ������ � ������ ���� ������.
	if (ResourceRequestCount(config.GetIntValue("request_resources_timeleft")) == 0 && config.GetBoolValue("lock_if_enough_apples"))
		return true;

	return false;
}

bool LockerComponent::IsLockedTask(std::string_view name) const
{
	auto day_zone = mTimeDayZone.GetDayZone(mTimeDayZone.GetCurrentTime());

	auto it = std::find_if(mUnavaliableTasks.begin(), mUnavaliableTasks.end(), [this, &day_zone, &name](const Record& record)
		{
			auto dz = mTimeDayZone.GetDayZone(record.time);
			return dz.day == day_zone.day && dz.zone == day_zone.zone && record.name == name;
		});

	return it != mUnavaliableTasks.end();
}



size_t LockerComponent::SuccessTasksToday(std::string_view name) const
{
	auto day_zone = mTimeDayZone.GetDayZone(mTimeDayZone.GetCurrentTime());

	size_t out = 0;
	for (const auto &data : mSuccessTasks)
		if (mTimeDayZone.GetDayZone(data.time).day == day_zone.day && data.name == name)
			++out;

	return out;
}

size_t LockerComponent::SuccessTasksZone(std::string_view name) const
{
	auto day_zone = mTimeDayZone.GetDayZone(mTimeDayZone.GetCurrentTime());

	size_t out = 0;
	for (const auto& data : mSuccessTasks)
	{
		auto dz = mTimeDayZone.GetDayZone(data.time);
		if (dz.day == day_zone.day && dz.zone == day_zone.zone && data.name == name)
			++out;
	}

	return out;
}

size_t LockerComponent::ResourceRequestCount(int timeleft_minutes) const
{
	// ���������� ������ �������� �� �������.
	auto border_time = mTimeDayZone.GetCurrentTime() - static_cast<time_t>(timeleft_minutes) * 60;

	size_t count = 0;
	for (const auto& record : mRequirementTasks)
		count += (record.time > border_time);
	return count;	
}

bool LockerComponent::ResourceRequested(std::string_view name, int timeleft_minutes) const
{
	auto border_time = mTimeDayZone.GetCurrentTime() - static_cast<time_t>(timeleft_minutes) * 60;

	for (const auto& record : mRequirementTasks)
		if (record.name == name)
			if (record.time > border_time)
				return true;
	return false;
}

bool LockerComponent::RemoveOldRecords(RecordType type)
{
	auto border_time = mTimeDayZone.GetCurrentTime() - 3 * 24 * 60 * 60;

	auto it = std::remove_if(
		mData[static_cast<std::underlying_type<RecordType>::type>(type)].begin(),
		mData[static_cast<std::underlying_type<RecordType>::type>(type)].end(),
		[border_time](const Record& record)
		{
			return record.time < border_time;
		});
	if (it != mData[static_cast<std::underlying_type<RecordType>::type>(type)].end())
	{
		mData[static_cast<std::underlying_type<RecordType>::type>(type)].erase(it, mData[static_cast<std::underlying_type<RecordType>::type>(type)].end());
		return true;
	}
	return false;
}
