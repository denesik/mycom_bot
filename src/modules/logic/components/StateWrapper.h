#pragma once

#include <memory>

#include "ILogicComponent.h"

class Detector;

class StateWrapper : public ILogicComponent
{
public:
	StateWrapper(std::shared_ptr<Detector> detector);

	std::string PageFirstName() const;
	std::string PageFullName() const;
	bool PageCheckName(std::string_view name) const;

	bool IsKnownPage() const;
	int PageLifeTime() const;

	bool PageState(const std::string& name) const;

	int PageNumberPower(const std::string& name, int scale = 100) const;

	size_t IconsCount(std::string_view name) const;
	size_t ImagesCount(std::string_view name) const;
	size_t ButtonsCount(std::string_view name) const;

private:
	std::shared_ptr<Detector> mDetector;


};

