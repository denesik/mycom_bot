#include "LighthouseHelper.h"

#include <algorithm>

void LighthouseHelper::Clear()
{
	mData.clear();
}

LighthouseHelper& LighthouseHelper::Add(int position, int power, bool avalaible)
{
	mData.emplace_back(position, power, avalaible);
	return *this;
}

int LighthouseHelper::GetPosition() const
{
	auto data = mData;
	std::sort(data.begin(), data.end(), [](const Data& a, const Data& b) {return a.power < b.power; });
	for (const auto &el : data)
		if (el.avalaible)
			return el.pos;
	return 0;
}
