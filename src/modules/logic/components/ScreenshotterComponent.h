#pragma once

#include <memory>
#include <filesystem>

#include "ILogicComponent.h"

class Emulator;

class ScreenshotterComponent : public ILogicComponent
{
public:
	ScreenshotterComponent(std::shared_ptr<Emulator> emulator, const std::filesystem::path& screenshot_dir);

	void CreateScreenshot();

private:
	std::shared_ptr<Emulator> mEmulator;
	const std::filesystem::path mScreenshotsDirectory;

};

