#pragma once

#include <vector>

class LighthouseHelper
{
public:

	void Clear();

	LighthouseHelper& Add(int position, int power, bool avalaible);

	int GetPosition() const;

private:
	struct Data
	{
		int pos = 0;
		int power = -1;
		bool avalaible = false;
	};
	std::vector<Data> mData;

};

