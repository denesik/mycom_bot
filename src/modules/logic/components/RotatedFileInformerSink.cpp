#include "RotatedFileInformerSink.h"

#include <fstream>
#include <zip_file.hpp>

#include "Informer.h"

RotatedFileInformerSink::RotatedFileInformerSink(std::shared_ptr<Informer> informer, const std::filesystem::path& path)
{
	mPath = path;
	mFileTimeleftController.SetDirectory(mPath);
	mConnectionPrint = informer->signal_print.connect([this](const std::string& msg)
		{
			if (!mRunned)
				return;

			std::stringstream ss;
			ss << time << " " << msg << std::endl;
			std::lock_guard<std::mutex> lock(mMutex);
			mBuffer += ss.str();
		});
}

RotatedFileInformerSink::~RotatedFileInformerSink()
{
	mConnectionPrint.disconnect();
}

void RotatedFileInformerSink::SetEnable(bool value)
{
	if (mEnable != value)
	{
		mEnable = value;
		signal_data();
	}
}

bool RotatedFileInformerSink::GetEnable() const
{
	return mEnable;
}

void RotatedFileInformerSink::SetCompressed(bool value)
{
	if (mCompressed != value)
	{
		mCompressed = value;
		signal_data();
	}
}

bool RotatedFileInformerSink::GetCompressed() const
{
	return mCompressed;
}

void RotatedFileInformerSink::SetStoreTime(unsigned int hours)
{
	if (mFileTimeleftController.GetStoreTime() / 60 != hours)
	{
		mFileTimeleftController.SetStoreTime(hours * 60);
		signal_data();
	}
}

unsigned int RotatedFileInformerSink::GetStoreTime() const
{
	return mFileTimeleftController.GetStoreTime() / 60;
}

void RotatedFileInformerSink::SetLevel(LogicResult level)
{
	if (mLevel != level)
	{
		mLevel = level;
		signal_data();
	}
}

LogicResult RotatedFileInformerSink::GetLevel() const
{
	return mLevel;
}

void RotatedFileInformerSink::BeginComponent(std::string_view program)
{
	mRunned = true;
	mRunnedName = GenerateName(program);
	mBuffer.clear();
}

void RotatedFileInformerSink::EndComponent(LogicResult result)
{
	if (mBuffer.empty())
		return;

	mFileTimeleftController.RemoveUnwanted(".txt");
	mFileTimeleftController.RemoveUnwanted(".zip");

	if (mEnable && ((mLevel & result) == result))
	{
		bool can_save = true;
		try
		{
			if (!std::filesystem::exists(mPath))
				std::filesystem::create_directories(mPath);
		}
		catch (std::filesystem::filesystem_error &)
		{
			can_save = false;
		}

		if (can_save)
		{
			mRunnedName = std::string(LogicResultDescription(result)) + " " + mRunnedName;

			if (mCompressed)
			{
				miniz_cpp::zip_file file;
				file.writestr(mRunnedName + ".txt", mBuffer);
				file.save((mPath / (mRunnedName + ".zip")).string());
			}
			else
			{
				std::ofstream mLogFile;
				mLogFile.open(mPath / (mRunnedName + ".txt"), std::ofstream::app);
				if (mLogFile.is_open())
					mLogFile << mBuffer;
			}
		}
	}

	mRunned = false;
	mRunnedName.clear();
	mBuffer.clear();
}

std::string RotatedFileInformerSink::GenerateName(std::string_view program)
{
	std::string time;
	{
		auto t = std::time(nullptr);
		struct tm buf;
		localtime_s(&buf, &t);
		std::stringstream ss;
		ss << std::put_time(&buf, "%d-%m-%y %H-%M-%S");
		time = ss.str();
	}

	return std::string(program) + " " + time;
}
