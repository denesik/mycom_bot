#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ILogicComponent.h"

class TaskStatisticComponent : public ILogicComponent
{
public:
	TaskStatisticComponent(const std::filesystem::path &path);

	sigslot::signal<> signal_data;

	void SetEnable(bool value);
	bool GetEnable() const;

	virtual void BeginComponent(std::string_view program) override;

	virtual void EndComponent(LogicResult result) override;

private:
	std::filesystem::path mPath;
	bool mEnable = false;

	std::string mRecord;
};
