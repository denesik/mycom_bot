#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class ProgramReport;

class ProgramReportSettings : public ActorComponent
{
public:
	ProgramReportSettings(std::shared_ptr<ProgramReport> program_report, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mUserPath;

	std::shared_ptr<ProgramReport> mProgramReport;

	sigslot::connection mProgramReportConnection;
};


