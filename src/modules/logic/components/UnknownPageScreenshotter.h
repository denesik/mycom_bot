#pragma once

#include "ILogicComponent.h"

#include <opencv2/core/mat.hpp>
#include <filesystem>
#include <sigslot/signal.hpp>

class Page;
class Detector;

class UnknownPageScreenshotter : public ILogicComponent
{
public:
	UnknownPageScreenshotter(const std::filesystem::path &path, std::shared_ptr<Detector> detector);

	sigslot::signal<> signal_data;

	void SetEnable(bool value);
	bool GetEnable() const;

	void SetRecoveryMode(bool value);
	bool GetRecoveryMode() const;

	void SetStoreTime(unsigned int hours);
	unsigned int GetStoreTime() const;

	void Screenshot(std::string_view prefix);

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

private:
	const std::filesystem::path mPath;

	bool mEnable = false;
	bool mRecoveryMode = false;
	unsigned int mStoreTime = 24 * 3;

	std::shared_ptr<Detector> mDetector;

	sigslot::connection mConnectionDetect;
};




