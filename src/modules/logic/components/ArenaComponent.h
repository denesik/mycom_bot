#pragma once


#include <vector>
#include "IArenaAlgorithm.h"
#include <memory>

#include "ILogicComponent.h"

class Informer;

class ArenaComponent : public ILogicComponent
{
public:
	ArenaComponent(std::shared_ptr<Informer> informer);

	void Init();

	

	void ClearPowers();

	void SetSize(size_t value);

	int GetSize() const;

	void SetPowerInfo(int pos, PowerInfo::Status status, int my_power_min, int my_power_max, int other_power);



	bool IsValidPowers() const;

	bool IsValidFinish() const;

	void SetAttackPosition(int algorithm);
	int GetAttackPosition() const;
	bool CheckAttackPosition(int other_power) const;

	void SetBattleResult(PowerInfo::Status status);

	void PrintCurrentPhase() const;

private:
	int mAttackPosition = 0;

	std::shared_ptr<Informer> mInformer;

	std::vector<std::unique_ptr<IArenaAlgorithm>> mAlgorithms;
	std::vector<PowerInfo> mPowerInfoList;
	int mAlgorithm = 0;

	struct Statistic
	{
		std::vector<PowerInfo> info;
		PowerInfo::Status status = PowerInfo::Status::UNKNOWN;
		int attack_pos = 0;
		int algorithm = 0;
	};
	Statistic mPhasesStatistic[5];

};

