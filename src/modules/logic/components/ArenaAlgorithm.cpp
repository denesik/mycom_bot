#include "ArenaAlgorithm.h"
#include <algorithm>

int ArenaAlgorithm::GetAttackPos(const std::vector<PowerInfo>& data) const
{
	auto phase = arena_utility::GetPhase(data) + 1;

	if (phase == 1)
	{
		auto all_available = arena_utility::GetAllSorted(data);
		auto all = arena_utility::GetAllWithoutBlacklistSorted(data);
		auto weak = arena_utility::GetWeakSorted(data);

		// ���� �� �� ����� ��������� 5 ������� � � ��� ���� ������������, ��������� � ������ �������������;
		if (!all_available.empty())
			if (weak.size() < 5 && all_available.back().other_power > all_available.back().my_power_max)
				return all_available.back().pos;

		// ������� �� ������ ���� ��������� ������ �������.
		if (weak.size() != 0)
			for (size_t i = 0; i < all.size(); ++i)
			{
				if (all[i].pos == weak[0].pos)
				{
					all.erase(std::next(all.begin(), i));
					break;
				}
			}

		if (all.size() != 0)
		{
			// ������� ����� ������ �������.
			// ��������� ������ ������� �� 5 ����.
			return all[0].pos;
		}
	}

	if (phase == 2 || phase == 3 || phase == 4 || phase == 5)
	{
		if (arena_utility::CountDefeat(data) > 0)
		{
			// ���� �� �� ���� �� �������.

			// ������ ������ ����������� ���� ����, ������� ����� ����� �� ������ ������.
			// ������� ������ ������� ���� ����.
			auto list = arena_utility::GetWeakAboveSorted(data);
			if (list.size() != 0)
				return list[0].pos;
		}
		else
		{
			// ���� ����� ������� (3+ ��� 10, 4+ ��� 15), ������ ����, �����, �����, �����, �����
			if (arena_utility::CountStrong(data) > (data.size() + 1) / 5)
			{
				// ������ ������ ����������� ���� ����, ������� ����� ����� �� ������ ������.
				// ������� ������ ������� ���� ����.
				auto list = arena_utility::GetWeakAboveSorted(data);
				if (list.size() != 0)
					return list[0].pos;
			}
			else
			{
				if (phase == 3)
				{
					auto me_pos = arena_utility::FindMe(data);
					// ���� �� ������ �����, ������ ����� ����� ��� ���� ���� ����� ����� �����, ������� �����.
					if (me_pos > (data.size() / 2 + 1)) // (8 ��� 5)
					{
						// ������� ������ �������.
						auto opps = arena_utility::GetWeakAboveSorted(data);
						if (opps.size() != 0)
							return opps[0].pos;
					}
					else
					{
						auto opps = GetPhase3(data);
						//PrintPowerData(opps)
						if (opps.size() != 0)
						{
							// ��� �� ������� ������ �������� �� ������ ���� ����, ��� �� �������� ���� ������ �����.
							return opps[opps.size() - 1].pos;
						}
					}
				}

				if (phase == 4)
				{
					// �������� ������ ���� ����, ������� ����� ����� ���� �� ���� ����.
					auto list = arena_utility::GetWeakAboveMeBigSorted(data);
					if (list.size() >= 2)
					{
						// ���� ����� �����, ���� ������ ������� ���� ����.
						// �.�. ��� ��� ������� 2 ���������� ����� ������� ����� �� 4 ����.
						// ������ ������ ����������� ���� ����, ������� ����� ����� �� ������ ������.
						auto opps = arena_utility::GetWeakAboveSorted(data);
						if (opps.size() != 0)
							return opps[0].pos;
					}
					else
					{
						if (list.size() == 1)
						{
							// ��� 1 ���������, ������� ����� ������� ����� �� 4 ����.
							// ������ ������ ����������� ���� ����, ������� ����� ����� �� ������ ������.
							auto opps = arena_utility::GetWeakAboveSorted(data);
							for (auto i = 0; i < opps.size(); ++i)
							{
								if (opps[i].pos == list[0].pos)
								{
									opps.erase(std::next(opps.begin(), i));
									break;
								}
							}

							// ������� ������ ������� ���� ����, �� �� ����, ������� ����� ��������� �����.
							if (opps.size() != 0)
								return opps[0].pos;
						}
					}

					// ��� ��� ����������� ���� ����, ������� ����� ��������� �����.
					// ������ ������ ����������� ���� ����, ������� �� ����� ����� ���� �� ���� ����.

					auto opps1 = arena_utility::GetWeakAboveSorted(data);
					for (auto i = 0; i < opps1.size(); ++i)
					{
						if (opps1[i].pos == 0)
						{
							opps1.erase(std::next(opps1.begin(), i));
							break;
						}
					}
					// ������� ����� ������ �������. 
					// ���� ��� �� ���� ���� ������� ������ �������, �� ������� ������ ����� ��� � � ����� ���� ���� �� 5-�� ����, ��� ����� ����� ���������.
					if (opps1.size() != 0)
						return opps1[0].pos;

					// ���� ������ ������ ���� ������, ������� ���.
					auto opps2 = arena_utility::GetWeakAboveSorted(data);
					if (!opps2.empty())
						return opps2[0].pos;
				}

				if (phase == 5)
				{
					// ��� ������ ������� ������ �������.
					auto opps = arena_utility::GetWeakAboveSorted(data);
					if (opps.size() != 0)
						return opps[0].pos;
				}
			}
		}
	}

	{
		// � ��� �� ���������� ��������� ���, ��� �� ������. ������� ���� ����-������.
		// �������� ������ ������ ���� ����.
		// ������� �� ���� ������ ������� �� ���� ��������� �����������.
		// � ������� ����� ������ �������.
		auto opps = arena_utility::GetWeakBelowSorted(data);
		if (phase == 1 || phase == 2 || phase == 3)
		{
			auto weak = arena_utility::GetWeakSorted(data);
			if (weak.size() != 0)
			{
				for (auto i = 0; i < opps.size(); ++i)
				{
					if (opps[i].pos == weak[0].pos)
					{
						opps.erase(std::next(opps.begin(), i));
						break;
					}
				}
				if (opps.size() != 0)
					return opps[0].pos;
			}
		}
	}

	{
		// ���� �� ����������, ������� ������ ������� �� ������ ������.
		auto opps = arena_utility::GetWeakSorted(data);
		if (opps.size() != 0)
			return opps[0].pos;
	}

	{
		// ���� �� ����������, ������� ������ ������� �� ���� ���������, �������� ��� ��� � ��.
		auto opps = arena_utility::GetAllWithoutBlacklistSorted(data);
		if (opps.size() != 0)
			return opps[0].pos;
	}

	// ���� �� ����������, ������� ������ ������� �� ���� ���������.
	auto opps = arena_utility::GetAllSorted(data);
	if (opps.size() != 0)
		return opps[0].pos;

	return 0;
}

std::vector<PowerInfo> ArenaAlgorithm::GetPhase3(const std::vector<PowerInfo>& data) const
{
	std::vector<PowerInfo> out;

	auto me_pos = arena_utility::FindMe(data);

	// ������� ������ ������ �����������, ������� ����� ���������, ��� ���� ���� � ��� �� ����� ��������� 2 � ������ �����.
	// �.�. � 4-�� ���� ��� ����� �� ����� ���� ����.
	for (size_t i = 0; i < data.size(); ++i)
	{
		// ���� ����, �������� � ������.
		if (data[i].pos > me_pos && data[i].status == PowerInfo::Status::AVAILABLE && data[i].my_power_min > data[i].other_power && data[i].blacklist_count == 0)
			if (arena_utility::CountWeatAbove(data, data[i].pos) < 2)
				out.push_back(data[i]);
	}

	// ��������� ������ �������� ���� ����, ���� �� ����� ��������� �����.
	// �� ������, ���� �� ������ ������� �����.
	for (size_t i = 0; i < out.size(); ++i)
		if (out[i].pos == me_pos + 1)
		{
			if (arena_utility::CountWeatAbove(data, me_pos + 1) > 0)
				out.erase(std::next(out.begin(), i));
			break;
		}

	std::sort(out.begin(), out.end(), [](const PowerInfo& a, const PowerInfo& b)
		{
			return a.other_power < b.other_power;
		});

	return out;
}
