#include "RecoveryValuesComponent.h"

#include "Detector.h"

RecoveryValuesComponent::RecoveryValuesComponent(std::shared_ptr<Detector> detector)
	: mDetector(detector)
{

}

bool RecoveryValuesComponent::IsRecoveryPage() const
{
	const auto &page = mDetector->GetPage();

	for (const auto& name : mValues)
		if (page.FullName() == name)
			return true;

	return false;
}
