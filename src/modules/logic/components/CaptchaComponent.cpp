#include "CaptchaComponent.h"

#include <windows.h>

#include "utf_helper/PathUtf8.h"

CaptchaComponent::CaptchaComponent(const std::filesystem::path& path)
	: mPath(path)
{
	mVolume = GetSystemVolume();
}

void CaptchaComponent::PlayOne() const
{
	PlayStop();
	if (!mSoundName.empty())
	{
		auto name = mPath / utf_helper::PathFromUtf8(mSoundName + ".wav");
		sndPlaySound(name.c_str(), SND_FILENAME | SND_ASYNC);
	}
}

void CaptchaComponent::PlayStart() const
{
	PlayStop();
	if (!mSoundName.empty())
	{
		auto name = mPath / utf_helper::PathFromUtf8(mSoundName + ".wav");
		sndPlaySound(name.c_str(), SND_FILENAME | SND_ASYNC | SND_LOOP);
	}
}

void CaptchaComponent::PlayStop() const
{
	sndPlaySound(NULL, SND_ASYNC);
}

void CaptchaComponent::SetVolume(int value)
{
	if (mVolume != value)
	{
		mVolume = value;
		if (GetSystemVolume() != mVolume)
			SetSystemVolume(mVolume);
		signal_data();
	}
}

int CaptchaComponent::GetVolume() const
{
	return mVolume;
}

std::vector<std::string> CaptchaComponent::GetSoundNames() const
{
	std::vector<std::string> elements;
	elements.push_back("");

	for (auto& jt : std::filesystem::directory_iterator(mPath))
		if (jt.path().extension() == ".wav")
			elements.push_back(utf_helper::PathToUtf8(jt.path().stem()));

	return elements;
}

void CaptchaComponent::SetSoundName(std::string_view name)
{
	if (mSoundName != name)
	{
		mSoundName = name;
		signal_data();
	}
}

const std::string& CaptchaComponent::GetSoundName() const
{
	return mSoundName;
}

void CaptchaComponent::Tick()
{
	auto system_volume = GetSystemVolume();
	if (mVolume != system_volume)
	{
		mVolume = system_volume;
		signal_data();
	}
}

int CaptchaComponent::GetSystemVolume() const
{
	DWORD dwVolume;
	if (waveOutGetVolume(NULL, &dwVolume) == MMSYSERR_NOERROR)
	{
		auto volume = LOWORD(dwVolume);
		auto out = int(std::round(double(volume) * 100.0 / 65535.0));
		return out;
	}
	return 0;
}

void CaptchaComponent::SetSystemVolume(int value)
{
	auto volume = int(std::round(65535.0 * double(value) / 100.0));
	waveOutSetVolume(NULL, MAKELONG(volume, volume));
}

