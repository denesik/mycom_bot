#include "UnknownPageScreenshotterSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "UnknownPageScreenshotter.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

#undef GetObject

UnknownPageScreenshotterSettings::UnknownPageScreenshotterSettings(std::shared_ptr<UnknownPageScreenshotter> screenshotter, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mScreenshotter(screenshotter), mCorePath(core_path), mUserPath(user_path)
{

}

void UnknownPageScreenshotterSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void UnknownPageScreenshotterSettings::BeginPlay()
{
	mScreenshotterConnection = mScreenshotter->signal_data.connect([this]() { Save(mUserPath); });
}

void UnknownPageScreenshotterSettings::EndPlay()
{
	mScreenshotterConnection.disconnect();
}

void UnknownPageScreenshotterSettings::Deinitialize()
{
	Save(mUserPath);
}

void UnknownPageScreenshotterSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("unknown_screenshotter");
		if (it != json.MemberEnd() && it->value.IsObject())
		{
			auto section_json = it->value.GetObject();
			{
				auto jt = section_json.FindMember("enable");
				if (jt != section_json.MemberEnd() && jt->value.IsBool())
					mScreenshotter->SetEnable(jt->value.GetBool());
			}
			{
				auto jt = section_json.FindMember("recovery");
				if (jt != section_json.MemberEnd() && jt->value.IsBool())
					mScreenshotter->SetRecoveryMode(jt->value.GetBool());
			}
		}
	}
}

void UnknownPageScreenshotterSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		{
			auto it = json.FindMember("unknown_screenshotter");
			if (it == json.MemberEnd())
				json.AddMember("unknown_screenshotter", rapidjson::Value(rapidjson::kObjectType), json.GetAllocator());
		}
		if (!json["unknown_screenshotter"].IsObject())
			return;

		const auto& section_json = json["unknown_screenshotter"].GetObject();
		{
			auto it = section_json.FindMember("enable");
			if (it != section_json.MemberEnd() && it->value.IsBool())
				it->value = mScreenshotter->GetEnable();
			else
				section_json.AddMember("enable", mScreenshotter->GetEnable(), json.GetAllocator());
		}
		{
			auto it = section_json.FindMember("recovery");
			if (it != section_json.MemberEnd() && it->value.IsBool())
				it->value = mScreenshotter->GetRecoveryMode();
			else
				section_json.AddMember("recovery", mScreenshotter->GetRecoveryMode(), json.GetAllocator());
		}
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


