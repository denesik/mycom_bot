#pragma once

#include "IArenaAlgorithm.h"


class ArenaAlgorithm : public IArenaAlgorithm
{
public:
	virtual int GetAttackPos(const std::vector<PowerInfo> &data) const override;

private:

	std::vector<PowerInfo> GetPhase3(const std::vector<PowerInfo>& data) const;

};
