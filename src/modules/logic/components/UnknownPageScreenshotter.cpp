#include "UnknownPageScreenshotter.h"

#include "Profile.h"
#include "Page.h"
#include "Logger.h"
#include "utility/GenerateScreenshotPath.h"
#include "utility/OpencvFilesystem.h"
#include "utf_helper/PathUtf8.h"

#include "Detector.h"

UnknownPageScreenshotter::UnknownPageScreenshotter(const std::filesystem::path& path, std::shared_ptr<Detector> detector)
	: mPath(path), mDetector(detector)
{

}

void UnknownPageScreenshotter::SetEnable(bool value)
{
	if (mEnable != value)
	{
		mEnable = value;
		signal_data();
	}
}

bool UnknownPageScreenshotter::GetEnable() const
{
	return mEnable;
}

void UnknownPageScreenshotter::SetRecoveryMode(bool value)
{
	if (mRecoveryMode != value)
	{
		mRecoveryMode = value;
		signal_data();
	}
}

bool UnknownPageScreenshotter::GetRecoveryMode() const
{
	return mRecoveryMode;
}

void UnknownPageScreenshotter::SetStoreTime(unsigned int hours)
{
	if (mStoreTime != hours)
	{
		mStoreTime = hours;
		signal_data();
	}
}

unsigned int UnknownPageScreenshotter::GetStoreTime() const
{
	return mStoreTime;
}

void UnknownPageScreenshotter::Screenshot(std::string_view prefix)
{
	const Page& page = mDetector->GetPage();
	const cv::Mat& img = mDetector->GetImage();

	if (page.FullName() == "unknown")
	{
		auto path = utility::GenerateScreenshotPath(mPath, prefix);
		if (path == "")
			LOG_E("Error creating screenshot");
		else
		{
			if (!std::filesystem::exists(mPath))
				std::filesystem::create_directories(mPath);

			auto res = utility::Imwrite(path, img);
			if (!res)
				LOG_E("Error creating screenshot " + utf_helper::PathToUtf8(path));
		}
	}
}

void UnknownPageScreenshotter::BeginPlay()
{
	mConnectionDetect = mDetector->signal_detect.connect([this]()
		{
			if (mEnable)
				Screenshot("unknown_");
		});
}

void UnknownPageScreenshotter::EndPlay()
{
	mConnectionDetect.disconnect();
}
