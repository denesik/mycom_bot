#pragma once

#include <sigslot/signal.hpp>

#include "ILogicComponent.h"

class Emulator;
class Detector;
class Page;
class Informer;

class PageInformer : public ILogicComponent
{
public:
	PageInformer(std::shared_ptr<Emulator> emulator, std::shared_ptr<Detector> detector, std::shared_ptr<Informer> informer);

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void BeginComponent(std::string_view program) override;

	virtual void EndComponent(LogicResult result) override;

private:
	void Print(const Page& page) const;

private:
	std::shared_ptr<Emulator> mEmulator;
	std::shared_ptr<Detector> mDetector;
	std::shared_ptr<Informer> mInformer;
	
	sigslot::connection mConnectionDetect;

	bool mIsRunned = false;
};
