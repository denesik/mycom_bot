#include "TaskStatisticSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "TaskStatisticComponent.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

TaskStatisticSettings::TaskStatisticSettings(std::shared_ptr<TaskStatisticComponent> statistic, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mStatistic(statistic), mCorePath(core_path), mUserPath(user_path)
{

}

void TaskStatisticSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void TaskStatisticSettings::BeginPlay()
{
	mInformerConnection = mStatistic->signal_data.connect([this]() { Save(mUserPath); });
}

void TaskStatisticSettings::EndPlay()
{
	mInformerConnection.disconnect();
}

void TaskStatisticSettings::Deinitialize()
{
	Save(mUserPath);
}

void TaskStatisticSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("statistic");
		if (it != json.MemberEnd() && it->value.IsBool())
			mStatistic->SetEnable(it->value.GetBool());
	}
}

void TaskStatisticSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("statistic");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = mStatistic->GetEnable();
		else
			json.AddMember("statistic", mStatistic->GetEnable(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


