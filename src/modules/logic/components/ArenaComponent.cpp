#include "ArenaComponent.h"

#include <algorithm>
#include <sstream>
#include <iomanip>

#include "ArenaAlgorithm.h"
#include "Informer.h"

ArenaComponent::ArenaComponent(std::shared_ptr<Informer> informer)
	: mInformer(informer)
{
	mAlgorithms.emplace_back(std::make_unique<ArenaAlgorithm>());
}

void ArenaComponent::Init()
{
	for (auto &phase : mPhasesStatistic)
	{
		phase.info.clear();
		phase.status = PowerInfo::Status::UNKNOWN;
		phase.attack_pos = 0;
	}
}


void ArenaComponent::ClearPowers()
{
	mAttackPosition = 0;
	mAlgorithm = 0;
	for (auto &info : mPowerInfoList)
		info = {};
}

void ArenaComponent::SetSize(size_t value)
{
	if (value == 10 || value == 15)
		mPowerInfoList.resize(value);
}

int ArenaComponent::GetSize() const
{
	return static_cast<int>(mPowerInfoList.size());
}

void ArenaComponent::SetPowerInfo(int pos, PowerInfo::Status status, int my_power_min, int my_power_max, int other_power)
{
	if (pos < 0 || pos >= static_cast<int>(mPowerInfoList.size()))
		return;

	mPowerInfoList[pos] = { pos, status, my_power_min, my_power_max, other_power, 0 };
}



bool ArenaComponent::IsValidPowers() const
{
	return arena_utility::IsValidPowers(mPowerInfoList);
}

bool ArenaComponent::IsValidFinish() const
{
	int last_phase = 4;

	if (mPhasesStatistic[last_phase].status != PowerInfo::Status::DEFEAT && mPhasesStatistic[last_phase].status != PowerInfo::Status::VICTORY)
		return false;
	if (!arena_utility::IsValidPowers(mPhasesStatistic[last_phase].info))
		return false;
	if (arena_utility::GetPhase(mPhasesStatistic[last_phase].info) != last_phase)
		return false;

	return true;
}

void ArenaComponent::SetAttackPosition(int algorithm)
{
	int alg = algorithm;
	if (alg <= 0 || alg >= mAlgorithms.size())
		alg = 0;

	mAlgorithm = alg;
	mAttackPosition = std::clamp(mAlgorithms[mAlgorithm]->GetAttackPos(mPowerInfoList), 0, static_cast<int>(mPowerInfoList.size()));
}

int ArenaComponent::GetAttackPosition() const
{
	return mAttackPosition;
}

bool ArenaComponent::CheckAttackPosition(int other_power) const
{
	if (mAttackPosition < 0 || mAttackPosition >= static_cast<int>(mPowerInfoList.size()))
		return false;

	if (mPowerInfoList[mAttackPosition].status != PowerInfo::Status::AVAILABLE)
		return false;

	if (mPowerInfoList[mAttackPosition].other_power != other_power)
		return false;

	return true;
}

void ArenaComponent::SetBattleResult(PowerInfo::Status status)
{
	if (status == PowerInfo::Status::VICTORY)
		mInformer->Print("Battle result: victory");
	else if (status == PowerInfo::Status::DEFEAT)
		mInformer->Print("Battle result: defeat");
	else
		mInformer->Print("Battle result: unknown");

	if (arena_utility::IsValidPowers(mPowerInfoList))
	{
		auto phase = arena_utility::GetPhase(mPowerInfoList);
		if (phase >= 0 && phase < 5)
		{
			mPhasesStatistic[phase].info = mPowerInfoList;
			mPhasesStatistic[phase].status = status;
			mPhasesStatistic[phase].attack_pos = mAttackPosition;
			mPhasesStatistic[phase].algorithm = mAlgorithm;
		}
	}
}

void ArenaComponent::PrintCurrentPhase() const
{
	for (const auto &info : mPowerInfoList)
	{
		std::stringstream ss;
		ss << std::setiosflags(std::ios::fixed) << std::setw(4) << std::left
			<< info.pos + 1;
		if (info.status == PowerInfo::Status::AVAILABLE)
			ss << "A ";
		else if (info.status == PowerInfo::Status::DEFEAT)
			ss << "D ";
		else if (info.status == PowerInfo::Status::IS_ME)
			ss << "M ";
		else if (info.status == PowerInfo::Status::VICTORY)
			ss << "V ";
		else 
			ss << "U ";
		ss << std::setiosflags(std::ios::fixed) << std::setw(12) << std::left
			<< info.my_power_min;
		ss << std::setiosflags(std::ios::fixed) << std::setw(12) << std::left
			<< info.my_power_max;
		ss << std::setiosflags(std::ios::fixed) << std::setw(12) << std::left
			<< info.other_power;
		ss << std::setiosflags(std::ios::fixed) << std::setw(12) << std::left
			<< info.blacklist_count;

		mInformer->Print("Powers: " + ss.str());
	}
}
