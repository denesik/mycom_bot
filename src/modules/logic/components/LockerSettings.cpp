#include "LockerSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "LockerComponent.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

LockerSettings::LockerSettings(std::shared_ptr<LockerComponent> journal, const std::filesystem::path& user_path)
	: mJournal(journal), mUserPath(user_path)
{

}

void LockerSettings::Initialize()
{
	Load(mUserPath);
}

void LockerSettings::BeginPlay()
{
	mJournalConnection = mJournal->signal_data.connect([this]() { Save(mUserPath); });
}

void LockerSettings::EndPlay()
{
	mJournalConnection.disconnect();
}

void LockerSettings::Deinitialize()
{
	Save(mUserPath);
}

void LockerSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("locked_tasks");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& obj : it->value.GetArray())
				if (obj.IsObject() && obj.MemberCount() == 1)
					if (obj.MemberBegin()->name.IsString() && obj.MemberBegin()->value.IsUint64())
						mJournal->AddRecord(LockerComponent::RecordType::UnavaliableTask, obj.MemberBegin()->value.GetUint64(), std::string(obj.MemberBegin()->name.GetString(), obj.MemberBegin()->name.GetStringLength()));
	}
	{
		auto it = json.FindMember("resource_requests");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& obj : it->value.GetArray())
				if (obj.IsObject() && obj.MemberCount() == 1)
					if (obj.MemberBegin()->name.IsString() && obj.MemberBegin()->value.IsUint64())
						mJournal->AddRecord(LockerComponent::RecordType::RequirementTask, obj.MemberBegin()->value.GetUint64(), std::string(obj.MemberBegin()->name.GetString(), obj.MemberBegin()->name.GetStringLength()));
	}
	{
		auto it = json.FindMember("success_tasks");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& obj : it->value.GetArray())
				if (obj.IsObject() && obj.MemberCount() == 1)
					if (obj.MemberBegin()->name.IsString() && obj.MemberBegin()->value.IsUint64())
						mJournal->AddRecord(LockerComponent::RecordType::SuccessTask, obj.MemberBegin()->value.GetUint64(), std::string(obj.MemberBegin()->name.GetString(), obj.MemberBegin()->name.GetStringLength()));
	}
}

void LockerSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;

	if (!json.IsObject())
		json.SetObject();

	{
		if (json.FindMember("locked_tasks") == json.MemberEnd())
			json.AddMember("locked_tasks", rapidjson::Value(rapidjson::kArrayType), json.GetAllocator());
		if (json["locked_tasks"].IsArray())
		{
			auto json_array = json["locked_tasks"].GetArray();
			for (size_t i = 0; i < mJournal->CountRecords(LockerComponent::RecordType::UnavaliableTask); ++i)
			{
				rapidjson::Value obj(rapidjson::kObjectType);
				obj.AddMember(
					rapidjson::Value(mJournal->GetRecordName(LockerComponent::RecordType::UnavaliableTask, i), json.GetAllocator()),
					rapidjson::Value(mJournal->GetRecordTime(LockerComponent::RecordType::UnavaliableTask, i)),
					json.GetAllocator());
				json_array.PushBack(obj, json.GetAllocator());
			}
		}
	}
	{
		if (json.FindMember("resource_requests") == json.MemberEnd())
			json.AddMember("resource_requests", rapidjson::Value(rapidjson::kArrayType), json.GetAllocator());
		if (json["resource_requests"].IsArray())
		{
			auto json_array = json["resource_requests"].GetArray();
			for (size_t i = 0; i < mJournal->CountRecords(LockerComponent::RecordType::RequirementTask); ++i)
			{
				rapidjson::Value obj(rapidjson::kObjectType);
				obj.AddMember(
					rapidjson::Value(mJournal->GetRecordName(LockerComponent::RecordType::RequirementTask, i), json.GetAllocator()),
					rapidjson::Value(mJournal->GetRecordTime(LockerComponent::RecordType::RequirementTask, i)),
					json.GetAllocator());
				json_array.PushBack(obj, json.GetAllocator());
			}
		}
	}
	{
		if (json.FindMember("success_tasks") == json.MemberEnd())
			json.AddMember("success_tasks", rapidjson::Value(rapidjson::kArrayType), json.GetAllocator());
		if (json["success_tasks"].IsArray())
		{
			auto json_array = json["success_tasks"].GetArray();
			for (size_t i = 0; i < mJournal->CountRecords(LockerComponent::RecordType::SuccessTask); ++i)
			{
				rapidjson::Value obj(rapidjson::kObjectType);
				obj.AddMember(
					rapidjson::Value(mJournal->GetRecordName(LockerComponent::RecordType::SuccessTask, i), json.GetAllocator()),
					rapidjson::Value(mJournal->GetRecordTime(LockerComponent::RecordType::SuccessTask, i)),
					json.GetAllocator());
				json_array.PushBack(obj, json.GetAllocator());
			}
		}
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


