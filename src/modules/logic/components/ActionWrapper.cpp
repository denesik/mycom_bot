#include "ActionWrapper.h"

#include "Emulator.h"
#include "Detector.h"

#include "BlockButton.h"
#include "BlockIcon.h"
#include "BlockImage.h"
#include "BlockLine.h"
#include "utility/split_string.h"
#include "Logger.h"
#include "Informer.h"


ActionWrapper::ActionWrapper(std::shared_ptr<Detector> detector, std::shared_ptr<Emulator> emulator, std::shared_ptr<Informer> informer)
	: mDetector(detector), mController(emulator), mInformer(informer)
{
	mAsyncExecutor = std::make_unique<AsyncExecutor>();
}

ActionWrapper::~ActionWrapper()
{
	mAsyncExecutor.reset();
}

bool ActionWrapper::IsBusy() const
{
	return mAsyncExecutor->IsBusy();
}

void ActionWrapper::Move(const std::string& name) const
{
	bool finded = false;
	mDetector->GetPage().Foreach([&name, &finded, this](const IPageComponent& component)
		{
			if (component.GetType() == BlockLine::StaticType())
				if (component.GetName() == name)
				{
					auto start = static_cast<const BlockLine&>(component).GetPointStart();
					auto stop = static_cast<const BlockLine&>(component).GetPointStop();
					auto res = mAsyncExecutor->RunTask([start, stop, this]()
						{
							if (!mController.Move(start.x, start.y, stop.x, stop.y))
							;// LOG_E("%|40T |page: [%s]%|80T |Error move [%i %i]-[%i %i]"), mDetector->GetPage().FullName(), start.x, start.y, stop.x, stop.y);
						});
					mInformer->Print("%|40T |page: [%s]%|80T |Move line %s %b", mDetector->GetPage().FullName(), name, res);
					finded = true;
					return true;
				}
			return false;
		});
	if (!finded)
		;//LOG_E("%|40T |page: [%s]%|80T |Line %s not found"), mDetector->GetPage().FullName(), name);
}

void ActionWrapper::PageButtonPress(int index) const
{
	PageButtonPress(std::to_string(index));
}

void ActionWrapper::PageButtonPress(const std::string& name, int index) const
{
	PageButtonPress(name + "_" + std::to_string(index));
}

void ActionWrapper::PageButtonPress(const std::string& name) const
{
	bool finded = false;
	mDetector->GetPage().Foreach([&name, &finded, this](const IPageComponent& component)
		{
			if (component.GetType() == BlockButton::StaticType())
				if (component.GetName() == name)
				{
					auto rect = static_cast<const BlockButton&>(component).GetRect();
					auto res = mAsyncExecutor->RunTask([rect, this]()
						{
							std::this_thread::sleep_for(std::chrono::milliseconds(500));
							auto center = (rect.br() + rect.tl()) * 0.5;
							if (!mController.Click(center.x, center.y))
								;//LOG_E("%|40T |page: [%s]%|80T |Error click [%i %i]"), mDetector->GetPage().FullName(), center.x, center.y);
						});
					mInformer->Print("%|40T |page: [%s]%|80T |Press button %s %b", mDetector->GetPage().FullName(), name, res);
					finded = true;
					return true;
				}
			return false;
		});
	if (!finded)
		;//LOG_E("%|40T |page: [%s]%|80T |Button %s not found"), mDetector->GetPage().FullName(), name);
}

void ActionWrapper::EmulatorStart() const
{
	auto res = mAsyncExecutor->RunTask([this]()
		{
			if (!mController.Start())
			;//LOG_E(FormatLogicComponentMsg, mDetector->GetPage().FullName(), "Error start emulator");
		});
	mInformer->Print("%|40T |page: [%s]%|80T |Emulator Start %b", mDetector->GetPage().FullName(), res);
}

void ActionWrapper::EmulatorStop() const
{
	auto res = mAsyncExecutor->RunTask([this]()
		{
			if (!mController.Stop())
			;//LOG_E(FormatLogicComponentMsg, mDetector->GetPage().FullName(), "Error stop emulator");
		});
	mInformer->Print("%|40T |page: [%s]%|80T |Emulator Stop %b", mDetector->GetPage().FullName(), res);
}

void ActionWrapper::PressEscape() const
{
	auto res = mAsyncExecutor->RunTask([this]()
		{
			if (!mController.PressEscape())
			;//LOG_E(FormatLogicComponentMsg, mDetector->GetPage().FullName(), "Error press escape");
		});
	mInformer->Print("%|40T |page: [%s]%|80T |Press Escape %b", mDetector->GetPage().FullName(), res);
}

void ActionWrapper::ShowWindow() const
{
	auto res = mAsyncExecutor->RunTask([this]()
		{
			if (!mController.ShowWindow())
			;//LOG_E(FormatLogicComponentMsg, mDetector->GetPage().FullName(), "Error show window");
		});
	mInformer->Print("%|40T |page: [%s]%|80T |Show window %b", mDetector->GetPage().FullName(), res);
}

void ActionWrapper::PressIcon(std::string_view name, size_t index) const
{
	bool finded = false;
	mDetector->GetPage().Foreach([&name, &index, &finded, this](const IPageComponent& component)
		{
			if (component.GetType() == BlockIcon::StaticType())
				if (component.GetName() == name)
				{
					const auto & block = static_cast<const BlockIcon&>(component);
					if (index < block.GetCount())
					{
						auto rect = block.GetRect(index);
						auto res = mAsyncExecutor->RunTask([rect, this]()
							{
								auto center = (rect.br() + rect.tl()) * 0.5;
								if (!mController.Click(center.x, center.y))
									;//LOG_E("%|40T |page: [%s]%|80T |Error click [%i %i]"), mDetector->GetPage().FullName(), center.x, center.y);
							});
						mInformer->Print("%|40T |page: [%s]%|80T |Press icon %s %zu %b", mDetector->GetPage().FullName(), name, index, res);
					}
					else
						;//LOG_E("%|40T |page: [%s]%|80T |Not found index %zu from icon %s"), mDetector->GetPage().FullName(), index, name);
					finded = true;
					return true;
				}
			return false;
		});
	if (!finded)
		;//LOG_E("%|40T |page: [%s]%|80T |Icon %s not found"), mDetector->GetPage().FullName(), name);
}

void ActionWrapper::PressImage(std::string_view name, size_t index) const
{
	size_t offset = 0;
	bool finded_component = false;
	bool finded_index = false;
	mDetector->GetPage().Foreach([&name, &index, &finded_component, &finded_index, this, &offset](const IPageComponent& component)
		{
			if (component.GetType() == BlockImage::StaticType())
			{
				const auto& fullname = component.GetName();
				std::string first;
				std::string second;
				utility::split_string("-", fullname, first, second);

				if (name == fullname || name == first)
				{
					const auto& block = static_cast<const BlockImage&>(component);
					auto count = block.GetCount();
					auto local_index = index - offset;

					if (local_index < count)
					{
						auto rect = block.GetRect(local_index);
						auto res = mAsyncExecutor->RunTask([rect, this]()
							{
								auto center = (rect.br() + rect.tl()) * 0.5;
								if (!mController.Click(center.x, center.y))
									;//LOG_E("%|40T |page: [%s]%|80T |Error click [%i %i]"), mDetector->GetPage().FullName(), center.x, center.y);
							});
						mInformer->Print("%|40T |page: [%s]%|80T |Press image %s %zu %b", mDetector->GetPage().FullName(), name, local_index, res);
						finded_index = true;
						finded_component = true;
						return true;
					}

					offset += count;
					finded_component = true;
				}
			}
			return false;
		});
	if (!finded_component)
		;//LOG_E("%|40T |page: [%s]%|80T |Image %s not found"), mDetector->GetPage().FullName(), name);
	else if (!finded_index)
		;//LOG_E("%|40T |page: [%s]%|80T |Not found index %zu from image %s"), mDetector->GetPage().FullName(), index, name);
}
