#include "PageInformer.h"

#include "Emulator.h"
#include "Page.h"
#include "Detector.h"
#include "Informer.h"

PageInformer::PageInformer(std::shared_ptr<Emulator> emulator, std::shared_ptr<Detector> detector, std::shared_ptr<Informer> informer)
	: mEmulator(emulator), mDetector(detector), mInformer(informer)
{

}

void PageInformer::BeginPlay()
{
	mConnectionDetect = mDetector->signal_detect.connect([this]()
		{
			Print(mDetector->GetPage());
		});
}

void PageInformer::EndPlay()
{
	mConnectionDetect.disconnect();
}

void PageInformer::BeginComponent(std::string_view program)
{
	mIsRunned = true;
}

void PageInformer::EndComponent(LogicResult result)
{
	mIsRunned = false;
}

void PageInformer::Print(const Page& page) const
{
	if (mIsRunned)
		mInformer->Print("Emulator: %s [%i]%|40T |page: [%s]", mEmulator->GetWindowName(), mEmulator->GetInstance(), page.FullName());
}
