#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class TaskStatisticComponent;

class TaskStatisticSettings : public ActorComponent
{
public:
	TaskStatisticSettings(std::shared_ptr<TaskStatisticComponent> statistic, const std::filesystem::path& core_path, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mCorePath;
	const std::filesystem::path mUserPath;

	std::shared_ptr<TaskStatisticComponent> mStatistic;

	sigslot::connection mInformerConnection;
};


