#include "CaptchaSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "CaptchaComponent.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

CaptchaSettings::CaptchaSettings(std::shared_ptr<CaptchaComponent> captcha, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mCaptcha(captcha), mCorePath(core_path), mUserPath(user_path)
{

}

void CaptchaSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void CaptchaSettings::BeginPlay()
{
	mCaptchaConnection = mCaptcha->signal_data.connect([this]() { Save(mUserPath); });
}

void CaptchaSettings::EndPlay()
{
	mCaptchaConnection.disconnect();
}

void CaptchaSettings::Deinitialize()
{
	Save(mUserPath);
}

void CaptchaSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("sound_name");
		if (it != json.MemberEnd() && it->value.IsString())
			mCaptcha->SetSoundName(std::string(it->value.GetString(), it->value.GetStringLength()));
	}
	{
		auto it = json.FindMember("sound_volume");
		if (it != json.MemberEnd() && it->value.IsInt())
			mCaptcha->SetVolume(it->value.GetInt());
	}
}

void CaptchaSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("sound_name");
		if (it != json.MemberEnd() && it->value.IsString())
			it->value = rapidjson::Value(mCaptcha->GetSoundName(), json.GetAllocator());
		else
			json.AddMember("sound_name", rapidjson::Value(mCaptcha->GetSoundName(), json.GetAllocator()), json.GetAllocator());
	}
	{
		auto it = json.FindMember("sound_volume");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = mCaptcha->GetVolume();
		else
			json.AddMember("sound_volume", mCaptcha->GetVolume(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


