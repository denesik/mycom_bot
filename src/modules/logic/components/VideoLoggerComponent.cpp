#include "VideoLoggerComponent.h"

#include <fstream>

#include "Logger.h"
#include "utility/CaptureSize.h"
#include <opencv2/imgproc.hpp>

#include "Detector.h"

//https://stackoverflow.com/questions/48817441/opencv-encoding-to-h264

namespace
{
//	static const std::string video_ext = ".avi";
	static const std::string video_ext = ".mp4";
}

VideoLoggerComponent::VideoLoggerComponent(const std::filesystem::path &path, std::shared_ptr<Detector> detector)
	: mDetector(detector)
{
	mDirectory = path;
	mFileTimeleftController.SetDirectory(mDirectory);

	mFfmpegEncoder.SetLogCallback([](const std::string& msg)
		{
			LOG_E("FFMPEG " + msg);
		});
}

void VideoLoggerComponent::SetEnable(bool value)
{
	if (mEnable != value)
	{
		mEnable = value;
		signal_data();
	}
}

bool VideoLoggerComponent::GetEnable() const
{
	return mEnable;
}

void VideoLoggerComponent::SetStoreTime(unsigned int hours)
{
	if (mFileTimeleftController.GetStoreTime() / 60 != hours)
	{
		mFileTimeleftController.SetStoreTime(hours * 60);
		signal_data();
	}
}

unsigned int VideoLoggerComponent::GetStoreTime() const
{
	return mFileTimeleftController.GetStoreTime() / 60;
}

void VideoLoggerComponent::SetLevel(LogicResult level)
{
	if (mLevel != level)
	{
		mLevel = level;
		signal_data();
	}
}

LogicResult VideoLoggerComponent::GetLevel() const
{
	return mLevel;
}

void VideoLoggerComponent::BeginComponent(std::string_view program)
{
	if (!mEnable)
		return;


	mFfmpegEncoder.Close();

	try
	{
		if (!std::filesystem::is_directory(mDirectory))
			std::filesystem::create_directories(mDirectory);

		if (std::filesystem::exists(mDirectory / "video.tmp"))
			std::filesystem::remove(mDirectory / "video.tmp");
	}
	catch (std::filesystem::filesystem_error &)
	{
		return;
	}

	mRunned = true;
	mRunnedName = GenerateName(program);

	{
		// 		std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
		// 		std::stringstream string_stream;
		// 
		// 		std::cout.rdbuf(string_stream.rdbuf());
		// 
		// 		//auto fourcc = cv::VideoWriter::fourcc('M', 'J', 'P', 'G');
		// 		//auto fourcc = cv::VideoWriter::fourcc('M', 'P', '4', 'V');
		// 		//auto fourcc = cv::VideoWriter::fourcc('X', 'V', 'I', 'D');
		// 		//auto fourcc = cv::VideoWriter::fourcc('X', '2', '6', '4');
		// 		auto fourcc = cv::VideoWriter::fourcc('H', '2', '6', '4');
		// 
		// 		mVideoWriter.open("logs/temp" + video_ext, fourcc, 20.0, utility::CAPTURE_IMAGE_SIZE, true);
		// 		auto opened = mVideoWriter.isOpened();
		// 
		// 		std::cout.rdbuf(cout_sbuf);
		// 
		// 		if (!opened)
		// 		{
		// 			std::string line;
		// 			while (std::getline(string_stream, line))
		// 				LOG_I(line);
		// 		}

		FfmpegEncoder::Params param;
		param.width = utility::GetCaptureSize().width;
		param.height = utility::GetCaptureSize().height;
		param.fps = 20;
		param.bitrate = static_cast<uint32_t>(utility::GetCaptureSize().width * utility::GetCaptureSize().height * 3 * 8 * param.fps / 60);
		param.preset = "veryslow";
		param.crf = 40;
		param.src_format = AV_PIX_FMT_BGR24;
		param.dst_format = AV_PIX_FMT_GRAY8;

		mFfmpegEncoder.Open((mDirectory / "video.tmp").string().c_str(), param, video_ext.c_str());
	}

}

void VideoLoggerComponent::EndComponent(LogicResult result)
{
	mFileTimeleftController.RemoveUnwanted(video_ext);

	mFfmpegEncoder.Close();

	try
	{
		if (mEnable && mRunned && ((mLevel & result) == result))
		{
			if (std::filesystem::exists(mDirectory / "video.tmp"))
			{
				mRunnedName = std::string(LogicResultDescription(result)) + " " + mRunnedName;
				std::filesystem::rename(mDirectory / "video.tmp", mDirectory / (mRunnedName + video_ext));
			}
		}

		if (std::filesystem::exists(mDirectory / "video.tmp"))
			std::filesystem::remove(mDirectory / "video.tmp");
	}
	catch (std::filesystem::filesystem_error& )
	{
	}

	mRunnedName.clear();
	mRunned = false;
}

void VideoLoggerComponent::SetImage(const cv::Mat& img)
{
	if (!mRunned)
		return;

	img.copyTo(mImage);

	cv::rectangle(mImage, cv::Rect(0, 0, 90, 28), cv::Scalar(255, 255, 255), -1);

	std::string time_str;
	{
		auto time = std::time(nullptr);
		struct tm buf;
		localtime_s(&buf, &time);
		std::stringstream ss;
		ss << std::put_time(&buf, "%H:%M:%S");
		time_str = ss.str();
	}

	cv::putText(mImage, time_str, cv::Point(5, 19), cv::FONT_HERSHEY_COMPLEX, 0.5, cv::Scalar(0, 0, 0), 1, cv::LINE_AA);
	mFfmpegEncoder.Write(mImage.data);
}

void VideoLoggerComponent::BeginPlay()
{
	mConnectionDetect = mDetector->signal_detect.connect([this]()
		{
			SetImage(mDetector->GetImage());
		});
}

void VideoLoggerComponent::EndPlay()
{
	mConnectionDetect.disconnect();
}

std::string VideoLoggerComponent::GenerateName(std::string_view program)
{
	std::string time;
	{
		auto t = std::time(nullptr);
		struct tm buf;
		localtime_s(&buf, &t);
		std::stringstream ss;
		ss << std::put_time(&buf, "%d-%m-%y %H-%M-%S");
		time = ss.str();
	}

	return std::string(program) + " " + time;
}

