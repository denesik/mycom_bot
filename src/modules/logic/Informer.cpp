#define _CRT_SECURE_NO_WARNINGS

#include "Informer.h"

#include "Version.h"

#include <boost/format.hpp>

#include <ctime>
#include <sstream>
#include <iomanip>

Informer::Informer()
	: mVersion(LoadVersion())
{

}

std::string Informer::Print1(const char* category, std::string_view msg) const
{
	return (boost::format("%s %s (%s)%|20T |%s") % mVersion % GetTime() % category % msg).str();
}

std::string Informer::GetTime() const
{
	std::time_t t = std::time(nullptr);
	char mbstr[300];
	if (std::strftime(mbstr, sizeof(mbstr), "%H:%M:%S", localtime(&t)))
		return mbstr;
	return {};
}
