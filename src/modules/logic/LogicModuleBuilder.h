#pragma once

#include "IModuleBuilder.h"

class LogicModuleBuilder : public IModuleBuilder
{
public:

	virtual void AddComponents(const Profile& profile, Actor* actor) override;

};


