#pragma once

#include "LogicComponents.h"
#include "ActorComponent.h"

#include "Informer.h"

class LogicContext;
class Informer;

class Logic : public LogicComponents, public ActorComponent
{
public:
	Logic(std::shared_ptr<Informer> informer, LogicComponentList components);

	bool Apply(const Program::Task& task);
	void Stop();

	bool IsBusy() const;

	void SetPause(bool val);

	bool IsPaused() const;

	virtual void Tick() override;

	template <class T>
	T & find()
	{
		return *mInformer;
	}

	template <class T>
	void exec(const std::function<void(T&)>& func)
	{
		func(*mInformer);
	}

	// public for smc
public:
	LogicData config;
	std::string program_type;
	Informer& informer;

public:

	void OnFinished(LogicResult result);

private:
	void Print(const LogicData& data) const;

private:
	std::shared_ptr<Informer> mInformer;

private:
	enum class Status
	{
		Ready,
		Busy,
		Stopping,
	};

	Status mStatus = Status::Ready;

	bool mPause = false;
	int mLastStateId = -1;

	LogicResult mFinishedResult = LogicResult::Unknown;
	bool mFinished = false;
private:
	std::shared_ptr<LogicContext> mStateMachine;

};
