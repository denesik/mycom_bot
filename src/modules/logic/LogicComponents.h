#pragma once

#include <memory>

#include "LogicData.h"
#include "LogicTools.h"
#include "ActionWrapper.h"
#include "StateWrapper.h"
#include "ArenaComponent.h"
#include "RecoveryValuesComponent.h"
#include "CaptchaComponent.h"
#include "LockerComponent.h"
#include "UnknownPageScreenshotter.h"
#include "LighthouseHelper.h"
#include "TimeDayZone.h"

using LogicComponentList = std::vector<std::shared_ptr<ILogicComponent>>;

class LogicComponents
{
public:
	LogicComponents(LogicComponentList list);

	LogicData values;
	LogicTools tools;
	LighthouseHelper lighthouse;

	const ActionWrapper& action;
	const StateWrapper& info;
	TimeDayZone time;

	CaptchaComponent& captcha;
	ArenaComponent& arena;
	RecoveryValuesComponent& recovery;
	LockerComponent& journal;
	UnknownPageScreenshotter& screenshotter;

protected:

	void BeginComponent(std::string_view program);

	void EndComponent(LogicResult result);

private:
	LogicComponentList mComponents;

private:
	template<class T>
	const T* FindComponent(const LogicComponentList& list) const
	{
		for (auto& v : list)
			if (auto component = dynamic_cast<T *>(v.get()))
				return static_cast<const T*>(v.get());

		return nullptr;
	}

	template<class T>
	T* FindComponent(LogicComponentList& list)
	{
		for (auto& v : list)
			if (auto component = dynamic_cast<T *>(v.get()))
				return static_cast<T*>(v.get());

		return nullptr;
	}
};
