#include "LogicComponents.h"

#include "Actor.h"

#include <ranges>

LogicComponents::LogicComponents(LogicComponentList list)
	: mComponents(list),
	action(*FindComponent<ActionWrapper>(list)),
	info(*FindComponent<StateWrapper>(list)),
	arena(*FindComponent<ArenaComponent>(list)),
	recovery(*FindComponent<RecoveryValuesComponent>(list)),
	captcha(*FindComponent<CaptchaComponent>(list)),
	journal(*FindComponent<LockerComponent>(list)),
	screenshotter(*FindComponent<UnknownPageScreenshotter>(list))
{

}

void LogicComponents::BeginComponent(std::string_view program)
{
	for (const auto& component : mComponents)
		component.get()->BeginComponent(program);
}

void LogicComponents::EndComponent(LogicResult result)
{
	for (const auto& component : std::ranges::reverse_view(mComponents))
		component.get()->EndComponent(result);
}

