#pragma once

#include <string>
#include "EnumFlagOperators.h"

enum class LogicResult : int
{
	None = 0,

	Full = 1 << 0,						// ��������� �������� ���������, ��������� ���������, ������ ��������� ���������.
	Partly = 1 << 1,					// ��������� �������� ���������, ��������� ���������, ������ ���������  ��������.
	Error = 1 << 2,						// ��������� �������� �� ���������.
	Stop = 1 << 3,						// �������� �������������.

	Condition = 1 << 4,
	Unavailable = 1 << 5,

	Requirement = 1 << 6,

	Unknown = 1 << 7,

	All = Full | Partly | Error | Stop | Condition | Unavailable | Unknown | Requirement,
	Size = 8,
};

const char* LogicResultString(LogicResult value);
LogicResult LogicResultString(std::string_view msg);

const char* LogicResultDescription(LogicResult result);

DEFINE_ENUM_FLAGS_OPERATORS(LogicResult)
