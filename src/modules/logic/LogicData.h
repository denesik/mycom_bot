#pragma once

#include "Program.h"

class LogicData
{
public:
	void Clear();

	void Set(const Program::Values& values);

	bool SetValue(const std::string& name, const Value &value);
	bool SetValue(const std::string& name, int value);
	bool SetValue(const std::string& name, bool value);
	bool SetValue(const std::string& name, const char* value);
	bool SetValue(const std::string& name, const std::string &value);
	bool SetValue(const std::string& name, std::string_view value);
	int GetIntValue(const std::string& name, int default_val = 0) const;
	bool GetBoolValue(const std::string& name, bool default_val = false) const;
	std::string GetStringValue(const std::string &name, const std::string &default_val = "") const;

	bool FindIntValue(const std::string& name, int& value) const;
	bool FindBoolValue(const std::string& name, bool& value) const;
	bool FindStringValue(const std::string& name, std::string& value) const;

	const auto begin() const { return mData.begin(); }
	const auto end() const { return mData.end(); }

	friend auto operator<=>(const LogicData& a, const LogicData& b) = default;

private:
	Program::Values mData;
};


