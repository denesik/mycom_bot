#include <wx/wxprec.h>
#include <wx/notebook.h>

#include "EditorLogicPageBuilder.h"

#include "EditorReportWidget.h"
#include "EditorInformerWidget.h"
#include "EditorAdditionallyWidget.h"

#include "Actor.h"
#include "CaptchaComponent.h"
#include "TaskStatisticComponent.h"
#include "UnknownPageScreenshotter.h"
#include "EditorLogicOptions.h"
#include "Dictionary.h"
#include "ProgramReport.h"
#include "Informer.h"

void EditorLogicPageBuilder::AddPages(wxNotebook* notebook)
{
	notebook->AddPage(new EditorAdditionallyWidget(notebook, mCaptchaComponent, mTaskStatisticComponent, mUnknownPageScreenshotter, mRotatedFileInformerSink, mVideoLoggerComponent), _("Additionally"));
	notebook->AddPage(new EditorInformerWidget(notebook, mInformer), _("Informer"));
}
