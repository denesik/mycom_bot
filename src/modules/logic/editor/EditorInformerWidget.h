#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>
#include <deque>
#include <mutex>

class Informer;

class EditorInformerWidget : public wxScrolledWindow
{
public:
	EditorInformerWidget(wxWindow* parent, std::shared_ptr<Informer> informer);
	~EditorInformerWidget();

private:
	void Print();

private:
	std::shared_ptr<Informer> mInformer;


	class wxTextCtrl* mConsoleLog = nullptr;
	wxTimer mTimerLogger;

	std::mutex mMutex;
	struct Data
	{
		std::string msg;
		wxColour color;
	};
	std::deque<Data> mBuffer;

	sigslot::connection mConnectionPrint;
};


