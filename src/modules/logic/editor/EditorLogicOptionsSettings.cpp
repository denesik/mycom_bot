#include "EditorLogicOptionsSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "EditorLogicOptions.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

#undef GetObject


EditorLogicOptionsSettings::EditorLogicOptionsSettings(std::shared_ptr<EditorLogicOptions> editor, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mEditor(editor), mCorePath(core_path), mUserPath(user_path)
{

}

void EditorLogicOptionsSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void EditorLogicOptionsSettings::BeginPlay()
{
	mEditorConnection = mEditor->signal_data.connect([this]() { Save(mUserPath); });
}

void EditorLogicOptionsSettings::EndPlay()
{
	mEditorConnection.disconnect();
}

void EditorLogicOptionsSettings::Deinitialize()
{
	Save(mUserPath);
}

void EditorLogicOptionsSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("program_report_period");
		if (it != json.MemberEnd() && it->value.IsUint64())
			mEditor->SetProgramReportPeriod(it->value.GetUint64());
	}
}

void EditorLogicOptionsSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("program_report_period");
		if (it != json.MemberEnd() && it->value.IsUint64())
			it->value = mEditor->GetProgramReportPeriod();
		else
			json.AddMember("program_report_period", mEditor->GetProgramReportPeriod(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


