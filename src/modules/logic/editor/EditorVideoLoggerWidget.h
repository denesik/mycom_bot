#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>

class VideoLoggerComponent;

class EditorVideoLoggerWidget : public wxPanel
{
public:
	EditorVideoLoggerWidget(wxWindow* parent, std::shared_ptr<VideoLoggerComponent> video_logger);

private:
	void UpdateVideoLoggerComponentData();

private:
	std::shared_ptr<VideoLoggerComponent> mVideoLoggerComponent;

	class wxCheckListBox* mVideoLoggerCheckBox = nullptr;

	class wxCheckBox* mVideoLoggerComponentEnable = nullptr;
	class wxSpinCtrl* mVideoLoggerComponentStoreTime = nullptr;

	sigslot::scoped_connection mConnectionVideoLoggerComponent;
};
