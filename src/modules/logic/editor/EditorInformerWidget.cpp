#include "EditorInformerWidget.h"

#include "Informer.h"


EditorInformerWidget::EditorInformerWidget(wxWindow* parent, std::shared_ptr<Informer> informer)
	: wxScrolledWindow(parent, wxID_ANY), mInformer(informer)
{
	SetScrollbars(0, 10, 0, 100, 0, 0);
	SetScrollRate(5, 30);
	ShowScrollbars(wxSHOW_SB_NEVER, wxSHOW_SB_DEFAULT);

	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);

	vsizer_owner->AddSpacer(10);
	{
		wxTextCtrl* element = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(-1, 10000), wxTE_MULTILINE | wxTE_READONLY | wxTE_NO_VSCROLL | wxTE_RICH2);
		element->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, "Consolas"));
		mConsoleLog = element;
		vsizer_owner->Add(element, 1, wxEXPAND | wxLEFT | wxRIGHT, 5);
	}
	vsizer_owner->AddSpacer(6);

	SetSizer(vsizer_owner);

	mTimerLogger.Bind(wxEVT_TIMER, [this](wxTimerEvent& event)
		{
			Print();
		});
	mTimerLogger.Start(1000 / 5);

	mConnectionPrint = mInformer->signal_print.connect([this](const std::string& msg)
		{
			std::lock_guard<std::mutex> lock(mMutex);
			union
			{
				uint32_t data = 0;
				uint8_t channels[4];
			} col;

			mBuffer.emplace_back(msg + '\n', wxColour(col.channels[2], col.channels[1], col.channels[0]));
			while (mBuffer.size() > 1000)
				mBuffer.pop_front();
		});
}

EditorInformerWidget::~EditorInformerWidget()
{
	mConnectionPrint.disconnect();
}

void EditorInformerWidget::Print()
{
	decltype(mBuffer) buffer;
	{
		std::lock_guard<std::mutex> lock(mMutex);
		buffer = std::move(mBuffer);
	}

	const int min_lines = 1000;
	const int max_lines = min_lines + 100;
	if (mConsoleLog->GetNumberOfLines() > max_lines)
	{
		int offset = 0;
		size_t count_lines = static_cast<size_t>(mConsoleLog->GetNumberOfLines() - min_lines);
		for (size_t i = 0; i < count_lines; ++i)
			offset += mConsoleLog->GetLineLength(i) + 1;

		mConsoleLog->Remove(0, offset);
	}

	while (!buffer.empty())
	{
		auto start = mConsoleLog->GetLastPosition();
		mConsoleLog->AppendText(buffer.front().msg);
		mConsoleLog->SetStyle(start, mConsoleLog->GetLastPosition(), wxTextAttr(buffer.front().color));
		buffer.pop_front();
	}
}

