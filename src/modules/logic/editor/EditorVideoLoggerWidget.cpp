#include "EditorVideoLoggerWidget.h"

#include <wx/spinctrl.h>

#include "VideoLoggerComponent.h"



EditorVideoLoggerWidget::EditorVideoLoggerWidget(wxWindow* parent, std::shared_ptr<VideoLoggerComponent> video_logger)
	: wxPanel(parent, wxID_ANY), mVideoLoggerComponent(video_logger)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);

	{
		auto element = new wxCheckBox(this, wxID_ANY, _("Video logger enable"));
		element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
			{
				mVideoLoggerComponent->SetEnable(element->GetValue());
			});
		mVideoLoggerComponentEnable = element;
		vsizer_owner->Add(element, 0);
	}
	vsizer_owner->AddSpacer(5);
	{
		wxArrayString elements;
		for (size_t i = 0; i < static_cast<size_t>(LogicResult::Size); ++i)
			elements.push_back(_(LogicResultDescription(static_cast<LogicResult>(1 << i))));
		auto element = new wxCheckListBox(this, wxID_ANY, wxDefaultPosition, wxSize(216, -1), elements);
		element->Bind(wxEVT_CHECKLISTBOX, [this, element](wxCommandEvent& evt)
			{
				LogicResult info = LogicResult::None;
				for (uint32_t i = 0; i < element->GetCount(); ++i)
					info |= element->IsChecked(i) ? static_cast<LogicResult>(1 << i) : LogicResult::None;
				mVideoLoggerComponent->SetLevel(info);
			});
		mVideoLoggerCheckBox = element;
		vsizer_owner->Add(element, 0);
	}
	vsizer_owner->AddSpacer(5);
	{
		auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
		{
			auto element = new wxStaticText(this, wxID_ANY, _("Store time (hours)"), wxDefaultPosition, wxSize(150, -1));
			hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
		}
		{
			auto element = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxSize(64, -1));
			element->Bind(wxEVT_SPINCTRL, [this, element](wxCommandEvent& evt)
				{
					mVideoLoggerComponent->SetStoreTime(element->GetValue());
				});
			mVideoLoggerComponentStoreTime = element;
			hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
		}
		hsizer_block->AddStretchSpacer();
		vsizer_owner->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
	}

	SetSizer(vsizer_owner);

	mConnectionVideoLoggerComponent = mVideoLoggerComponent->signal_data.connect(&EditorVideoLoggerWidget::UpdateVideoLoggerComponentData, this);
	UpdateVideoLoggerComponentData();
}

void EditorVideoLoggerWidget::UpdateVideoLoggerComponentData()
{
	LogicResult info = mVideoLoggerComponent->GetLevel();
	for (uint32_t i = 0; i < mVideoLoggerCheckBox->GetCount(); ++i)
		mVideoLoggerCheckBox->Check(i, (static_cast<uint32_t>(info) & 1U << i) == 1U << i);
	mVideoLoggerComponentEnable->SetValue(mVideoLoggerComponent->GetEnable());
	mVideoLoggerComponentStoreTime->SetValue(mVideoLoggerComponent->GetStoreTime());
}
