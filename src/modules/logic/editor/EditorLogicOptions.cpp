#include "EditorLogicOptions.h"

size_t EditorLogicOptions::GetProgramReportPeriod() const
{
	return mProgramReportPeriod;
}

void EditorLogicOptions::SetProgramReportPeriod(size_t value)
{
	if (mProgramReportPeriod != value)
	{
		mProgramReportPeriod = value;
		signal_data();
	}
}
