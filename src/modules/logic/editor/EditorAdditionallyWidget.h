#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>

class CaptchaComponent;
class TaskStatisticComponent;
class UnknownPageScreenshotter;
class RotatedFileInformerSink;
class VideoLoggerComponent;

class EditorAdditionallyWidget : public wxPanel
{
public:
	EditorAdditionallyWidget(wxWindow* parent, std::shared_ptr<CaptchaComponent> captcha, std::shared_ptr<TaskStatisticComponent> task_stats, 
		std::shared_ptr<UnknownPageScreenshotter> unkn_screenshotter, std::shared_ptr<RotatedFileInformerSink> file_informer_sink, std::shared_ptr<VideoLoggerComponent> video_logger);

private:
	void UpdateTaskStatisticComponent();
	void UpdateUnknownPageScreenshotter();

private:
	std::shared_ptr<CaptchaComponent> mCaptchaComponent;
	std::shared_ptr<TaskStatisticComponent> mTaskStatisticComponent;
	std::shared_ptr<UnknownPageScreenshotter> mUnknownPageScreenshotter;

	class wxCheckBox* mTaskStatisticComponentEnable = nullptr;
	class wxCheckBox* mUnknownPageScreenshotterEnable = nullptr;
	class wxCheckBox* mUnknownPageScreenshotterRecovery = nullptr;

	class wxComboBox* mSoundName = nullptr;
	class wxSlider* mVolume = nullptr;

	sigslot::scoped_connection mConnectionPagePrinterData;
	sigslot::scoped_connection mConnectionTaskStatisticComponent;
	sigslot::scoped_connection mConnectionUnknownPageScreenshotter;
};
