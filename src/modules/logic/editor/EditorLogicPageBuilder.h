#pragma once

#include <memory>

#include "IEditorPageBuilder.h"
#include "ActorComponent.h"

class CaptchaComponent;
class TaskStatisticComponent;
class UnknownPageScreenshotter;
class Informer;
class RotatedFileInformerSink;
class VideoLoggerComponent;

class EditorLogicPageBuilder : public ActorComponent, public IEditorPageBuilder
{
public:
	EditorLogicPageBuilder(
		std::shared_ptr<CaptchaComponent> captcha,
		std::shared_ptr<TaskStatisticComponent> stats,
		std::shared_ptr<UnknownPageScreenshotter> screenshotter,
		std::shared_ptr<Informer> informer,
		std::shared_ptr<RotatedFileInformerSink> file_informer_sink,
		std::shared_ptr<VideoLoggerComponent> video_logger
	) : 
		mCaptchaComponent(captcha),
		mTaskStatisticComponent(stats),
		mUnknownPageScreenshotter(screenshotter),
		mInformer(informer),
		mRotatedFileInformerSink(file_informer_sink),
		mVideoLoggerComponent(video_logger)
	{	}

	virtual void AddPages(wxNotebook* notebook) override;

private:
	std::shared_ptr<CaptchaComponent> mCaptchaComponent;
	std::shared_ptr<TaskStatisticComponent> mTaskStatisticComponent;
	std::shared_ptr<UnknownPageScreenshotter> mUnknownPageScreenshotter;
	std::shared_ptr<Informer> mInformer;
	std::shared_ptr<RotatedFileInformerSink> mRotatedFileInformerSink;
	std::shared_ptr<VideoLoggerComponent> mVideoLoggerComponent;

};


