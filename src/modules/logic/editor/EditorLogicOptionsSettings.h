#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class EditorLogicOptions;

class EditorLogicOptionsSettings : public ActorComponent
{
public:
	EditorLogicOptionsSettings(std::shared_ptr<EditorLogicOptions> editor, const std::filesystem::path& core_path, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mCorePath;
	const std::filesystem::path mUserPath;

	std::shared_ptr<EditorLogicOptions> mEditor;

	sigslot::connection mEditorConnection;
};


