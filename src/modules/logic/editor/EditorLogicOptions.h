#pragma once

#include <sigslot/signal.hpp>

#include "ActorComponent.h"
#include "Point.h"

class EditorLogicOptions : public ActorComponent
{
public:
	EditorLogicOptions() = default;

	sigslot::signal<> signal_data;

	size_t GetProgramReportPeriod() const;
	void SetProgramReportPeriod(size_t value);

private:
	size_t mProgramReportPeriod = 0;

};
