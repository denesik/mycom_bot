#include "EditorAdditionallyWidget.h"

#include <wx/statline.h>

#include "TaskStatisticComponent.h"
#include "UnknownPageScreenshotter.h"

#include "EditorFileInformerWidget.h"
#include "EditorVideoLoggerWidget.h"
#include "CaptchaComponent.h"

EditorAdditionallyWidget::EditorAdditionallyWidget(wxWindow* parent, std::shared_ptr<CaptchaComponent> captcha, std::shared_ptr<TaskStatisticComponent> task_stats, 
		std::shared_ptr<UnknownPageScreenshotter> unkn_screenshotter, std::shared_ptr<RotatedFileInformerSink> file_informer_sink, std::shared_ptr<VideoLoggerComponent> video_logger)
	: wxPanel(parent, wxID_ANY), mCaptchaComponent(captcha), mTaskStatisticComponent(task_stats), mUnknownPageScreenshotter(unkn_screenshotter)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);
	vsizer_owner->AddSpacer(3);
	{
		auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this);

		vsizer_block->GetStaticBox()->SetForegroundColour(wxColor(0, 170, 0));
		vsizer_block->GetStaticBox()->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		vsizer_block->AddSpacer(5);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxStaticText(this, wxID_ANY, _("Captcha sound"), wxDefaultPosition, wxSize(250, -1));
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			hsizer_block->AddSpacer(5);
			{
				wxArrayString elements;
				auto sound_files = mCaptchaComponent->GetSoundNames();
				auto sound_file = mCaptchaComponent->GetSoundName();
				for (const auto& name : sound_files)
					if (name.empty())
						elements.push_back(_("Disabled"));
					else
						elements.push_back(name);

				auto element = new wxComboBox(this, wxID_ANY, sound_file.empty() ? _("Disabled") : sound_file, wxDefaultPosition, wxSize(158, 23), elements);
				element->Bind(wxEVT_COMBOBOX, [this, element](wxCommandEvent& event)
					{
						auto selection = element->GetStringSelection();
				mCaptchaComponent->SetSoundName(selection == _("Disabled") ? "" : selection.ToStdString());
					});
				mSoundName = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 1);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxButton(this, wxID_ANY, _("Play"), wxDefaultPosition, wxSize(160, 25));
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
					{
						mCaptchaComponent->PlayOne();
					});
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		vsizer_block->AddSpacer(5);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxStaticText(this, wxID_ANY, _("Volume"), wxDefaultPosition, wxSize(250, -1));
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxSlider(this, wxID_ANY, mCaptchaComponent->GetVolume(), 0, 100, wxDefaultPosition, wxSize(160, -1));
				element->Bind(wxEVT_SLIDER, [this, element](wxCommandEvent& event)
					{
						mCaptchaComponent->SetVolume(element->GetValue());
					});
				mVolume = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		vsizer_block->AddSpacer(5);
		{
			auto element = new wxCheckBox(this, wxID_ANY, _("Statistic enable"));
			element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
				{
					mTaskStatisticComponent->SetEnable(element->GetValue());
				});
			mTaskStatisticComponentEnable = element;
			vsizer_block->Add(element, 0);
		}
		vsizer_block->AddSpacer(5);
		{
			auto element = new wxCheckBox(this, wxID_ANY, _("Enable saving all unknown screenshots"));
			element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
				{
					mUnknownPageScreenshotter->SetEnable(element->GetValue());
				});
			mUnknownPageScreenshotterEnable = element;
			vsizer_block->Add(element, 0);
		}
		vsizer_block->AddSpacer(5);
		{
			auto element = new wxCheckBox(this, wxID_ANY, _("Enable saving unknown screenshots on recovery"));
			element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
				{
					mUnknownPageScreenshotter->SetRecoveryMode(element->GetValue());
				});
			mUnknownPageScreenshotterRecovery = element;
			vsizer_block->Add(element, 0);
		}

		vsizer_block->AddSpacer(10);
		{
			auto element = new wxStaticLine(this, wxID_ANY);
			vsizer_block->Add(element, 0, wxEXPAND);
		}

		vsizer_block->AddSpacer(10);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new EditorFileInformerWidget(this, file_informer_sink);
				hsizer_block->Add(element, 0);
			}
			hsizer_block->AddSpacer(40);
			{
				auto element = new EditorVideoLoggerWidget(this, video_logger);
				hsizer_block->Add(element, 0);
			}
			vsizer_block->Add(hsizer_block, 0);
		}

		vsizer_owner->Add(vsizer_block, 1, wxEXPAND | wxLEFT | wxRIGHT | wxDOWN, 5);
	}

	SetSizer(vsizer_owner);

	mConnectionTaskStatisticComponent = mTaskStatisticComponent->signal_data.connect(&EditorAdditionallyWidget::UpdateTaskStatisticComponent, this);
	mConnectionUnknownPageScreenshotter = mUnknownPageScreenshotter->signal_data.connect(&EditorAdditionallyWidget::UpdateUnknownPageScreenshotter, this);
	UpdateTaskStatisticComponent();
	UpdateUnknownPageScreenshotter();
}

void EditorAdditionallyWidget::UpdateTaskStatisticComponent()
{
	mTaskStatisticComponentEnable->SetValue(mTaskStatisticComponent->GetEnable());
}

void EditorAdditionallyWidget::UpdateUnknownPageScreenshotter()
{
	mUnknownPageScreenshotterEnable->SetValue(mUnknownPageScreenshotter->GetEnable());
	mUnknownPageScreenshotterRecovery->SetValue(mUnknownPageScreenshotter->GetRecoveryMode());
}


