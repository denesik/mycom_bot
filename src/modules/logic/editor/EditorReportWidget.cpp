#include "EditorReportWidget.h"

#include <wx/spinctrl.h>
#include <wx/grid.h>

#include <bit>
#include <string>

#include "ProgramReport.h"
#include "EditorLogicOptions.h"
#include "Dictionary.h"


EditorReportWidget::EditorReportWidget(wxWindow* parent, std::shared_ptr<EditorLogicOptions> options, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<ProgramReport> report)
	: wxPanel(parent, wxID_ANY), mEditorLogicOptions(options), mDictionary(dictionary), mProgramReport(report)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);

	vsizer_owner->AddSpacer(3);
	{
		auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this);

		vsizer_block->GetStaticBox()->SetForegroundColour(wxColor(0, 170, 0));
		vsizer_block->GetStaticBox()->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		vsizer_block->AddSpacer(5);
		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxStaticText(this, wxID_ANY, _("Report period (hours)"), wxDefaultPosition, wxSize(250, -1));
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxSize(158, -1));
				element->SetMax(72);
				element->Bind(wxEVT_SPINCTRL, [this, element](wxCommandEvent& evt)
					{
						mEditorLogicOptions->SetProgramReportPeriod(element->GetValue());
					});
				mProgramReportPeriod = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 1);
			}
			hsizer_block->AddSpacer(5);
			{
				auto element = new wxButton(this, wxID_ANY, _("Clear"), wxDefaultPosition, wxSize(160, 25));
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
					{
						mProgramReport->Clear();
					});
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		vsizer_block->AddSpacer(5);
		{
			wxArrayString names;
			for (const auto& data : *mDictionary)
			{
				for (int i = 0; i < static_cast<int>(LogicResult::Size); ++i)
					mProgramReportData.emplace_back(static_cast<int>(names.size()), i, data.task.name, static_cast<LogicResult>(1 << i));
				names.push_back(_(data.title));
			}

			auto element = new wxGrid(this, wxID_ANY);
			element->CreateGrid(names.size(), static_cast<int>(LogicResult::Size) + 1, wxGrid::wxGridSelectNone);
			
			element->SetColLabelValue(0, _("Status"));
			for (size_t i = 0; i < static_cast<size_t>(LogicResult::Size); ++i)
				element->SetColLabelValue(i + 1, _(LogicResultDescription(static_cast<LogicResult>(1 << i))));

			for (size_t i = 0; i < names.size(); ++i)
				element->SetRowLabelValue(i, names[i]);
			element->SetRowLabelSize(200);

			element->SetDefaultCellAlignment(wxALIGN_CENTRE, wxALIGN_CENTRE);
			mGrid = element;
			vsizer_block->Add(element, 0);
		}

		vsizer_owner->Add(vsizer_block, 1, wxEXPAND | wxLEFT | wxRIGHT | wxDOWN, 5);
	}

	SetSizer(vsizer_owner);

	mConnectionProgramReport = mProgramReport->signal_data.connect(&EditorReportWidget::UpdateProgramReportData, this);
	mConnectionEditorSettings = mEditorLogicOptions->signal_data.connect(&EditorReportWidget::UpdateProgramReportData, this);

	UpdateProgramReportData();
}

void EditorReportWidget::UpdateProgramReportData()
{
	auto editor_settings = mEditorLogicOptions;

	auto period = editor_settings->GetProgramReportPeriod();
	if (mProgramReportPeriod->GetValue() != period)
		mProgramReportPeriod->SetValue(period);

	period *= (60 * 60);
	auto program_report = mProgramReport;
	for (const auto& data : mProgramReportData)
		mGrid->SetCellValue(data.row, data.col + 1, std::to_string(program_report->GetResultCount(data.program, data.result, period)));

	// ����������, �������, ����������: ���������� �������� ��� ��� [����������]
	// ��������� � ��� ������: ������� �������� [�����]
	// ����������� � ��� ������: �������� ��������, ���������� �������� �� ���������. [����������]
	// �������� ��������� � ���� ������: �������� �� ������ [�����]
	// ����������� � ���� ������: ���������� �������� �� ���������, �� ��� � �����. [������]
	// ��� ������� ��� �������� ��������� � ���� ������: [������]

	std::vector<wxString> status = { _("Success"), _("Unknown"), _("Fail"), _("Error")};
	std::vector<wxColor> color = { wxColor(12, 156, 19), wxColor(84, 65, 160), wxColor(118, 46, 122), wxColor(219, 65, 6)};
	for (size_t i = 0; i < mGrid->GetNumberRows(); ++i)
	{
		size_t full_count = 0;
		size_t error_count = 0;
		mGrid->GetCellValue(i, std::bit_width(static_cast<size_t>(LogicResult::Full))).ToULongLong(&full_count);
		mGrid->GetCellValue(i, std::bit_width(static_cast<size_t>(LogicResult::Error))).ToULongLong(&error_count);
		size_t index = 1;
		
		if (error_count)
		{
			if (full_count)
				index = 2;
			else
				index = 3;
		}
		else if (full_count)
			index = 0;

		mGrid->SetCellValue(i, 0, status[index]);
		mGrid->SetCellTextColour(i, 0, color[index]);
	}
}


