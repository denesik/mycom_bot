#include "EditorFileInformerWidget.h"

#include <wx/spinctrl.h>

#include "RotatedFileInformerSink.h"

EditorFileInformerWidget::EditorFileInformerWidget(wxWindow* parent, std::shared_ptr<RotatedFileInformerSink> file_informer)
	: wxPanel(parent, wxID_ANY), mRotatedFileInformerSink(file_informer)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);

	{
		auto element = new wxCheckBox(this, wxID_ANY, _("File informer enable"));
		element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
			{
				mRotatedFileInformerSink->SetEnable(element->GetValue());
			});
		mFileInformerComponentEnable = element;
		vsizer_owner->Add(element, 0);
	}
	vsizer_owner->AddSpacer(5);
	{
		wxArrayString elements;
		for (size_t i = 0; i < static_cast<size_t>(LogicResult::Size); ++i)
			elements.push_back(_(LogicResultDescription(static_cast<LogicResult>(1 << i))));
		auto element = new wxCheckListBox(this, wxID_ANY, wxDefaultPosition, wxSize(216, -1), elements);
		element->Bind(wxEVT_CHECKLISTBOX, [this, element](wxCommandEvent& evt)
			{
				LogicResult info = LogicResult::None;
				for (uint32_t i = 0; i < element->GetCount(); ++i)
					info |= element->IsChecked(i) ? static_cast<LogicResult>(1 << i) : LogicResult::None;
				mRotatedFileInformerSink->SetLevel(info);
			});
		mFileInformerComponent = element;
		vsizer_owner->Add(element, 0);
	}
	vsizer_owner->AddSpacer(5);
	{
		auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
		{
			auto element = new wxStaticText(this, wxID_ANY, _("Store time (hours)"), wxDefaultPosition, wxSize(150, -1));
			hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
		}
		{
			auto element = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxSize(64, -1));
			element->Bind(wxEVT_SPINCTRL, [this, element](wxCommandEvent& evt)
				{
					mRotatedFileInformerSink->SetStoreTime(element->GetValue());
				});
			mFileInformerComponentStoreTime = element;
			hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
		}
		hsizer_block->AddStretchSpacer();
		vsizer_owner->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
	}
	vsizer_owner->AddSpacer(5);
	{
		auto element = new wxCheckBox(this, wxID_ANY, _("Compressed"));
		element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& event)
			{
				mRotatedFileInformerSink->SetCompressed(element->GetValue());
			});
		mFileInformerComponentCompressed = element;
		vsizer_owner->Add(element, 0);
	}

	SetSizer(vsizer_owner);

	mConnectionFileInformerComponent = mRotatedFileInformerSink->signal_data.connect(&EditorFileInformerWidget::UpdateFileInformerComponentData, this);
	UpdateFileInformerComponentData();
}

void EditorFileInformerWidget::UpdateFileInformerComponentData()
{
	LogicResult info = mRotatedFileInformerSink->GetLevel();
	for (uint32_t i = 0; i < mFileInformerComponent->GetCount(); ++i)
		mFileInformerComponent->Check(i, (static_cast<uint32_t>(info) & 1U << i) == 1U << i);
	mFileInformerComponentEnable->SetValue(mRotatedFileInformerSink->GetEnable());
	mFileInformerComponentCompressed->SetValue(mRotatedFileInformerSink->GetCompressed());
	mFileInformerComponentStoreTime->SetValue(mRotatedFileInformerSink->GetStoreTime());
}
