#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>

#include "LogicResult.h"

class EditorLogicOptions;
class Dictionary;
class ProgramReport;

class EditorReportWidget : public wxPanel
{
public:
	EditorReportWidget(wxWindow* parent, std::shared_ptr<EditorLogicOptions> options, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<ProgramReport> report);

private:
	void UpdateProgramReportData();

private:
	std::shared_ptr<EditorLogicOptions> mEditorLogicOptions;
	std::shared_ptr<Dictionary> mDictionary;
	std::shared_ptr<ProgramReport> mProgramReport;

	sigslot::scoped_connection mConnectionProgramReport;
	sigslot::scoped_connection mConnectionEditorSettings;

	class wxSpinCtrl* mProgramReportPeriod = nullptr;
	class wxGrid* mGrid = nullptr;

	struct Data 
	{
		int row = 0;
		int col = 0;
		std::string program;
		LogicResult result;
	};
	std::vector<Data> mProgramReportData;
};
