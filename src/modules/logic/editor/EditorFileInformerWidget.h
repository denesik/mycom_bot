#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>

class RotatedFileInformerSink;

class EditorFileInformerWidget : public wxPanel
{
public:
	EditorFileInformerWidget(wxWindow* parent, std::shared_ptr<RotatedFileInformerSink> file_informer);

private:
	void UpdateFileInformerComponentData();

private:
	std::shared_ptr<RotatedFileInformerSink> mRotatedFileInformerSink;

	class wxCheckListBox* mFileInformerComponent = nullptr;

	class wxCheckBox* mFileInformerComponentEnable = nullptr;
	class wxCheckBox* mFileInformerComponentCompressed = nullptr;
	class wxSpinCtrl* mFileInformerComponentStoreTime = nullptr;

	sigslot::scoped_connection mConnectionFileInformerComponent;
};
