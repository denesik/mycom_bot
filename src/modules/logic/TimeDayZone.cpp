#include "TimeDayZone.h"

#include <chrono>

namespace
{
	time_t TIME_OFFSET = 3 * 60 * 60;
	time_t DAY_SECONDS = 24 * 60 * 60;
}

size_t TimeDayZone::GetCurrentTimeZone() const
{
	return GetDayZone(GetCurrentTime()).zone;
}

TimeDayZone::DayZone TimeDayZone::GetDayZone(time_t time) const
{
	auto day = time / DAY_SECONDS;
	auto zone = ((time % DAY_SECONDS) / 3600) / 2;
	return { day, zone };
}

time_t TimeDayZone::GetCurrentTime() const
{
	auto current_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	return current_time - TIME_OFFSET;
}