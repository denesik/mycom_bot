#pragma once

#include "LogicResult.h"

#include <rapidjson/document.h>

class LogicResultJson
{
public:

	void Serialize(const rapidjson::Value& json, LogicResult& value) const;
	void Serialize(const LogicResult& value, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};


