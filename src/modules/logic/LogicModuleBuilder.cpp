#include "LogicModuleBuilder.h"

#include "Actor.h"
#include "Profile.h"

#include "Emulator.h"
#include "Detector.h"
#include "Informer.h"
#include "FileInformerSink.h"
#include "ProgramReport.h"
#include "ActionWrapper.h"
#include "StateWrapper.h"
#include "ArenaComponent.h"
#include "RecoveryValuesComponent.h"
#include "CaptchaComponent.h"
#include "TaskStatisticComponent.h"
#include "RotatedFileInformerSink.h"
#include "VideoLoggerComponent.h"
#include "LockerComponent.h"
#include "UnknownPageScreenshotter.h"
#include "Logic.h"
#include "PageInformer.h"
#include "CaptchaSettings.h"
#include "ProgramReportSettings.h"
#include "LockerSettings.h"
#include "UnknownPageScreenshotterSettings.h"
#include "RotatedFileInformerSettings.h"
#include "VideoLoggerSettings.h"
#include "TaskStatisticSettings.h"
#include "EditorLogicPageBuilder.h"
#include "EditorLogicOptions.h"
#include "EditorLogicOptionsSettings.h"
#include "Dictionary.h"


void LogicModuleBuilder::AddComponents(const Profile& profile, Actor* actor)
{
	auto emulator = actor->FindComponent<Emulator>();
	auto detector = actor->FindComponent<Detector>();

	auto informer = actor->CreateComponent<Informer>();
	actor->CreateComponent<FileInformerSink>(informer, profile.FindPath("log_user_file"));

	auto captcha_component = actor->CreateComponent<CaptchaComponent>(profile.FindPath("captcha_sounds_dir"));
	auto task_statistic_component = actor->CreateComponent<TaskStatisticComponent>(profile.FindPath("statistic_file"));
	auto file_informer = actor->CreateComponent<RotatedFileInformerSink>(informer, profile.FindPath("log_user_dir"));
	auto video_logger_component = actor->CreateComponent<VideoLoggerComponent>(profile.FindPath("log_user_dir"), detector);
	auto journal_component = actor->CreateComponent<LockerComponent>();
	auto unknown_page_screenshotter = actor->CreateComponent<UnknownPageScreenshotter>(profile.FindPath("unknowns_screenshots_dir"), detector);

	actor->CreateComponent<Logic>(informer, LogicComponentList{
		actor->CreateComponent<PageInformer>(emulator, detector, informer),
		actor->CreateComponent<ActionWrapper>(detector, emulator, informer),
		actor->CreateComponent<StateWrapper>(detector),
		actor->CreateComponent<ArenaComponent>(informer),
		actor->CreateComponent<RecoveryValuesComponent>(detector),
		captcha_component,
		task_statistic_component,
		file_informer,
		video_logger_component,
		journal_component,
		unknown_page_screenshotter
		});

	actor->CreateComponent<CaptchaSettings>(captcha_component, profile.FindPath("captcha_core_file"), profile.FindPath("captcha_user_file"));
	actor->CreateComponent<LockerSettings>(journal_component, profile.FindPath("journal_user_file"));
	actor->CreateComponent<UnknownPageScreenshotterSettings>(unknown_page_screenshotter, profile.FindPath("core_settings_core_file"), profile.FindPath("core_settings_user_file"));
	actor->CreateComponent<RotatedFileInformerSettings>(file_informer, profile.FindPath("core_settings_core_file"), profile.FindPath("core_settings_user_file"));
	actor->CreateComponent<VideoLoggerSettings>(video_logger_component, profile.FindPath("core_settings_core_file"), profile.FindPath("core_settings_user_file"));
	actor->CreateComponent<TaskStatisticSettings>(task_statistic_component, profile.FindPath("core_settings_core_file"), profile.FindPath("core_settings_user_file"));

	actor->CreateComponent<EditorLogicPageBuilder>(captcha_component, task_statistic_component, unknown_page_screenshotter, informer, file_informer, video_logger_component);
}
