#pragma once

#include <time.h>

class TimeDayZone
{
public:
	struct DayZone
	{
		time_t day = 0;
		time_t zone = 0;
	};

public:

	size_t GetCurrentTimeZone() const;

	time_t GetCurrentTime() const;
	DayZone GetDayZone(time_t time) const;

};
