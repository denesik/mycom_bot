#include "FileInformerSink.h"

#include <fstream>
#include "Informer.h"

FileInformerSink::FileInformerSink(std::shared_ptr<Informer> informer, const std::filesystem::path& path)
	: mInformer(informer), mPath(path)
{

}


void FileInformerSink::BeginPlay()
{
	std::ofstream mLogFile;
	mLogFile.open(mPath);
	mLogFile.clear();

	mConnectionPrint = mInformer->signal_print.connect([this](const std::string& msg)
		{
			std::lock_guard<std::mutex> lock(mMutex);
	std::ofstream mLogFile;
	mLogFile.open(mPath, std::ofstream::app);
	if (mLogFile.is_open())
		mLogFile << time << " " << msg << std::endl;
		});
}

void FileInformerSink::EndPlay()
{
	mConnectionPrint.disconnect();
}

