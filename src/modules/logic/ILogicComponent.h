#pragma once

#include "LogicResult.h"

class LogicActor;

#include "ActorComponent.h"

class ILogicComponent : public ActorComponent
{
public:
	virtual ~ILogicComponent() = default;

	virtual void BeginComponent(std::string_view program) {};

	virtual void EndComponent(LogicResult result) {};
};

