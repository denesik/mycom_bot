#include "LogicTools.h"
#include "utility/split_string.h"


bool LogicTools::StartTimer(const std::string &name)
{
	mTimers[name].Start();
	return true;
}

int LogicTools::GetTimer(const std::string &name) const
{
	auto it = mTimers.find(name);
	if (it != mTimers.end())
		return it->second.Elapsed();

	return 0;
}

bool LogicTools::StartCounter(const std::string &name)
{
	mCounters[name] = 0;
	return true;
}

int LogicTools::GetCounter(const std::string &name) const
{
	auto it = mCounters.find(name);
	if (it != mCounters.end())
		return it->second;

	return 0;
}

bool LogicTools::IncCounter(const std::string &name)
{
	++mCounters[name];
	return true;
}
