#include "LogicResult.h"

const char* LogicResultString(LogicResult value)
{
	switch (value)
	{
	case LogicResult::Full:
		return "full_success";
	case LogicResult::Partly:
		return "partly_success";
	case LogicResult::Error:
		return "error";
	case LogicResult::Stop:
		return "stop";
	case LogicResult::Condition:
		return "condition";
	case LogicResult::Unavailable:
		return "unavailable";
	case LogicResult::Unknown:
		return "unknown";
	case LogicResult::Requirement:
		return "requirement";
	default:
		break;
	}

	return "";
}

LogicResult LogicResultString(std::string_view msg)
{
	if (msg == LogicResultString(LogicResult::Full))
		return LogicResult::Full;
	if (msg == LogicResultString(LogicResult::Partly))
		return LogicResult::Partly;
	if (msg == LogicResultString(LogicResult::Error))
		return LogicResult::Error;
	if (msg == LogicResultString(LogicResult::Stop))
		return LogicResult::Stop;
	if (msg == LogicResultString(LogicResult::Condition))
		return LogicResult::Condition;
	if (msg == LogicResultString(LogicResult::Unavailable))
		return LogicResult::Unavailable;
	if (msg == LogicResultString(LogicResult::Unknown))
		return LogicResult::Unknown;
	if (msg == LogicResultString(LogicResult::Requirement))
		return LogicResult::Requirement;

	return LogicResult::None;
}

const char* LogicResultDescription(LogicResult result)
{
	switch (result)
	{
	case LogicResult::Full:
		return "Full";
	case LogicResult::Partly:
		return "Partly";
	case LogicResult::Error:
		return "Error";
	case LogicResult::Stop:
		return "Stop";
	case LogicResult::Condition:
		return "Condition";
	case LogicResult::Unavailable:
		return "Unavailable";
	case LogicResult::Unknown:
		return "Unknown";
	case LogicResult::Requirement:
		return "Requirement";
	default:
		break;
	}

	return "";
}

