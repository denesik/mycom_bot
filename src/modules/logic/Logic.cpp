#include "LogicSMC.h"

#include "Logic.h"

#include <sstream>
#include <iomanip>

#include "Actor.h"
#include "Messages.h"

#include "LogicComponents.h"

Logic::Logic(std::shared_ptr<Informer> informer, LogicComponentList components)
	: mInformer(informer), LogicComponents(components), informer(*informer)
{
	mStateMachine = std::make_shared<LogicContext>(*this);
	mStateMachine->enterStartState();

	mLastStateId = mStateMachine->getState().getId();
	mInformer->Print("%s%|40T |page: [%s]%|80T |%s", mStateMachine->getState().getName(), info.PageFullName(), "Entry");
}

bool Logic::Apply(const Program::Task& task)
{
	if (mStatus == Status::Ready)
	{
		config.Set(task.values);
		program_type = task.name;

		mStatus = Status::Busy;

		GetActor()->Send<MessageLogicStart>(program_type);
		BeginComponent(program_type);

		mStateMachine->Start();
		mInformer->Print("%s%|40T |page: [%s]%|80T |%s", mStateMachine->getState().getName(), info.PageFullName(), "Start");
		Print(config);
		return true;
	}
	else if (program_type == task.name)
	{
		config.Set(task.values);
		Print(config);
		return true;
	}
	return false;
}

void Logic::Stop()
{
	if (mStatus != Status::Busy)
		return;

	mStatus = Status::Stopping;

	mStateMachine->Stop();
	mInformer->Print("%s%|40T |page: [%s]%|80T |%s", mStateMachine->getState().getName(), info.PageFullName(), "Stop");
}

bool Logic::IsBusy() const
{
	return mStatus != Status::Ready;
}

void Logic::SetPause(bool val)
{
	mPause = val;
}

bool Logic::IsPaused() const
{
	return mPause;
}

void Logic::Tick()
{
	if (!mPause)
	{
		if (info.PageFullName() == "captcha")
			mStateMachine->CaptchaDetect();

		if (!action.IsBusy())
			mStateMachine->Next();

		if (mLastStateId != mStateMachine->getState().getId())
		{
			mLastStateId = mStateMachine->getState().getId();
			mInformer->Print("%s%|40T |page: [%s]%|80T |%s", mStateMachine->getState().getName(), info.PageFullName(), "Next");
		}

		if (mFinished)
		{
			mInformer->Print("Task result: " + std::string(LogicResultDescription(mFinishedResult)));
			EndComponent(mFinishedResult);
			GetActor()->Send<MessageLogicFinish>(mFinishedResult);
			mFinished = false;
			mFinishedResult = LogicResult::Unknown;
			mStatus = Status::Ready;
		}
	}
}

void Logic::OnFinished(LogicResult result)
{
	mFinished = true;
	mFinishedResult = result;
}

void Logic::Print(const LogicData& data) const
{
	for (const auto& [name, value] : data)
	{
		std::visit([this, &name](auto&& arg) 
			{
				std::stringstream ss;
				ss << std::setiosflags(std::ios::fixed) << std::setw(40) << std::left << std::boolalpha
					<< name << " ";
				ss << arg;
				mInformer->Print(ss.str());
			}, value);
	}
}

