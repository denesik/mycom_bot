#pragma once

#include "ActorComponent.h"

#include <sigslot/signal.hpp>
#include <boost/format.hpp>

class Informer : public ActorComponent
{
public:
	Informer();

	sigslot::signal<const std::string&> signal_print;

	template<class T, class... Args>
	void Print(T&& fmt, Args&&... args)
	//void Print(T &&fmt, Args&&... args)
	{
		boost::format f(std::forward<T>(fmt));
		int unroll[]{ 0, (f % std::forward<Args>(args), 0)... };
		static_cast<void>(unroll);

		auto out = Print1("Logic", boost::str(f));
		signal_print(out);
	}

	bool IsPrintable() { return true; }

private:
	std::string Print1(const char* category, std::string_view msg) const;
	std::string GetTime() const;

private:
	const std::string mVersion;

};

