#pragma once

#include <string>
#include <mutex>
#include <sigslot/signal.hpp>
#include <filesystem>
#include <memory>

#include "ActorComponent.h"

class Informer;

class FileInformerSink : public ActorComponent
{
public:
	FileInformerSink(std::shared_ptr<Informer> informer, const std::filesystem::path& path);

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

private:
	std::mutex mMutex;

	std::filesystem::path mPath;

	std::shared_ptr<Informer> mInformer;
	sigslot::connection mConnectionPrint;
};

