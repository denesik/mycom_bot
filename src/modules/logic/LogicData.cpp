#include "LogicData.h"

#include <algorithm>


void LogicData::Clear()
{
	mData.clear();
}

void LogicData::Set(const Program::Values& values)
{
	mData.clear();
	for (const auto& val : values)
		SetValue(val.first, val.second);
}

bool LogicData::SetValue(const std::string& name, const Value& value)
{
	mData[name] = value;
	return true;
}

bool LogicData::SetValue(const std::string& name, int value)
{
	mData[name] = value;
	return true;
}

bool LogicData::SetValue(const std::string& name, bool value)
{
	mData[name] = value;
	return true;
}

bool LogicData::SetValue(const std::string& name, const char* value)
{
	mData[name] = value;
	return true;
}

bool LogicData::SetValue(const std::string& name, std::string_view value)
{
	mData[name] = std::string(value);
	return true;
}

bool LogicData::SetValue(const std::string& name, const std::string& value)
{
	mData[name] = value;
	return true;
}

int LogicData::GetIntValue(const std::string& name, int default_val) const
{
	auto out = default_val;
	FindIntValue(name, out);
	return out;
}

bool LogicData::GetBoolValue(const std::string& name, bool default_val) const
{
	auto out = default_val;
	FindBoolValue(name, out);
	return out;
}

std::string LogicData::GetStringValue(const std::string& name, const std::string& default_val) const
{
	auto out = default_val;
	FindStringValue(name, out);
	return out;
}


bool LogicData::FindIntValue(const std::string& name, int& value) const
{
	auto it = mData.find(name);
	if (it != mData.end())
		if (auto val = std::get_if<int>(&(it->second)))
		{
			value = *val;
			return true;
		}

	return false;
}

bool LogicData::FindBoolValue(const std::string& name, bool& value) const
{
	auto it = mData.find(name);
	if (it != mData.end())
		if (auto val = std::get_if<bool>(&(it->second)))
		{
			value = *val;
			return true;
		}

	return false;
}

bool LogicData::FindStringValue(const std::string& name, std::string& value) const
{
	auto it = mData.find(name);
	if (it != mData.end())
		if (auto val = std::get_if<std::string>(&(it->second)))
		{
			value = *val;
			return true;
		}

	return false;
}

