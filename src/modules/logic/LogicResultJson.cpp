#include "LogicResultJson.h"


void LogicResultJson::Serialize(const rapidjson::Value& json, LogicResult& value) const
{
	if (!json.IsObject())
		return;

	{
		auto jt = json.FindMember("full");
		if (jt != json.MemberEnd() && jt->value.IsBool())
			if (jt->value.GetBool())
				value |= LogicResult::Full;
			else
				value &= ~LogicResult::Full;
	}
	{
		auto jt = json.FindMember("partly");
		if (jt != json.MemberEnd() && jt->value.IsBool())
			if (jt->value.GetBool())
				value |= LogicResult::Partly;
			else
				value &= ~LogicResult::Partly;
	}
	{
		auto jt = json.FindMember("error");
		if (jt != json.MemberEnd() && jt->value.IsBool())
			if (jt->value.GetBool())
				value |= LogicResult::Error;
			else
				value &= ~LogicResult::Error;
	}
	{
		auto jt = json.FindMember("stop");
		if (jt != json.MemberEnd() && jt->value.IsBool())
			if (jt->value.GetBool())
				value |= LogicResult::Stop;
			else
				value &= ~LogicResult::Stop;
	}
	{
		auto jt = json.FindMember("condition");
		if (jt != json.MemberEnd() && jt->value.IsBool())
			if (jt->value.GetBool())
				value |= LogicResult::Condition;
			else
				value &= ~LogicResult::Condition;
	}
	{
		auto jt = json.FindMember("unavailable");
		if (jt != json.MemberEnd() && jt->value.IsBool())
			if (jt->value.GetBool())
				value |= LogicResult::Unavailable;
			else
				value &= ~LogicResult::Unavailable;
	}
	{
		auto jt = json.FindMember("unknown");
		if (jt != json.MemberEnd() && jt->value.IsBool())
			if (jt->value.GetBool())
				value |= LogicResult::Unknown;
			else
				value &= ~LogicResult::Unknown;
	}
}

void LogicResultJson::Serialize(const LogicResult& value, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	{
		auto it = json.FindMember("full");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = (value & LogicResult::Full) == LogicResult::Full;
		else
			json.AddMember("full", (value & LogicResult::Full) == LogicResult::Full, allocator);
	}
	{
		auto it = json.FindMember("partly");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = (value & LogicResult::Partly) == LogicResult::Partly;
		else
			json.AddMember("partly", (value & LogicResult::Partly) == LogicResult::Partly, allocator);
	}
	{
		auto it = json.FindMember("error");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = (value & LogicResult::Error) == LogicResult::Error;
		else
			json.AddMember("error", (value & LogicResult::Error) == LogicResult::Error, allocator);
	}
	{
		auto it = json.FindMember("stop");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = (value & LogicResult::Stop) == LogicResult::Stop;
		else
			json.AddMember("stop", (value & LogicResult::Stop) == LogicResult::Stop, allocator);
	}
	{
		auto it = json.FindMember("condition");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = (value & LogicResult::Condition) == LogicResult::Condition;
		else
			json.AddMember("condition", (value & LogicResult::Condition) == LogicResult::Condition, allocator);
	}
	{
		auto it = json.FindMember("unavailable");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = (value & LogicResult::Unavailable) == LogicResult::Unavailable;
		else
			json.AddMember("unavailable", (value & LogicResult::Unavailable) == LogicResult::Unavailable, allocator);
	}
	{
		auto it = json.FindMember("unknown");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = (value & LogicResult::Unknown) == LogicResult::Unknown;
		else
			json.AddMember("unknown", (value & LogicResult::Unknown) == LogicResult::Unknown, allocator);
	}
}
