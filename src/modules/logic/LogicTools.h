#pragma once

#include <string>
#include <map>

#include "Timer.h"

class LogicTools
{
public:
	bool StartTimer(const std::string &name);
	int GetTimer(const std::string &name) const;

	bool StartCounter(const std::string &name);
	int GetCounter(const std::string &name) const;
	bool IncCounter(const std::string &name);

private:

	std::map<std::string, Timer> mTimers;
	std::map<std::string, int> mCounters;
};






