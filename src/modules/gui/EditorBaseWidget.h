#pragma once

#include <wx/wxprec.h>

class Actor;
class EditorOptions;
class IEditorPageBuilder;

class EditorBaseWidget : public wxFrame
{
public:
	EditorBaseWidget(const std::vector<std::shared_ptr<IEditorPageBuilder>> &builders, std::shared_ptr<EditorOptions> options);

private:
	void PositionWasUpdated();

private:
	std::vector<std::shared_ptr<IEditorPageBuilder>> mBuilders;
	std::shared_ptr<EditorOptions> mEditorOptions;

	Actor *mActor = nullptr;

	wxString mTitle;
};

