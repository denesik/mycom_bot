#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class EditorLanguage;

class EditorLanguageSettings : public ActorComponent
{
public:
	EditorLanguageSettings(std::shared_ptr<EditorLanguage> editor, const std::filesystem::path& core_path, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mCorePath;
	const std::filesystem::path mUserPath;

	std::shared_ptr<EditorLanguage> mEditor;

	sigslot::connection mEditorConnection;
};


