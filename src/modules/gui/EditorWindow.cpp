#include "EditorBaseWidget.h"

#include "EditorApplication.h"
#include "EditorWindow.h"
#include "EditorLanguage.h"

EditorWindow::EditorWindow(const std::vector<std::shared_ptr<IEditorPageBuilder>> &builders, std::shared_ptr<EditorLanguage> editor_language, std::shared_ptr<EditorOptions> options)
	: mBuilders(builders), mEditorLanguage(editor_language), mEditorOptions(options)
{

}

void EditorWindow::BeginPlay()
{
	CreateWindow();
	mConnectionEditorLanguage = mEditorLanguage->signal_data.connect([this]
		{
			if (mEditorLanguage->GetId() != mLocale->GetLanguage())
			{
				DestroyWindow();
				CreateWindow();
			}
		});
}

void EditorWindow::EndPlay()
{
	mConnectionEditorLanguage.disconnect();
	DestroyWindow();
}

void EditorWindow::CreateWindow()
{
	mLocale = new wxLocale(mEditorLanguage->GetId(), wxLOCALE_DONT_LOAD_DEFAULT);
	auto name = mLocale->GetCanonicalName();
	name.resize(2);
	mLocale->AddCatalog(name);

	mEditorFrame = new EditorBaseWidget(mBuilders, mEditorOptions);
	mEditorFrame->Bind(wxEVT_CLOSE_WINDOW, [this](wxCloseEvent& event)
	{
		wxGetApp().Exit();
	});

	wxGetApp().SetTopWindow(mEditorFrame);	
	mEditorFrame->Show();
}

void EditorWindow::DestroyWindow()
{
	if (mEditorFrame)
	{
		if (mEditorFrame == wxGetApp().GetTopWindow())
			wxGetApp().SetTopWindow(nullptr);

		mEditorFrame->Destroy();
		mEditorFrame = nullptr;
	}

	if (mLocale)
	{
		wxDELETE(mLocale);
		mLocale = nullptr;
	}
}


