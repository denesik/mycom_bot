#include <wx/intl.h>

#include "EditorLanguage.h"


EditorLanguage::EditorLanguage(const std::filesystem::path& langs_dir)
	: mLanguages(LoadLanguages(langs_dir))
{
	wxLocale::AddCatalogLookupPathPrefix(langs_dir.string());

	SetId(wxLocale::GetSystemLanguage());
}

int EditorLanguage::GetId() const
{
	return mId;
}

void EditorLanguage::SetId(int value)
{
	auto correct_lang = CorrectLanguage(value);
	if (mId != correct_lang)
	{
		mId = correct_lang;
		signal_data();
	}
}

void EditorLanguage::SetName(const std::string& name)
{
	auto it = std::find_if(mLanguages.begin(), mLanguages.end(), [&name](const Language& lang) { return lang.name == name; });
	if (it != mLanguages.end())
		SetId(it->id);
}

std::string EditorLanguage::GetName() const
{
	auto it = std::find_if(mLanguages.begin(), mLanguages.end(), [this](const Language& lang) { return lang.id == mId; });
	if (it != mLanguages.end())
		return it->name;

	return wxLocale::GetLanguageName(mId).utf8_string();
}

const EditorLanguage::Languages& EditorLanguage::GetSupportedLanguages() const
{
	return mLanguages;
}

EditorLanguage::Languages EditorLanguage::LoadLanguages(const std::filesystem::path& langs_dir) const
{
	Languages out;
	out.emplace_back(wxLANGUAGE_ENGLISH, wxLocale::GetLanguageName(wxLANGUAGE_ENGLISH).utf8_string());

	for (auto& it : std::filesystem::directory_iterator(langs_dir))
		if (it.is_directory())
		{
			auto dir = it.path().stem();
			if (auto langinfo = wxLocale::FindLanguageInfo(dir.string()))
				if (std::filesystem::exists((it.path() / dir).string() + ".mo"))
					out.emplace_back(langinfo->Language, langinfo->Description.utf8_string());
		}

	return out;
}

int EditorLanguage::CorrectLanguage(int id) const
{
	for (const auto &lang : mLanguages)
		if (lang.id == id)
			return id;

	return wxLocale::GetSystemLanguage();
}

