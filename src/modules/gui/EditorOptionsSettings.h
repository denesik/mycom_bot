#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class EditorOptions;

class EditorOptionsSettings : public ActorComponent
{
public:
	EditorOptionsSettings(std::shared_ptr<EditorOptions> editor, const std::filesystem::path& core_path, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mCorePath;
	const std::filesystem::path mUserPath;

	std::shared_ptr<EditorOptions> mEditor;

	sigslot::connection mEditorConnection;
};


