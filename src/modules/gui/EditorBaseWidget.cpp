#include "EditorBaseWidget.h"

#include <wx/notebook.h>

#include "Actor.h"
#include "EditorOptions.h"

#include "IEditorPageBuilder.h"
#include "Version.h"


EditorBaseWidget::EditorBaseWidget(const std::vector<std::shared_ptr<IEditorPageBuilder>> &builders, std::shared_ptr<EditorOptions> options)
	: wxFrame(NULL, wxID_ANY, "mg_bot", wxDefaultPosition, wxSize(870, 770)), mBuilders(builders), mEditorOptions(options)
{
	SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_FRAMEBK));

	mTitle = _(mEditorOptions->GetAppTitle()) + " (" + LoadVersion() + ")";
	SetTitle(mTitle);

	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);	
	vsizer_owner->AddSpacer(2);

	auto panel = new wxPanel(this);
	{
		auto vsizer_border = new wxBoxSizer(wxVERTICAL);

		auto vsizer_panel = new wxBoxSizer(wxVERTICAL);
		vsizer_panel->AddSpacer(4);

		auto notebook = new wxNotebook(panel, wxID_ANY);

		
		for (const auto& builder : mBuilders)
		{
			builder->AddPages(notebook);
		}
		
		vsizer_panel->Add(notebook, 1, wxEXPAND);
		vsizer_panel->AddSpacer(1);

		vsizer_border->Add(vsizer_panel, 1, wxEXPAND | wxALL, 2);

		panel->SetSizer(vsizer_border);
	}

	vsizer_owner->Add(panel, 1, wxEXPAND);
	SetSizer(vsizer_owner);

	{
		const auto& data_rect = mEditorOptions->GetWindowPos();
		wxRect rect(data_rect.x, data_rect.y, data_rect.width, data_rect.height);
		if (!rect.IsEmpty())
		{
			SetPosition(rect.GetPosition());
			SetSize(rect.GetSize());
		}
		else
			Centre();
	}

	Bind(wxEVT_SIZE, [this](wxSizeEvent& event)
		{
			PositionWasUpdated();
			event.Skip();
		});

	Bind(wxEVT_MOVE, [this](wxMoveEvent& event)
		{
			PositionWasUpdated();
			event.Skip();
		});
}

void EditorBaseWidget::PositionWasUpdated()
{
	wxRect rect(GetPosition(), GetSize());
	mEditorOptions->SetWindowPos({ rect.x, rect.y, rect.width, rect.height });
}

