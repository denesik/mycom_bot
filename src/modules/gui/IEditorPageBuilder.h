#pragma once

class wxNotebook;

class IEditorPageBuilder
{
public:
	~IEditorPageBuilder() = default;

	virtual void AddPages(wxNotebook * notebook) = 0;

};
