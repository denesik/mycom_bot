#include "EditorOptionsSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "EditorOptions.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

#undef GetObject

#include "JsonSerializerSection.h"
#include "RectJson.h"
#include "PointJson.h"

EditorOptionsSettings::EditorOptionsSettings(std::shared_ptr<EditorOptions> editor, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mEditor(editor), mCorePath(core_path), mUserPath(user_path)
{

}

void EditorOptionsSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void EditorOptionsSettings::BeginPlay()
{
	mEditorConnection = mEditor->signal_data.connect([this]() { Save(mUserPath); });
}

void EditorOptionsSettings::EndPlay()
{
	mEditorConnection.disconnect();
}

void EditorOptionsSettings::Deinitialize()
{
	Save(mUserPath);
}

void EditorOptionsSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("app_title");
		if (it != json.MemberEnd() && it->value.IsString())
			mEditor->SetAppTitle(std::string(it->value.GetString(), it->value.GetStringLength()));
	}
	{
		Rect rect;
		JsonSerializerSection<Rect>("window_pos").Serialize(json, rect);
		mEditor->SetWindowPos(rect);
	}
}

void EditorOptionsSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	JsonSerializerSection<Rect>("window_pos").Serialize(mEditor->GetWindowPos(), json, json.GetAllocator());

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


