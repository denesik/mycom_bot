#pragma once

#include "IModuleBuilder.h"

class GuiModuleBuilder : public IModuleBuilder
{
public:

	virtual void AddComponents(const Profile& profile, Actor* actor) override;

};


