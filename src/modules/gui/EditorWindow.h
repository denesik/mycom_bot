#pragma once

#include "ActorComponent.h"

#include <sigslot/signal.hpp>

class EditorLanguage;
class EditorOptions;
class IEditorPageBuilder;

class EditorWindow : public ActorComponent
{
public:
	EditorWindow(const std::vector<std::shared_ptr<IEditorPageBuilder>> &builders, std::shared_ptr<EditorLanguage> editor_language, std::shared_ptr<EditorOptions> options);

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

public:

	void CreateWindow();

	void DestroyWindow();

private:
	std::vector<std::shared_ptr<IEditorPageBuilder>> mBuilders;
	std::shared_ptr<EditorLanguage> mEditorLanguage;
	std::shared_ptr<EditorOptions> mEditorOptions;

	class EditorBaseWidget* mEditorFrame = nullptr;
	class wxLocale* mLocale = nullptr;

	sigslot::connection mConnectionEditorLanguage;
};



