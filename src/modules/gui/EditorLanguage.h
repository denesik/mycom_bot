#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>
#include <vector>

#include "ActorComponent.h"

class EditorLanguage : public ActorComponent
{
public:
	struct Language
	{
		int id;
		std::string name;
	};
	using Languages = std::vector<Language>;

public:	
	EditorLanguage(const std::filesystem::path& langs_dir);

	sigslot::signal<> signal_data;

	int GetId() const;
	void SetId(int value);

	void SetName(const std::string& name);
	std::string GetName() const;

	const Languages& GetSupportedLanguages() const;

private:
	int mId = 0;

	const Languages mLanguages;

private:
	Languages LoadLanguages(const std::filesystem::path& langs_dir) const;

	int CorrectLanguage(int id) const;

};
