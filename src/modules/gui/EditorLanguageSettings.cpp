#include "EditorLanguageSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "EditorLanguage.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

EditorLanguageSettings::EditorLanguageSettings(std::shared_ptr<EditorLanguage> editor, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mEditor(editor), mCorePath(core_path), mUserPath(user_path)
{

}

void EditorLanguageSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void EditorLanguageSettings::BeginPlay()
{
	mEditorConnection = mEditor->signal_data.connect([this]() { Save(mUserPath); });
}

void EditorLanguageSettings::EndPlay()
{
	mEditorConnection.disconnect();
}

void EditorLanguageSettings::Deinitialize()
{
	Save(mUserPath);
}

void EditorLanguageSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("language_id");
		if (it != json.MemberEnd() && it->value.IsInt())
			mEditor->SetId(it->value.GetInt());
	}
}

void EditorLanguageSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("language_id");
		if (it != json.MemberEnd() && it->value.IsInt())
			it->value = mEditor->GetId();
		else
			json.AddMember("language_id", mEditor->GetId(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


