#pragma once

#include <memory>

#include "IEditorPageBuilder.h"
#include "ActorComponent.h"

class EditorLanguage;

class EditorGuiBuilder : public ActorComponent, public IEditorPageBuilder
{
public:
	EditorGuiBuilder(std::shared_ptr<EditorLanguage> lang);

	virtual void AddPages(wxNotebook* notebook) override;

private:
	std::shared_ptr<EditorLanguage> mEditorLanguage;

};


