#include <wx/wxprec.h>
#include <wx/notebook.h>

#include "EditorGuiBuilder.h"

#include "EditorGuiSettingsWidget.h"

#include "Actor.h"

EditorGuiBuilder::EditorGuiBuilder(std::shared_ptr<EditorLanguage> lang)
	: mEditorLanguage(lang)
{

}

void EditorGuiBuilder::AddPages(wxNotebook* notebook)
{
	notebook->AddPage(new EditorGuiSettingsWidget(notebook, mEditorLanguage), _("Settings"));
}
