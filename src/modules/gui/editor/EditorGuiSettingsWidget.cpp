#include "EditorGuiSettingsWidget.h"

#include "EditorLanguage.h"

EditorGuiSettingsWidget::EditorGuiSettingsWidget(wxWindow* parent, std::shared_ptr<EditorLanguage> lang)
	: wxPanel(parent, wxID_ANY), mEditorLanguage(lang)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);
	vsizer_owner->AddSpacer(3);
	{
		auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this);

		vsizer_block->GetStaticBox()->SetForegroundColour(wxColor(0, 170, 0));
		vsizer_block->GetStaticBox()->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		vsizer_block->AddSpacer(5);

		{
			auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
			{
				auto element = new wxStaticText(this, wxID_ANY, _("Language"), wxDefaultPosition, wxSize(250, -1));
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL);
			}
			hsizer_block->AddSpacer(5);
			{
				wxArrayString elements;
				for (const auto &langs : mEditorLanguage->GetSupportedLanguages())
					elements.push_back(wxString::FromUTF8(langs.name));

				auto element = new wxComboBox(this, wxID_ANY, elements.IsEmpty() ? wxString() : elements.front(), wxDefaultPosition, wxSize(158, 23), elements);
				element->Bind(wxEVT_COMBOBOX, [this](wxCommandEvent& event)
					{
						mEditorLanguage->SetName(mLanguageList->GetStringSelection().utf8_string());
					});
				element->SetSelection(0);
				mLanguageList = element;
				hsizer_block->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 1);
			}
			vsizer_block->Add(hsizer_block, 0, wxEXPAND | wxLEFT | wxRIGHT, 2);
		}
		
		vsizer_owner->Add(vsizer_block, 1, wxEXPAND | wxLEFT | wxRIGHT | wxDOWN, 5);
	}
	SetSizer(vsizer_owner);

	mConnectionEditorLanguage = mEditorLanguage->signal_data.connect(&EditorGuiSettingsWidget::UpdateEditorLanguage, this);

	UpdateEditorLanguage();
}

void EditorGuiSettingsWidget::UpdateEditorLanguage()
{
	mLanguageList->SetStringSelection(mEditorLanguage->GetName());
}

