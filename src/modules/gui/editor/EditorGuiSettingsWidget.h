#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>

class EditorLanguage;

class EditorGuiSettingsWidget : public wxPanel
{
public:
	EditorGuiSettingsWidget(wxWindow *parent, std::shared_ptr<EditorLanguage> lang);

private:
	void UpdateEditorLanguage();

private:
	std::shared_ptr<EditorLanguage> mEditorLanguage;

	wxComboBox *mLanguageList = nullptr;

	sigslot::scoped_connection mConnectionEditorLanguage;
};


