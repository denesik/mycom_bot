#include "EditorOptions.h"

void EditorOptions::SetAppTitle(std::string_view value)
{
	if (mAppTitle != value)
	{
		mAppTitle = value;
		signal_data();
	}
}

const std::string& EditorOptions::GetAppTitle() const
{
	return mAppTitle;
}

void EditorOptions::SetWindowPos(const Rect& rect)
{
	if (mWindowPos != rect)
	{
		mWindowPos = rect;
		signal_data();
	}
}

const Rect& EditorOptions::GetWindowPos() const
{
	return mWindowPos;
}

