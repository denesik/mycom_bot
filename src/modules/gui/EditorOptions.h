#pragma once

#include <string>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"
#include "Rect.h"

class EditorOptions : public ActorComponent
{
public:
	EditorOptions() = default;

	sigslot::signal<> signal_data;

	void SetAppTitle(std::string_view value);
	const std::string& GetAppTitle() const;

	void SetWindowPos(const Rect& rect);
	const Rect& GetWindowPos() const;

private:
	Rect mWindowPos;
	std::string mAppTitle;

};
