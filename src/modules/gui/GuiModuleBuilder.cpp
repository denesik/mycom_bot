#include "GuiModuleBuilder.h"

#include "Actor.h"
#include "Profile.h"

#include "EditorWindow.h"
#include "EditorOptions.h"
#include "EditorLanguage.h"
#include "EditorOptionsSettings.h"
#include "EditorLanguageSettings.h"
#include "EditorGuiBuilder.h"

void GuiModuleBuilder::AddComponents(const Profile& profile, Actor* actor)
{
	auto editor_options = actor->CreateComponent<EditorOptions>();
	auto editor_language = actor->CreateComponent<EditorLanguage>(profile.FindPath("langs_dir"));
	actor->CreateComponent<EditorGuiBuilder>(editor_language);

	auto builders = actor->FindComponents<IEditorPageBuilder>();
	actor->CreateComponent<EditorWindow>(builders, editor_language, editor_options);

	actor->CreateComponent<EditorOptionsSettings>(editor_options, profile.FindPath("editor_core_file"), profile.FindPath("editor_user_file"));
	actor->CreateComponent<EditorLanguageSettings>(editor_language, profile.FindPath("editor_core_file"), profile.FindPath("editor_user_file"));

}
