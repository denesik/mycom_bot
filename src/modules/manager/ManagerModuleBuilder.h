#pragma once

#include "IModuleBuilder.h"

class ManagerModuleBuilder : public IModuleBuilder
{
public:

	virtual void AddComponents(const Profile& profile, Actor* actor) override;

};


