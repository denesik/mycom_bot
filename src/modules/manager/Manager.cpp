#include "Manager.h"

#include "Logic.h"


Manager::Manager(std::shared_ptr<Logic> logic, std::shared_ptr<Program> program)
	: mLogic(logic), mProgram(program)
{

}

void Manager::SetPause(bool value)
{
	if (mLogic->IsPaused() != value)
	{
		mLogic->SetPause(value);
		signal_state();
	}
}

bool Manager::IsPaused() const
{
	return mLogic->IsPaused();
}

void Manager::Tick()
{
	if (mStatus == Status::Ready)
		return;

	if (mStatus == Status::Stopping)
		if (!mLogic->IsBusy())
		{
			mStatus = Status::Ready;
			signal_state();
			return;
		}

	if (mStatus == Status::Busy)
		if (!mLogic->IsBusy())
		{
			if (!SetNextTask())
			{
				mStatus = Status::Ready;
				signal_state();
				return;
			}
			RunCurrentTask();
		}
}

void Manager::Start()
{
	if (mStatus == Status::Ready)
	{
		if (!mProgram->CountTasks())
			return;

		SetPause(false);
		mCurrentTaskIndex = 0;
		RunCurrentTask();
	}
}

void Manager::Stop()
{
	if (mStatus == Status::Busy)
	{
		SetPause(false);
		mLogic->Stop();
		mStatus = Status::Stopping;
		signal_state();
	}
}

Manager::Status Manager::GetStatus() const
{
	return mStatus;
}

size_t Manager::GetCurrentTaskId() const
{
	return mCurrentTaskId;
}

bool Manager::SetNextTask()
{
	if (!mProgram->CountTasks())
	{
		mCurrentTaskIndex = -1;
		return false;
	}

	mCurrentTaskIndex = mCurrentTaskIndex + 1;
	if (mCurrentTaskIndex >= static_cast<decltype(mCurrentTaskIndex)>(mProgram->CountTasks()))
		mCurrentTaskIndex = 0;
	return true;
}

void Manager::RunCurrentTask()
{
	mCurrentTaskId = mProgram->GetTaskId(mCurrentTaskIndex);
	mTaskValues = mProgram->GetTask(mCurrentTaskIndex);

	if (mLogic->Apply(GetCurrentTaskProperties()))
	{
		mStatus = Status::Busy;
		signal_state();
	}
}


void Manager::BeginPlay()
{
	mConnectionProgram = mProgram->signal_data.connect([this]() { if (mLogic->IsBusy()) mLogic->Apply(GetCurrentTaskProperties()); });
}

void Manager::EndPlay()
{
	mConnectionProgram.disconnect();
}

Program::Task Manager::GetCurrentTaskProperties()
{
	size_t index = 0;
	if (mProgram->FindTaskFromId(mCurrentTaskId, index))
	{
		Program::Task task;
		task.name = mProgram->GetTask(index).name;
		for (const auto& [name, val] : mProgram->GetConfig())
			task.values[name] = val;
		for (const auto& [name, val] : mProgram->GetTask(index).values)
			task.values[name] = val;
		return task;
	}
	Program::Task task = mTaskValues;
	for (const auto& [name, val] : mProgram->GetConfig())
		task.values[name] = val;
	return task;
}
