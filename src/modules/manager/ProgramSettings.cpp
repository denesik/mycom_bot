#include "ProgramSettings.h"

#include <rapidjson/document.h>
#include <type_traits>

#include "LoadJson.h"
#include "Program.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

namespace
{
#undef GetObject
}

namespace
{
	void ValueToJson(const std::string& name, int value, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator)
	{
		auto it = json.FindMember(name);
		if (it != json.MemberEnd() && it->name == name && it->value.IsInt())
			it->value = value;
		else
		{
			auto mem_name = rapidjson::Value(name, allocator);
			json.AddMember(mem_name, value, allocator);
		}
	}

	void ValueToJson(const std::string& name, bool value, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator)
	{
		auto it = json.FindMember(name);
		if (it != json.MemberEnd() && it->name == name && it->value.IsBool())
			it->value = value;
		else
		{
			auto mem_name = rapidjson::Value(name, allocator);
			json.AddMember(mem_name, value, allocator);
		}
	}

	void ValueToJson(const std::string& name, std::string value, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator)
	{
		auto mem_value = rapidjson::Value(value, allocator);

		auto it = json.FindMember(name);
		if (it != json.MemberEnd() && it->name == name && it->value.IsInt())
			it->value = mem_value;
		else
		{
			auto mem_name = rapidjson::Value(name, allocator);
			json.AddMember(mem_name, mem_value, allocator);
		}
	}

	void SetValueFromJson(const std::string& name, const rapidjson::Value& json, Program::Values& values)
	{
		if (json.IsInt())
			values[name] = json.GetInt();
		if (json.IsBool())
			values[name] = json.GetBool();
		if (json.IsString())
			values[name] = std::string(json.GetString(), json.GetStringLength());
	}

	void FromJson(const rapidjson::Value& json, Program::Values& values)
	{
		if (json.IsObject())
			for (auto jt = json.MemberBegin(); jt != json.MemberEnd(); ++jt)
				SetValueFromJson(jt->name.GetString(), jt->value, values);
	}

	void ToJson(const Program::Values& values, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator)
	{
		if (!json.IsObject())
			return;

		for (const auto& value : values)
		{
			std::visit([&](auto&& arg) { ValueToJson(value.first, arg, json, allocator); }, value.second);
		}
	}
}




ProgramSettings::ProgramSettings(std::shared_ptr<Program> program, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mProgram(program), mCorePath(core_path), mUserPath(user_path)
{

}

void ProgramSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void ProgramSettings::BeginPlay()
{
	mProgramConnection = mProgram->signal_data.connect([this]() { Save(mUserPath); });
}

void ProgramSettings::EndPlay()
{
	mProgramConnection.disconnect();
}

void ProgramSettings::Deinitialize()
{
	Save(mUserPath);
}

void ProgramSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto core_json = json.FindMember("core");
		if (core_json != json.MemberEnd())
			if (core_json->value.IsObject())
			{
				auto config = mProgram->GetConfig();
				FromJson(core_json->value, config);
				mProgram->SetConfig(config);
			}
	}

	auto member_tasks = json.FindMember("tasks");
	if (member_tasks != json.MemberEnd())
		if (member_tasks->value.IsArray())
		{
			const auto& array_tasks = member_tasks->value.GetArray();
			if (!array_tasks.Empty())
				mProgram->ClearTasks();

			for (const auto& section : array_tasks)
			{
				if (section.IsObject() && section.MemberCount() == 1)
				{
					auto it = section.MemberBegin();
					if (it->value.IsObject())
					{

						Program::Task task;
						task.name = std::string(it->name.GetString(), it->name.GetStringLength());
						FromJson(it->value, task.values);
						mProgram->AddTask(task);
					}
				}
			}
		}
}

void ProgramSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		{
			auto it = json.FindMember("tasks");
			if (it == json.MemberEnd())
				json.AddMember("tasks", rapidjson::Value(rapidjson::kArrayType), json.GetAllocator());
		}
		if (!json["tasks"].IsArray())
			return;

		auto json_tasks = json["tasks"].GetArray();
		json_tasks.Clear();

		for (size_t i = 0; i < mProgram->CountTasks(); ++i)
		{
			const auto& task = mProgram->GetTask(i);

			auto json_values = rapidjson::Value(rapidjson::kObjectType);
			ToJson(task.values, json_values, json.GetAllocator());

			auto section = rapidjson::Value(rapidjson::kObjectType);
			section.AddMember(rapidjson::Value(task.name, json.GetAllocator()), json_values, json.GetAllocator());
			json_tasks.PushBack(section, json.GetAllocator());
		}
	}

	{
		{
			auto it = json.FindMember("core");
			if (it == json.MemberEnd())
				json.AddMember("core", rapidjson::Value(rapidjson::kObjectType), json.GetAllocator());
		}
		if (!json["core"].IsObject())
			return;

		auto core_json = json["core"].GetObject();
		core_json.RemoveAllMembers();

		ToJson(mProgram->GetConfig(), core_json, json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


