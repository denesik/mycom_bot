#pragma once

#include "ActorComponent.h"

#include <memory>

class Program;
class Dictionary;

class ProgramCorrector : public ActorComponent
{
public:
	ProgramCorrector(std::shared_ptr<Program> program, std::shared_ptr<Dictionary> library);

	virtual void Initialize() override;

private:
	std::shared_ptr<Program> mProgram;
	std::shared_ptr<Dictionary> mLibrary;
};


