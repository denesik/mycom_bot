#include "AutoRunnerSettings.h"

#include <rapidjson/document.h>

#include "LoadJson.h"
#include "AutoRunner.h"
#include "PathUtf8.h"
#include "Logger.h"
#include "SaveJson.h"

AutoRunnerSettings::AutoRunnerSettings(std::shared_ptr<AutoRunner> runner, const std::filesystem::path& core_path, const std::filesystem::path& user_path)
	: mRunner(runner), mCorePath(core_path), mUserPath(user_path)
{

}

void AutoRunnerSettings::Initialize()
{
	Load(mCorePath);
	Load(mUserPath);
}

void AutoRunnerSettings::BeginPlay()
{
	mRunnerConnection = mRunner->signal_data.connect([this]() { Save(mUserPath); });
}

void AutoRunnerSettings::EndPlay()
{
	mRunnerConnection.disconnect();
}

void AutoRunnerSettings::Deinitialize()
{
	Save(mUserPath);
}

void AutoRunnerSettings::Load(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
		return;
	}

	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("autorun_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			mRunner->SetAutorunMode(it->value.GetBool());
	}
	{
		auto it = json.FindMember("stop_emulator_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			mRunner->SetStopEmulatorMode(it->value.GetBool());
	}
}

void AutoRunnerSettings::Save(const std::filesystem::path& path)
{
	rapidjson::Document json;
	if (!utility::LoadJson(path, json))
	{
		LOG_E("Error load config. File: " + utf_helper::PathToUtf8(path));
	}

	if (!json.IsObject())
		json.SetObject();

	{
		auto it = json.FindMember("autorun_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = mRunner->GetAutorunMode();
		else
			json.AddMember("autorun_mode", mRunner->GetAutorunMode(), json.GetAllocator());
	}
	{
		auto it = json.FindMember("stop_emulator_mode");
		if (it != json.MemberEnd() && it->value.IsBool())
			it->value = mRunner->GetStopEmulatorMode();
		else
			json.AddMember("stop_emulator_mode", mRunner->GetStopEmulatorMode(), json.GetAllocator());
	}

	if (!utility::SaveJson(path, json))
	{
		LOG_E("Error save config. File: " + utf_helper::PathToUtf8(path));
	}
}


