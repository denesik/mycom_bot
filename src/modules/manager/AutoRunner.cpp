#include "AutoRunner.h"

#include "Controller.h"
#include "Manager.h"

AutoRunner::AutoRunner(std::shared_ptr<Manager> manager, std::shared_ptr<Emulator> emulator)
	: mEmulator(emulator), mManager(manager)
{

}

void AutoRunner::SetAutorunMode(bool value)
{
	if (value != mAutorunMode)
	{
		mAutorunMode = value;
		signal_data();
	}
}

bool AutoRunner::GetAutorunMode() const
{
	return mAutorunMode;
}

void AutoRunner::SetStopEmulatorMode(bool value)
{
	if (value != mStopEmulatorMode)
	{
		mStopEmulatorMode = value;
		signal_data();
	}
}

bool AutoRunner::GetStopEmulatorMode() const
{
	return mStopEmulatorMode;
}


void AutoRunner::Tick()
{
	if (!mIsFirstTick)
	{
		mIsFirstTick = true;
		if (mAutorunMode)
			mManager->Start();
	}
}

void AutoRunner::EndPlay()
{
	if (mStopEmulatorMode)
		Controller(mEmulator).Stop();
}
