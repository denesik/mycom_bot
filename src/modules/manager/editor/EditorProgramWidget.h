#pragma once

#include <wx/wxprec.h>

#include <sigslot/signal.hpp>
#include <vector>

class EditorTaskFrameWidget;
class Dictionary;
class Program;
class Manager;
class EditorFactoryValue;

class EditorProgramWidget : public wxPanel
{
public:
	EditorProgramWidget(wxWindow *parent, std::shared_ptr<Program> program, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<Manager> manager, std::shared_ptr<EditorFactoryValue> factory_value);

	void UpdateSizes();

private:
	std::shared_ptr<Program> mProgram;
	std::shared_ptr<Dictionary> mDictionary;
	std::shared_ptr<Manager> mManager;

	wxButton *mButtonStart = nullptr;
	wxButton *mButtonPause = nullptr;

	wxButton *mButtonAdd = nullptr;
	wxButton *mButtonRemove = nullptr;
	class wxComboBox* mLibraryTaskList = nullptr;

	struct PageWidget
	{
		std::string name;
		class EditorTaskFrameWidget* widget = nullptr;
	};
	std::vector<PageWidget> mPages;


	class wxSimpleHtmlListBox* mProgramList = nullptr;

	sigslot::scoped_connection mConnectionProgramProperties;
	sigslot::scoped_connection mConnectionManagerState;

	size_t mSelectedProgramId = 0;

	std::vector<std::string> mTaskNames;

	wxTimer mSizeTimer;
private:
	void UpdateProgramProperties();
	void UpdateManagerState();

	void UpdatePage(size_t index);

	wxString GetProgramPropertiesLabel(const std::shared_ptr<Dictionary>& library, const std::shared_ptr<Program>& properties, size_t index) const;

};

