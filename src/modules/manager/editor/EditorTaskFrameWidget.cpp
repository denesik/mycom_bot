#include "EditorTaskFrameWidget.h"

#include "EditorTaskWidget.h"

EditorTaskFrameWidget::EditorTaskFrameWidget(wxWindow* parent, std::string_view name, const wxString& title, 
	std::shared_ptr<Program> program, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<EditorFactoryValue> factory_value)
	: wxPanel(parent, wxID_ANY)
{
	auto vsizer_block = new wxStaticBoxSizer(wxVERTICAL, this, _(title));
//	vsizer_block->GetStaticBox()->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
//	vsizer_block->GetStaticBox()->SetForegroundColour(wxColor(0, 170, 0));

	auto element = new EditorTaskWidget(this, name, program, dictionary, factory_value);
	vsizer_block->Add(element, 1, wxEXPAND);

	mEditorPageWidget = element;
	SetSizer(vsizer_block);
}

void EditorTaskFrameWidget::Set(size_t program_id)
{
	mEditorPageWidget->Set(program_id);
}
