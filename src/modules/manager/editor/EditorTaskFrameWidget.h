#pragma once

#include <wx/wxprec.h>

class EditorTaskWidget;
class Program;
class Dictionary;
class EditorFactoryValue;

class EditorTaskFrameWidget : public wxPanel
{
public:
	EditorTaskFrameWidget(wxWindow *parent, std::string_view name, const wxString &title, std::shared_ptr<Program> program, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<EditorFactoryValue> factory_value);

	void Set(size_t program_id);

private:
	EditorTaskWidget* mEditorPageWidget = nullptr;
};


