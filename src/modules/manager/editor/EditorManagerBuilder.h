#pragma once

#include <memory>

#include "IEditorPageBuilder.h"
#include "ActorComponent.h"

class Manager;
class Dictionary;
class Program;
class AutoRunner;
class EditorFactoryValue;
class EditorLogicOptions;
class ProgramReport;

class EditorManagerBuilder : public ActorComponent, public IEditorPageBuilder
{
public:
		EditorManagerBuilder(
		std::shared_ptr<Manager> manager,
		std::shared_ptr<Dictionary> dictionary,
		std::shared_ptr<Program> program,
		std::shared_ptr<AutoRunner> auto_runner,
		std::shared_ptr<EditorLogicOptions> options,
		std::shared_ptr<ProgramReport> report,
		std::shared_ptr<EditorFactoryValue> factory_value
	) : 
		mManager(manager),
		mDictionary(dictionary),
		mProgram(program),
		mAutoRunner(auto_runner),
		mEditorLogicOptions(options),
		mProgramReport(report),
		mEditorFactoryValue(factory_value)
	{	}


	virtual void AddPages(wxNotebook* notebook) override;

private:
	std::shared_ptr<Manager> mManager;
	std::shared_ptr<Dictionary> mDictionary;
	std::shared_ptr<Program> mProgram;
	std::shared_ptr<AutoRunner> mAutoRunner;
	std::shared_ptr<EditorFactoryValue> mEditorFactoryValue;
	std::shared_ptr<EditorLogicOptions> mEditorLogicOptions;
	std::shared_ptr<ProgramReport> mProgramReport;
};


