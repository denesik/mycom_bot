#pragma once

#include <wx/wxprec.h>

#include "Value.h"

wxDECLARE_EVENT(wxEVT_EDITOR_VALUE, wxCommandEvent);

class EditorValueBase : public wxPanel
{
public:
	EditorValueBase(wxWindow* parent)
		: wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(130, 31))
	{};
	
	virtual void Set(const Value& value) = 0;
	virtual Value Get() const = 0;


	void SetIndent(int value);
	int GetIndent() const;

	void SetLabelColor(const wxColour& color);
	const wxColour& GetLabelColor() const;

	void SetLabelName(const wxString& value);
	const wxString& GetLabelName() const;

private:
	int mIndent = 0;
	wxString mLabelName;
	wxColour mLabelColour;

};


