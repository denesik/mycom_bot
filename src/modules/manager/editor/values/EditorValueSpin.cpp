#include "EditorValueSpin.h"

#include <wx/spinctrl.h>

EditorValueSpin::EditorValueSpin(wxWindow *parent)
	: EditorValueBase(parent)
{
	auto hsizer_owner = new wxBoxSizer(wxHORIZONTAL);
	hsizer_owner->AddSpacer(6);
	{
		auto element = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxSize(130, -1));
		element->SetMin(std::numeric_limits<int>::min());
		element->SetMax(std::numeric_limits<int>::max());
		element->Bind(wxEVT_SPINCTRL, [this, element](wxCommandEvent& evt)
			{
				wxPostEvent(this, wxCommandEvent(wxEVT_EDITOR_VALUE));
			});
		mElement = element;
		hsizer_owner->Add(element, 0, wxALIGN_CENTER_VERTICAL);
	}
	hsizer_owner->AddSpacer(6);

	SetSizer(hsizer_owner);
}

void EditorValueSpin::Set(const Value& value)
{
	if (auto val = std::get_if<int>(&value))
		SetValue(*val);
}

Value EditorValueSpin::Get() const
{
	return mElement->GetValue();
}

void EditorValueSpin::SetValue(int value)
{
	if (mElement->GetValue() != value)
		mElement->SetValue(value);
}

void EditorValueSpin::SetMin(int value)
{
	mElement->SetMin(value);
}

void EditorValueSpin::SetMax(int value)
{
	mElement->SetMax(value);
}
