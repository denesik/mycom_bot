#pragma once

#include "EditorValueBase.h"

class EditorValueCombo : public EditorValueBase
{
public:
	EditorValueCombo(wxWindow *parent);

	virtual void Set(const Value& value) override;

	virtual Value Get() const override;

	void SetValue(int value);

	void SetChoices(const std::vector<std::string>& choices);

private:
	class wxComboBox* mElement = nullptr;
};


