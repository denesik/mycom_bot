#pragma once

#include "EditorValueBase.h"

class EditorValueSpin : public EditorValueBase
{
public:
	EditorValueSpin(wxWindow *parent);

	virtual void Set(const Value& value) override;

	virtual Value Get() const override;

	void SetValue(int value);

	void SetMin(int value);
	void SetMax(int value);

private:
	class wxSpinCtrl* mElement = nullptr;
};


