#pragma once

#include <rapidjson/document.h>

#include "JsonSerializer.h"

class EditorValueCombo;

template<>
class JsonSerializer<EditorValueCombo>
{
public:

	void Serialize(const rapidjson::Value& json, EditorValueCombo& object) const;
	void Serialize(const EditorValueCombo& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};



