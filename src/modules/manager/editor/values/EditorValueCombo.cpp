#include "EditorValueCombo.h"



EditorValueCombo::EditorValueCombo(wxWindow *parent)
	: EditorValueBase(parent)
{
	auto hsizer_owner = new wxBoxSizer(wxHORIZONTAL);
	hsizer_owner->AddSpacer(6);
	{
		auto element = new wxComboBox(this, wxID_ANY, "", wxDefaultPosition, wxSize(130, 26));
		element->Bind(wxEVT_COMBOBOX, [this, element](wxCommandEvent& evt)
			{
				if (element->GetSelection() > 0)
					wxPostEvent(this, wxCommandEvent(wxEVT_EDITOR_VALUE));
			});
		mElement = element;
		hsizer_owner->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxUP, 3);
	}
	hsizer_owner->AddSpacer(6);

	SetSizer(hsizer_owner);
}

void EditorValueCombo::Set(const Value& value)
{
	if (auto val = std::get_if<int>(&value))
		SetValue(*val);
}

Value EditorValueCombo::Get() const
{
	return mElement->GetSelection();
}

void EditorValueCombo::SetValue(int value)
{
	if (mElement->GetSelection() != value)
		mElement->Select(value);
}

void EditorValueCombo::SetChoices(const std::vector<std::string>& choices)
{
	std::vector<wxString> values;
	for (const auto& val : choices)
		values.push_back(_(wxString::FromUTF8(val)));
	mElement->Append(values);
}

