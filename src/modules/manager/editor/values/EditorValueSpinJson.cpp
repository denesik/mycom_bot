#include "EditorValueSpin.h"

#include "EditorValueSpinJson.h"
#include "EditorValueBaseJson.h"


void JsonSerializer<EditorValueSpin>::Serialize(const rapidjson::Value& json, EditorValueSpin& object) const
{
	if (!json.IsObject())
		return;

	JsonSerializer<EditorValueBase>().Serialize(json, object);

	{
		auto it = json.FindMember("value");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.SetValue(it->value.GetInt());
	}
	{
		auto it = json.FindMember("min");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.SetMin(it->value.GetInt());
	}
	{
		auto it = json.FindMember("max");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.SetMax(it->value.GetInt());
	}
}


void JsonSerializer<EditorValueSpin>::Serialize(const EditorValueSpin& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();


}
