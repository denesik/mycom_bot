#pragma once

#include <rapidjson/document.h>

#include "JsonSerializer.h"

class EditorValueSpin;

template<>
class JsonSerializer<EditorValueSpin>
{
public:

	void Serialize(const rapidjson::Value& json, EditorValueSpin& object) const;
	void Serialize(const EditorValueSpin& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};



