#pragma once

#include <rapidjson/document.h>

#include "JsonSerializer.h"

class EditorValueBase;

template<>
class JsonSerializer<EditorValueBase>
{
public:

	void Serialize(const rapidjson::Value& json, EditorValueBase& object) const;
	void Serialize(const EditorValueBase& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};



