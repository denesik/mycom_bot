#pragma once

#include <rapidjson/document.h>

#include "JsonSerializer.h"

class EditorValueCheck;

template<>
class JsonSerializer<EditorValueCheck>
{
public:

	void Serialize(const rapidjson::Value& json, EditorValueCheck& object) const;
	void Serialize(const EditorValueCheck& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const;

};



