#pragma once

#include "EditorValueBase.h"

class EditorValueCheck : public EditorValueBase
{
public:
	EditorValueCheck(wxWindow *parent);

	virtual void Set(const Value& value) override;

	virtual Value Get() const override;

	void SetValue(bool value);

private:
	class wxCheckBox* mElement = nullptr;
};


