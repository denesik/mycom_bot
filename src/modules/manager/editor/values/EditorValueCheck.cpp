#include "EditorValueCheck.h"



EditorValueCheck::EditorValueCheck(wxWindow *parent)
	: EditorValueBase(parent)
{
	auto hsizer_owner = new wxBoxSizer(wxHORIZONTAL);
	hsizer_owner->AddSpacer(6);
	{
		auto element = new wxCheckBox(this, wxID_ANY, "", wxDefaultPosition, wxSize(130, 26));
		element->Bind(wxEVT_CHECKBOX, [this, element](wxCommandEvent& evt)
			{
				wxPostEvent(this, wxCommandEvent(wxEVT_EDITOR_VALUE));
			});
		mElement = element;
		hsizer_owner->Add(element, 0, wxALIGN_CENTER_VERTICAL | wxUP, 1);
	}
	hsizer_owner->AddSpacer(6);

	SetSizer(hsizer_owner);
}

void EditorValueCheck::Set(const Value& value)
{
	if (auto val = std::get_if<bool>(&value))
		SetValue(*val);
}

Value EditorValueCheck::Get() const
{
	return mElement->GetValue();
}

void EditorValueCheck::SetValue(bool value)
{
	if (mElement->GetValue() != value)
		mElement->SetValue(value);
}

