#include "EditorValueCombo.h"

#include "EditorValueComboJson.h"
#include "EditorValueBaseJson.h"


void JsonSerializer<EditorValueCombo>::Serialize(const rapidjson::Value& json, EditorValueCombo& object) const
{
	if (!json.IsObject())
		return;

	JsonSerializer<EditorValueBase>().Serialize(json, object);

	{
		std::vector<std::string> values;
		auto it = json.FindMember("choices");
		if (it != json.MemberEnd() && it->value.IsArray())
			for (const auto& el : it->value.GetArray())
				if (el.IsString())
					values.emplace_back(el.GetString(), el.GetStringLength());
		object.SetChoices(values);
	}
	{
		auto it = json.FindMember("value");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.SetValue(it->value.GetInt());
	}

}


void JsonSerializer<EditorValueCombo>::Serialize(const EditorValueCombo& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();


}
