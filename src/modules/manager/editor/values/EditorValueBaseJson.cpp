#include "EditorValueBase.h"

#include "EditorValueBaseJson.h"


void JsonSerializer<EditorValueBase>::Serialize(const rapidjson::Value& json, EditorValueBase& object) const
{
	if (!json.IsObject())
		return;

	{
		auto it = json.FindMember("title");
		if (it != json.MemberEnd() && it->value.IsString())
			object.SetLabelName(_(wxString::FromUTF8(std::string(it->value.GetString(), it->value.GetStringLength()))));
	}
	{
		auto it = json.FindMember("color");
		if (it != json.MemberEnd() && it->value.IsString())
		{
			auto str = std::string(it->value.GetString(), it->value.GetStringLength());
			do
			{
				union
				{
					uint32_t data;
					uint8_t channels[4];
				} color;
				try
				{
					color.data = std::stoul(str, nullptr, 0);
				}
				catch (const std::exception&)
				{
					break;
				}
				object.SetLabelColor(wxColour(color.channels[2], color.channels[1], color.channels[0]));
			} while (false);
		}
	}
	{
		auto it = json.FindMember("indent");
		if (it != json.MemberEnd() && it->value.IsInt())
			object.SetIndent(it->value.GetInt());
	}

}


void JsonSerializer<EditorValueBase>::Serialize(const EditorValueBase& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();


}
