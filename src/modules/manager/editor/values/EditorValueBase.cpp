#include "EditorValueBase.h"

wxDEFINE_EVENT(wxEVT_EDITOR_VALUE, wxCommandEvent);


void EditorValueBase::SetIndent(int value)
{
	mIndent = value;
}

int EditorValueBase::GetIndent() const
{
	return mIndent;
}

void EditorValueBase::SetLabelColor(const wxColour& color)
{
	mLabelColour = color;
}

const wxColour& EditorValueBase::GetLabelColor() const
{
	return mLabelColour;
}

void EditorValueBase::SetLabelName(const wxString& value)
{
	mLabelName = value;
}

const wxString& EditorValueBase::GetLabelName() const
{
	return mLabelName;
}
