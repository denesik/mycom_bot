#include "EditorValueCheck.h"

#include "EditorValueCheckJson.h"
#include "EditorValueBaseJson.h"


void JsonSerializer<EditorValueCheck>::Serialize(const rapidjson::Value& json, EditorValueCheck& object) const
{
	if (!json.IsObject())
		return;

	JsonSerializer<EditorValueBase>().Serialize(json, object);

	{
		auto it = json.FindMember("value");
		if (it != json.MemberEnd() && it->value.IsBool())
			object.SetValue(it->value.GetBool());
	}
}


void JsonSerializer<EditorValueCheck>::Serialize(const EditorValueCheck& object, rapidjson::Value& json, rapidjson::Value::AllocatorType& allocator) const
{
	if (!json.IsObject())
		json.SetObject();


}
