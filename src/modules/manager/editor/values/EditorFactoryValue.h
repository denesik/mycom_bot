#pragma once

#include <string>
#include <map>
#include <functional>

#include "ActorComponent.h"

class wxWindow;
class EditorValueBase;

class EditorFactoryValue : public ActorComponent
{
public:
	EditorFactoryValue();

	EditorValueBase* Create(wxWindow* parent, std::string_view data);

private:
	template<class T>
	void RegisterEditorValue(const std::string& type);

private:
	std::map<std::string, std::function<EditorValueBase*(wxWindow* parent, std::string_view data)>> mData;

};

