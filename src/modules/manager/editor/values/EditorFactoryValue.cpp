#include "EditorValueBase.h"

#include <wx/wxprec.h>

#include "EditorFactoryValue.h"

#include "StringJson.h"

#include "EditorValueSpin.h"
#include "EditorValueSpinJson.h"
#include "EditorValueCheck.h"
#include "EditorValueCheckJson.h"
#include "EditorValueCombo.h"
#include "EditorValueComboJson.h"

template<class T>
void EditorFactoryValue::RegisterEditorValue(const std::string& type)
{
	mData[type] = [](wxWindow* parent, std::string_view data) -> EditorValueBase*
	{
		auto element = new T(parent);
		rapidjson::Document json;
		utility::JsonFromString(data, json);
		JsonSerializer<T>().Serialize(json, *element);
		return element;
	};
}

EditorFactoryValue::EditorFactoryValue()
{
	RegisterEditorValue<EditorValueSpin>("spin");
	RegisterEditorValue<EditorValueCheck>("check");
	RegisterEditorValue<EditorValueCombo>("combo");
}

EditorValueBase* EditorFactoryValue::Create(wxWindow* parent, std::string_view data)
{
	rapidjson::Document json;
	utility::JsonFromString(data, json);
	std::string type;
	if (json.IsObject())
		if (json.HasMember("type") && json["type"].IsString())
			type = std::string(json["type"].GetString(), json["type"].GetStringLength());

	auto it = mData.find(type);
	if (it == mData.end())
		return nullptr;

	return mData[type](parent, data);
}
