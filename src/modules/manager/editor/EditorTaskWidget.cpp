#include "EditorTaskWidget.h"

#include "EditorValueSpin.h"

#include "Dictionary.h"
#include "EditorFactoryValue.h"
#include "Program.h"

EditorTaskWidget::EditorTaskWidget(wxWindow *parent, std::string_view task_name, std::shared_ptr<Program> program, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<EditorFactoryValue> factory_value)
	: wxScrolledWindow(parent, wxID_ANY), mProgram(program), mDictionary(dictionary), mEditorFactoryValue(factory_value)
{
	SetScrollbars(0, 10, 0, 100, 0, 0);
	SetScrollRate(5, 30);
	ShowScrollbars(wxSHOW_SB_NEVER, wxSHOW_SB_DEFAULT);

	auto hsizer_owner = new wxBoxSizer(wxHORIZONTAL);

	auto library = mDictionary;
	auto factory = mEditorFactoryValue;

	{
		auto vsizer_label = new wxBoxSizer(wxVERTICAL);
		auto vsizer_value = new wxBoxSizer(wxVERTICAL);
		auto vsizer_panel = new wxBoxSizer(wxVERTICAL);
		auto it = std::find_if(library->begin(), library->end(), [&task_name](const Dictionary::Description& data) { return data.task.name == task_name; });
		if (it != library->end() || task_name == library->GetConfig().task.name)
		{
			const auto &desc = (it != library->end() ? *it : library->GetConfig());

			for (const auto & [val_name, meta] : desc.meta)
			{
				if (auto element_value = factory->Create(this, meta))
				{
					{
						auto panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 31));
						auto hsizer_panel = new wxBoxSizer(wxHORIZONTAL);
						{
							auto element_label = new wxStaticText(panel, wxID_ANY, element_value->GetLabelName(), wxDefaultPosition, wxSize(-1, -1), wxST_ELLIPSIZE_END | wxALIGN_RIGHT);
							//element_label->SetFont(wxFont(wxFONTSIZE_SMALL, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
							//element_label->SetForegroundColour(element_value->GetLabelColor());
							hsizer_panel->Add(element_label, 1, wxALIGN_CENTER_VERTICAL);
						}
						panel->SetSizer(hsizer_panel);

						// 						if (!(i % 2))
						// 							panel->SetBackgroundColour(wxColour(0xF8F8F8));

						vsizer_label->Add(panel, 0, wxEXPAND);
						vsizer_label->AddSpacer(element_value->GetIndent());
					}

					// 					if (!(i % 2))
					// 						element_value->SetBackgroundColour(wxColour(0xF8F8F8));

					element_value->Bind(wxEVT_EDITOR_VALUE, [this, val_name, element_value](wxCommandEvent& evt)
						{
							auto program = mProgram;
							if (mProgramId == 0)
							{
								auto values = program->GetConfig();
								values[val_name] = element_value->Get();
								program->SetConfig(values);
							}
							else
							{
								size_t index = 0;
								if (program->FindTaskFromId(mProgramId, index))
								{
									auto task = program->GetTask(index);
									task.values[val_name] = element_value->Get();
									program->SetTask(index, task);
								}
							}
						});
					mData.emplace_back(val_name, element_value);
					vsizer_value->Add(element_value, 0);
					vsizer_value->AddSpacer(element_value->GetIndent());

					{
						auto panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 31));

						// 						if (!(i % 2))
						// 							panel->SetBackgroundColour(wxColour(0xF8F8F8));

						vsizer_panel->Add(panel, 0, wxEXPAND);
						vsizer_panel->AddSpacer(element_value->GetIndent());
					}
				}
			}
		}
		hsizer_owner->Add(vsizer_label, 1);
		hsizer_owner->Add(vsizer_value, 0);
		hsizer_owner->Add(vsizer_panel, 100);
	}
	
	SetSizer(hsizer_owner);
}

void EditorTaskWidget::Set(size_t program_id)
{
	mProgramId = program_id;

	auto program = mProgram;

	if (mProgramId == 0)
	{
		const auto &values = program->GetConfig();
		for (const auto& data : mData)
		{
			auto it = values.find(data.name);
			if (it == values.end())
				data.widget->Enable(false);
			else
			{
				data.widget->Enable(true);
				data.widget->Set(it->second);
			}
		}
	}
	else
	{
		size_t index = 0;
		if (program->FindTaskFromId(program_id, index))
		{
			const auto& task = program->GetTask(index);

			for (const auto& data : mData)
			{
				auto it = task.values.find(data.name);
				if (it == task.values.end())
					data.widget->Enable(false);
				else
				{
					data.widget->Enable(true);
					data.widget->Set(it->second);
				}
			}
		}
	}
}
