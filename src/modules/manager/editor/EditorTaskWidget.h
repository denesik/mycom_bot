#pragma once

#include <wx/wxprec.h>

class EditorValueBase;
class Program;
class Dictionary;
class EditorFactoryValue;

class EditorTaskWidget : public wxScrolledWindow
{
public:
	EditorTaskWidget(wxWindow *parent, std::string_view name, std::shared_ptr<Program> program, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<EditorFactoryValue> factory_value);

	void Set(size_t program_id);

private:
	std::shared_ptr<Program> mProgram;
	std::shared_ptr<Dictionary> mDictionary;
	std::shared_ptr<EditorFactoryValue> mEditorFactoryValue;

	struct Data
	{
		std::string name;
		EditorValueBase* widget = nullptr;
	};
	std::vector<Data> mData;

	size_t mProgramId = 0;

};


