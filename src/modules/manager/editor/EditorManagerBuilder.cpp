#include <wx/wxprec.h>
#include <wx/notebook.h>

#include "EditorManagerBuilder.h"

#include "EditorProgramWidget.h"
#include "EditorSettingsWidget.h"
#include "EditorReportWidget.h"

#include "Actor.h"

void EditorManagerBuilder::AddPages(wxNotebook* notebook)
{
	notebook->AddPage(new EditorProgramWidget(notebook, mProgram, mDictionary, mManager, mEditorFactoryValue), _("Program"));
	notebook->AddPage(new EditorSettingsWidget(notebook, mAutoRunner), _("Settings"));
	notebook->AddPage(new EditorReportWidget(notebook, mEditorLogicOptions, mDictionary, mProgramReport), _("Report"));
}
