#include "EditorProgramWidget.h"

#include <wx/spinctrl.h>
#include <wx/notebook.h>
#include <wx/htmllbox.h>

#include "EditorTaskFrameWidget.h"
#include "Manager.h"
#include "Dictionary.h"
#include "Program.h"

EditorProgramWidget::EditorProgramWidget(wxWindow* parent, std::shared_ptr<Program> program, std::shared_ptr<Dictionary> dictionary, std::shared_ptr<Manager> manager, std::shared_ptr<EditorFactoryValue> factory_value)
	: wxPanel(parent, wxID_ANY), mProgram(program), mDictionary(dictionary), mManager(manager)
{
	auto vsizer_owner = new wxBoxSizer(wxVERTICAL);
	vsizer_owner->AddSpacer(3);

	auto hsizer_owner = new wxBoxSizer(wxHORIZONTAL);
	hsizer_owner->AddSpacer(5);

	{
		auto vsizer_block = new wxBoxSizer(wxVERTICAL);
		{
			auto vsizer_frame = new wxStaticBoxSizer(wxVERTICAL, this);
			vsizer_frame->AddSpacer(3);
			{
				auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
				hsizer_block->AddSpacer(4);
				{
					wxArrayString elements;
					auto library = mDictionary;
					for (const auto& data : *library)
					{
						mTaskNames.push_back(data.task.name);
						elements.push_back(_(wxString::FromUTF8(data.title)));
					}

					auto element = new wxComboBox(this, wxID_ANY, elements.IsEmpty() ? wxString() : elements.front(), wxDefaultPosition, wxSize(154, -1), elements);
					if (!elements.IsEmpty())
						element->SetSelection(0);
					mLibraryTaskList = element;
					hsizer_block->Add(element, 0, wxDOWN, 1);
				}
				hsizer_block->AddSpacer(6);
				{
					auto element = new wxButton(this, wxID_ANY, _("Add"), wxDefaultPosition, wxSize(-1, 25));
					element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
						{
							auto selection = mLibraryTaskList->GetSelection();
							if (selection >= 0 && static_cast<size_t>(selection) < mTaskNames.size())
							{
								auto program = mProgram;
								auto dictionary = mDictionary;
								auto it = std::find_if(dictionary->begin(), dictionary->end(), [name = mTaskNames[selection]](const Dictionary::Description& val) 
									{ 
										return val.task.name == name; 
									});
								if (it != dictionary->end())
									program->AddTask({ mTaskNames[selection], it->task.values });
								mSelectedProgramId = program->GetTaskId(program->CountTasks() - 1);
								UpdateProgramProperties();
							}
						});
					mButtonAdd = element;
					hsizer_block->Add(element, 0, wxDOWN, 1);
				}
				hsizer_block->AddSpacer(4);
				vsizer_frame->Add(hsizer_block);
			}
			vsizer_frame->AddSpacer(5);
			{
				auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
				hsizer_block->AddSpacer(3);
				{
					auto element = new wxButton(this, wxID_ANY, _("Up"), wxDefaultPosition, wxSize(-1, 25));
					element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
						{
							size_t index = 0;
							auto program = mProgram;
							if (program->FindTaskFromId(mSelectedProgramId, index))
								program->Move(index, -1);
						});
					hsizer_block->Add(element, 0);
				}
				hsizer_block->AddSpacer(6);
				{
					auto element = new wxButton(this, wxID_ANY, _("Down"), wxDefaultPosition, wxSize(-1, 25));
					element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
						{
							size_t index = 0;
							auto program = mProgram;
							if (program->FindTaskFromId(mSelectedProgramId, index))
								program->Move(index, 1);
						});
					hsizer_block->Add(element, 0, wxDOWN, 1);
				}
				hsizer_block->AddSpacer(6);
				{
					auto element = new wxButton(this, wxID_ANY, _("Remove"), wxDefaultPosition, wxSize(-1, 25));
					element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
						{
							size_t index = 0;
							auto program = mProgram;
							if (program->FindTaskFromId(mSelectedProgramId, index))
							{
								if (index + 1 < program->CountTasks())
									mSelectedProgramId = program->GetTaskId(index + 1);
								else if (index > 0)
									mSelectedProgramId = program->GetTaskId(index - 1);
								program->RemoveTask(index);
							}
						});
					hsizer_block->Add(element, 0, wxDOWN, 1);
				}
				hsizer_block->AddSpacer(4);
				vsizer_frame->Add(hsizer_block);
			}
			vsizer_frame->AddSpacer(5);
			{
				auto hsizer_block = new wxBoxSizer(wxHORIZONTAL);
				hsizer_block->AddSpacer(3);
				{
					auto element = new wxButton(this, wxID_ANY, _("Fill"), wxDefaultPosition, wxSize(115, 25));
					element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
						{
							auto program = mProgram;
							program->ClearTasks();
							auto library = mDictionary;
							for (const auto& data : *library)
							{
								program->AddTask(data.task);
							}
						});
					hsizer_block->Add(element, 0);
				}
				hsizer_block->AddSpacer(7);
				{
					auto element = new wxButton(this, wxID_ANY, _("Clear"), wxDefaultPosition, wxSize(115, 25));
					element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
						{
							auto program = mProgram;
							program->ClearTasks();
						});
					hsizer_block->Add(element, 0, wxDOWN, 1);
				}
				hsizer_block->AddSpacer(4);
				vsizer_frame->Add(hsizer_block);
			}
			vsizer_frame->AddSpacer(3);
			vsizer_block->Add(vsizer_frame);
		}

		{
			auto vsizer_list = new wxStaticBoxSizer(wxHORIZONTAL, this);
			{
				wxArrayString elements;
				auto element = new wxSimpleHtmlListBox(this, 0, wxDefaultPosition, wxDefaultSize, elements, wxLB_SINGLE | wxLB_ALWAYS_SB | wxNO_BORDER);
				element->Bind(wxEVT_LISTBOX, [this](wxCommandEvent& event)
					{
						mSelectedProgramId = 0;
						auto selected = mProgramList->GetSelection();
						if (selected > 0)
							mSelectedProgramId = mProgram->GetTaskId(selected - 1);
						UpdatePage(selected);
					});
				mProgramList = element;
				vsizer_list->Add(element, 1, wxEXPAND);
			}
			vsizer_block->Add(vsizer_list, 1, wxEXPAND);
		}

		{
			auto hsizer_block = new wxStaticBoxSizer(wxHORIZONTAL, this);
			hsizer_block->AddSpacer(16);
			{
				auto element = new wxButton(this, wxID_ANY, "", wxDefaultPosition, wxSize(80, -1));
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
					{
						auto manager = mManager;
						auto status = manager->GetStatus();
						if (status == Manager::Status::Ready)
							manager->Start();
						else if (status == Manager::Status::Busy)
							manager->Stop();
					});
				mButtonStart = element;
				hsizer_block->Add(element, 0, wxDOWN, 1);
			}
			hsizer_block->AddSpacer(51);
			{
				auto element = new wxButton(this, wxID_ANY, "", wxDefaultPosition, wxSize(80, -1));
				mButtonPause = element;
				element->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event)
					{
						auto manager = mManager;
						manager->SetPause(!manager->IsPaused());
					});
				hsizer_block->Add(element, 0, wxDOWN, 1);
			}
			hsizer_block->AddSpacer(17);
			vsizer_block->Add(hsizer_block, 0);
		}

		hsizer_owner->Add(vsizer_block, 0, wxEXPAND);
	}
	hsizer_owner->AddSpacer(8);
	{
		auto library = mDictionary;
		{
			const auto& config = library->GetConfig();
			auto element = mPages.emplace_back(config.task.name, new EditorTaskFrameWidget(this, config.task.name, wxString::FromUTF8(config.title), program, dictionary, factory_value)).widget;
			element->Hide();
			hsizer_owner->Add(element, 1, wxEXPAND);
		}
		for (const auto& data : *library)
		{
			auto element = mPages.emplace_back(data.task.name, new EditorTaskFrameWidget(this, data.task.name, wxString::FromUTF8(data.title), program, dictionary, factory_value)).widget;
			element->Hide();
			hsizer_owner->Add(element, 1, wxEXPAND);
		}
	}

	hsizer_owner->AddSpacer(5);
	vsizer_owner->Add(hsizer_owner, 1, wxEXPAND);
	vsizer_owner->AddSpacer(5);

	Bind(wxEVT_SIZE, [this](wxSizeEvent& event)
		{
			if (mSizeTimer.IsRunning())
			if (mSizeTimer.GetInterval() > 500)
			{
				event.Skip();
				mSizeTimer.Stop();
			}
		  mSizeTimer.StartOnce(600);
		});

	mSizeTimer.Bind(wxEVT_TIMER, [this](wxTimerEvent& event)
		{
			UpdateSizes();
			GetParent()->Layout();
		});

	SetSizer(vsizer_owner);

	mConnectionProgramProperties = mProgram->signal_data.connect(&EditorProgramWidget::UpdateProgramProperties, this);
	mConnectionManagerState = mManager->signal_state.connect(&EditorProgramWidget::UpdateManagerState, this);
	UpdateManagerState();
}

void EditorProgramWidget::UpdateSizes()
{
	decltype(PageWidget::widget) widget = nullptr;
	for (const auto& page : mPages)
		if (page.widget->IsShown())
		{
			widget = page.widget;
			page.widget->Show(false);
			break;
		}

	for (const auto& page : mPages)
		if (page.widget != widget)
		{
			page.widget->Show(true);
			Layout();
			page.widget->Show(false);
		}

	if (widget)
	{
		widget->Show(true);
		Layout();
	}
}

void EditorProgramWidget::UpdateProgramProperties()
{
	auto manager = mManager;
	auto library = mDictionary;
	auto program = mProgram;

	wxArrayString elements;
	elements.push_back(_(library->GetConfig().title));
	for (size_t i = 0; i < program->CountTasks(); ++i)
		elements.push_back(GetProgramPropertiesLabel(library, program, i));

	if (!elements.empty())
	{
		wxArrayString html_elements;
		html_elements.resize(elements.size());

		size_t current_task = 0;
		if (manager->GetStatus() != Manager::Status::Ready)
			if (program->FindTaskFromId(manager->GetCurrentTaskId(), current_task))
				++current_task;

		html_elements[0] = "<code><font color=#555555 size=4>" + elements[0] + "</font></code>";
		for (size_t i = 1; i < elements.size(); ++i)
			if (current_task == i)
				html_elements[i] = "<code><font color=#40DC24 size=4>" + elements[i] + "</font></code>";
			else
				html_elements[i] = "<code><font size=4>" + elements[i] + "</font></code>";

		mProgramList->Set(html_elements);
	}

	{
		size_t index = 0;
		if (program->FindTaskFromId(mSelectedProgramId, index))
			mProgramList->SetSelection(++index);
		UpdatePage(index);
	}
}

void EditorProgramWidget::UpdateManagerState()
{
	UpdateProgramProperties();

	auto manager = mManager;
	auto status = manager->GetStatus();
	if (status == Manager::Status::Busy)
		mButtonStart->SetLabel(_("Stop"));
	else
		mButtonStart->SetLabel(_("Start"));

	mButtonStart->Enable(status != Manager::Status::Stopping);

	mButtonPause->SetLabel(manager->IsPaused() ? _("Continue") : _("Pause"));
}

void EditorProgramWidget::UpdatePage(size_t index)
{
	auto library = mDictionary;
	auto program = mProgram;

	const auto& name = (index == 0 ? library->GetConfig().task.name : program->GetTask(index - 1).name);

	// ���� ������ ��������.
	// ��������� ������ ���� ��������.
	// ���� �������� ����������, ��������� ��������� � ���������� ����� ��������.

	auto it = std::find_if(mPages.begin(), mPages.end(), [&name](const PageWidget& page) { return page.name == name; });
	if (it != mPages.end())
	{
		it->widget->Set(index == 0 ? 0 : program->GetTaskId(index - 1));

		if (it->widget->IsShown())
			return;

		for (auto jt = mPages.begin(); jt != mPages.end(); ++jt)
		{
			jt->widget->Hide();
		}

		for (auto jt = mPages.begin(); jt != mPages.end(); ++jt)
		{
			if (jt == it)
			{
				jt->widget->Show();
				break;
			}
		}

		Layout();
	}
}

wxString EditorProgramWidget::GetProgramPropertiesLabel(const std::shared_ptr<Dictionary>& library, const std::shared_ptr<Program>& properties, size_t index) const
{
	const auto& name = properties->GetTask(index).name;
	auto it = std::find_if(library->begin(), library->end(), [&name](const Dictionary::Description& data) { return data.task.name == name; });
	if (it != library->end())
		return _(it->title);
	return name;
}
