#pragma once

#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class Emulator;
class Manager;

class AutoRunner : public ActorComponent
{
public:
	AutoRunner(std::shared_ptr<Manager> manager, std::shared_ptr<Emulator> emulator);
	~AutoRunner() = default;

	sigslot::signal<> signal_data;

	void SetAutorunMode(bool value);
	bool GetAutorunMode() const;

	void SetStopEmulatorMode(bool value);
	bool GetStopEmulatorMode() const;

	virtual void Tick() override;

	virtual void EndPlay() override;

private:
	std::shared_ptr<Emulator> mEmulator;
	std::shared_ptr<Manager> mManager;

private:
	bool mAutorunMode = false;
	bool mStopEmulatorMode = false;
	bool mIsFirstTick = false;
};
