#include "Program.h"

#include <algorithm>

void Program::SetConfig(const Values& values)
{
	if (mConfig != values)
	{
		mConfig = values;
		signal_data();
	}
}

const Program::Values& Program::GetConfig() const
{
	return mConfig;
}

size_t Program::CountTasks() const
{
	return mTasks.size();
}

void Program::AddTask(const Task& task)
{
	mTasks.emplace_back(task, GetNewIndex());
	signal_data();
}

void Program::RemoveTask(size_t index)
{
	if (index < mTasks.size())
	{
		mTasks.erase(mTasks.begin() + index);
		signal_data();
	}
}

void Program::ClearTasks()
{
	if (!mTasks.empty())
	{
		mTasks.clear();
		signal_data();
	}
}

const Program::Task& Program::GetTask(size_t index) const
{
	return std::get<Task>(mTasks[index]);
}

void Program::SetTask(size_t index, const Task& task)
{
	if (std::get<Task>(mTasks[index]) != task)
	{
		std::get<Task>(mTasks[index]) = task;
		signal_data();
	}
}

size_t Program::GetTaskId(size_t index) const
{
	return std::get<size_t>(mTasks[index]);
}

bool Program::FindTaskFromId(size_t id, size_t& index) const
{
	auto it = std::find_if(mTasks.begin(), mTasks.end(), [id](const std::tuple<Task, size_t>& data) { return std::get<size_t>(data) == id; });
	if (it != mTasks.end())
	{
		index = std::distance(mTasks.begin(), it);
		return true;
	}
	return false;
}

bool Program::Move(size_t index, int pos)
{
	if (mTasks.size() < index)
		return false;

	size_t new_index = std::clamp(static_cast<int64_t>(index) + static_cast<int64_t>(pos), int64_t(0), static_cast<int64_t>(mTasks.size() - 1));
	if (index == new_index)
		return true;

	auto data = std::move(mTasks[index]);
	mTasks.erase(std::next(mTasks.begin(), index));
	mTasks.insert(std::next(mTasks.begin(), new_index), std::move(data));
	signal_data();
	return true;
}

size_t Program::GetNewIndex()
{
	++mLastUniqueIndex;
	return mLastUniqueIndex == 0 ? ++mLastUniqueIndex : mLastUniqueIndex;
}
