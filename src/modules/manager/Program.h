#pragma once

#include <vector>
#include <map>
#include <tuple>
#include <sigslot/signal.hpp>

#include "Value.h"

#include "ActorComponent.h"

class Program : public ActorComponent
{
public:
	using Values = std::map<std::string, Value>;

	struct Task
	{
		std::string name;
		Values values;

		friend auto operator<=>(const Task&, const Task&) = default;
	};

public:
	sigslot::signal<> signal_data;

	void SetConfig(const Values & values);
	const Values& GetConfig() const;

	size_t CountTasks() const;
	void AddTask(const Task& task);
	void RemoveTask(size_t index);
	void ClearTasks();

	const Task& GetTask(size_t index) const;
	void SetTask(size_t index, const Task& task);

	size_t GetTaskId(size_t index) const;
	bool FindTaskFromId(size_t id, size_t& index) const;

	bool Move(size_t index, int pos);
private:

	Values mConfig;
	std::vector<std::tuple<Task, size_t>> mTasks;

	size_t mLastUniqueIndex = 0;
	size_t GetNewIndex();
};

