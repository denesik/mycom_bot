#include "Dictionary.h"
#include "LoadJson.h"
#include "Logger.h"
#include "PathUtf8.h"
#include "StringJson.h"


namespace
{
	bool SetValueFromJson(const std::string& name, const rapidjson::Value& json, Program::Values& values)
	{
		if (json.IsInt())
		{
			values[name] = json.GetInt();
			return true;
		}
		if (json.IsBool())
		{
			values[name] = json.GetBool();
			return true;
		}
		if (json.IsString())
		{
			values[name] = std::string(json.GetString(), json.GetStringLength());
			return true;
		}
		return false;
	}

	void FromJson(const rapidjson::Value& json, Dictionary::Description& data)
	{
		if (!json.IsObject())
			return;

		{
			auto jt = json.FindMember("title");
			if (jt != json.MemberEnd() && jt->value.IsString())
				data.title = std::string(jt->value.GetString(), jt->value.GetStringLength());
		}

		for (auto it = json.MemberBegin(); it != json.MemberEnd(); ++it)
		{
			if (it->value.IsObject())
			{
				auto jt = it->value.FindMember("value");
				if (jt != it->value.MemberEnd())
				{
					if (SetValueFromJson(it->name.GetString(), jt->value, data.task.values))
					{
						rapidjson::Document doc;
						doc.CopyFrom(it->value, doc.GetAllocator());
						utility::JsonToString(doc, std::get<1>(data.meta.emplace_back(std::string(it->name.GetString()), "")));
					}
				}
			}
		}
	}
}



Dictionary::Dictionary(const std::filesystem::path& path, const std::filesystem::path& core_path)
	: mTasksDirectory(path), mCoreFilename(core_path)
{

}

void Dictionary::Initialize()
{
	{
		auto name = utf_helper::PathToUtf8(mCoreFilename.stem());

		rapidjson::Document json;
		if (utility::LoadJson(mCoreFilename, json))
		{
			FromJson(json, mConfig);
		}
		else
			LOG_E("Error load DescriptionsTable. File: " + utf_helper::PathToUtf8(mCoreFilename));
	}

	if (!std::filesystem::exists(mTasksDirectory))
	{
		LOG_E("Directory not found. Path: " + utf_helper::PathToUtf8(mTasksDirectory));
	}
	else
	{
		for (auto& it : std::filesystem::directory_iterator(mTasksDirectory))
		{
			auto name = utf_helper::PathToUtf8(it.path().stem());

			rapidjson::Document json;
			if (utility::LoadJson(it.path(), json))
			{
				FromJson(json, FindOrCreateTable(name));
			}
			else
				LOG_E("Error load DescriptionsTable. File: " + utf_helper::PathToUtf8(it.path()));
		}
	}
}

const Dictionary::Description& Dictionary::GetConfig() const
{
	return mConfig;
}

Dictionary::Description& Dictionary::FindOrCreateTable(const std::string& name)
{
	auto it = std::find_if(mDescriptions.begin(), mDescriptions.end(), [&name](const Description& desc) { return desc.task.name == name; });
	if (it == mDescriptions.end())
	{
		auto& desc = mDescriptions.emplace_back();
		desc.task.name = name;
		return desc;
	}
	return *it;
}

