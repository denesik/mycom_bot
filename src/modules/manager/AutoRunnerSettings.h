#pragma once

#include <filesystem>
#include <sigslot/signal.hpp>

#include "ActorComponent.h"

class AutoRunner;

class AutoRunnerSettings : public ActorComponent
{
public:
	AutoRunnerSettings(std::shared_ptr<AutoRunner> runner, const std::filesystem::path& core_path, const std::filesystem::path& user_path);

	virtual void Initialize() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

	virtual void Deinitialize() override;

private:
	void Load(const std::filesystem::path& path);
	void Save(const std::filesystem::path& path);

private:
	const std::filesystem::path mCorePath;
	const std::filesystem::path mUserPath;

	std::shared_ptr<AutoRunner> mRunner;

	sigslot::connection mRunnerConnection;
};


