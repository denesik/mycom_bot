#pragma once

#include <sigslot/signal.hpp>

#include "ActorComponent.h"
#include "Program.h"

class Logic;

class Manager : public ActorComponent
{
public:
	Manager(std::shared_ptr<Logic> logic, std::shared_ptr<Program> program);
	~Manager() = default;

	sigslot::signal<> signal_state;

	virtual void Tick() override;

	virtual void BeginPlay() override;

	virtual void EndPlay() override;

public:
	// TODO: ����� ���� ��������� ����� ������������ � �����-�� �� ���� ���������?
	enum class Status
	{
		Ready,
		Busy,
		Stopping,
	};
	
	void Start();
	void Stop();

	Status GetStatus() const;

	void SetPause(bool value);
	bool IsPaused() const;

	size_t GetCurrentTaskId() const;

private:
	Program::Task GetCurrentTaskProperties();

private:
	bool SetNextTask();
	void RunCurrentTask();

private:
	std::shared_ptr<Logic> mLogic;
	std::shared_ptr<Program> mProgram;

	int mCurrentTaskIndex = -1;
	size_t mCurrentTaskId = 0;
	Status mStatus = Status::Ready;


private:
	sigslot::connection mConnectionProgram;

	Program::Task mTaskValues;
};



