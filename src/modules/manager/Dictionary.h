#pragma once

#include <filesystem>
#include <string>
#include <vector>

#include "ActorComponent.h"

#include "Program.h"


class Dictionary : public ActorComponent
{
public:
	struct Description
	{
		std::string title;
		Program::Task task;
		std::vector<std::tuple<std::string, std::string>> meta;
	};

public:
	Dictionary(const std::filesystem::path& tasks_path, const std::filesystem::path& core_path);

	virtual void Initialize() override;

	const Description& GetConfig() const;

	auto begin() { return mDescriptions.begin(); }
	auto end() { return mDescriptions.end(); }

private:
	Description& FindOrCreateTable(const std::string& name);

private:
	const std::filesystem::path mTasksDirectory;
	const std::filesystem::path mCoreFilename;

	Description mConfig;
	std::vector<Description> mDescriptions;
};
