#include "ManagerModuleBuilder.h"

#include "Actor.h"
#include "Profile.h"

#include "Emulator.h"
#include "Book.h"
#include "Detector.h"
#include "ScreenshotterComponent.h"
#include "Manager.h"
#include "AutoRunner.h"
#include "EmulatorSettings.h"
#include "DetectorSettings.h"
#include "AutoRunnerSettings.h"
#include "EditorBaseOptions.h"
#include "EditorBaseOptionsSettings.h"
#include "NumberSaver.h"
#include "NumberSaverSettings.h"

#include "Program.h"
#include "ProgramSettings.h"
#include "Dictionary.h"
#include "ProgramCorrector.h"
#include "Ocr.h"
#include "EditorDetectorBuilder.h"
#include "EditorManagerBuilder.h"
#include "EditorFactoryValue.h"
#include "ProgramReport.h"
#include "ProgramReportSettings.h"
#include "EditorLogicOptions.h"
#include "EditorLogicOptionsSettings.h"
#include "Logic.h"

void ManagerModuleBuilder::AddComponents(const Profile& profile, Actor* actor)
{
	auto emulator = actor->FindComponent<Emulator>();  // TODO: ���������� �� ���� �����������
	auto logic = actor->FindComponent<Logic>();

	auto program_report = actor->CreateComponent<ProgramReport>(); // TODO: ����� ��������� ������� ��������� ��������� ���.
		
	auto program = actor->CreateComponent<Program>();
	actor->CreateComponent<ProgramSettings>(program, profile.FindPath("program_core_file"), profile.FindPath("program_user_file"));
	auto dictionary = actor->CreateComponent<Dictionary>(profile.FindPath("config_tasks_dir"), profile.FindPath("config_tasks_core"));
	actor->CreateComponent<ProgramCorrector>(program, dictionary);

	actor->CreateComponent<ProgramReportSettings>(program_report, profile.FindPath("program_report"));

	auto manager = actor->CreateComponent<Manager>(logic, program);
	auto auto_runner = actor->CreateComponent<AutoRunner>(manager, emulator);
	actor->CreateComponent<AutoRunnerSettings>(auto_runner, profile.FindPath("core_settings_core_file"), profile.FindPath("core_settings_user_file"));

	auto editor_logic_options = actor->CreateComponent<EditorLogicOptions>();
	actor->CreateComponent<EditorLogicOptionsSettings>(editor_logic_options, profile.FindPath("editor_core_file"), profile.FindPath("editor_user_file"));
	
	auto factory_value = actor->CreateComponent<EditorFactoryValue>();
	actor->CreateComponent<EditorManagerBuilder>(manager, dictionary, program, auto_runner, editor_logic_options, program_report, factory_value);
}

