#include "ProgramCorrector.h"

#include "Dictionary.h"

#include "Program.h"
#include "Dictionary.h"
#include <ranges>

namespace
{
	Program::Values SetValueAccordingToTemplate(const Program::Values& templ, const Program::Values& src)
	{
		// ������������� ��� ���� ��������� ����������� �� ���������.
		Program::Values out = templ;

		// ���� ����� �������� ���� � ��� � � ���������, ��������� ��� �������� �� ���������.
		for (const auto& [name, value] : src)
			out[name] = value;

		return out;
	}
}

ProgramCorrector::ProgramCorrector(std::shared_ptr<Program> program, std::shared_ptr<Dictionary> library)
	: mProgram(program), mLibrary(library)
{

}

void ProgramCorrector::Initialize()
{
	// ������� ����������� ������.
	{
		std::vector<size_t> indexes;
		for (size_t i = 0; i < mProgram->CountTasks(); ++i)
		{
			bool finded = false;
			for (const auto& data : *mLibrary)
				if (data.task.name == mProgram->GetTask(i).name)
				{
					finded = true;
					continue;
				}
			if (!finded)
				indexes.push_back(i);
		}
		for (auto index : std::ranges::reverse_view(indexes))
			mProgram->RemoveTask(index);
	}

	for (size_t i = 0; i < mProgram->CountTasks(); ++i)
	{
		const auto& task = mProgram->GetTask(i);

		Program::Values values;
		for (const auto& data : *mLibrary)
			if (data.task.name == mProgram->GetTask(i).name)
			{
				values = SetValueAccordingToTemplate(data.task.values, task.values);
				break;
			}

		if (values != task.values)
			mProgram->SetTask(i, { task.name, values });
	}

	mProgram->SetConfig(SetValueAccordingToTemplate(mLibrary->GetConfig().task.values, mProgram->GetConfig()));
}




