#pragma once

#include <opencv2\core\base.hpp>
#include <opencv2\core\types.hpp>

namespace utility
{

	cv::Size GetCaptureSize();

	bool CheckCaptureSize(const cv::Mat& img);

}


