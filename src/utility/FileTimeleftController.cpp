#include "FileTimeleftController.h"

#include <ctime>
#include <iomanip>
#include <chrono>


void FileTimeleftController::RemoveUnwanted(std::string_view extension)
{
	if (mStoreTime == 0)
		return;

	if (!std::filesystem::exists(mDirectory))
		return;

	std::vector<std::filesystem::path> paths;

	for (auto& it : std::filesystem::directory_iterator(mDirectory))
	{
		if (it.path().extension() == extension)
		{
			const auto fileTime = std::filesystem::last_write_time(it.path());
			const auto systemTime = std::chrono::clock_cast<std::chrono::system_clock>(fileTime);
			const auto ctime_t = std::chrono::system_clock::now();

			auto minutes = std::chrono::floor<std::chrono::minutes>(ctime_t - systemTime);

			if (static_cast<decltype(mStoreTime)>(minutes.count()) > mStoreTime)
				paths.emplace_back(it.path());
		}
	}

	for (auto& p : paths)
		std::filesystem::remove(p);
}

void FileTimeleftController::SetStoreTime(unsigned int minutes)
{
	mStoreTime = minutes;
}

unsigned int FileTimeleftController::GetStoreTime() const
{
	return mStoreTime;
}

void FileTimeleftController::SetDirectory(const std::filesystem::path &directory)
{
	mDirectory = directory;
}

std::filesystem::path FileTimeleftController::GetDirectory() const
{
	return mDirectory;
}


