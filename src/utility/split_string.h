#pragma once

#include <string>

namespace utility
{
	void split_string(std::string_view delim, std::string_view data, std::string& prefix, std::string& suffix);
}

