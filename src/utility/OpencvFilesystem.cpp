#include "OpencvFilesystem.h"

cv::Mat utility::Imread(const std::filesystem::path& path, int flags)
{
	return cv::imread(path.string(), flags);
}

bool utility::Imwrite(const std::filesystem::path& path, cv::InputArray img, const std::vector<int>& params)
{
	if (!std::filesystem::exists(path.parent_path()))
		std::filesystem::create_directories(path.parent_path());
	return cv::imwrite(path.string(), img, params);
}
