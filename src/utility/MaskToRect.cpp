#include "MaskToRect.h"

#include <opencv2/imgproc.hpp>


bool utility::MaskToRect(const cv::Mat &img, cv::Rect &rect)
{
	if (!img.size().empty())
	{
		cv::Mat gray;
		cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

		cv::Mat mask;
		cv::threshold(gray, mask, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);

		std::vector<std::vector<cv::Point>> contours;
		cv::findContours(mask, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

		if (contours.size() >= 1)
		{
			auto bbox = cv::minAreaRect(contours[0]);
			rect = bbox.boundingRect();
			return true;
		}
	}
	return false;
}

bool utility::MaskToRect(const cv::Mat& img, std::vector<cv::Rect>& array)
{
	if (!img.size().empty())
	{
		cv::Mat gray;
		cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

		cv::Mat mask;
		cv::threshold(gray, mask, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);

		std::vector<std::vector<cv::Point>> contours;
		cv::findContours(mask, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

		for (size_t i = 0; i < contours.size(); ++i)
		{
			auto bbox = cv::minAreaRect(contours[i]);
			array.emplace_back(bbox.boundingRect());
		}
		return !contours.empty();
	}
	return false;
}
