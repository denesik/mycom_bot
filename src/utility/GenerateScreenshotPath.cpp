#include "GenerateScreenshotPath.h"

std::filesystem::path utility::GenerateScreenshotPath(const std::filesystem::path& path, std::string_view prefix)
{
	std::string time;
	{
		auto t = std::time(nullptr);
		struct tm buf;
		localtime_s(&buf, &t);
		std::stringstream ss;
		ss << std::put_time(&buf, "%d-%m-%y %H-%M-%S");
		time = ss.str();
	}

	if (!std::filesystem::exists(path))
		return path / (std::string(prefix) + time + " " + std::to_string(1) + ".png");

	for (int i = 1; i < 100; ++i)
	{
		auto out = path / (std::string(prefix) + time + " " + std::to_string(i) + ".png");
		if (!std::filesystem::exists(out))
			return out;
	}

	return "";
}
