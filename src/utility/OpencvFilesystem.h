#pragma once

#include <filesystem>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>




namespace utility
{
	cv::Mat Imread(const std::filesystem::path& path, int flags = cv::IMREAD_COLOR);

	bool Imwrite(const std::filesystem::path& path, cv::InputArray img, const std::vector<int>& params = std::vector<int>());

}


