#pragma once

#include <opencv2\core\base.hpp>
#include <opencv2\core\types.hpp>

#include <vector>

namespace utility
{

	bool MaskToRect(const cv::Mat& img, cv::Rect& rect);
	bool MaskToRect(const cv::Mat& img, std::vector<cv::Rect>& array);

}


