#pragma once

#include <filesystem>
#include <string>

class FileTimeleftController
{
public:
	
	void RemoveUnwanted(std::string_view extension);

	void SetStoreTime(unsigned int minutes);
	unsigned int GetStoreTime() const;

	void SetDirectory(const std::filesystem::path &directory);
	std::filesystem::path GetDirectory() const;

private:
	std::filesystem::path mDirectory;
	unsigned int mStoreTime = 0;
};

