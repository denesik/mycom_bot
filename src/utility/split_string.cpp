#include "split_string.h"


void utility::split_string(std::string_view delim, std::string_view data, std::string& prefix, std::string& suffix)
{
	auto s = data;
	auto end = s.find(delim);
	size_t start = 0;
	prefix = s.substr(start, end - start);
	if (end != std::string::npos)
	{
		start = end + delim.length();
		suffix = s.substr(start, s.length());
	}
}
