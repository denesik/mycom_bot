#include "CaptureSize.h"

#include <opencv2/core/mat.hpp>

namespace
{
	static const cv::Size CAPTURE_IMAGE_SIZE = { 1280, 720 };
}

cv::Size utility::GetCaptureSize()
{
	return CAPTURE_IMAGE_SIZE;
}

bool utility::CheckCaptureSize(const cv::Mat& img)
{
	return img.size() == CAPTURE_IMAGE_SIZE;
}
