#pragma once

#include <filesystem>

namespace utility
{

	std::filesystem::path GenerateScreenshotPath(const std::filesystem::path& path, std::string_view prefix = "");

}

